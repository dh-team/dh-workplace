var helper = require('sendgrid').mail;
var sg = require('sendgrid')("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");

var mailgun = require('mailgun-js')({
    apiKey: "key-4b86ea7947d33e0269cb4a7c217234a7",
    domain: "sandboxd8f074fa4d1b43fdbd2505dfc54027a1.mailgun.org"
});


function sendEmailUsingSendgrid(m, callback) {
    var from = new helper.Email(m.from ? m.from : "no-reply@wspace.vn");
    var to = new helper.Email(m.to);
    var subject = m.subject;
    var content = new helper.Content("text/html", m.content);
    var mail = new helper.Mail(from, subject, to, content);


    var request = sg.emptyRequest({
        method: 'POST',
        path: '/v3/mail/send',
        body: mail.toJSON()
    });

    sg.API(request, function(err, response) {
        // console.log(response.statusCode);
        // console.log(response.body);
        // console.log(response.headers);

        if (err) return callback(err);
        callback(null, response);
    });
};

function sendEmailUsingMailgun(m, callback) {
    var data = {
        from: m.from ? m.from : "no-reply@wspace.vn",
        to: m.to,
        subject: m.subject,
        html: m.content
    };

    mailgun.messages().send(data, function(err, body) {
        // console.log(err, body);
        // console.log(body);
        if(err) return callback(err);
        callback(null);
    });
};

module.exports = {
    sendEmail: sendEmailUsingMailgun
}
