function workStatusInt(status) {
    switch (status) {
        case 'created':
            return 1;

        case 'assigned':
            return 2;

        case 'assigned_accept':
            return 3;

        case 'assigned_reject':
            return 3.1;

        case 'todo':
            return 4;

        case 'doing':
            return 5;

        case 'done':
            return 6;

        case 'redo':
            return 4.1;

        case 'completed':
            return 7;

        case 'closed':
            return 7.1;
    }
}

function compareStatus(status1, status2) {
    return workStatusInt(status1) - workStatusInt(status2);
}

function canAddSubWork(work) {
    const status = work.doing && work.doing.status;
    if (compareStatus(status, "completed") >= 0) return false;
    if (work.child_number === 0 && compareStatus(status, "todo") >= 0) return false;
    return true;
}

function getPerformerOrCreator(work) {
    if(work.resource && work.resource.performer) return work.resource.performer;
    return work.meta && work.meta.create_user && work.meta.create_user.user_id;
}

module.exports = {
    workStatusInt: workStatusInt,
    compareStatus: compareStatus,
    canAddSubWork: canAddSubWork,
    getPerformerOrCreator: getPerformerOrCreator
};
