'use strict';
const constant = require("./constant");

/**
 * Split scope to object "u:USER_ID" -> {type: "u", id: USER_ID}
 * @param {string} scope 
 */
function extractScope(scope) {
    var subs = scope.split(":");
    if (!subs || subs.length < 2) return null;
    if (subs[0] !== "g" && subs[0] !== "u") return null;
    if (!subs[1] || !subs[1].trim()) return null;
    return {
        type: subs[0],
        id: subs[1].trim()
    };
}

/**
 * Check current user in scope, return error if failure
 * @param {string} scope 
 * @param {object} auth 
 */
function checkInScope(scope, auth) {
    var grant = extractScope(scope);
    if (!grant) return null;

    if (grant.type === "u" && grant.id !== auth.user_id) {
        return constant.err.permission_deny;
    }

    if (grant.type === "g" && grant.id !== "all" && auth.groups.indexOf(grant.id) < 0) {
        return constant.err.permission_deny;
    }

    return null;
}


/**
 * Build scope string for user
 * @param {string} userId 
 */
function buildUserScope(userId) {
    return "u:" + userId;
}

/**
 * Build scope string for group
 * @param {string} groupId 
 */
function buildGroupScope(groupId) {
    return "g:" + groupId;
}

/**
 * Authorize action with object
 * @param {object} obj Object to interact {space, scope, meta}
 * @param {String} action Action to do 'view' | 'interact' | 'modify' | 'manage'
 * @param {object} auth Authen information
 */
function authorizeAction(obj, action, auth) {
    if (obj.space && obj.space !== auth.space_id) return constant.err.permission_deny;
    const owner = obj.meta && obj.meta.create_user && obj.meta.create_user.user_id;
    if (obj.scope) {
        let scope = obj.scope[action + "_scope"];
        if(!scope || typeof(scope) !== 'string') return null;
        if (scope === "u:owner" && owner !== auth.user_id) return constant.err.permission_deny;
        return checkInScope(scope, auth);
    }

    if (action !== 'view' && owner !== auth.user_id) return constant.err.permission_deny;
    return null;
}

module.exports = {
    extractScope: extractScope,
    checkInScope: checkInScope,
    buildUserScope: buildUserScope,
    buildGroupScope: buildGroupScope,
    authorizeAction
};
