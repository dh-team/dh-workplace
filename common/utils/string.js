var htmlToText = require('html-to-text');

function inlinePreviewOfPlainText(content, maxWords) {
    if (!content) return content;
    if (!maxWords) maxWords = 10;

    var words = content.split(" ");
    if (words.length <= maxWords) return content;
    return words.slice(0, maxWords).join(" ") + "...";
}

function plainPreviewOfHtml(content) {
    content = htmlToText.fromString(content, {
        ignoreHref: true,
        ignoreImage: true,
        wordwrap: false
    });
    content = content.replace(/\n\n/g, "\n");
    content = content.replace(/\n/g, "<br/>");
    content = subContent(content);
    return content;
}

// get sub content with max lines, words
function subContent(input, maxLine, maxWord, lineSeparate) {
    if (!lineSeparate) lineSeparate = "<br/>";
    if (!maxLine) maxLine = 4;
    if (!maxWord) maxWord = 120;

    var lineCount = 0;
    var wordCount = 0;
    var line;
    var lineWc;
    var arr = [];

    var subs = input.split(lineSeparate);
    for (var i = 0; i < subs.length; i++) {
        line = subs[i].trim();
        if (line === "") continue;
        lineWc = countWords(line);
        if (lineWc + wordCount > maxWord) {
            arr.push(line.split(" ", lineWc + wordCount - maxWord).join(" ") + " ...");
            break;
        }
        arr.push(line);
        lineCount++;
        wordCount += lineWc;
        if (lineCount >= maxLine || wordCount >= maxWord) break;
    }
    return arr.join(lineSeparate);
}

function countWords(str) {
    return str.split(" ").length;
}

module.exports = {
    subContent: subContent,
    inlinePreviewOfPlainText: inlinePreviewOfPlainText,
    plainPreviewOfHtml: plainPreviewOfHtml
}
