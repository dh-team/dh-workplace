function extractParam(req, name) {
    if (req.body[name]) return req.body[name];
    if (req.query[name]) return req.query[name];
    if (req.params[name]) return req.params[name]; // routing params
    if (req.cookies[name]) return req.cookies[name];
    return null;
}

function extractObject(req, props) {
    var obj = {};
    for (var i = 0; i < props.length; ++i) {
        obj[props[i]] = extractParam(req, props[i]);
    }
    return obj;
}

function responseInvalid(res, err) {
    res.status(400);
    return res.json({
        error: err || "invalid_token"
    });
}

function response(res, err, r) {
    if (err) {
        return responseInvalid(res, err);
    }
    return res.json(r);
}

function extractAuthInfo(req) {
    return {
        user_id: req.user.user_id,
        space_id: req.authInfo.token.using_space.space_id,
        uname: req.authInfo.token.using_space.uname,
        roles: req.authInfo.token.using_space.roles,
        positions: req.authInfo.token.using_space.positions,
        groups: req.authInfo.token.using_space.groups
    }
}

module.exports = {
    extractParam: extractParam,
    extractObject: extractObject,
    extractAuthInfo: extractAuthInfo,
    response: response,
    responseInvalid: responseInvalid
}
