'use strict';

/**
 * Return a unique identifier with the given `len`.
 *
 *     utils.uid(10);
 *     // => "FDaS435D2z"
 *
 * @param {Number} len
 * @return {String}
 * @api public
 */
function uid(len) {
    var buf = [];
    var chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charlen = chars.length;

    for (var i = 0; i < len; ++i) {
        buf.push(chars[getRandomInt(0, charlen - 1)]);
    }

    return buf.join('');
};

/**
 * Copy fields of `from` and set to `to`
 * @param {object} from 
 * @param {object} to 
 */
function copy(from, to) {
    if (null == from || "object" != typeof from) return to;
    for (var attr in from) {
        if (from.hasOwnProperty(attr)) to[attr] = from[attr];
    }
    return to;
}

/**
 * Update `to` with field value in `from`
 * @param {object} from 
 * @param {object} to 
 * @param {[string]} fields 
 */
function update(from, to, fields) {
    if (null == from || "object" != typeof from) return to;
    if (fields) {
        fields.map(function(attr) {
            if (from.hasOwnProperty(attr)) {
                doUpdate(from, to, attr);
            }
        });
    }
    else {
        for (var attr in from) {
            doUpdate(from, to, attr);
        }
    }
    return to;
}

function doUpdate(from, to, attr) {
    if (from[attr] === null) {
        to[attr] = null;
    }
    else if ("object" === typeof from[attr] && !(from[attr] instanceof Date) && !(from[attr] instanceof Array)) {
        if (!to[attr]) to[attr] = {};
        update(from[attr], to[attr]);
    }
    else {
        to[attr] = from[attr];
    }

}

function removeEmptyProps(obj, props) {
    if (!props) props = Object.keys(obj);
    props.map(function(prop) {
        if (obj.hasOwnProperty(prop) && obj[prop] === null || obj[prop] === undefined) {
            delete obj[prop];
        }
    });
    return obj;
}

/**
 * Return a random int, used by `utils.uid()`
 *
 * @param {Number} min
 * @param {Number} max
 * @return {Number}
 * @api private
 */
function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function changeItemOrder(arr, from, to) {
    console.log("changeItemOrder", from, to);
    if ((from > arr.length - 1) || (to > arr.length - 1) || (from === to) || (from < 0) || (to < 0)) return arr;

    var item = arr[from];
    arr.splice(from, 1);
    arr.splice(to, 0, item);
    return arr;
}

// {a1: {a11, a12} } => {"a1.a11", "a1.a12"}

function getProps(obj, prefix, map){
    const props = Object.keys(obj);
    
    props.map((prop) => {
        if(obj[prop] && isObject(obj[prop])) {
            getProps(obj[prop], prefix + prop + ".", map);
        }else{
            map[prefix + prop] = obj[prop];
        }
    });
}

function objectToMap(obj){
    let m = {};
    getProps(obj, "", m);
    return m;
}

function isObject(obj){
    return "object" === typeof (obj) && !(obj instanceof Date) && !(obj instanceof Array);
}

function toPlainObj(obj){
    return JSON.parse(JSON.stringify(obj));
}


module.exports = {
    uid: uid,
    copy: copy,
    update: update,
    removeEmptyProps: removeEmptyProps,
    changeItemOrder: changeItemOrder,
    objectToMap,
    toPlainObj
};
