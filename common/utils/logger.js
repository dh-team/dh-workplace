var winston = require('winston');

var Logger = function(file) {
    return new(winston.Logger)({
        level: 'debug',
        transports: [
            new(winston.transports.Console)(),
            new(winston.transports.File)({
                filename: file
            })
        ]
    });
}

module.exports = Logger;
