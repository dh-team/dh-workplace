exports.required = function(input) {
    return input && input.trim() && input.length > 0 && input.trim().length > 0;
}

exports.hasLength = function(input, min, max) {
    if (!max || max < 0) max = null;
    if (!min || min < 0) min = 0;
    if (max != null && max < min) max = min;
    if (!input) return min === 0;

    input = input.trim();
    return (min == 0 || input.length >= min) && (max == null || input.length <= max);
}

exports.isEmail = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}


exports.isID = function(input, minLen, maxLen){
    if(!minLen || minLen < 0){
        minLen = 0;
    }
    if(!maxLen || maxLen < minLen){
        maxLen = "";
    }
    var re = new RegExp("^[a-z0-9\\_\\-]{" + minLen + "," + maxLen + "}$");
    // console.log("^[a-z0-9\\_\\-]{" + minLen + "," + maxLen + "}$");
    return re.test(input);
}

exports.isEmail = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

exports.isDate = function(str) {
    return str && str.length > 0;
}