module.exports = {
    err: {
        not_exist: "not_exist",
        already_exist: "already_exist",
        permission_deny: "permission_deny",
        deleted: "deleted",
        params_invalid: "params_invalid"
    },
    act: {
        view: "view",
        access: "access",
        modify: "modify",
        manage: "manage",
        interact: "interact",
    },
    work_status: {
        created: "created",
        assigned: "assigned",
        assigned_accept: "assigned_accept",
        assigned_reject: "assigned_reject",
        closed: "closed",
        todo: "todo",
        doing: "doing",
        redo: "redo",
        done: "done",
        completed: "completed"
    },
};
