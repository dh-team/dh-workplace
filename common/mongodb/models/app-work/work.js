'use strict';

var mongoose = require('mongoose');
var Meta = require('../meta');

var Task = new mongoose.Schema({
    start_time: Date,
    stop_time: Date,
    spent_time: Number, // in minutes
    note: String,
    status: String,
    display_order: Number
});

var WorkSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function () {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },

    name: {
        type: String,
        required: true
    },
    detail: String,
    attachments: Array, // [ file ], file: {id, name, size, mime_type}

    parent: String,
    all_parents: {
        type: [String],
        default: []
    },
    childs: {
        type: [String],
        default: []
    },
    child_number: {
        type: Number,
        default: 0
    },
    objective: String,
    predecessors: [String], // id of predecessor works

    source: mongoose.Schema.Types.Mixed, // {type, reference} project, business, personal, team
    knowledge_tags: [String],
    category_tags: [String],
    priority_label: String, // A B C D E

    resource: {
        performer: mongoose.Schema.Types.Mixed, // user who does this work
        monitor: mongoose.Schema.Types.Mixed, // user who check this work 
        followers: Array
    },
    planning: {
        start_date: Date,
        due_date: Date,
        duration: Number, // hours        
    },
    doing: {
        status: String, // created, assigned, assigned_accept, assigned_reject, todo, doing, done, completed, closed, redo
        tasks: [Task],
        spent_time: Number, // in hours
        progress: Number, // percentage
        display_type: String, // checklist, historical
    },
    result: {
        note: String,
        attachments: Array,
        issue_time: Date
    },
    evaluation: {
        note: String,
        rate: Number,
        issue_date: Date
    },

    view_scope: String, // user/group can view this work    
    display_order: Number,
    meta: Meta,
    comment_stream: String,
    reaction_stream: String
});

module.exports = mongoose.model('works', WorkSchema);
