'use strict';

var mongoose = require('mongoose');
var Meta = require('../meta');

var KeyResult = new mongoose.Schema({
    content: {
        type: String,
        required: true
    },
    progress: {
        type: Number,
        required: false
    },
    display_order: {
        type: Number,
        required: false
    }
});

var ObjectiveSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function () {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    detail: String,
    time: {
        range: String, // "annual", "quarterly", "monthly"
        year: Number,
        quarter: Number,        
        from: Date,
        to: Date
    },
    owner: String, // "g:all" , "g:GROUP_ID", "u:own", "u:USER_ID",
    type: String,
    parent: String,
    all_parents: [String],
    key_results: [KeyResult],

    display_order: Number,
    view_scope: String, // user/group can view this objective
    meta: Meta
});


// Export the Mongoose model
module.exports = mongoose.model('objectives', ObjectiveSchema);
