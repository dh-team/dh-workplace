'use strict';

var mongoose = require('mongoose');

var TagSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: false
    },
    keyword: {
        type: String,
        required: true
    },
    subject: String
});

module.exports = mongoose.model('tags', TagSchema);
