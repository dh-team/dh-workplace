'use strict';

var mongoose = require('mongoose');
var Meta = require('./meta');

var GroupSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space_id: {
        type: String,
        required: true
    },
    uname: {  // unique in space
        type: String,
        required: false
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    type: {
        type: String, // 'org' | 'user'
        required: true,
        default: 'org'
    },
    privacy: {
        type: String, // 'public' | 'closed' | 'secret'
        required: false,
        default: 'closed'
    },
    org_info: {
        parent_id: {
            type: String,
            required: false,
            default: null
        },
        all_parents: {
            type: [String], // [String] - array of parent' ids, from bottom
            required: false
        },
        is_head: { // head of parent group
            type: Boolean,
            required: false
        },
        type: { // head of parent group
            type: String, // 'unique' | 'group' | 'sealed '
            required: false,
            default: 'group'
        }
    },
    is_active: {
        type: Boolean,
        required: false,
        default: true
    },
    users: [], // user's ids
    meta: Meta
});

GroupSchema.pre('save', function(next) {
    var group = this;
    if (!group.org_info || !group.isModified('org_info.parent_id')) return next();
    if (!group.org_info.parent_id || group.org_info.parent_id === 'root') {
        group.org_info.parent_id = null;
    }
    return next();
});

module.exports = mongoose.model('groups', GroupSchema);
