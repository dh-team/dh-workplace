'use strict';
// Load required packages
var mongoose = require('mongoose');
var bcrypt = require('bcrypt-nodejs');

// Define our user schema
var UserSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    display_name: {
        type: String,
        required: false
    },
    password: {
        type: String,
        required: false
    },
    emails: [
        {
            email: String,
            is_verified: Boolean,
            is_main: Boolean
        }
    ],
    phones: [
        {
            phone: String,
            is_verified: Boolean,
            is_main: Boolean
        }
    ],
    is_active: {
        type: Boolean,
        required: true
    }
});

// Execute before each user.save() call
UserSchema.pre('save', function(callback) {
    var user = this;

    // Break out if the password hasn't changed
    if (!user.isModified('password')) return callback();

    // Password changed so we need to hash it
    bcrypt.genSalt(5, function(err, salt) {
        if (err) return callback(err);

        bcrypt.hash(user.password, salt, null, function(err, hash) {
            if (err) return callback(err);
            user.password = hash;
            callback();
        });
    });
});

// Export the Mongoose model
module.exports = mongoose.model('users', UserSchema);
