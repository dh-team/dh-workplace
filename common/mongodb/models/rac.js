'use strict';

var mongoose = require('mongoose');
var Meta = require('./meta');


var ControlList = new mongoose.Schema({
    include: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    },
    exclude: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: []
    }    
});


var ResourceAccessControlSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space_id: {
        type: String,
        required: true
    },
    ref_type: {
        type: String, // "post" | "file" ...
        required: true
    },
    ref_id: {
        type: String, // "POST_ID" | "FILE_ID" ...
        required: true
    },
    ref_obj: mongoose.Schema.Types.Mixed,
    view_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    },
    interact_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    },
    modify_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["u:owner"]
    },
    manage_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["u:owner"]
    },
    notify_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    }
});

ResourceAccessControlSchema.pre('save', function(next) {
    var group = this;
    if (!group.org_info || !group.isModified('org_info.parent_id')) return next();
    if (!group.org_info.parent_id || group.org_info.parent_id === 'root') {
        group.org_info.parent_id = null;
    }
    return next();
});

module.exports = mongoose.model('groups', GroupSchema);
