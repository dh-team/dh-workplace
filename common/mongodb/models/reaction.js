'use strict';

var mongoose = require('mongoose');

var Reaction = new mongoose.Schema({
    issue_user: mongoose.Schema.Types.Mixed,
    issue_time: {
        type: Date,
        default: Date.now
    },
    reaction: String
});

module.exports = Reaction;
