'use strict';

var mongoose = require('mongoose');

var VerifyCodeSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    user_id: {
        type: String,
        required: false
    },
    to_verify: {  // email or phone
        type: String,
        required: true
    },
    type: {  // email or phone
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    issue_time: {
        type: Date,
        required: false
    },
});

module.exports = mongoose.model('verify_codes', VerifyCodeSchema);
