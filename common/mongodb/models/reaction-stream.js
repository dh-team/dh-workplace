'use strict';

const mongoose = require('mongoose');

const ActionSchema = new mongoose.Schema({
    issue_user: mongoose.Schema.Types.Mixed,
    issue_time: {
        type: Date,
        default: Date.now
    },
    data: mongoose.Schema.Types.Mixed
});

const ReactStreamSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    owner: String,
    ref_type: {
        type: String, // "post" | "file" ...
        required: true
    },
    ref_id: {
        type: String, // "POST_ID" | "FILE_ID" ...
        required: true
    },
    ref_obj: mongoose.Schema.Types.Mixed,
    reaction: {
        type: {
            liked: [ActionSchema],
            followed: [ActionSchema],
            unfollowed: [ActionSchema],
            rated: [ActionSchema],
            bookmarked: [ActionSchema]
        },
        required: true,
        default: {
            liked: [],
            followed: [],
            unfollowed: [],
            rated: [],
            bookmarked: []
        }
    }
});

module.exports = mongoose.model('reaction_streams', ReactStreamSchema);
