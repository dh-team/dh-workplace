'use strict';

var mongoose = require('mongoose');

var SpaceMembership = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    user_id: {
        type: String,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    uname: {    // unique in space
        type: String,
        required: false
    },
    groups: [String], // ['group_id', ...]
    roles: [String], // ['role1', ...]
    is_default: { // default space of user
        type: Boolean,
        required: true,
        default: false
    },
    is_active: {
        type: Boolean,
        required: true,
        default: true
    }
});

module.exports = mongoose.model('space_mbs', SpaceMembership);
