'use strict';

var mongoose = require('mongoose');
var Reaction = require('./reaction');
var CommentSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    ref_type: {
        type: String, // "post" | "file" ...
        required: false
    },
    ref_id: {
        type: String, // "POST_ID" | "FILE_ID" ...
        required: false
    },
    reply_to: {
        type: String, // reply to comment id
        required: false
    },
    content: {
        type: String,
        required: true
    },
    stat: {
        read_count: Number,
        like_count: Number,
        comment_count: Number,
        last_interact_time: Date,
    },
    allow_reply: { // unset is true
        type: Boolean,
        required: false,
        default: true
    },
    is_active: {
        type: Boolean,
        required: false,
        default: true
    },
    meta: {
        create_user: mongoose.Schema.Types.Mixed,
        create_time: {
            type: Date,
            default: Date.now
        },
        modify_time: Date,
    },
    reactions: {
        type: [Reaction],
        default: []
    }
});

module.exports = mongoose.model('old_comments', CommentSchema);