'use strict';

var mongoose = require('mongoose');

var Meta = new mongoose.Schema({
    create_user: mongoose.Schema.Types.Mixed,
    create_time: {
        type: Date,
        default: Date.now
    },
    modify_user: mongoose.Schema.Types.Mixed,
    modify_time: Date,
});

module.exports = Meta;
