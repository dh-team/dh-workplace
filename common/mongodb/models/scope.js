'use strict';

var mongoose = require('mongoose');

var Scope = new mongoose.Schema({
    view_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    },
    interact_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    },
    modify_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["u:owner"]
    },
    manage_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["u:owner"]
    },
    notify_scope: {
        type: [String], // [String], example [ "g:GROUP_ID", "u:USER_ID" ]
        required: false,
        default: ["g:all"]
    }
});

module.exports = Scope;
