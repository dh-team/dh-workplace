'use strict';

var mongoose = require('mongoose');

var TokenSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    value: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    client_id: {
        type: String,
        required: true
    },
    scope: {
        type: String,
        required: false
    },
    using_space: {
        space_id: String,
        uname: String,
        groups: [String], // ['group_id', ...]
        positions: [String], // ['position_id', ...]
        roles: [String], // ['role1', ...]
    },
    expiration_date: {
        type: Date,
        required: true
    }
});

// Export the Mongoose model
module.exports = mongoose.model('tokens', TokenSchema);
