'use strict';

var mongoose = require('mongoose');

var SpaceSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    name: {
        type: String,
        required: true
    },
    short_name: {
        type: String,
        required: false
    },
    slogan: {
        type: String,
        required: false
    },
    uname: {
        type: String,
        required: true
    },
    url: {
        type: Array, // [String]
        required: false
    },
    country: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    },
    phone: {
        type: String,
        required: false
    },
    size: {
        type: String,
        required: false
    },
    type: {
        type: Number,
        required: false
    },
    logo_url: {
        type: String,
        required: false
    },
    is_verified: {
        type: Boolean,
        required: true,
        default: true
    },
    is_active: {
        type: Boolean,
        required: true,
        default: true
    },
    meta: {
        create_user: String,
        create_time: {
            type: Date,
            default: Date.now
        },
        modify_user: String,
        modify_time: Date,
    }
});

module.exports = mongoose.model('spaces', SpaceSchema);
