'use strict';

var mongoose = require('mongoose');
var NotifiedUser = new mongoose.Schema({
    user_id: String,
    is_read: Boolean,
    is_active: Boolean
});

var NotificationSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    notified_users: [NotifiedUser],
    content: {
        type: String,
        required: false
    },
    type: {
        type: String, // "post_new" | "post_edit" ...
        required: false
    },
    ref_data: {
        type: mongoose.Schema.Types.Mixed,
        required: false
    },
    ref_link: {
        type: String,
        required: false
    },
    create_time: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('notifications', NotificationSchema);
