'use strict';

var mongoose = require('mongoose');
var Meta = require('./meta');
var Scope = require('./scope');
var Reaction = require('./reaction');

var PostSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: false
    },
    content: {
        type: String,
        required: true
    },
    tags: {
        type: [String],
        required: false
    },
    categories: {
        type: [String],
        required: false
    },
    type: {
        type: String, // "archive" | "live"
        required: false
    },
    attachments: {
        display_type: String, // "gallery | list"
        files: Array // [ file ], file: {id, name, size, mime_type}
    },
    stat: {
        read_count: Number,
        like_count: Number,
        comment_count: Number,
        last_interact_time: Date,
    },
    allow_comment: { // unset is true
        type: Boolean,
        required: false,
        default: true
    },
    is_active: {
        type: Boolean,
        required: false,
        default: true
    },
    scope: {
        type: Scope,
        default: {
            view_scope: ["g:all"],
            interact_scope: ["g:all"],
            modify_scope: ["u:owner"],
            manage_scope: ["u:owner"],
            notify_scope: ["g:all"]
        }
    },
    meta: Meta,
    reactions: {
        type: [Reaction],
        default: []
    }, 
    comment_stream: String,
    reaction_stream: String
});

module.exports = mongoose.model('posts', PostSchema);
