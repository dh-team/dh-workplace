'use strict';

var mongoose = require('mongoose');

var CodeSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    value: {
        type: String,
        required: true
    },
    redirect_uri: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    client_id: {
        type: String,
        required: true
    },
    scope: {
        type: String,
        required: false
    }
});

module.exports = mongoose.model('codes', CodeSchema);
