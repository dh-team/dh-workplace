'use strict';

var mongoose = require('mongoose');
var Meta = require('./meta');
var Scope = require('./scope');

var CategorySchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space: {
        type: String,
        required: true
    },
    parent: {
        type: String,
        required: false,
        default: null
    },
    all_parents: {
        type: [String],
        required: false,
        default: []
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: false
    },
    is_active: {
        type: Boolean,
        required: false,
        default: true
    },
    scope: {
        type: Scope,
        default: {
            view_scope: ["g:all"],
            interact_scope: ["g:all"],
            modify_scope: ["u:owner"],
            manage_scope: ["u:owner"],
            notify_scope: ["g:all"]
        }
    },
    stat: {
        type: {
            post_count: Number
        },
        default: {
            post_count: 0
        }
    },
    meta: Meta
});

CategorySchema.pre('save', function(next) {
    var cat = this;
    if (cat.isModified('parent')) {
        if (!cat.parent || cat.parent === 'root') {
            cat.parent = null;
        }
    }
    return next();
});


module.exports = mongoose.model('post_categories', CategorySchema);
