'use strict';

var mongoose = require('mongoose');

var OAuthMembership = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    user_id: {
        type: String,
        required: true
    },
    provider: {
        type: String,
        required: true
    },
    provider_user_id: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('oauth_mbs', OAuthMembership);
