'use strict';

var mongoose = require('mongoose');

var ClientSchema = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        unique: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    secret: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    redirect_uri: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: false
    },
    is_trusted: {
        type: Boolean,
        required: false
    }
});

module.exports = mongoose.model('clients', ClientSchema);
