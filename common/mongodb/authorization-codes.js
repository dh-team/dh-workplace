/*jslint node: true */
"use strict";

var Code = require("./models/code");
var utils = require("../utils/common");

//The authorization codes.
//You will use these to get the access codes to get to the data in your endpoints as outlined
//in the RFC The OAuth 2.0 Authorization Framework: Bearer Token Usage
//(http://tools.ietf.org/html/rfc6750)

/**
 * Returns an authorization code if it finds one, otherwise returns
 * null if one is not found.
 * @param key The key to the authorization code
 * @param done The function to call next
 * @returns The authorization code if found, otherwise returns null
 */
exports.find = function(key, done) {
    Code.findOne({
        value: key
    }, function(err, obj) {
        if (err) return done(err);
        return done(null, obj);
    });
};

/**
 * Saves a authorization code, client id, redirect uri, user id, and scope.
 * @param code The authorization code (required)
 * @param clientId The client ID (required)
 * @param userId The user ID (required)
 * @param redirectURI The redirect URI of where to send access tokens once exchanged (required)
 * @param scope The scope (optional)
 * @param done Calls this with null always
 * @returns returns this with null
 */
exports.save = function(code, done) {
    var newCode = new Code();
    utils.copy(code, newCode);
    newCode.save(function(err) {
        if (err) return done(err);

        return done(null, newCode);
    });
};

/**
 * Deletes an authorization code
 * @param key The authorization code to delete
 * @param done Calls this with null always
 */
exports.delete = function(key, done) {
    Code.findOneAndRemove({
        value: key
    }, function(err) {
        if (err) return done(err);

        return done(null);
    });
};
