/*jslint node: true */
'use strict';

var SpaceMbs = require('./models/space-mbs');
var Group = require('./models/group');
var utils = require('../utils/common');

function findOne(cond, done) {
    SpaceMbs.findOne(cond, function (err, r) {
        if (err) return done(err);
        return done(null, r);
    });
};

function findByUserID(spaceID, userID, done) {
    return findOne({
        space_id: spaceID,
        user_id: userID
    }, done);
};

function findByUname(spaceID, uname, done) {
    return findOne({
        space_id: spaceID,
        uname: uname
    }, done);
};

function filter(cond, sort, done) {
    if ("function" === typeof sort) {
        done = sort;
        SpaceMbs.find(cond, done);
    }
    else {
        SpaceMbs.find(cond).sort(sort).exec(done);
    }
}

function insert(mbs, done) {
    var newMbs = new SpaceMbs();
    utils.copy(mbs, newMbs);
    newMbs.save(function (err) {
        if (err) return done(err);
        return done(null, newMbs);
    });
};

function remove(spaceID, userID, done) {
    SpaceMbs.findOneAndRemove({
        space_id: spaceID,
        user_id: userID
    }, function (err, deleted) {
        if (err) return done(err);
        return done(null, deleted);
    });
};

function activeMembership(spaceID, userID, done) {
    SpaceMbs.findOneAndUpdate({
        space_id: spaceID,
        user_id: userID
    }, {
            $set: {
                is_active: true
            }
        }, {
            new: true // return doc after update
        }, function (err, updated) {
            if (err) return done(err);
            return done(null, updated);
        });
};

function getSpacesOfUser(userID, done) {
    SpaceMbs.aggregate([{
        $match: {
            user_id: userID
        }
    }, {
        $lookup: {
            from: "spaces",
            localField: "space_id",
            foreignField: "_id",
            as: "space_docs"
        }
    }, {
        $project: {
            is_active: 1,
            is_default: 1,
            groups: 1,
            uname: 1,
            roles: 1,
            space: "$space_docs"
        }
    }], function (err, r) {
        if (err) return done(err);

        return done(null, r.map(function (item) {
            item.space = item.space.length > 0 ? item.space[0] : null;
            return item;
        }));
    });
}

function filterUsersInSpace(cond, done) {
    SpaceMbs.aggregate([{
        $match: cond
    }, {
        $lookup: {
            from: "users",
            localField: "user_id",
            foreignField: "_id",
            as: "user_docs"
        }
    }, {
        $project: {
            is_active: 1,
            is_default: 1,
            groups: 1,
            uname: 1,
            roles: 1,
            user: "$user_docs"
        }
    }], function (err, r) {
        if (err) return done(err);

        return done(null, r.map(function (item) {
            item.user = item.user.length > 0 ? item.user[0] : null;
            return item;
        }));
    });
}

function getUsersInSpace(spaceID, done) {
    return filterUsersInSpace({
        space_id: spaceID
    }, done);
}

function getUsersInGroup(spaceID, groupID, done) {
    return filterUsersInSpace({
        space_id: spaceID,
        groups: groupID
    }, done);
};

function addUserToGroup(spaceID, userID, groupID, done) {
    SpaceMbs.findOneAndUpdate(
        {
            space_id: spaceID,
            user_id: userID
        }, {
            $addToSet: {
                groups: groupID
            }
        }, {
            new: true // return doc after update
        }, function (err, updated) {
            if (err) return done(err);
            // add user to list users in group
            Group.findOneAndUpdate(
                {
                    _id: groupID,
                    space_id: spaceID
                }, {
                    $addToSet: {
                        users: userID
                    }
                }, function (err) {
                    if (err) return done(err);
                    return done(null, updated);
                });
        });
};

function deleteUserInGroup(spaceID, userID, groupID, done) {
    SpaceMbs.findOneAndUpdate(
        {
            space_id: spaceID,
            user_id: userID
        }, {
            $pull: {
                groups: groupID
            }
        }, {
            new: true // return doc after update
        }, function (err, updated) {
            if (err) return done(err);

            Group.findOneAndUpdate(
                {
                    _id: groupID,
                    space_id: spaceID
                }, {
                    $pull: {
                        users: userID
                    }
                }, function (err) {
                    if (err) return done(err);
                    return done(null, updated);
                });
        });
};

module.exports = {
    findOne: findOne,
    findByUserID: findByUserID,
    findByUname: findByUname,
    filter: filter,
    insert: insert,
    delete: remove,
    activeMembership: activeMembership,
    getSpacesOfUser: getSpacesOfUser,
    filterUsersInSpace: filterUsersInSpace,
    getUsersInSpace: getUsersInSpace,
    getUsersInGroup: getUsersInGroup,
    addUserToGroup: addUserToGroup,
    deleteUserInGroup: deleteUserInGroup
}
