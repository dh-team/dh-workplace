"use strict";

var VerifyCode = require("./models/verify-code");
var utils = require('../utils/common');

exports.insertCode = function(code, done) {
    var newCode = new VerifyCode();
    utils.copy(code, newCode);
    newCode.code = utils.uid(16); // generate code
    newCode.issue_time = new Date();

    newCode.save(function(err) {
        if (err) return done(err);
        return done(null, newCode);
    });
};

exports.verifyCode = function(toVerify, code, callback) {
    VerifyCode.findOne({
        to_verify: toVerify,
        code: code
    }, function(err, foundCode) {
        if (err) return callback(err);
        if (!foundCode) return callback(null, null);

        // delete code
        VerifyCode.remove({
            to_verify: toVerify
        }, function(err) {
            if (err) return callback(err);
            callback(null, foundCode);
        });
    });
};
