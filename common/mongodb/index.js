'use strict';

var mongoose = require('mongoose');

exports.init = function(dbName, callback) {
     var uri = 'mongodb://localhost:27017/' + dbName;
    // Create the database connection
    mongoose.connect(uri);

    // CONNECTION EVENTS
    // When successfully connected
    mongoose.connection.on('connected', function() {
        console.log('Mongoose connection open to ' + uri);
        callback && callback(null);
    });

    // If the connection throws an error
    mongoose.connection.on('error', function(err) {
        console.log('Mongoose connection error: ' + err);
        callback && callback(err);
    });

    // When the connection is disconnected
    mongoose.connection.on('disconnected', function() {
        console.log('Mongoose connection disconnected');
    });
}

exports.spaces = require('./spaces');
exports.users = require('./users');
exports.clients = require('./clients');
exports.spaceMbs = require('./space-mbs');
exports.accessTokens = require('./access-tokens');
exports.authorizationCodes = require('./authorization-codes');
exports.refreshTokens = require('./refresh-tokens');
exports.verifyCodes = require('./verify-codes');
exports.groups = require('./groups');
// exports.postCats = require('./post-categories');
// exports.posts = require('./posts');
// exports.tags = require('./tags');
// exports.comments = require('./comments');
// exports.notifications = require('./notifications');
// exports.reactions = require('./reactions');
