/*jslint node: true */
"use strict";

var RefreshToken = require("./models/refresh-token");
var utils = require("../utils/common");

//The refresh tokens.
//You will use these to get access tokens to access your end point data through the means outlined
//in the RFC The OAuth 2.0 Authorization Framework: Bearer Token Usage
//(http://tools.ietf.org/html/rfc6750)


/**
 * Returns a refresh token if it finds one, otherwise returns
 * null if one is not found.
 * @param key The key to the refresh token
 * @param done The function to call next
 * @returns The refresh token if found, otherwise returns null
 */
exports.find = function(key, done) {
    RefreshToken.findOne({
        value: key
    }, function(err, obj) {
        if (err) return done(err);
        return done(null, obj);
    });
};

/**
 * Saves a refresh token, user id, client id, and scope.
 * @param token The refresh token (required)
 * @param userId The user ID (required)
 * @param clientId The client ID (required)
 * @param scope The scope (optional)
 * @param done Calls this with null always
 * @returns returns this with null
 */
exports.save = function(token, userId, clientId, scope, done) {
    var newToken = new RefreshToken();
    utils.copy(token, newToken);
    newToken.save(function(err) {
        if (err) return done(err);

        return done(null, newToken);
    });
};

/**
 * Deletes a refresh token
 * @param key The refresh token to delete
 * @param done returns this when done
 */
exports.delete = function(key, done) {
    RefreshToken.findOneAndRemove({
        value: key
    }, function(err) {
        if (err) return done(err);

        return done(null);
    });
};
