/*jslint node: true */
'use strict';

var asyn = require("async");
var Group = require('./models/group');
var spaceMbs = require("./space-mbs");
var utils = require('../utils/common');

function findOne(cond, done) {
    Group.findOne(cond, function(err, group) {
        if (err) return done(err);
        return done(null, group);
    });
};

function findByID(id, done) {
    return findOne({
        _id: id
    }, done);
};

function findByUname(spaceID, uname, done) {
    return findOne({
        space_id: spaceID,
        uname: uname
    }, done);
};

function filter(cond, done) {
    Group.find(cond, function(err, groups) {
        if (err) return done(err);
        return done(null, groups);
    });
}

function list(spaceID, done) {
    return filter({
        space_id: spaceID
    }, done);
};

function insert(group, done) {
    var newGroup = new Group();
    utils.copy(group, newGroup);
    newGroup.save(function(err) {
        if (err) return done(err);
        buildTree(newGroup, function(err) {
            if (err) return done(err);
            done(null, newGroup);
        });
    });
};

function update(group, done) {
    Group.findOne({
        _id: group.id
    }, function(err, r) {
        if (err || !r) return done(err, r);
        utils.update(group, r);
        var rebuildTree = r.type === "org" && r.isModified('org_info.parent_id');
        r.save(function(err) {
            if (err) return done(err);
            if (rebuildTree) {
                console.log("change parent => rebuild tree");
                buildTreeForSpace(r.space_id, function(err) {
                    if (err) return done(err);
                    return done(null, r);
                });
            }
            else {
                return done(null, r);
            }
        });
    });
};

function moveChildsUp(deleted, done) {
    // move childs to parent of this
    Group.update({
        "type": "org",
        "org_info.parent_id": deleted._id
    }, {
        $set: {
            "org_info.parent_id": deleted.org_info.parent_id
        }
    }, {
        multi: true
    }, done);
}

function deleteGroup(id, done) {
    Group.findOneAndRemove({
        _id: id
    }, function(err, deleted) {
        if (err || !deleted) return done(err, deleted);
        if (deleted.type !== 'org') {
            return done(null, deleted);
        }
        moveChildsUp(deleted, function(err) {
            if (err) return done(err);
            buildTreeForSpace(deleted.space_id, function(err) {
                if (err) return done(err);
                return done(null, deleted);
            });
        });
    });
}

function buildTreeForSpace(spaceID, done) {
    filter({
        "space_id": spaceID,
        "type": "org",
        "org_info.parent_id": null
    }, function(err, groups) {
        if (err) return done(err);
        asyn.each(groups, buildTreeR, done);
    });
}

function buildTreeByGroupID(groupID, done) {
    findByID(groupID, function(err, group) {
        if (err || !group) return done(err, group);
        buildTree(group, done);
    });
}

function buildTree(group, done) {
    getParents(group, function(err, parents) {
        if (err) return done(err);
        group.org_info.all_parents = parents;
        group.save(function(err) {
            if (err) return done(err);
            buildTreeR(group, function(err) {
                if (err) return done(err);
                done(null, group);
            });
        });
    });
}

function buildTreeR(group, done) {
    console.log("buildTreeR: " + group._id + ", parents: " + JSON.stringify(group.org_info.all_parents));
    var parents = group.org_info.all_parents;
    parents.unshift(group._id);
    var cond = {
        "type": "org",
        "org_info.parent_id": group._id
    };
    // update childs
    Group.update(cond, {
        $set: {
            "org_info.all_parents": parents
        }
    }, {
        multi: true
    }, function(err, r) {
        if (err) return done(err);
        if (r.nMatched === 0) return done(null); // no childs
        filter(cond, function(err, childs) {
            if (err) return done(err);
            if (!childs || childs.length === 0) return done(null);
            asyn.each(childs, buildTreeR, function(err) {
                if (err) return done(err);
                done(null);
            });
        });
    });
}

function getParents(group, done) {
    getParentsR([], group, done);
}

function getParentsR(parents, group, done) {
    if (!group.org_info || !group.org_info.parent_id) {
        return done(null, parents);
    }

    findOne({
        _id: group.org_info.parent_id
    }, function(err, r) {
        if (err) return done(err);
        if (!r) return done(null, parents)
        parents.push(r._id);
        getParentsR(parents, r, done);
    });
}

function listGroupsOfUser(spaceID, userID, done) {
    spaceMbs.findByUserID(spaceID, userID, function(err, mbs) {
        if (err) return done(err);
        if (!mbs) return done(null, null);
        if (!mbs.groups || mbs.groups.length === 0) {
            return done(null, null);
        }
        var cond = {
            space_id: spaceID,
            // "org_info.type": {
            //     $ne: "unique"
            // },
            _id: {
                $in: mbs.groups
            }
        };

        filter(cond, function(err, groups) {
            if (err) return done(err);
            var moreGroupIDs = [];
            groups.map(function(item) {
                if (item.org_info && item.org_info.all_parents) {
                    item.org_info.all_parents.map(function(id) {
                        if (mbs.groups.indexOf(id) < 0 && moreGroupIDs.indexOf(id) < 0) {
                            moreGroupIDs.push(id);
                        }
                    });
                }
            });

            cond._id = {
                $in: moreGroupIDs
            };
            filter(cond, function(err, moreGroups) {
                if (err) return done(err);
                if (!moreGroups) return done(null, groups);
                return done(null, groups.concat(moreGroups));
            });
        });
    });
}

// list parent groups of input groups
function listParentGroups(groupIDs, done) {
    filter({
        _id: {
            $in: groupIDs
        }
    }, function(err, groups) {
        if (err) return done(err);
        var moreGroupIDs = [];
        groups.map(function(item) {
            if (item.org_info && item.org_info.all_parents) {
                item.org_info.all_parents.map(function(id) {
                    if (groupIDs.indexOf(id) < 0 && moreGroupIDs.indexOf(id) < 0) {
                        moreGroupIDs.push(id);
                    }
                });
            }
        });
        done(null, moreGroupIDs);
    });
}

function listDescendantGroups(groupID, done) {
    filter({
        "org_info.all_parents": groupID
    }, function(err, groups) {
        if (err) return done(err);
        return done(null, groups);
    });
}


module.exports = {
    findOne: findOne,
    findByID: findByID,
    findByUname: findByUname,
    filter: filter,
    getParents: getParents,
    buildTree: buildTree,
    buildTreeForSpace: buildTreeForSpace,
    buildTreeByGroupID: buildTreeByGroupID,
    list: list,
    insert: insert,
    update: update,
    moveChildsUp: moveChildsUp,
    delete: deleteGroup,
    listGroupsOfUser: listGroupsOfUser,
    listParentGroups: listParentGroups,
    listDescendantGroups: listDescendantGroups
}
