"use strict";

var bcrypt = require("bcrypt-nodejs");
var User = require("./models/user");
var OAuthMembership = require("./models/oauth-mbs");
var SpaceMembership = require("./models/space-mbs");
var utils = require('../utils/common');

/**
 * Returns a user if it finds one, otherwise returns
 * null if a user is not found.
 * @param id The unique id of the user to find
 * @param done The function to call next
 * @returns The user if found, otherwise returns null
 */
function findByID(id, done) {
    User.findOne({
        _id: id
    }, function(err, user) {
        if (err) return done(err);
        return done(null, user);
    });
};

/**
 * Returns a user if it finds one, otherwise returns
 * null if a user is not found.
 * @param username The unique user name to find
 * @param done The function to call next
 * @returns The user if found, otherwise returns null
 */
function findByEmail(email, done) {
    User.findOne({
        "emails.email": email,
        // "emails.is_verified": true
    }, function(err, user) {
        if (err) return done(err);
        return done(null, user);
    });
};

function findByPhone(phone, done) {
    User.findOne({
        "phones.phone": phone,
        // "phones.is_verified": true
    }, function(err, user) {
        if (err) return done(err);
        return done(null, user);
    });
};

function findByUsername(username, done) {
    User.findOne({
        $or: [{
            "emails.email": username,
            // "emails.is_verified": true,
            // "emails.is_main": true
        }, {
            "phones.phone": username,
            // "phones.is_verified": true,
            // "phones.is_main": true
        }]
    }, function(err, user) {
        if (err) return done(err);
        return done(null, user);
    });
};


function insert(user, done) {
    var newUser = new User();
    utils.copy(user, newUser);
    newUser.save(function(err) {
        if (err) return done(err);

        return done(null, newUser);
    });
};

function verifyPassword(stored, password, cb) {
    if (!stored || stored.length == 0) return cb(null, false);
    bcrypt.compare(password, stored, function(err, isMatch) {
        if (err) return cb(err);
        cb(null, isMatch);
    });
};

function findOAuthMembership(provider, identity, cb) {
    OAuthMembership.findOne({
        provider: provider,
        provider_user_id: identity.user_id
    }, function(err, mbs) {
        if (err) return cb(err);
        if (!mbs) {
            // membership not found, try find and link by email
            return tryLinkOAuthMembershipByEmail(provider, identity, cb);
        }

        findByID(mbs.user_id, function(err, foundUser) {
            if (err) return cb(err);
            if (foundUser) return cb(null, foundUser);

            console.log("no user found, leak here: may be user is deleted but membership still exist.");
            OAuthMembership.remove({
                provider: provider,
                provider_user_id: identity.user_id
            }, function(err) {
                if (err) return cb(err);
                tryLinkOAuthMembershipByEmail(provider, identity, cb);
            });
        });
    });
};

function tryLinkOAuthMembershipByEmail(provider, identity, cb) {
    if (!identity.email) return cb(null, null);

    // find user by email
    findByUsername(identity.email, function(err, foundUser) {
        if (err) return cb(err);
        if (!foundUser) return cb(null, null);

        console.log("found user with email " + identity.email + ", create membership");
        // link accounts
        insertOAuthMembership({
            user_id: foundUser.id,
            provider: provider,
            provider_user_id: identity.user_id
        }, function(err) {
            if (err) return cb(err);
            return cb(null, foundUser);
        });
    });
}

function insertOAuthMembership(membership, done) {
    var newMembership = new OAuthMembership();
    utils.copy(membership, newMembership);
    newMembership.save(function(err) {
        if (err) return done(err);
        return done(null, newMembership);
    });
};

function findSpaceMembership(userId, spaceId, callback) {
    SpaceMembership.findOne({
        user_id: userId,
        space_id: spaceId
    }, callback);
}

module.exports = {
    findByID: findByID,
    findByEmail: findByEmail,
    findByPhone: findByPhone,
    findByUsername: findByUsername,
    insert: insert,
    verifyPassword: verifyPassword,
    findOAuthMembership: findOAuthMembership,
    insertOAuthMembership: insertOAuthMembership,
    findSpaceMembership: findSpaceMembership,
};
