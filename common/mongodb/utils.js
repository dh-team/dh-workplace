var mongoose = require('mongoose');

function newId() {
    return mongoose.Types.ObjectId().toString();
}

module.exports = {
    newId
};
