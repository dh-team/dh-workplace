/*jslint node: true */
"use strict";

var Token = require("./models/token");
var utils = require("../utils/common");

//The access tokens.
//You will use these to access your end point data through the means outlined
//in the RFC The OAuth 2.0 Authorization Framework: Bearer Token Usage
//(http://tools.ietf.org/html/rfc6750)

/**
 * Returns an access token if it finds one, otherwise returns
 * null if one is not found.
 * @param key The key to the access token
 * @param done The function to call next
 * @returns The access token if found, otherwise returns null
 */
exports.find = function(key, done) {
    Token.findOne({
        value: key
    }, function(err, obj) {
        if (err) return done(err);
        return done(null, obj);
    });
};

/**
 * Saves a access token, expiration date, user id, client id, and scope.
 * @param token The access token (required)
 * @param expirationDate The expiration of the access token that is a javascript Date() object (required)
 * @param userId The user ID (required)
 * @param clientId The client ID (required)
 * @param scope The scope (optional)
 * @param done Calls this with null always
 * @returns returns this with null
 */
exports.save = function(token, done) {
    var newToken = new Token();
    utils.copy(token, newToken);
    newToken.save(function(err) {
        if (err) return done(err);

        return done(null, newToken);
    });
};

/**
 * Deletes an access token
 * @param key The access token to delete
 * @param done returns this when done
 */
exports.delete = function(key, done) {
    Token.findOneAndRemove({
        value: key
    }, function(err) {
        if (err) return done(err);

        return done(null);
    });
};

/**
 * Removes expired access tokens.  It does this by looping through them all
 * and then removing the expired ones it finds.
 * @param done returns this when done.
 * @returns done
 */
exports.removeExpired = function(done) {
    Token.remove({
        expiration_date: {
            $lt: new Date()
        }
    }, function(err) {
        if (err) return done(err);

        return done(null);
    });
};
