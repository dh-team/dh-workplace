/*jslint node: true */
'use strict';

var Space = require('./models/space');
var SpaceMbs = require('./models/space-mbs');
var groups = require("./groups");
var spaceMbs = require("./space-mbs");
var utils = require('../utils/common');

function findOne(cond, done) {
    Space.findOne(cond, function(err, group) {
        if (err) return done(err);
        return done(null, group);
    });
};

exports.findByID = function(id, done) {
    return findOne({
        _id: id
    }, done);
};

exports.findByUname = function(name, done) {
    Space.findOne({
        uname: name
    }, function(err, space) {
        if (err) return done(err);
        return done(null, space);
    });
};

exports.insert = function(space, done) {
    var newSpace = new Space();
    utils.copy(space, newSpace);
    newSpace.save(function(err) {
        if (err) return done(err);
        return done(null, newSpace);
    });
};

exports.update = function(space, done) {
    Space.findOne({
        _id: space.id
    }, function(err, r) {
        if (err) return done(err);
        utils.update(space, r);        
        r.save(function(err){
            if (err) return done(err);
            return done(null, r);
        });
    });
};

// find group | user in space by uname
exports.findUname = function(spaceID, uname, done) {
    // find group
    groups.findByUname(spaceID, uname, function(err, group){
        if (err) return done(err);
        if(group) return done(null, group, "group");

        spaceMbs.findByUname(spaceID, uname, function(err, user){
            if (err) return done(err);
            if(user) return done(null, user, "user");
            return done(null, null);
        });
    });
};
