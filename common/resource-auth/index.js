'use strict';

var request = require('request');
var passport = require('passport');
var BearerStrategy = require('passport-http-bearer').Strategy;

var db = require('./db');
var httpUtils = require("../utils/http");

var setting = {};

function bearerAuth(accessToken, done) {
    // search token from cache
    db.accessTokens.find(accessToken, function(err, foundToken) {
        if (err) return done(err);
        if (foundToken) {
            if (foundToken.expires_in && (new Date() > foundToken.expires_in)) {
                console.log('token expired: ' + accessToken);
                // delete expired token
                return db.accessTokens.delete(accessToken, function(err) {
                    if (err) return done(err);
                    return done(null, false);
                });
            }
            return done(null, foundToken.user, {
                token: foundToken
            });
        }

        // validate token
        request.get(setting.authServerUrl + "/as/oauth/tokeninfo?token=" + accessToken,
            function(err, response, body) {
                if (err) {
                    console.log('get token info error:' + JSON.stringify(err));
                    return done(null, false);
                }

                if (response.statusCode !== 200) {
                    return done(null, false);
                }

                var tokenInfo = JSON.parse(body);
                console.log("get token info: " + JSON.stringify(tokenInfo));
                if (tokenInfo.error) {
                    return done(null, false);
                }
                tokenInfo.token = accessToken;
                //TODO scopes
                db.accessTokens.save(tokenInfo, function(err, savedToken) {
                    if (err) return done(err);
                    return done(null, savedToken.user, {
                        token: savedToken
                    });
                });
            }
        );
    });
}

function bearerSpaceAuth(accessToken, done) {
    bearerAuth(accessToken, function(err, user, authInfo) {
        if (err || !user) return done(err, user);
        if (!authInfo.token || !authInfo.token.using_space) return done(null, null);
        return done(null, user, authInfo);
    });
}

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate either users or clients based on an access token
 * (aka a bearer token).  If a user, they must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
passport.use('bearer', new BearerStrategy(bearerAuth));
passport.use('bearer-space', new BearerStrategy(bearerSpaceAuth));

// Register serialialization and deserialization functions.
//
// When a client redirects a user to user authorization endpoint, an
// authorization transaction is initiated.  To complete the transaction, the
// user must authenticate and approve the authorization request.  Because this
// may involve multiple HTTPS request/response exchanges, the transaction is
// stored in the session.
//
// An application must supply serialization functions, which determine how the
// client object is serialized into the session.  Typically this will be a
// simple matter of serializing the client's ID, and deserializing by finding
// the client by ID from the database.

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});


module.exports.authSuccess = passport.authenticate('bearer', {
    session: false
});


module.exports.spaceAuthSuccess = passport.authenticate('bearer-space', {
    session: false
});

module.exports.configure = function(config) {
    setting = config;
}

module.exports.authorizedWithRole = function(role) {
    return function(req, res, next) {
        var auth = httpUtils.extractAuthInfo(req);
        // console.log("authorizedWithRole user: " + JSON.stringify(auth))
        if (!auth || !auth.roles || auth.roles.indexOf(role) === -1) {
            return httpUtils.responseInvalid(res, "unauthorized");
        }
        else {
            next();
        }
    }
}

module.exports.authorizedWithRoles = function(roles) {
    return function(req, res, next) {
        var auth = httpUtils.extractAuthInfo(req);
        if (!auth || !auth.roles) {
            return httpUtils.responseInvalid(res, "unauthorized");
        }
        for (var r in roles) {
            if (auth.roles.indexOf(r) > -1) {
                return next();
            }
        }
        return httpUtils.responseInvalid(res, "unauthorized");
    }
}

module.exports.authorizedWithAllRoles = function(roles) {
    return function(req, res, next) {
        var auth = httpUtils.extractAuthInfo(req);
        if (!auth || !auth.roles) {
            return httpUtils.responseInvalid(res, "unauthorized");
        }
        for (var r in roles) {
            if (auth.roles.indexOf(r) === -1) {
                return httpUtils.responseInvalid(res, "unauthorized");
            }
        }
        return next();
    }
}

module.exports.adminAuthorized = module.exports.authorizedWithRole("administrator");
