var assert = require("assert");
var asyn = require("async");
var mongoose = require('mongoose');

var config = require("../conf.json");
var db = require("../mongodb");

var Client = require("../mongodb/models/client.js");
var User = require("../mongodb/models/user.js");
var OAuthMbs = require("../mongodb/models/oauth-mbs");
var Space = require("../mongodb/models/space");
var SpaceMbs = require("../mongodb/models/space-mbs");
var Group = require("../mongodb/models/group");
var Post = require("../mongodb/models/post");
var OldComment = require("../mongodb/models/old-comment");

var RefreshToken = require("../mongodb/models/refresh-token");
var AccessToken = require("../mongodb/models/token");
var AuthCode = require("../mongodb/models/code");
var VerifyCode = require("../mongodb/models/verify-code");

function clearData(callback) {
    asyn.each([Client, User, OAuthMbs, SpaceMbs, Space, Group, RefreshToken, AccessToken, AuthCode, VerifyCode],
        function(collection, cb) {
            collection.remove({}, cb);
        }, callback);
}

function initData(callback) {
    db.init("main-db", function(err) {
        if (err) return callback(err);
        clearData(function(err) {
            if (err) return callback(err);
            asyn.parallel([
                function(cb) {
                    db.clients.insert({
                        _id: "upspace_web_app",
                        secret: "upspace_web_app_secret",
                        name: "Upspace web application",
                        redirect_uri: config.publishUrl + "/as/auth/handle",
                        type: "web-app",
                        is_trusted: true
                    }, function(err, r) {
                        assert.equal(err, null);
                        assert.notEqual(r, null);

                        db.clients.find(r.id, function(err, r1) {
                            assert.equal(err, null);
                            assert.equal(r.id, r1.id);

                            console.log(JSON.stringify(r1));
                            cb(null);
                        });
                    });
                },
                function(cb) {
                    db.clients.insert({
                        _id: "space_web_app",
                        secret: "space_web_app_secret",
                        name: "Upspace web application for space",
                        redirect_uri: config.publishUrl + "/as/auth/handle",
                        type: "web-app",
                        is_trusted: true
                    }, function(err, r) {
                        assert.equal(err, null);
                        assert.notEqual(r, null);
                        cb(null);
                    });
                },
                function(cb) {
                    db.clients.insert({
                        name: "Testing application",
                        redirect_uri: config.publishUrl + "/as/auth/handle",
                        type: "web-app",
                        is_trusted: false
                    }, function(err, r) {
                        assert.equal(err, null);
                        assert.notEqual(r, null);

                        console.log(JSON.stringify(r));
                        cb(null);
                    });
                },
                function(cb) {
                    db.users.insert({
                        display_name: "Testing user",
                        emails: [{
                            email: "user1_email1@upspace.vn",
                            is_main: true
                        }, {
                            email: "user1_email2@upspace.vn",
                            is_main: false
                        }],
                        phones: [{
                            phone: "user1_phone1",
                            is_main: true
                        }, {
                            phone: "user1_phone2",
                            is_main: false
                        }],
                        password: "1234567",
                        is_active: true
                    }, function(err, r) {
                        assert.equal(err, null);
                        assert.notEqual(r, null);

                        db.users.findByUsername("user1_email1@upspace.vn", function(err, r1) {
                            assert.equal(err, null);
                            assert.equal(r.id, r1.id);

                            console.log(JSON.stringify(r1));
                            cb(null);
                        });
                    });
                }
            ], callback);
        });
    });
}

// old table comments to new table comment_streams
function migrateComment() {
    db.init("main-db", function(err) {
        if (err) return console.log(err);

        // get list of post
        db.posts.filter({}, function(err, r) {
            if (err) return console.log(err);
            console.log("found " + r.length + " posts");

            asyn.eachSeries(r, function(post, cb) {
                updatePost(post, cb);
            }, function(err) {
                if (err) return console.log(err);
                // insert comment
                OldComment.find({}, function(err, r) {
                    if (err) return console.log(err);

                    asyn.eachSeries(r, function(comment, cb) {
                        insertCommentToPost(comment, cb);
                    }, function(err) {
                        if (err) return console.log(err);
                        console.log("success!!!")
                    })
                })
            })
        });
    });
}

function updatePost(post, callback) {
    // create new comment stream
    var newStream = {
        space: post.space,
        ref_type: "post",
        ref_id: post._id
    };
    db.comments.insertStream(newStream, function(err, newCommentStream) {
        if (err) return callback(err);

        // update post
        Post.update({
            _id: post._id
        }, {
            $set: {
                comment_stream: newCommentStream._id
            }
        }, function(err) {
            if (err) return callback(err);
            console.log("update post " + post._id + " stream " + newCommentStream._id);
            callback();
        });
    })
}


function insertCommentToPost(newComment, done) {
    var postId = newComment.ref_id;
    newComment = newComment.toObject();
    delete newComment.space;
    delete newComment.ref_type;
    delete newComment.ref_id;

    db.posts.findByID(postId, function(err, post) {
        if (err) return done(err);
        if (!post || !post.comment_stream) return done();
        console.log("insert comment for post " + postId)
        insertComment(post.comment_stream, newComment, done);
    })
}

function insertComment(streamId, newComment, done) {
    db.comments.findStreamByID(streamId, function(err, cs) {
        if (err) return done(err);
        if (!cs) return done("comment_stream_not_exist");
        // add to array
        if (!cs.comments) cs.comments = [];
        if (!newComment._id) {
            newComment._id = mongoose.Types.ObjectId().toString();
        }
        cs.comments.push(newComment);

        // update stat
        cs.stat = {
            comment_count: cs.comments.length,
            last_comment_time: newComment.meta.create_time,
            last_comment_user: newComment.meta.create_user
        };

        // update
        cs.save(done);
    });
};

module.exports = {
    initData: initData,
    migrateComment: migrateComment
};
