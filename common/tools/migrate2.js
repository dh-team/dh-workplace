var assert = require("assert");
var asyn = require("async");
var db = require("../mongodb");

var Post = require("../mongodb/models/post");
var Work = require("../mongodb/models/app-work/work");
var ReactionStream = require("../mongodb/models/reaction-stream");
var CommentStream = require("../mongodb/models/comment");

const cmUtils = require('../utils/common');
const dbUtils = require('../mongodb/utils');


function migrate() {
    db.init("main-db", function (err) {
        if (err) return console.log(err);

        migratePost(function (err) {
            if (err) return console.log(err);
            migrateWork(function (err) {
                if (err) return console.log(err);
                migrateComment(function (err) {
                    if (err) return console.log(err);
                    console.log("migration completed");
                });
            })
        })
    });
}

function migratePost(done) {
    Post.find({}, function (err, r) {
        if (err) return done(err);

        console.log("found " + r.length + " posts");

        asyn.eachSeries(r, function (post, cb) {
            updatePostReaction(post, function (err) {
                if (err) return cb(err);
                return updatePostComment(post, cb);
            });
        }, function (err) {
            if (err) return done(err);

            console.log("update posts success");
            done();
        });
    });
}

function updatePostReaction(post, done) {
    if (post.reaction_stream) {
        return process.nextTick(function () {
            done();
        });
    }

    post.reaction_stream = dbUtils.newId();
    Post.update(
        {
            _id: post._id
        },
        {
            $set: {
                reaction_stream: post.reaction_stream
            }
        },
        function (err) {
            if (err) return done(err);

            var newStream = new ReactionStream();
            cmUtils.copy({
                _id: post.reaction_stream,
                space: post.space,
                owner: post.meta.create_user.user_id,
                ref_type: "post",
                ref_id: post._id,
            }, newStream);

            if (post.reactions && post.reactions.length > 0) {
                newStream.reaction.liked = post.reactions.map((item) => {
                    return {
                        issue_user: item.issue_user,
                        issue_time: item.issue_time
                    };
                });
            }

            newStream.save(done);
        });
}

function updatePostComment(post, done) {
    if (post.comment_stream) {
        Post.update(
            {
                _id: post._id
            },
            {
                $set: {
                    space: post.space,
                    owner: post.meta.create_user.user_id,
                    ref_type: "post",
                    ref_id: post._id,
                    ref_obj: {
                        type: post.type,
                        create_user: post.meta.create_user.user_id
                    }
                }
            },
            done);

    } else {
        post.comment_stream = dbUtils.newId();
        Post.update(
            {
                _id: post._id
            },
            {
                $set: {
                    comment_stream: post.comment_stream
                }
            },
            function (err) {
                if (err) return done(err);

                var newStream = new CommentStream();
                cmUtils.copy({
                    _id: post.comment_stream,
                    space: post.space,
                    owner: post.meta.create_user.user_id,
                    ref_type: "post",
                    ref_id: post._id,
                    ref_obj: {
                        type: post.type,
                        create_user: post.meta.create_user.user_id
                    }
                }, newStream);
                newStream.save(done);
            });
    }
}

function migrateComment(done) {
    CommentStream.find({}, function (err, r) {
        if (err) return done(err);

        console.log("found " + r.length + " comment stream");

        asyn.eachSeries(r, function (stream, cb) {
            if (!stream.comments || stream.comments.length === 0) {
                return process.nextTick(function () {
                    cb();
                })
            }

            asyn.eachSeries(stream.comments, function (comment, cb1) {
                updateCommentReaction(stream, comment, cb1);
            }, cb);
        }, function (err) {
            if (err) return done(err);

            console.log("update comment success");
            done();
        });
    });
}

function updateCommentReaction(stream, comment, done) {
    if (comment.reaction_stream) {
        return process.nextTick(function () {
            done();
        });
    }

    comment.reaction_stream = dbUtils.newId();
    CommentStream.update(
        {
            _id: stream._id,
            "comments._id": comment._id,
        },
        {
            $set: {
                "comments.$.reaction_stream": comment.reaction_stream
            }
        },
        function (err) {
            if (err) return done(err);

            var newStream = new ReactionStream();
            cmUtils.copy({
                _id: comment.reaction_stream,
                space: stream.space,
                owner: comment.meta.create_user.user_id,
                ref_type: "comment",
                ref_id: comment._id,
            }, newStream);

            if (comment.reactions && comment.reactions.length > 0) {
                newStream.reaction.liked = comment.reactions.map((item) => {
                    return {
                        issue_user: item.issue_user,
                        issue_time: item.issue_time
                    };
                });
            }

            newStream.save(function (err) {
                if (err) return done(err);
                return done(null, newStream);
            });
        });
}


function updateWorkReaction(work, done) {
    if (work.reaction_stream) {
        return process.nextTick(function () {
            done();
        });
    }

    work.reaction_stream = dbUtils.newId();
    Work.update(
        {
            _id: work._id
        },
        {
            $set: {
                reaction_stream: work.reaction_stream
            }
        },
        function (err) {
            if (err) return done(err);

            var newStream = new ReactionStream();
            cmUtils.copy({
                _id: work.reaction_stream,
                space: work.space,
                owner: work.meta.create_user.user_id,
                ref_type: "work",
                ref_id: work._id,
                ref_obj: {
                    source: work.source
                }
            }, newStream);

            if (work.reactions && work.reactions.length > 0) {
                newStream.reaction.liked = work.reactions.map((item) => {
                    return {
                        issue_user: item.issue_user,
                        issue_time: item.issue_time
                    };
                });
            }

            newStream.save(done);
        });
}

function updateWorkComment(work, done) {
    if (work.comment_stream) {
        Work.update(
            {
                _id: work._id
            },
            {
                $set: {
                    space: work.space,
                    owner: work.meta.create_user.user_id,
                    ref_type: "work",
                    ref_id: work._id,
                    ref_obj: {
                        source: work.source,
                        resource: work.resource
                    }
                }
            },
            done);

    } else {
        work.comment_stream = dbUtils.newId();
        Work.update(
            {
                _id: work._id
            },
            {
                $set: {
                    comment_stream: work.comment_stream
                }
            },
            function (err) {
                if (err) return done(err);

                var newStream = new CommentStream();
                cmUtils.copy({
                    _id: work.comment_stream,
                    space: work.space,
                    owner: work.meta.create_user.user_id,
                    ref_type: "work",
                    ref_id: work._id,
                    ref_obj: {
                        source: work.source,
                        resource: work.resource
                    }
                }, newStream);
                newStream.save(done);
            });
    }
}

function migrateWork(done) {
    // get list of post
    Work.find({}, function (err, r) {
        if (err) return done(err);

        console.log("found " + r.length + " works");

        asyn.eachSeries(r, function (work, cb) {
            updateWorkReaction(work, function (err) {
                if (err) return cb(err);
                return updateWorkComment(work, cb);
            });
        }, function (err) {
            if (err) return done(err);

            console.log("update works success");
            done();
        });
    });
}

migrate();

module.exports = {
    migrateComment: migrate
};
