dir=$(pwd)

if [ "$1" == 'web' -o "$1" == '' ]
then
    echo "Build web-app..."
    cd $dir/web-app
    NODE_ENV=production node_modules/.bin/webpack -p
fi

if [ "$1" == 'space' -o "$1" == '' ]
then
    echo "Build space-app..."
    cd $dir/space-app
    NODE_ENV=production node_modules/.bin/webpack -p
fi


echo "Build completed!"
