export default function clientMiddleware(client) {
    return ({dispatch, getState}) => {
        return next => action => {
            
            if (typeof action === 'function') {
                return action(dispatch, getState);
            }
            
            const { promise, atype, rid, payload, success, success_title, failure, failure_title, ...rest } = action; 
            
            if(!promise){
                return next(action);
            }
            
            if(atype){
                const REQUEST = atype;
                const SUCCESS = atype + "_SUCCESS";
                const FAILURE = atype + "_FAILURE";
                
                // set async state
                var reqId = rid;
                if(!reqId && payload && reqId !== false){
                    reqId = JSON.stringify(payload);
                }
                next({type: "ASYNC_REQUEST", request_type: atype, request_id: reqId});
                
                // begin request
                next({...rest, type: REQUEST, payload: payload});
                
                const actionPromise = promise(client, dispatch, getState);
                actionPromise.then(
                    (response) => {
                        var result = response.data;
                        next({...rest, payload: result, type: SUCCESS, request_id: reqId, request_payload: payload});
                        // do success callback
                        success && success(result, dispatch, getState);
                        // set async state
                        next({type: "ASYNC_SUCCESS", request_type: atype, request_id: reqId});
                        // show success message
                        if(success_title){
                            next({
                                type: "SET_TOAST_MSG",
                                payload: {
                                    type: "success",
                                    content: {
                                        title: success_title
                                    }
                                }
                            });
                        }
                    },
                    (error) => {
                        console.error('ASYNC REQUEST ERROR:', error);
                        var err = error.response ? error.response.data : error.message;
                        next({...rest, payload: err, type: FAILURE, request_id: reqId, request_payload: payload});
                        // do failure callback
                        failure && failure(err, dispatch, getState);
                        // set async state
                        next({type: "ASYNC_FAILURE", request_type: atype, request_id: reqId, error: err});
                        // show failure message
                        next({
                            type: "SET_TOAST_MSG",
                            payload: {
                                type: "error",
                                content: {
                                    title: failure_title,
                                    content: JSON.stringify(err)
                                }
                            }
                        });
                    }
                ).catch((error) => {
                    console.error('MIDDLEWARE ERROR:', error);
                    next({...rest, payload: error, type: FAILURE, request_id: reqId, request_payload: payload});
                     // do failure callback
                    failure && failure(error, dispatch, getState);
                    // set async state
                    next({type: "ASYNC_FAILURE", request_type: atype, request_id: reqId, error: error});
                });
                
                return actionPromise;
            }
            
            if(typeof action !== 'function' && action.promise){
                const { promise, type, ...rest } = action;
                if(type){
                    next({type, ...rest});
                }
                return promise(client, dispatch, getState);
            }
            return next(action);
            
        };
    };
}
