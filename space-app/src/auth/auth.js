/*jslint node: true */
'use strict';

var request = require('request');
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth2').Strategy;
var config = require('../config');

class MyStrategy extends OAuth2Strategy {
    // override userProfile method of OAuth2Strategy
    userProfile(accessToken, done) {
        // get user's profile
        request.get(config.authServer.userProfileUrl, {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        }, function(err, res, body) {
            if (err) return done(err);
            var profile = JSON.parse(body);
            if (res.statusCode === 200) {
                return done(null, profile);
            }

            return done(null, false); // get user's profile fail
        });
    }
}

passport.use(new MyStrategy({
        authorizationURL: config.authServer.authorizationUrl,
        tokenURL: config.authServer.tokenUrl,
        clientID: config.authServer.clientId,
        clientSecret: config.authServer.clientSecret,
        callbackURL: config.authServer.callbackUrl,
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function(req, accessToken, refreshToken, profile, cb) {
        // console.log("auth space_id " + req.cookies.space_id);
        // console.log("accessToken:" + accessToken);
        // console.log("refreshToken:" + refreshToken);
        console.log(profile);
        if (!req.cookies.space_id) {
            console.log("no space id");
            return cb(null, false);
        }
        getSpaceToken(req.cookies.space_id, accessToken, function(err, spaceToken) {
            if (err) return cb(null, false);
            var authInfo = {
                accessToken: accessToken,
                refreshToken: refreshToken,
                spaceToken: spaceToken
            };
            cb(null, profile.user, authInfo);
        })

    }
));

function getSpaceToken(spaceID, accessToken, callback) {
    // get space token
    request.get(config.authServer.url + "/as/oauth/space-token?space_id=" + spaceID, {
        headers: {
            Authorization: 'Bearer ' + accessToken
        }
    }, function(err, res, body) {
        if (err) return callback(err);
        // console.log("getSpaceToken - body: " + res.body);

        if (res.statusCode === 200) {
            var token = JSON.parse(body);
            console.log("space token: " + JSON.stringify(token));
            return callback(null, token);
        }

        return callback("permision_denied"); // get token fail
    });
}

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});
