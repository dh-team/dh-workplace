'use strict';

var path = require("path");
var https = require('https');
var http = require('http');
var fs = require('fs');

import Express from 'express';
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var login = require("connect-ensure-login");

import React from 'react';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import axios from 'axios';

import routes from './routes';
import NotFoundPage from './containers/NotFoundPage';
import configureStore from './store/configureStore'

var config = require("./config");
var auth = require('./auth/auth');
var logger = require("./logger");


//Pull in the mongo store if we're configured to use it
//else pull in MemoryStore for the session configuration
var sessionStorage;
if (config.session.type === 'MongoStore') {
    var MongoStore = require('connect-mongo')({
        session: expressSession
    });
    logger.debug('Using MongoDB for the Session');
    sessionStorage = new MongoStore({
        db: config.session.dbName
    });
}
else if (config.session.type === 'MemoryStore') {
    var MemoryStore = expressSession.MemoryStore;
    logger.debug('Using MemoryStore for the Session');
    sessionStorage = new MemoryStore();
}
else {
    //We have no idea here
    throw new Error("Within config/index.js the session.type is unknown: " + config.session.type);
}
// initialize the server and configure support for ejs templates
const app = new Express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(cookieParser());
//Session Configuration
app.use(expressSession({
    saveUninitialized: true,
    resave: true,
    secret: config.session.secret,
    store: sessionStorage,
    key: "spaceapp.sid",
    cookie: {
        maxAge: config.session.maxAge
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(passport.initialize());
app.use(passport.session());


// define the folder that will be used for static assets
app.use("/static", Express.static(path.join(__dirname, 'static')));

app.get("/space/auth", function(req, res, next){
    if(!req.cookies || !req.cookies.space_id){
        return res.redirect("/ws/space/list");
    }
    passport.authenticate('oauth2')(req, res, next);
});
// handle login
app.get("/space/auth/handle", passport.authenticate('oauth2'), function(req, res) {
    // save token to cookie
    logger.debug("oauth2 success, req.authInfo: ", req.authInfo);
    res.cookie(config.cookies.spaceTokenKey, req.authInfo.spaceToken.value, {
        maxAge: config.cookies.maxAge,
        httpOnly: false // cookie can be accessed in javascript
    });
    res.redirect(req.session.returnTo || "/ws");
});
app.get('/space/logout', function(req, res) {
    // logout
    req.logout();
    res.redirect("/ws/logout");
});

app.get('/', function(req, res) {
    res.redirect('/space/');
});

// space/open?space_id=SPACE_ID
app.get('/space/open', function(req, res) {
    // logout to
    logout(req, res);
    res.cookie('space_id', req.query.space_id, {
        maxAge: 900000,
        httpOnly: false // cookie can be accessed in javascript
    });
    res.redirect('/space/');
});

function logout(req, res){
    // passport log out
    req.logout();

    // clear token in cookie
    res.clearCookie("space_token");
}


// universal routing and rendering
var handleSite = [
    login.ensureLoggedIn({
        redirectTo: "/space/auth"
    }),
    (req, res) => {
        var template = "index";
        // console.log("Request cookies: " + JSON.stringify(req.cookies));
        var client = axios.create({
            baseURL: config.proxyServer.url,
            headers: {'Authorization': "Bearer " + req.cookies.space_token}
        });
        const store = configureStore({}, client);
        match({
                routes,
                location: req.url
            },
            (err, redirectLocation, renderProps) => {
                // in case of error display the error message
                if (err) {
                    return res.status(500).send(err.message);
                }

                // in case of redirect propagate the redirect to the browser
                if (redirectLocation) {
                    return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
                }

                // generate the React markup for the current route
                let markup;

                if (renderProps) {
                    // if the current route matched we have renderProps
                    // console.log("begin loadOnServer");
                    // async load and render
                    loadOnServer({ ...renderProps, store}).then(() => {
                        // console.log("callback loadOnServer");
                        // console.log(store.getState());
                        try{
                            markup = renderToString(
                                <Provider store={store} key="provider">
                                    <ReduxAsyncConnect {...renderProps} />
                                </Provider>);
                            // console.log("render markup ok");
                            // render the index template with the embedded React markup
                            return res.render(template, {
                                markup: markup,
                                initState: JSON.stringify(store.getState()).replace(/</g, '\\x3c')
                            });
                        }catch(e){
                            logger.error("renderToString error: ", e);    
                            return res.redirect(302, "/space/logout");
                        }
                        
                    }, (error) => {
                        console.log("loadOnServer error: " + JSON.stringify());
                        return null;
                    });
                }
                else {
                    // otherwise we can render a 404 page
                    markup = renderToString(<NotFoundPage/>);
                    res.status(404);
                    // render the index template with the embedded React markup
                    return res.render(template, {
                        markup,
                        initState: JSON.stringify(store.getState()).replace(/</g, '\\x3c')
                    });
                }
            }
        );
    }
];

app.get('/space', handleSite);
app.get('/space/*', handleSite);

// Create HTTP | HTTPS server
if (config.spaceWebServer.type === 'http') {
    http.createServer(app).listen(config.spaceWebServer.port, "0.0.0.0");
}
else {
    var options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.spaceWebServer.port, "0.0.0.0");
}
console.log("Space Web Server started on port " + config.spaceWebServer.port);
