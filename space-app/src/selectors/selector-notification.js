import { createSelector } from 'reselect';

export const getNotifications = (state) => state.notification.ids.map((id) => state.notification.store[id]);
export const getNotificationCount = (state) => state.notification.count;
export const getFetchNotificationsStatus = (state) => state.async_status["FETCH_NOTIFICATIONS"];


