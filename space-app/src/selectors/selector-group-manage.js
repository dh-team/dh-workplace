import { createSelector } from 'reselect';

export const getGroups = (state) => state.group_manage.ids.map((id) => state.group_manage.store[id]);

export const getGroup = (state, groupId) => {    
    if(!state.group_manage.store[groupId]) return null;
    var group = {...state.group_manage.store[groupId]};
    if(group.users){
        group.users = group.users.map((id) => state.space_user.store[id]);
    }else{
        group.users = [];
    } 

    return group;
}

export const getActiveGroupId = (state) => state.group_manage.active_group;

export const getActiveGroup = (state) => {
    const id = state.group_manage.active_group;
    if(!id) return null;
    return getGroup(state, id);
}


// export const getFetchGroupsStatus = (state) => state.async_status["FETCH_GROUPS_OF_USER"];

// export const getFetchGroupStatus = (state) => state.async_status["FETCH_GROUP_INFO"];

