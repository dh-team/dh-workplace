import {
    createSelector
}
from 'reselect';

export const getPosts = (state) => state.wiki_post.ids.map((id) => state.wiki_post.store[id]);

export const getPost = (state, postId) => state.wiki_post.store[postId];

export const getFetchPostsStatus = (state) => state.async_status["FETCH_WIKI_POSTS"];

export const getFetchPostStatus = (state) => state.async_status["FETCH_WIKI_POST"];
export const getAddPostStatus = (state) => state.async_status["ADD_WIKI_POST"];
export const getEditPostStatus = (state) => state.async_status["EDIT_WIKI_POST"];
export const getDeletePostStatus = (state) => state.async_status["DELETE_WIKI_POST"];

// export const getPostActionStatus = (state) => state.wiki_post.post_action_status;

export const getActivePostId = (state) => state.wiki_post.active_post;

export const getActivePost = (state) => {
    var postId = state.wiki_post.active_post;
    if (!postId) return null;
    return state.wiki_post.store[postId];
};
