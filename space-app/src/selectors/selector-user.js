import { createSelector } from 'reselect';

export const getCurrentUser = (state) => state.user.user_profile;
export const getFetchUserProfileStatus = (state) => state.async_status["FETCH_USER_PROFILE"];

export const getUsersInSpace = (state) => state.space_user.ids.map((id) => state.space_user.store[id]);
export const getUsersInSpaceStatus = (state) => state.async_status["FETCH_SPACE_USERS"];

export const getUser = (state, userId) => state.space_user.store[userId];