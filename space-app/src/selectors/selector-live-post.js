import {
    createSelector
}
from 'reselect';

export const getPosts = (state) => state.live_post.ids.map((id) => state.live_post.store[id]);
export const getPost = (state, postId) => state.live_post.store[postId];
export const getFetchPostsStatus = (state) => state.async_status["FETCH_LIVE_POSTS"];
export const getAddPostStatus = (state) => state.async_status["ADD_LIVE_POST"];
export const getEditPostStatus = (state) => state.async_status["EDIT_LIVE_POST"];
export const getDeletePostStatus = (state) => state.async_status["DELETE_LIVE_POST"];
