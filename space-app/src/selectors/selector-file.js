import { createSelector } from 'reselect';

export const getStartDir = (state) => state.file.start_dir;

export const getNode = (state, nodeId) => {
    var node = state.file.store[nodeId];
    if(!node || !node.content) return node;
    return {
        ...node,
        content: node.content.map((id) => state.file.store[id] && state.file.store[id].meta)
    }
}

export const getUploadStatus = (state) => state.file.upload;




