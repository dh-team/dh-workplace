import { createSelector } from 'reselect';

export const getTags = (state, subject) => {
    const bySubject = state.tag.by_subject[subject];
    if (!bySubject) return [];
    return bySubject.map((key) => state.tag.store[subject + "_" + key]);
}
export const getFetchTagsStatus = (state, subject) => {
    return state.async_status["FETCH_TAGS"] && state.async_status["FETCH_TAGS"].requests && state.async_status["FETCH_TAGS"].requests[subject];
};

