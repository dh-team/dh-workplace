import {
    createSelector
}
from 'reselect';

export const getAllCategories = (state) => state.wiki_category.ids.map((id) => state.wiki_category.store[id]);

export const getCategory = (state, categoryId) => state.wiki_category.store[categoryId];

export const getFetchCategoriesStatus = (state) => state.async_status["FETCH_WIKI_CATEGORIES"];

export const getFetchCategoryStatus = (state) => state.async_status["FETCH_WIKI_CATEGORY"];

export const getActiveCategoryId = (state) => state.wiki_category.active_category;

export const getActiveCategory = (state) => {
    var catId = state.wiki_category.active_category;
    if (!catId) return null;
    if (catId === "root") return "root";
    return state.wiki_category.store[catId];
};



export const getSortedCategories = createSelector(
    [getAllCategories],
    (categories) => {
        var levelPattern = "---";
        var cats = categories.map(function(item) {
            var arr = Array.from(item.all_parents).reverse();
            arr.push(item._id);
            item.sort_key = arr.join("_");
            item.display_field = levelPattern.repeat(item.all_parents.length) + " " + item.name;
            return item;
        });
        cats = cats.sort(function(item1, item2) {
            return item1.sort_key.localeCompare(item2.sort_key);
        });
        return cats;
    }
)
