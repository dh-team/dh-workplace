import { createSelector } from 'reselect';

export const getSpaceInfo = (state) => state.space.space_info;
export const getFetchSpaceInfoStatus = (state) => state.async_status["FETCH_SPACE_INFO"];


