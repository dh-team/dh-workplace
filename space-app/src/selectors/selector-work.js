import { createSelector } from 'reselect';
import lo from 'lodash';

export const getAllWorks = (state) => state.work.ids.map((id) => state.work.store[id]);

export const getWork = (state, id) => state.work.store[id];

export const getWorks = (state, ids) => {
    return ids.map((item) => state.work.store[item]);
};

export const getSubworks = (state, id) => {
    const work = state.work.store[id];
    if (!work || !work.childs || work.childs.length === 0) return [];
    return work.childs.map((item) => state.work.store[item]);
};

export const getLastAddedWork = (state) => {
    if (!state.work.last_added) return null;
    return state.work.store[state.work.last_added];
};

export const getWorkHierarchy = (state, id) => {
    var parents = [];
    var work = state.work.store[id];
    while (work) {
        parents.unshift(work);
        work = work.parent && state.work.store[work.parent];
    }
    return parents;
};

export const getAllPerformWorks = (state) => state.work.perform.map((id) => state.work.store[id]);
export const getAllMonitorWorks = (state) => {
    const ids = lo.difference(state.work.monitor, state.work.perform);
    return ids.map(id => state.work.store[id]);
};

export const getAllFollowedWorks = (state) => {
    const ids = lo.difference(state.work.followed, state.work.perform, state.work.monitor);
    return ids.map(id => state.work.store[id]);
};

export const getPerformWorks = createSelector(
    [getAllPerformWorks],
    (works) => getLeafs(works)
);

export const getMonitorWorks = createSelector(
    [getAllMonitorWorks],
    (works) => getLeafs(works)
);

export const getFollowedWorks = createSelector(
    [getAllFollowedWorks],
    (works) => getLeafs(works)
);

export const getTodoWorks = createSelector(
    [getPerformWorks],
    (works) => {
        return works.filter((item) => item.doing && item.doing.status === "todo");
    }
);

export const getDoingWorks = createSelector(
    [getPerformWorks],
    (works) => {
        return works.filter((item) => item.doing && item.doing.status === "doing");
    }
);

export const getDoneWorks = createSelector(
    [getPerformWorks],
    (works) => {
        return works.filter((item) => item.doing && item.doing.status === "done");
    }
);
function mutableFromMap(id, map) {
    if (!map[id])
        map[id] = {
            id: id,
            childs: []
        };
    return map[id];
}
function getLeafs(works) {
    let map = {};
    works.map((item) => {
        let current = mutableFromMap(item._id, map);
        current.item = item;
        if (item.parent) {
            let parent = mutableFromMap(item.parent, map);
            parent.childs.push(item._id);
        }
    });
    return works.filter((item) => {
        return map[item._id].childs.length === 0;
    });
}

export const getFetchWorksStatus = (state) => state.async_status["FETCH_WORKS"];
export const getFetchPerformWorksStatus = (state) => state.async_status["FETCH_PERFORM_WORKS"];
export const getFetchMonitorWorksStatus = (state) => state.async_status["FETCH_MONITOR_WORKS"];
export const getEditWorkTaskStatus = (state) => state.async_status["EDIT_WORK_TASK"];
export const getDeleteWorkStatus = (state) => state.async_status["DELETE_WORK"];
export const getAddWorkStatus = (state) => state.async_status["ADD_WORK"];