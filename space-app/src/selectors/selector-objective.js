import { createSelector } from 'reselect';

export const getAllObjectives = (state) => state.objective.ids.map((id) => state.objective.store[id]);
export const getObjective = (state, id) => state.objective.store[id];

export const getFetchObjectivesStatus = (state) => state.async_status["FETCH_OBJECTIVES"];


export const getTimelineObjectives = createSelector(
    [getAllObjectives],
    (objectives) => {
        var pointsList = [];
        var pointsMap = {};
        objectives.map((item) => {
            var key = getTimeKey(item);
            if (!pointsMap[key]) {
                pointsMap[key] = {
                    key: key,
                    type: item.time.range,
                    text: getTimeText(item)
                }
                pointsList.push(key);
            }
            pointsMap[item._id] = {
                type: "objective",
                objective: item
            };
            pointsList.push(item._id);
        });

        return pointsList.map((item) => pointsMap[item]);
    }
)

function getTimeText(item) {
    var text = "";
    switch (item.time.range) {
        case "annual": {
            text += item.time.year;
            break;
        }
        case "quarterly": {
            text += "Quý " + item.time.quarter;
            break;
        }
        case "manual": {
            var date = new Date(item.time.to);
            text += date.getDate() + "/" + (date.getMonth() + 1);
            break;
        }
    }
    return text;
}

function getTimeKey(item) {
    var key = item.time.range + "_";
    switch (item.time.range) {
        case "annual": {
            key += item.time.year;
            break;
        }
        case "quarterly": {
            key += item.time.year + "_" + item.time.quarter;
            break;
        }
        case "manual": {
            key += item.time.to;
            break;
        }
    }
    return key;
}

