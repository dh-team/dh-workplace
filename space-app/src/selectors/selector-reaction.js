import { createSelector } from 'reselect';

export const getReactionStream = (state, id) => state.reaction.store[id];