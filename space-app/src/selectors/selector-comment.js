import { createSelector } from 'reselect';

export const getCommentStream = (state, id) => state.comment.store[id];