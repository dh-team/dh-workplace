import { createSelector } from 'reselect';

export const getAllGroups = (state) => state.group_display.ids.map((id) => state.group_display.store[id]);

export const getGroup = (state, groupId) => state.group_display.store[groupId];

export const getFetchGroupsStatus = (state) => state.async_status["FETCH_GROUPS_OF_USER"];

export const getFetchGroupStatus = (state) => state.async_status["FETCH_GROUP_INFO"];

