import { createSelector } from 'reselect';

export const getAsyncStatus = (state, type, requestPayload) => {
    var status = state.async_status[type];
    if (!status) return null;

    if (requestPayload) {
        if (!status.requests) return null;
        var reqId = JSON(requestPayload);
        return status.requests[reqId];
    }

    if (!status.requests) return status;
    var combineStatus = {
        loading: false
    };
    Object.keys(status.requests).map((key) => {
        if (status.requests[key].loading) combineStatus.loading = true;
    });
    return combineStatus;
};

