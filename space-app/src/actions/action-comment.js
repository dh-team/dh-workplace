export const FETCH_COMMENT_STREAM = 'FETCH_COMMENT_STREAM';
export const FETCH_COMMENT_STREAM_SUCCESS = 'FETCH_COMMENT_STREAM_SUCCESS';
export const FETCH_COMMENT_STREAM_FAILURE = 'FETCH_COMMENT_STREAM_FAILURE';

export const ADD_COMMENT = 'ADD_COMMENT';
export const ADD_COMMENT_SUCCESS = 'ADD_COMMENT_SUCCESS';
export const ADD_COMMENT_FAILURE = 'ADD_COMMENT_FAILURE';

export const EDIT_COMMENT = 'EDIT_COMMENT';
export const EDIT_COMMENT_SUCCESS = 'EDIT_COMMENT_SUCCESS';
export const EDIT_COMMENT_FAILURE = 'EDIT_COMMENT_FAILURE';

export const DELETE_COMMENT = 'DELETE_COMMENT';
export const DELETE_COMMENT_SUCCESS = 'DELETE_COMMENT_SUCCESS';
export const DELETE_COMMENT_FAILURE = 'DELETE_COMMENT_FAILURE';

export const COMMENTS_URL = '/api/space/comment-streams';

///////////////
export function fetchCommentStream(streamId) {
    return {
        atype: FETCH_COMMENT_STREAM,
        payload: streamId,
        promise: function(client, dispatch, getState) {
            return client.get(COMMENTS_URL + "/" + streamId);
        }
    };
}

///////////////
// params: {stream_id, content, reply_to}
export function addComment(params) {
    return {
        atype: ADD_COMMENT,
        payload: params.stream_id,
        promise: function(client, dispatch, getState) {
            return client.post(COMMENTS_URL + "/" + params.stream_id + "/comments", params);
        }
    };
}

///////////////
// params: {stream_id, comment_id, content}
export function editComment(params) {
    return {
        atype: EDIT_COMMENT,
        success_title: "Cập nhật bình luận thành công",
        promise: function(client, dispatch, getState) {
            return client.put(COMMENTS_URL + "/" + params.stream_id + "/comments/" + params.comment_id, params);
        }
    };
}

///////////////
// params: {stream_id, comment_id}
export function deleteComment(params) {
    return {
        atype: DELETE_COMMENT,
        rid: params.stream_id,
        payload: params,
        success_title: "Xóa bình luận thành công",
        promise: function(client, dispatch, getState) {
            return client.delete(COMMENTS_URL + "/" + params.stream_id + "/comments/" + params.comment_id);
        }
    };
}
