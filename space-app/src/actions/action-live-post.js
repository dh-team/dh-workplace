import {setMsg} from './action-msg';

// List post
export const FETCH_LIVE_POSTS = 'FETCH_LIVE_POSTS';
export const FETCH_LIVE_POSTS_SUCCESS = 'FETCH_LIVE_POSTS_SUCCESS';
export const FETCH_LIVE_POSTS_FAILURE = 'FETCH_LIVE_POSTS_FAILURE';
export const FETCH_LIVE_POSTS_RESET = 'FETCH_LIVE_POSTS_RESET';

// Get post detail
export const FETCH_LIVE_POST = 'FETCH_LIVE_POST';
export const FETCH_LIVE_POST_SUCCESS = 'FETCH_LIVE_POST_SUCCESS';
export const FETCH_LIVE_POST_FAILURE = 'FETCH_LIVE_POST_FAILURE';
export const FETCH_LIVE_POST_RESET = 'FETCH_LIVE_POST_RESET';

export const SET_ACTIVE_LIVE_POST = 'SET_ACTIVE_LIVE_POST';

export const ADD_LIVE_POST = 'ADD_LIVE_POST';
export const ADD_LIVE_POST_SUCCESS = 'ADD_LIVE_POST_SUCCESS';
export const ADD_LIVE_POST_FAILURE = 'ADD_LIVE_POST_FAILURE';
export const ADD_LIVE_POST_RESET = 'ADD_LIVE_POST_RESET';

export const EDIT_LIVE_POST = 'EDIT_LIVE_POST';
export const EDIT_LIVE_POST_SUCCESS = 'EDIT_LIVE_POST_SUCCESS';
export const EDIT_LIVE_POST_FAILURE = 'EDIT_LIVE_POST_FAILURE';
export const EDIT_LIVE_POST_RESET = 'EDIT_LIVE_POST_RESET';

export const DELETE_LIVE_POST = 'DELETE_LIVE_POST';
export const DELETE_LIVE_POST_SUCCESS = 'DELETE_LIVE_POST_SUCCESS';
export const DELETE_LIVE_POST_FAILURE = 'DELETE_LIVE_POST_FAILURE';

///////////////
export function fetchLivePosts(groupId) {
    return {
        atype: FETCH_LIVE_POSTS,
        promise: function(client, dispatch, getState) {
            var state = getState();
            var params = {
                cursor: state.live_post.fetch.cursor,
                limit: state.live_post.fetch.limit,
            };
            params.type = "live";
            if(groupId) params.group_id = groupId;
            return client.post("/api/space/post/list", params);
        }
    };
}

export function fetchLivePostsReset() {
    return {
        type: FETCH_LIVE_POSTS_RESET
    };
}
///////////////
export function fetchLivePost(id) {
    return {
        atype: FETCH_LIVE_POST,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/post/get?post_id=" + id);
        }
    };
}

///////////////
export function setActiveLivePost(id) {
    return {
        type: SET_ACTIVE_LIVE_POST,
        payload: id
    };
}
///////////////
export function addLivePost(data) {
    return {
        atype: ADD_LIVE_POST,
        success_title: "Đăng chia sẻ thành công",
        promise: function(client, dispatch, getState) {
            data.type = "live";
            return client.post("/api/space/post/add", data);
        }
    };
}

export function addLivePostReset() {
    return {
        type: ADD_LIVE_POST_RESET
    };
}
///////////////
export function editLivePost(data) {
    return {
        atype: EDIT_LIVE_POST,
        success_title: "Cập nhật chia sẻ thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/edit", data);
        }
    };
}

export function editLivePostReset() {
    return {
        type: EDIT_LIVE_POST_RESET
    };
}
///////////////
export function deleteLivePost(id) {
    return {
        atype: DELETE_LIVE_POST,
        success_title: "Xóa thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/delete", {id});
        }
    };
}
