// List post
export const FETCH_WIKI_POSTS = 'FETCH_WIKI_POSTS';
export const FETCH_WIKI_POSTS_SUCCESS = 'FETCH_WIKI_POSTS_SUCCESS';
export const FETCH_WIKI_POSTS_FAILURE = 'FETCH_WIKI_POSTS_FAILURE';

// Get post detail
export const SET_ACTIVE_WIKI_POST = 'SET_ACTIVE_WIKI_POST';

export const FETCH_WIKI_POST = 'FETCH_WIKI_POST';
export const FETCH_WIKI_POST_SUCCESS = 'FETCH_WIKI_POST_SUCCESS';
export const FETCH_WIKI_POST_FAILURE = 'FETCH_WIKI_POST_FAILURE';

export const ADD_WIKI_POST = 'ADD_WIKI_POST';
export const ADD_WIKI_POST_SUCCESS = 'ADD_WIKI_POST_SUCCESS';
export const ADD_WIKI_POST_FAILURE = 'ADD_WIKI_POST_FAILURE';

export const EDIT_WIKI_POST = 'EDIT_WIKI_POST';
export const EDIT_WIKI_POST_SUCCESS = 'EDIT_WIKI_POST_SUCCESS';
export const EDIT_WIKI_POST_FAILURE = 'EDIT_WIKI_POST_FAILURE';

export const DELETE_WIKI_POST = 'DELETE_WIKI_POST';
export const DELETE_WIKI_POST_SUCCESS = 'DELETE_WIKI_POST_SUCCESS';
export const DELETE_WIKI_POST_FAILURE = 'DELETE_WIKI_POST_FAILURE';

///////////////
export function fetchPosts(catId) {
    return {
        atype: FETCH_WIKI_POSTS,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/post/list?type=archive&category_id=" + catId);
        }
    };
}

///////////////
export function setActivePost(postId) {
    return {
        type: SET_ACTIVE_WIKI_POST,
        payload: postId
    };
}

///////////////
export function fetchPost(postId) {
    return {
        atype: FETCH_WIKI_POST,       
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/post/get?post_id=" + postId);
        }
    };
}

///////////////
export function addPost(data) {
    return {
        atype: ADD_WIKI_POST,
        promise: function(client, dispatch, getState) {
            data.type = "archive";
            return client.post("/api/space/post/add", data);
        },
        success_title: "Thêm bài viết thành công"
    };
}

///////////////
export function editPost(data) {
    return {
        atype: EDIT_WIKI_POST,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/edit", data);
        },
        success_title: "Cập nhật bài viết thành công"
    };
}

///////////////
export function deletePost(postId) {
    return {
        atype: DELETE_WIKI_POST,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/delete", {id: postId});
        },
        success_title: "Xóa bài viết thành công"
    };
}
