import {setMsg} from './action-msg';

export const FETCH_SPACE_INFO = 'FETCH_SPACE_INFO';
export const FETCH_SPACE_INFO_SUCCESS = 'FETCH_SPACE_INFO_SUCCESS';
export const FETCH_SPACE_INFO_FAILURE = 'FETCH_SPACE_INFO_FAILURE';

export const EDIT_SPACE_INFO = 'EDIT_SPACE_INFO';
export const EDIT_SPACE_INFO_SUCCESS = 'EDIT_SPACE_INFO_SUCCESS';
export const EDIT_SPACE_INFO_FAILURE = 'EDIT_SPACE_INFO_FAILURE';

export const FETCH_USER_PROFILE = 'FETCH_USER_PROFILE';
export const FETCH_USER_PROFILE_SUCCESS = 'FETCH_USER_PROFILE_SUCCESS';
export const FETCH_USER_PROFILE_FAILURE = 'FETCH_USER_PROFILE_FAILURE';

///////////////
export function fetchSpaceInfo() {
    return {
        atype: FETCH_SPACE_INFO,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/space-info");
        }
    };
}

///////////////
export function editSpaceInfo(data) {
    return {
        atype: EDIT_SPACE_INFO,
        success_title: "Cập nhật thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/update-space-info", data);
        }
    };
}

///////////////
export function fetchUserProfile() {
    return {
        atype: FETCH_USER_PROFILE,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/user-profile");
        }
    };
}

