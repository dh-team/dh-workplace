import { setMsg } from './action-msg';

export const FETCH_NOTIFICATIONS = 'FETCH_NOTIFICATIONS';
export const FETCH_NOTIFICATIONS_SUCCESS = 'FETCH_NOTIFICATIONS_SUCCESS';
export const FETCH_NOTIFICATIONS_FAILURE = 'FETCH_NOTIFICATIONS_FAILURE';
export const FETCH_NOTIFICATIONS_RESET = 'FETCH_NOTIFICATIONS_RESET';

export const MARK_NOTIFICATION_READ = 'MARK_NOTIFICATION_READ';
export const MARK_NOTIFICATION_READ_SUCCESS = 'MARK_NOTIFICATION_READ_SUCCESS';
export const MARK_NOTIFICATION_READ_FAILURE = 'MARK_NOTIFICATION_READ_FAILURE';

export const COUNT_NOTIFICATION = 'COUNT_NOTIFICATION';
export const COUNT_NOTIFICATION_SUCCESS = 'COUNT_NOTIFICATION_SUCCESS';
export const COUNT_NOTIFICATION_FAILURE = 'COUNT_NOTIFICATION_FAILURE';

export const NOTIFICATIONS_URL = "/api/space/notifications";

///////////////
export function fetchNotifications() {
    return {
        atype: FETCH_NOTIFICATIONS,
        promise: function (client, dispatch, getState) {
            const state = getState();
            const params = {
                cursor: state.notification.fetch.cursor,
                limit: state.notification.fetch.limit,
            }
            return client.get(NOTIFICATIONS_URL, { params: params });
        }
    };
}

export function fetchNotificationsReset() {
    return {
        type: FETCH_NOTIFICATIONS_RESET
    }
}
///////////////
export function markNotificationRead(id) {
    return {
        atype: MARK_NOTIFICATION_READ,
        rid: false,
        payload: id,
        promise: function (client, dispatch, getState) {
            return client.post(NOTIFICATIONS_URL + "/mark-read", { id });
        }
    };
}


///////////////
export function countNotification() {
    return {
        atype: COUNT_NOTIFICATION,
        promise: function (client, dispatch, getState) {
            return client.get(NOTIFICATIONS_URL + "/count-unread");
        }
    };
}
