import {setMsg} from './action-msg';

// List users of space
export const FETCH_SPACE_USERS = 'FETCH_SPACE_USERS';
export const FETCH_SPACE_USERS_SUCCESS = 'FETCH_SPACE_USERS_SUCCESS';
export const FETCH_SPACE_USERS_FAILURE = 'FETCH_SPACE_USERS_FAILURE';

// Add user to space
export const ADD_SPACE_USER = 'ADD_SPACE_USER';
export const ADD_SPACE_USER_SUCCESS = 'ADD_SPACE_USER_SUCCESS';
export const ADD_SPACE_USER_FAILURE = 'ADD_SPACE_USER_FAILURE';

// Add user to space
export const DELETE_SPACE_USER = 'DELETE_SPACE_USER';
export const DELETE_SPACE_USER_SUCCESS = 'DELETE_SPACE_USER_SUCCESS';
export const DELETE_SPACE_USER_FAILURE = 'DELETE_SPACE_USER_FAILURE';

///////////////
export function fetchSpaceUsers() {
    return {
        atype: FETCH_SPACE_USERS,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/user/list");
        }
    };
}

///////////////
export function addSpaceUser(props) {
    return {
        atype: ADD_SPACE_USER,
        success_title: "Thêm người dùng thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/user/add", props);            
        }
    };
}

///////////////
export function deleteSpaceUser(props) {
    return {
        atype: DELETE_SPACE_USER,
        success_title: "Xóa người dùng thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/user/delete", props);
        }
    };
}

///////////////