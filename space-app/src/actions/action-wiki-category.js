import {setMsg} from './action-msg';

// List users of space
export const FETCH_WIKI_CATEGORIES = 'FETCH_WIKI_CATEGORIES';
export const FETCH_WIKI_CATEGORIES_SUCCESS = 'FETCH_WIKI_CATEGORIES_SUCCESS';
export const FETCH_WIKI_CATEGORIES_FAILURE = 'FETCH_WIKI_CATEGORIES_FAILURE';

// Get category info
export const FETCH_WIKI_CATEGORY = 'FETCH_WIKI_CATEGORY';
export const FETCH_WIKI_CATEGORY_SUCCESS = 'FETCH_WIKI_CATEGORY_SUCCESS';
export const FETCH_WIKI_CATEGORY_FAILURE = 'FETCH_WIKI_CATEGORY_FAILURE';

export const ADD_WIKI_CATEGORY = 'ADD_WIKI_CATEGORY';
export const EDIT_WIKI_CATEGORY = 'EDIT_WIKI_CATEGORY';
export const DELETE_CATEGORY = 'DELETE_CATEGORY';
export const UPDATE_CATEGORY_PARENT = 'UPDATE_CATEGORY_PARENT';
export const SET_ACTIVE_CATEGORY = 'SET_ACTIVE_CATEGORY';

///////////////
export function fetchPostCategories() {
    return {
        atype: FETCH_WIKI_CATEGORIES,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/post/category/list");
        }
    };
}

///////////////
export function fetchPostCategory(id) {
    return {
        atype: FETCH_WIKI_CATEGORY,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/post/category/info?category_id=" + id);
        }
    };
}

///////////////
export function addPostCategory(data) {
    return {
        atype: ADD_WIKI_CATEGORY,
        success_title: "Thêm chủ đề thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/category/add", data);
        },
        success: function(result, dispatch, getState){
            dispatch(fetchPostCategories());
        }
    };
}
///////////////
export function editPostCategory(data) {
    return {
        atype: EDIT_WIKI_CATEGORY,
        success_title: "Cập nhật chủ đề thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/category/edit", data);
        },
        success: function(result, dispatch, getState){
            dispatch(fetchPostCategories());
        }
    };
}
///////////////
export function deleteCategory(groupId) {
    return {
        atype: DELETE_CATEGORY,
        success_title: "Xóa chủ đề thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/category/delete", {id:groupId});
        },
        success: function(result, dispatch, getState){
            dispatch(fetchPostCategories());
        }
    };
}
///////////////
export function updateCategoryParent(data) {
    return {
        atype: UPDATE_CATEGORY_PARENT,
        success_title: "Cập nhật thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/post/category/update-parent", data);
        },
        success: function(result, dispatch, getState){
            dispatch(fetchPostCategories());
        }
    };
}
///////////////
export function setActiveCategory(categoryId) {
    return {
        type: SET_ACTIVE_CATEGORY,
        payload: categoryId
    };
}