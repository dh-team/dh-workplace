export const FETCH_WORKS = 'FETCH_WORKS';
export const FETCH_WORKS_SUCCESS = 'FETCH_WORKS_SUCCESS';
export const FETCH_WORKS_FAILURE = 'FETCH_WORKS_FAILURE';

export const FETCH_SUB_WORKS = 'FETCH_SUB_WORKS';
export const FETCH_SUB_WORKS_SUCCESS = 'FETCH_SUB_WORKS_SUCCESS';
export const FETCH_SUB_WORKS_FAILURE = 'FETCH_SUB_WORKS_FAILURE';

export const FETCH_PARENT_WORKS = 'FETCH_PARENT_WORKS';
export const FETCH_PARENT_WORKS_SUCCESS = 'FETCH_PARENT_WORKS_SUCCESS';
export const FETCH_PARENT_WORKS_FAILURE = 'FETCH_PARENT_WORKS_FAILURE';

export const FETCH_PERFORM_WORKS = 'FETCH_PERFORM_WORKS';
export const FETCH_PERFORM_WORKS_SUCCESS = 'FETCH_PERFORM_WORKS_SUCCESS';
export const FETCH_PERFORM_WORKS_FAILURE = 'FETCH_PERFORM_WORKS_FAILURE';

export const FETCH_FOLLOWED_WORKS = 'FETCH_FOLLOWED_WORKS';
export const FETCH_FOLLOWED_WORKS_SUCCESS = 'FETCH_FOLLOWED_WORKS_SUCCESS';
export const FETCH_FOLLOWED_WORKS_FAILURE = 'FETCH_FOLLOWED_WORKS_FAILURE';

export const FETCH_MONITOR_WORKS = 'FETCH_MONITOR_WORKS';
export const FETCH_MONITOR_WORKS_SUCCESS = 'FETCH_MONITOR_WORKS_SUCCESS';
export const FETCH_MONITOR_WORKS_FAILURE = 'FETCH_MONITOR_WORKS_FAILURE';

export const FETCH_WORK_DETAIL = 'FETCH_WORK_DETAIL';
export const FETCH_WORK_DETAIL_SUCCESS = 'FETCH_WORK_DETAIL_SUCCESS';
export const FETCH_WORK_DETAIL_FAILURE = 'FETCH_WORK_DETAIL_FAILURE';

export const ADD_WORK = 'ADD_WORK';
export const ADD_WORK_SUCCESS = 'ADD_WORK_SUCCESS';
export const ADD_WORK_FAILURE = 'ADD_WORK_FAILURE';

export const EDIT_WORK = 'EDIT_WORK';
export const EDIT_WORK_SUCCESS = 'EDIT_WORK_SUCCESS';
export const EDIT_WORK_FAILURE = 'EDIT_WORK_FAILURE';

export const DELETE_WORK = 'DELETE_WORK';
export const DELETE_WORK_SUCCESS = 'DELETE_WORK_SUCCESS';
export const DELETE_WORK_FAILURE = 'DELETE_WORK_FAILURE';

export const ADD_WORK_TASK = 'ADD_WORK_TASK';
export const ADD_WORK_TASK_SUCCESS = 'ADD_WORK_TASK_SUCCESS';
export const ADD_WORK_TASK_FAILURE = 'ADD_WORK_TASK_FAILURE';

export const EDIT_WORK_TASK = 'EDIT_WORK_TASK';
export const EDIT_WORK_TASK_SUCCESS = 'EDIT_WORK_TASK_SUCCESS';
export const EDIT_WORK_TASK_FAILURE = 'EDIT_WORK_TASK_FAILURE';

export const DELETE_WORK_TASK = 'DELETE_WORK_TASK';
export const DELETE_WORK_TASK_SUCCESS = 'DELETE_WORK_TASK_SUCCESS';
export const DELETE_WORK_TASK_FAILURE = 'DELETE_WORK_TASK_FAILURE';


export const WORKS_URL = "/api/space/works";
///////////////
export function fetchWorks(params) {
    return {
        atype: FETCH_WORKS,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

export function fetchSubWorks(parentId) {
    var params = {parent_id: parentId};
    return {
        atype: FETCH_SUB_WORKS,
        rid: false,
        payload: params,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

export function fetchParentWorks(descendantId) {
    var params = {descendant_id: descendantId};
    return {
        atype: FETCH_PARENT_WORKS,
        rid: false,
        payload: params,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

export function fetchPerformWorks() {
    var params = {performer: "me", status: "unfinished"};
    return {
        atype: FETCH_PERFORM_WORKS,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

export function fetchMonitorWorks() {
    var params = {monitor: "me", status: "unfinished"};
    return {
        atype: FETCH_MONITOR_WORKS,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

export function fetchFollowedWorks() {
    var params = {followed: 1, status: "unfinished"};
    return {
        atype: FETCH_FOLLOWED_WORKS,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL, {params: params});
        }
    };
}

///////////////
export function fetchWork(id) {
    return {
        atype: FETCH_WORK_DETAIL,
        promise: function(client, dispatch, getState) {
            return client.get(WORKS_URL + "/" + id);
        }
    };
}

///////////////
export function addWork(data) {
    return {
        atype: ADD_WORK,
        promise: function(client, dispatch, getState) {
            return client.post(WORKS_URL, data);
        },
        success_title: "Tạo công việc thành công"
    };
}

///////////////
export function editWork(data) {
    return {
        atype: EDIT_WORK,
        promise: function(client, dispatch, getState) {
            return client.put(WORKS_URL + "/" + data.id, data);
        },
        success_title: "Cập nhật công việc thành công"
    };
}

///////////////
export function deleteWork(id) {
    return {
        atype: DELETE_WORK,
        promise: function(client, dispatch, getState) {
            return client.delete(WORKS_URL + "/" + id);
        },
        success_title: "Xóa công việc thành công"
    };
}


///////////////
export function addWorkTask(params) {
    const {work_id, ...rest} = params;
    return {
        atype: ADD_WORK_TASK,
        rid: false,
        payload: params,
        promise: function(client, dispatch, getState) {
            return client.post(WORKS_URL + "/" + work_id + "/tasks", rest);
        },
        // success_title: "Tạo ghi chú công việc thành công"
    };
}

///////////////
export function editWorkTask(params) {
    const {work_id, ...rest} = params;
    return {
        atype: EDIT_WORK_TASK,
        rid: false,
        payload: params,
        promise: function(client, dispatch, getState) {
            return client.put(WORKS_URL + "/" + work_id + "/tasks/" + rest.id, rest);
        },
        // success_title: "Cập nhật ghi chú công việc thành công"
    };
}

///////////////
export function deleteWorkTask(params) {
    const {work_id, task_id} = params;
    return {
        atype: DELETE_WORK_TASK,
        rid: false,
        payload: params,
        promise: function(client, dispatch, getState) {
            return client.delete(WORKS_URL + "/" + work_id + "/tasks/" + task_id);
        },
        // success_title: "Xóa ghi chú công việc thành công"
    };
}
