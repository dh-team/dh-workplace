export const FETCH_OBJECTIVES = 'FETCH_OBJECTIVES';
export const FETCH_OBJECTIVES_SUCCESS = 'FETCH_OBJECTIVES_SUCCESS';
export const FETCH_OBJECTIVES_FAILURE = 'FETCH_OBJECTIVES_FAILURE';

export const ADD_OBJECTIVE = 'ADD_OBJECTIVE';
export const ADD_OBJECTIVE_SUCCESS = 'ADD_OBJECTIVE_SUCCESS';
export const ADD_OBJECTIVE_FAILURE = 'ADD_OBJECTIVE_FAILURE';

export const EDIT_OBJECTIVE = 'EDIT_OBJECTIVE';
export const EDIT_OBJECTIVE_SUCCESS = 'EDIT_OBJECTIVE_SUCCESS';
export const EDIT_OBJECTIVE_FAILURE = 'EDIT_OBJECTIVE_FAILURE';

export const DELETE_OBJECTIVE = 'DELETE_OBJECTIVE';
export const DELETE_OBJECTIVE_SUCCESS = 'DELETE_OBJECTIVE_SUCCESS';
export const DELETE_OBJECTIVE_FAILURE = 'DELETE_OBJECTIVE_FAILURE';

export const OBJECTIVES_URL = "/api/space/objectives";
///////////////
export function fetchObjectives() {
    return {
        atype: FETCH_OBJECTIVES,
        promise: function(client, dispatch, getState) {
            return client.get(OBJECTIVES_URL);
        }
    };
}

///////////////
export function addObjective(data) {
    return {
        atype: ADD_OBJECTIVE,
        promise: function(client, dispatch, getState) {
            return client.post(OBJECTIVES_URL, data);
        },
        success_title: "Tạo mục tiêu thành công"
    };
}

///////////////
export function editObjective(data) {
    return {
        atype: EDIT_OBJECTIVE,
        promise: function(client, dispatch, getState) {
            return client.put(OBJECTIVES_URL + "/" + data.id, data);
        },
        success_title: "Cập nhật mục tiêu thành công"
    };
}

///////////////
export function deleteObjective(objId) {
    return {
        atype: DELETE_OBJECTIVE,
        promise: function(client, dispatch, getState) {
            return client.delete(OBJECTIVES_URL + "/" + objId);
        },
        success_title: "Xóa mục tiêu thành công"
    };
}
