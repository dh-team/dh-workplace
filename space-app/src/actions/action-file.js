import {setMsg} from './action-msg';

export const CHANGE_START_DIR = 'CHANGE_START_DIR';
export const FETCH_DIR_CONTENT = 'FETCH_DIR_CONTENT';
export const FETCH_DIR_CONTENT_SUCCESS = 'FETCH_DIR_CONTENT_SUCCESS';
export const FETCH_DIR_CONTENT_FAILURE = 'FETCH_DIR_CONTENT_FAILURE';

export const SELECT_NODES = 'SELECT_NODES';
export const DESELECT_NODES = 'DESELECT_NODES';
export const TOGGLE_SELECTED_NODE = 'TOGGLE_SELECTED_NODE';
export const CLEAR_SELECTED_NODES = 'CLEAR_SELECTED_NODES';

export const DELETE_NODES = 'DELETE_NODES';
export const DELETE_NODES_SUCCESS = 'DELETE_NODES_SUCCESS';
export const DELETE_NODES_FAILURE = 'DELETE_NODES_FAILURE';

export const RESTORE_NODES = 'RESTORE_NODES';
export const RESTORE_NODES_SUCCESS = 'RESTORE_NODES_SUCCESS';
export const RESTORE_NODES_FAILURE = 'RESTORE_NODES_FAILURE';

export const EMPTY_TRASH = 'EMPTY_TRASH';
export const EMPTY_TRASH_SUCCESS = 'EMPTY_TRASH_SUCCESS';
export const EMPTY_TRASH_FAILURE = 'EMPTY_TRASH_FAILURE';

export const CREATE_DIR = 'CREATE_DIR';
export const CREATE_DIR_SUCCESS = 'CREATE_DIR_SUCCESS';
export const CREATE_DIR_FAILURE = 'CREATE_DIR_FAILURE';

export const RENAME = 'RENAME';
export const RENAME_SUCCESS = 'RENAME_SUCCESS';
export const RENAME_FAILURE = 'RENAME_FAILURE';

export const MOVE_NODES = 'MOVE_NODES';
export const MOVE_NODES_SUCCESS = 'MOVE_NODES_SUCCESS';
export const MOVE_NODES_FAILURE = 'MOVE_NODES_FAILURE';

export const DOWNLOAD = 'DOWNLOAD';
export const DOWNLOAD_SUCCESS = 'DOWNLOAD_SUCCESS';
export const DOWNLOAD_FAILURE = 'DOWNLOAD_FAILURE';

export const BEGIN_UPLOAD = 'BEGIN_UPLOAD';
export const UPLOAD_PROGRESS = 'UPLOAD_PROGRESS';
export const UPLOAD_SUCCESS = 'UPLOAD_SUCCESS';
export const UPLOAD_FAILURE = 'UPLOAD_FAILURE';
export const CLEAR_UPLOAD = 'CLEAR_UPLOAD';

///////////////

export function fetchDirContent(params) {
    return {
        atype: FETCH_DIR_CONTENT,
        rid: false, // test
        payload: params,
        promise: function(client, dispatch, getState) {
            var postParams = {
                id: params.id,
                app: params.app,
                partition: params.partition
            };
            var url = params.trash ? "/fs/trash" : "/fs/dir-content";
            return client.post(url, postParams);
        }
    };
}

export function changeStartDir(params) {
    return {
        type: CHANGE_START_DIR,
        payload: params
    };
}

///////////////
export function deleteNode(nodeId, permanent) {
    return deleteNodes([nodeId], permanent, true);
}

function getFetchDirParams(node, trash){
    if(trash) return {
        partition: node.partition,
        app: node.app,
        trash: true
    };
    
    if(node.parent) return {id: node.parent};
    
    return {
        partition: node.partition,
        app: node.app
    };
}

export function deleteNodes(nodes, permanent, noReload) {
    return {
        atype: DELETE_NODES,        
        success_title: "Xóa thành công",
        promise: function(client, dispatch, getState) {
            var url = permanent ? "/fs/delete-permanent" : "/fs/delete";
            return client.post(url, {ids: nodes});
        },
        success: function(result, dispatch, getState){
            if(noReload || !result || result.length === 0) return;
            dispatch(fetchDirContent(getFetchDirParams(result[0], permanent)));
        }
    };
}

///////////////
export function restoreNodes(nodes) {
    return {
        atype: RESTORE_NODES,
        success_title: "Khôi phục thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/fs/restore", {ids: nodes});
        },
        success: function(result, dispatch, getState){
            if(!result || result.length === 0) return;
            dispatch(fetchDirContent(getFetchDirParams(result[0], true)));
        }
    };
}

///////////////
export function emptyTrashCurrent(params) {
    return (dispatch, getState) => {
        var activeDir = getState().file.active_dir;
        var params = {
            app: activeDir.app,
            partition: activeDir.partition
        };
        return dispatch(emptyTrash(params));
    };
}

export function emptyTrash(params) {
    return {
        atype: EMPTY_TRASH,        
        success_title: "Dọn thùng rác thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/fs/empty-trash", params);
        },
        success: function(result, dispatch, getState){
            dispatch(fetchDirContent({...params, trash: true}));
        }
    };
}

///////////////

// params : {name, parent, app, partition}
export function createDir(params) {
    return {
        atype: CREATE_DIR,
        success_title: "Tạo thư mục thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/fs/create-dir", params);
        },
        success: function(result, dispatch, getState){
            if(!result) return;
            dispatch(fetchDirContent(getFetchDirParams(result)));
        }
    };
}

///////////////
// dir : {id, new_name}
export function rename(params) {
    return {
        atype: RENAME,
        success_title: "Sửa tên thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/fs/rename", params);
        },
        success: function(result, dispatch, getState){
            if(!result) return;
            dispatch(fetchDirContent(getFetchDirParams(result)));
        }
    };
}

///////////////
// params : {target_id, ids}
export function moveNodes(params) {
    return {
        atype: MOVE_NODES,
        success_title: "Di chuyển thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/fs/move", params);
        },
        success: function(result, dispatch, getState){
            if(!result || result.length === 0) return;
            dispatch(fetchDirContent(getFetchDirParams(result[0])));
        }
    };
}

///////////////
// data: {id, name}
export function beginUpload(data) {
    return {
        type: BEGIN_UPLOAD,
        payload: data
    };
}
// data: {id, name, uploaded_bytes, total_bytes}
export function uploadProgress(data) {
    return {
        type: UPLOAD_PROGRESS,
        payload: data
    };
}
// data: {id, name, response}
export function uploadSuccess(data) {
    return {
        type: UPLOAD_SUCCESS,
        payload: data
    };
}
// data: {id, name, error}
export function uploadError(error) {
    return {
        type: UPLOAD_FAILURE,
        payload: error
    };
}

export function clearUpload() {
    return {
        type: CLEAR_UPLOAD
    };
}
///////////////
export function download(nodeID) {
    return {
        atype: DOWNLOAD,
        promise: function(client, dispatch, getState) {
            return client.post("/fs/download", {id : nodeID});
        }
    };
}