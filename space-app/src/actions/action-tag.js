import { setMsg } from './action-msg';

export const FETCH_TAGS = 'FETCH_TAGS';
export const FETCH_TAGS_SUCCESS = 'FETCH_TAGS_SUCCESS';
export const FETCH_TAGS_FAILURE = 'FETCH_TAGS_FAILURE';

export const ADD_TAG = 'ADD_TAG';
export const TAGS_URL = "/api/space/tags";

///////////////
export function fetchTags(subject) {
    return {
        atype: FETCH_TAGS,
        payload: subject,
        rid: subject,
        promise: function (client, dispatch, getState) {
            return client.get(TAGS_URL, { params: { subject } });
        }
    };
}

///////////////
export function addTag(data) {
    return {
        type: ADD_TAG,
        payload: data
    };
}