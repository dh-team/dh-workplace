export const FETCH_REACTION_STREAM = 'FETCH_REACTION_STREAM';
export const FETCH_REACTION_STREAM_SUCCESS = 'FETCH_REACTION_STREAM_SUCCESS';
export const FETCH_REACTION_STREAM_FAILURE = 'FETCH_REACTION_STREAM_FAILURE';

export const ADD_REACTION = 'ADD_REACTION';
export const ADD_REACTION_SUCCESS = 'ADD_REACTION_SUCCESS';
export const ADD_REACTION_FAILURE = 'ADD_REACTION_FAILURE';

export const DELETE_REACTION = 'DELETE_REACTION';
export const DELETE_REACTION_SUCCESS = 'DELETE_REACTION_SUCCESS';
export const DELETE_REACTION_FAILURE = 'DELETE_REACTION_FAILURE';


///////////////
export function fetchCommentStream(streamId) {
    return {
        atype: FETCH_REACTION_STREAM,
        payload: streamId,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/reaction-streams/" + streamId);
        }
    };
}

///////////////
// params: {stream_id, action, data}
export function userReact(params) {
    return {
        atype: "ADD_REACTION",
        payload: params.stream_id,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/reaction-streams/" + params.stream_id + "/react", params);
        }
    };
}

///////////////
// params: {stream_id, action}
export function undoUserReact(params) {
    return {
        atype: "DELETE_REACTION",
        payload: params.stream_id,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/reaction-streams/" + params.stream_id + "/undo-react", params);
        }
    };
}
