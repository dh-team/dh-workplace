import {setMsg} from './action-msg';

// List groups of user
export const FETCH_GROUPS_OF_USER = 'FETCH_GROUPS_OF_USER';
export const FETCH_GROUPS_OF_USER_SUCCESS = 'FETCH_GROUPS_OF_USER_SUCCESS';
export const FETCH_GROUPS_OF_USER_FAILURE = 'FETCH_GROUPS_OF_USER_FAILURE';

// Get group info
export const FETCH_GROUP_INFO = 'FETCH_GROUP_INFO';
export const FETCH_GROUP_INFO_SUCCESS = 'FETCH_GROUP_INFO_SUCCESS';
export const FETCH_GROUP_INFO_FAILURE = 'FETCH_GROUP_INFO_FAILURE';

///////////////
export function fetchGroupsOfUser() {
    return {
        atype: FETCH_GROUPS_OF_USER,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/group/user-groups");
        }
    };
}

///////////////
export function fetchGroupInfo(groupID) {
    return {
        atype: FETCH_GROUP_INFO,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/group/info?group_id=" + groupID);
        }
    };
}

