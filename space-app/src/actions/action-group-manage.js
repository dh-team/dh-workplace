import {setMsg} from './action-msg';

export const FETCH_GROUPS = 'FETCH_GROUPS';
export const FETCH_GROUPS_SUCCESS = 'FETCH_GROUPS_SUCCESS';
export const FETCH_GROUPS_FAILURE = 'FETCH_GROUPS_FAILURE';

export const EDIT_GROUP = 'EDIT_GROUP';
export const EDIT_GROUP_SUCCESS = 'EDIT_GROUP_SUCCESS';
export const EDIT_GROUP_FAILURE = 'EDIT_GROUP_FAILURE';

export const ADD_GROUP = 'ADD_GROUP';
export const ADD_GROUP_SUCCESS = 'ADD_GROUP_SUCCESS';
export const ADD_GROUP_FAILURE = 'ADD_GROUP_FAILURE';

export const DELETE_GROUP = 'DELETE_GROUP';
export const DELETE_GROUP_SUCCESS = 'DELETE_GROUP_SUCCESS';
export const DELETE_GROUP_FAILURE = 'DELETE_GROUP_FAILURE';

export const UPDATE_GROUP_PARENT = 'UPDATE_GROUP_PARENT';
export const UPDATE_GROUP_PARENT_SUCCESS = 'UPDATE_GROUP_PARENT_SUCCESS';
export const UPDATE_GROUP_PARENT_FAILURE = 'UPDATE_GROUP_PARENT_FAILURE';

export const SET_ACTIVE_GROUP = 'SET_ACTIVE_GROUP';

// List users of group
export const FETCH_GROUP_USERS = 'FETCH_GROUP_USERS';
export const FETCH_GROUP_USERS_SUCCESS = 'FETCH_GROUP_USERS_SUCCESS';
export const FETCH_GROUP_USERS_FAILURE = 'FETCH_GROUP_USERS_FAILURE';
export const RESET_GROUP_USERS = 'RESET_GROUP_USERS';
// Add user to group
export const ADD_GROUP_USER = 'ADD_GROUP_USER';
export const ADD_GROUP_USER_SUCCESS = 'ADD_GROUP_USER_SUCCESS';
export const ADD_GROUP_USER_FAILURE = 'ADD_GROUP_USER_FAILURE';
// Add user to group
export const DELETE_GROUP_USER = 'DELETE_GROUP_USER';
export const DELETE_GROUP_USER_SUCCESS = 'DELETE_GROUP_USER_SUCCESS';
export const DELETE_GROUP_USER_FAILURE = 'DELETE_GROUP_USER_FAILURE';

// List groups of user
export const FETCH_GROUPS_OF_USER = 'FETCH_GROUPS_OF_USER';
export const FETCH_GROUPS_OF_USER_SUCCESS = 'FETCH_GROUPS_OF_USER_SUCCESS';
export const FETCH_GROUPS_OF_USER_FAILURE = 'FETCH_GROUPS_OF_USER_FAILURE';

///////////////
export function fetchGroups() {
    return {
        atype: FETCH_GROUPS,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/group/list");
        }
    };
}

///////////////
export function editGroup(data) {
    return {
        atype: EDIT_GROUP,
        success_title: "Cập nhật thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/edit", data);
        },
        success: function(result, dispatch, getState){
            dispatch(fetchGroups());
        }
    };
}

///////////////
export function addGroup(data) {
    return {
        atype: ADD_GROUP,
        success_title: "Thêm nhóm thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/add", data);
        }
    };
}

///////////////
export function updateGroupParent(data) {
    return {
        atype: UPDATE_GROUP_PARENT,
        success_title: "Cập nhật thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/update-parent", data);            
        },
        success: function(result, dispatch, getState){
            dispatch(fetchGroups());
        }
    };
}

///////////////
export function deleteGroup(groupId) {
    return {
        atype: DELETE_GROUP,
        success_title: "Xóa thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/delete", {id:groupId});
        },
        success: function(result, dispatch, getState){
            dispatch(fetchGroups());
        }
    };
}

///////////////
export function setActiveGroup(groupId) {
    return {
        type: SET_ACTIVE_GROUP,
        payload: groupId
    };
}

///////////////
export function fetchGroupUsers(groupID) {
    return {
        atype: FETCH_GROUP_USERS,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/user/list", {group_id:groupID});
        }
    };
}

///////////////
export function addGroupUser(props) {
    return {
        atype: ADD_GROUP_USER,
        rid: false,
        payload: props,
        success_title: "Thêm người dùng thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/user/add", props);
        }
    };
}

///////////////
export function deleteGroupUser(props) {
    return {
        atype: DELETE_GROUP_USER,
        rid: false,
        payload: props,
        success_title: "Xóa người dùng thành công",
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/group/user/delete", props);
        }
    };
}

///////////////
export function fetchGroupsOfUser() {
    return {
        atype: FETCH_GROUPS_OF_USER,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/group/user-groups");
        }
    };
}
