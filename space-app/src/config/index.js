/*jslint node: true */
'use strict';
module.exports = require("../../conf.json");

/**
 * Session configuration
 *
 * type - The type of session to use.  MemoryStore for "in-memory",
 * or MongoStore for the mongo database store
 * maxAge - The maximum age in milliseconds of the session.  Use null for
 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
 * for a year.
 * secret - The session secret that you should change to what you want
 * dbName - The database name if you're using Mongo
 */
module.exports.session = {
    type: "MemoryStore",
    maxAge: 3600000 * 24 * 7 * 52,
    //TODO You need to change this secret to something that you choose for your secret
    secret: "PxGZT2bJpGyrFddDphNW",
    dbName: "dh-space-session"
};

module.exports.cookies = {    
    spaceTokenKey: "space_token",
    maxAge: 3600000 * 24 * 7 * 52
};

module.exports.authServer.clientId = "space_web_app";
module.exports.authServer.clientSecret = "space_web_app_secret";
module.exports.authServer.tokenUrl = module.exports.authServer.url + "/as/oauth/token";
module.exports.authServer.authorizationUrl = module.exports.publishUrl + '/as/dialog/oauth';
module.exports.authServer.callbackUrl = module.exports.publishUrl + "/space/auth/handle";
module.exports.authServer.userProfileUrl = module.exports.authServer.url + "/as/user/profile";
module.exports.authServer.logoutUrl = module.exports.publishUrl + "/as/logout";
