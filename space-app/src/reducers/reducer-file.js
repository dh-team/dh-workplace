import {
    FETCH_DIR_CONTENT, FETCH_DIR_CONTENT_SUCCESS, FETCH_DIR_CONTENT_FAILURE, CHANGE_START_DIR,
    SELECT_NODES, DESELECT_NODES, TOGGLE_SELECTED_NODE, CLEAR_SELECTED_NODES,
    BEGIN_UPLOAD, UPLOAD_PROGRESS, UPLOAD_SUCCESS, UPLOAD_FAILURE, CLEAR_UPLOAD
} from '../actions/action-file';

const INITIAL_STATE = {
    store: {
        // [nodeId] : {meta: {}, content: []}
    },
    start_dir: null,
    upload: {}, // {id, name, uploaded_bytes, total_bytes, status, error, response} (status: 'uploading' | 'success' | 'error')
};


export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
            //////////////////
        case CHANGE_START_DIR:
            return {...state,
                start_dir: action.payload
            };
        case FETCH_DIR_CONTENT_SUCCESS:
            var parent = action.request_payload;
            var parentId = parent.id ? parent.id : parent.partition + "_" + parent.app;
            var nodes = action.payload;

            var store = {...state.store};
            var ids = [];
            nodes.map(function(item) {
                ids.push(item.id);
                if(!store[item.id]) store[item.id] = {meta: {}, content: []};
                store[item.id] = {...store[item.id], meta: item};
            });
            
            if(!store[parentId]) store[parentId] = {meta: {}, content: []};
            store[parentId] = {...store[parentId], content: ids};
            return {...state,
                store: store
            };
       
            //////////////////
        case BEGIN_UPLOAD:
            var u = {
                id: "" + action.payload.id,
                name: action.payload.name,
                uploaded_bytes: 0,
                total_bytes: 0,
                status: 'uploading',
                error: null,
                response: null
            }
            var upload = {...state.upload};
            upload[u.id] = u;
            return {...state,
                upload: upload
            };
        case UPLOAD_PROGRESS:
            var upload = {...state.upload};
            var id = "" + action.payload.id;
            if (upload[id]) {
                upload[id].uploaded_bytes = action.payload.uploaded_bytes;
                upload[id].total_bytes = action.payload.total_bytes;
            }
            return {...state,
                upload: upload
            };
        case UPLOAD_SUCCESS:
            var upload = {...state.upload};
            var id = "" + action.payload.id;
            if (upload[id]) {
                upload[id].status = 'success';
                upload[id].response = action.payload.response;
            }
            return {...state,
                upload: upload
            };
        case UPLOAD_FAILURE:
            var upload = {...state.upload};
            var id = "" + action.payload.id;
            if (upload[id]) {
                upload[id].status = 'error';
                upload[id].error = action.payload.error;
            }
            return {...state,
                upload: upload
            };
        case CLEAR_UPLOAD:
            return {...state,
                upload: {}
            };
        default:
            return state;
    }
}
