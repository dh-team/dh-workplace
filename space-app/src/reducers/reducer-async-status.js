const ASYNC_REQUEST = 'ASYNC_REQUEST';
const ASYNC_FAILURE = 'ASYNC_FAILURE';
const ASYNC_SUCCESS = 'ASYNC_SUCCESS';

const INITIAL_STATE = {
    /* 
    [REQ_TYPE]: {
        loading: false,
        loaded: true,
        error: null,
        requests: {
            [REQ_ID]: {
                loading: false,
                loaded: false,
                error: null
            }
        }
    }
    */
};

export default function(state = INITIAL_STATE, action) {
    var reqType, status, reqId;
    switch (action.type) {
        case ASYNC_REQUEST:
            reqType = action.request_type;
            reqId = action.request_id;
            status = {...state[reqType]};
            if(!status) status = {
                loading: false,
                loaded: false,
                error: null
            };
            if(reqId) {                
                if(!status.requests) status.requests = {};
                if (status.requests[reqId]) {
                    status.requests[reqId] = {
                        ...status.requests[reqId],
                        loading: true,
                        error: null
                    };
                }
                else {
                    status.requests[reqId] = {
                        loading: true,
                        loaded: false,
                        error: null
                    };
                }
            }
            else {
                status = {
                    ...status,
                    loading: true,
                    error: null
                };
            }
            return {...state,
                [reqType]: status
            };

        case ASYNC_SUCCESS:
            reqType = action.request_type;
            reqId = action.request_id;
            status = {...state[reqType]};
            if (reqId) {                
                status.requests[reqId] = {
                    ...status.requests[reqId],
                    loading: false,
                    loaded: true,
                    error: null
                };
            }
            else {
                status = {
                    ...status,
                    loading: false,
                    loaded: true,
                    error: null
                };
            }
            return {...state,
                [reqType]: status
            };
            //////////////
        case ASYNC_FAILURE:
            reqType = action.request_type;
            reqId = action.request_id;
            status = {...state[reqType]};
            if (reqId) {                
                status.requests[reqId] = {
                    ...status.requests[reqId],
                    loading: false,
                    loaded: false,
                    error: action.error
                };
            }
            else {
                status = {
                    ...status,
                    loading: false,
                    loaded: false,
                    error: action.error
                };
            }
            return {...state,
                [reqType]: status
            };
        default:
            return state;
    }
}
