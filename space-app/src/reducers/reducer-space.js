import {
    FETCH_SPACE_INFO_SUCCESS, EDIT_SPACE_INFO_SUCCESS    
} from '../actions/action-space';

const INITIAL_STATE = {
    space_info: {},    
};
export default function(state = INITIAL_STATE, action) {    
    switch (action.type) {        
        case FETCH_SPACE_INFO_SUCCESS:
            return { ...state,
                space_info: action.payload                
            };
        
        case EDIT_SPACE_INFO_SUCCESS:
            return { ...state,
                space_info: action.payload                
            };        
        
        default:
            return state;
    }
}
