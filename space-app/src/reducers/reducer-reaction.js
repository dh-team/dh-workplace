import {
    FETCH_COMMENT_STREAM_SUCCESS
} from '../actions/action-comment';

import {
    FETCH_POSTS_SUCCESS,
    FETCH_POST_SUCCESS
} from '../actions/action-wiki-post';

import {
    FETCH_LIVE_POSTS_SUCCESS,
    FETCH_LIVE_POST_SUCCESS
} from '../actions/action-live-post';

import {
    FETCH_REACTION_STREAM_SUCCESS, ADD_REACTION_SUCCESS, DELETE_REACTION_SUCCESS
} from '../actions/action-reaction';

const INITIAL_STATE = {
    store: {}, // { [stream_id]: {_id, ref_type, ref_id, reaction} }
};

export default function (state = INITIAL_STATE, action) {
    switch (action.type) {
        //////////////////
        case FETCH_COMMENT_STREAM_SUCCESS:
        case FETCH_LIVE_POSTS_SUCCESS:
        case FETCH_POSTS_SUCCESS: {
            const items = action.type === FETCH_COMMENT_STREAM_SUCCESS ? action.payload.comments : action.payload;
            if (!items || items.length === 0) return state;

            const store = { ...state.store };
            items.map((item) => {
                if(item.reaction_stream && item.reaction_data){
                    store[item.reaction_stream] = Object.assign({}, store[item.reaction_stream], item.reaction_data);    
                }
            });

            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case FETCH_POST_SUCCESS:
        case FETCH_LIVE_POST_SUCCESS: {
            const item = action.payload;
            const store = { ...state.store };
            if(item.reaction_stream && item.reaction_data){
                store[item.reaction_stream] = Object.assign({}, store[item.reaction_stream], item.reaction_data);    
            }
            
            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case FETCH_REACTION_STREAM_SUCCESS: {
            const stream = action.payload;
            const store = { ...state.store };
            store[stream._id] = Object.assign({}, store[stream._id], stream);
            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case ADD_REACTION_SUCCESS: {
            const {reacted} = action.payload; // payload: {reacted : {stream_id, action, data}}
            const store = { ...state.store };
            const stream = store[reacted.stream_id] || {_id: reacted.stream_id, reaction: {}};
            
            store[reacted.stream_id] = {
                ...stream,
                reaction: {
                    ...stream.reaction,
                    [reacted.action]: [...(stream.reaction[reacted.action] || []), reacted.data]
                }
            };

            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case DELETE_REACTION_SUCCESS:{
            const {undoreacted} = action.payload; // payload: {undoreacted : {stream_id, action, user_id}}
            const store = { ...state.store };
            const stream = store[undoreacted.stream_id];
            if(!stream || !stream.reaction || !stream.reaction[undoreacted.action]) return state;
            
            store[undoreacted.stream_id] = {
                ...stream,
                reaction: {
                    ...stream.reaction,
                    [undoreacted.action]: stream.reaction[undoreacted.action].filter((item) => item.issue_user && item.issue_user.user_id !== undoreacted.user_id)
                }
            };

            return {
                ...state,
                store: store
            };
        }
        //////////////////
        default:
            return state;
    }
}
