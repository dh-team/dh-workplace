import {
    FETCH_GROUPS_OF_USER, FETCH_GROUPS_OF_USER_SUCCESS, FETCH_GROUPS_OF_USER_FAILURE,
    FETCH_GROUP_INFO, FETCH_GROUP_INFO_SUCCESS, FETCH_GROUP_INFO_FAILURE
} from '../actions/action-group-display';

const INITIAL_STATE = {
    store: {}, // {[id] : item}
    ids: [], // [id,id,...]    
};

export default function(state = INITIAL_STATE, action) {
    var store, ids, groups, group;
    switch (action.type) {       
        case FETCH_GROUPS_OF_USER_SUCCESS:
            groups = action.payload;
            store = {};
            ids = [];
            groups.map(function(item){
                store[item._id] = item;
                ids.push(item._id);
            });
            return {...state,                
                store: store,
                ids: ids
            };
            
            //////////////////
        case FETCH_GROUP_INFO_SUCCESS:
            group = action.payload;
            store = {...state.store};
            if(group){
                store[group._id] = group;
            }
            return {...state,                
                store: store
            };           
       
            //////////////////
        default:
            return state;
    }
}
