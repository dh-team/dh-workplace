import {
    FETCH_NOTIFICATIONS, FETCH_NOTIFICATIONS_SUCCESS, FETCH_NOTIFICATIONS_FAILURE, FETCH_NOTIFICATIONS_RESET, 
    MARK_NOTIFICATION_READ, MARK_NOTIFICATION_READ_SUCCESS, MARK_NOTIFICATION_READ_FAILURE,
    COUNT_NOTIFICATION, COUNT_NOTIFICATION_SUCCESS, COUNT_NOTIFICATION_FAILURE
} from '../actions/action-notification';

const INITIAL_STATE = {
    count: {
        unread: 0
    },
    store: {},
    ids: [],
    fetch: {
        cursor: null,
        limit: 5,
        has_more: true
    }    
};


export default function(state = INITIAL_STATE, action) {
    var store, ids, notifications, notification;
    switch (action.type) {
         //////////////////        
        case FETCH_NOTIFICATIONS_SUCCESS: 
            store = {...state.store};
            ids = [...state.ids];
            notifications = action.payload;
            notifications.map((item) => {
                store[item._id] = item;
                ids.push(item._id);
            });
            
            var nextCursor = ids.length > 0 ? ids[ids.length - 1] : null;
            var hasMore = notifications.length >= state.fetch.limit;
            return {...state,
                store: store,
                ids: ids,
                fetch: {
                    ...state.fetch,
                    cursor: nextCursor,
                    has_more: hasMore,
                }
            };
           
        case FETCH_NOTIFICATIONS_RESET: 
            return {...state,
                store: {},
                ids: [], 
                fetch: {
                    cursor: null,
                    limit: 5,
                    has_more: true
                }
            };    
                       
            //////////////////
        case MARK_NOTIFICATION_READ_SUCCESS: 
            var id = action.request_payload;            
            store = {...state.store};
            if(store[id]){
                store[id].is_read = true;
            }
            return {...state,
                store: store
            }; 
            
             //////////////////
        case COUNT_NOTIFICATION_SUCCESS: 
            return {...state,
                count: {
                    unread: action.payload
                }
            };
        default:
            return state;
    }
}
