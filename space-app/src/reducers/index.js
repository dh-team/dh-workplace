import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import { reducer as reduxFormReducer } from 'redux-form';
import { reducer as reduxAsyncConnect} from 'redux-connect';
import global_msg from './reducer-msg';
import space from './reducer-space';
import currentUser from './reducer-current-user';
import file from './reducer-file';
import spaceUser from './reducer-user';
import groupManage from './reducer-group-manage';
import groupDisplay from './reducer-group-display';
import wikiPost from './reducer-wiki-post';
import wikiCat from './reducer-wiki-category';
import livePost from './reducer-live-post';
import tag from './reducer-tag';
import comment from './reducer-comment';
import reaction from './reducer-reaction';
import notification from './reducer-notification';
import async_status from './reducer-async-status';
import objective from './reducer-objective';
import work from './reducer-work';

const rootReducer = combineReducers({
    form: reduxFormReducer,
    routing: routerReducer,
    reduxAsyncConnect,
    user: currentUser,
    global_msg: global_msg,
    space: space,
    file: file,
    group_manage: groupManage,
    group_display: groupDisplay,
    space_user: spaceUser,
    wiki_post: wikiPost,
    wiki_category: wikiCat,
    live_post: livePost,
    tag: tag,
    comment: comment,
    notification: notification,
    reaction: reaction,
    async_status: async_status,
    objective: objective,
    work: work
});

export default rootReducer;

