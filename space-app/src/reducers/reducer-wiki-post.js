import {
    FETCH_WIKI_POSTS_SUCCESS,
    FETCH_WIKI_POST_SUCCESS,
    ADD_WIKI_POST_SUCCESS,
    EDIT_WIKI_POST_SUCCESS,
    DELETE_WIKI_POST_SUCCESS,
    SET_ACTIVE_WIKI_POST
} from '../actions/action-wiki-post';

const INITIAL_STATE = {
    store: {}, // {[id] : item}
    ids: [], // [id, id,...]
    active_post: null, // id of active category    
};

export default function(state = INITIAL_STATE, action) {
    console.log("dispatch: " + JSON.stringify(action));
    var posts, post, store, ids;
    switch (action.type) {
        //////////////////       
        case FETCH_WIKI_POSTS_SUCCESS:
            posts = action.payload;
            store = {...state.store};
            ids = [];
            posts.map(function(item){
                store[item._id] = item;
                ids.push(item._id);
            });
            return {...state,                
                store: store,
                ids: ids
            };        
             //////////////////
        case SET_ACTIVE_WIKI_POST:
            return {...state,
                active_post: action.payload                
            };
            
        case FETCH_WIKI_POST_SUCCESS:
            post = action.payload;
            store = {...state.store};
            if(post){
                store[post._id] = post;
            }
            return {...state,               
                store: store
            };            
        
            //////////////////
        
        default:
            return state;
    }
}
