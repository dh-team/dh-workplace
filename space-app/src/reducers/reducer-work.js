import {
    FETCH_WORK_DETAIL_SUCCESS,
    FETCH_WORKS_SUCCESS,
    FETCH_SUB_WORKS_SUCCESS,
    FETCH_PARENT_WORKS_SUCCESS,
    FETCH_PERFORM_WORKS_SUCCESS,
    FETCH_MONITOR_WORKS_SUCCESS,
    FETCH_FOLLOWED_WORKS_SUCCESS,
    ADD_WORK_SUCCESS,
    EDIT_WORK_SUCCESS,
    DELETE_WORK_SUCCESS,
    ADD_WORK_TASK_SUCCESS,
    EDIT_WORK_TASK_SUCCESS,
    DELETE_WORK_TASK_SUCCESS
} from '../actions/action-work';

const INITIAL_STATE = {
    store: {}, // {[id] : item}
    ids: [], // [id, id,...]
    perform: [],
    monitor: [],
    followed: [],
    last_added: null, // id of last added work 
};

export default function (state = INITIAL_STATE, action) {
    // var works, work, store, ids, perform, monitor;
    switch (action.type) {
        //////////////////       
        case FETCH_WORK_DETAIL_SUCCESS: {
            let work = action.payload;
            let store = { ...state.store };
            if (!work) return state;

            // normalize parents
            if (work.all_parent_works) {
                work.all_parent_works.map(item => {
                    store[item._id] = Object.assign({}, store[item._id], item);
                });
                delete work.all_parent_works;
            }
            // normalize childs
            if (work.child_works) {
                work.child_works.map(item => {
                    store[item._id] = Object.assign({}, store[item._id], item);
                });
                delete work.child_works;
            }

            store[work._id] = Object.assign({}, store[work._id], work);

            return {
                ...state,
                store: store
            };
        }
       
        //////////////////       
        case FETCH_SUB_WORKS_SUCCESS: {
            let works = action.payload;
            let store = { ...state.store };
            let parentId = action.request_payload.parent_id;
            let childs = [];
            works.map(function (item) {
                store[item._id] = Object.assign({}, store[item._id], item);
                childs.push(item._id);
            });
            store[parentId] = Object.assign({}, store[parentId], { childs: childs });

            return {
                ...state,
                store: store
            };
        }
        //////////////////       
        case FETCH_PARENT_WORKS_SUCCESS: {
            let works = action.payload;
            let store = { ...state.store };
            const descendantId = action.request_payload.descendant_id;
            let parents = [];
            works.map(item => {
                store[item._id] = Object.assign({}, store[item._id], item);
                parents.push(item._id);
            });
            store[descendantId] = Object.assign({}, store[descendantId], { all_parents: parents });
            return {
                ...state,
                store: store
            };
        }
        //////////////////     
        case FETCH_WORKS_SUCCESS:  
        case FETCH_FOLLOWED_WORKS_SUCCESS:
        case FETCH_MONITOR_WORKS_SUCCESS:
        case FETCH_PERFORM_WORKS_SUCCESS: {
            let works = action.payload;
            let store = { ...state.store };
            let ids = [];
            works.map(function (item) {
                store[item._id] = Object.assign({}, store[item._id], item);
                ids.push(item._id);
            });
            const newState = {
                ...state,
                store: store
            };

            if(action.type === FETCH_FOLLOWED_WORKS_SUCCESS) newState.followed = ids;
            else if(action.type === FETCH_MONITOR_WORKS_SUCCESS) newState.monitor = ids;
            else if(action.type === FETCH_PERFORM_WORKS_SUCCESS) newState.perform = ids;
            else newState.ids = ids;

            return newState;
        }
        
        //////////////////       
        case ADD_WORK_SUCCESS: {
            let work = action.payload;
            let store = { ...state.store };
            store[work._id] = work;
            let ids = [...state.ids];
            let perform = [...state.perform];
            if (work.parent && store[work.parent]) {
                if (!store[work.parent].childs) store[work.parent].childs = [];
                store[work.parent].childs.push(work._id);
            }
            else {
                ids.push(work._id);
            }
            if (work.meta && work.meta.create_user && work.resource && work.meta.create_user.user_id === work.resource.performer) {
                perform.push(work._id);
            }

            return {
                ...state,
                store: store,
                ids: ids,
                perform: perform,
                last_added: work._id
            };
        }
        //////////////////       
        case EDIT_WORK_SUCCESS:
        case ADD_WORK_TASK_SUCCESS:
        case EDIT_WORK_TASK_SUCCESS:
        case DELETE_WORK_TASK_SUCCESS: {
            let work = action.payload;
            let store = { ...state.store };
            store[work._id] = Object.assign({}, store[work._id], work);

            return {
                ...state,
                store: store
            };
        }
        //////////////////       
        case DELETE_WORK_SUCCESS: {
            let work = action.payload;
            let id = work._id;
            let store = { ...state.store };

            // if (store[id]) delete store[id];
            if (work.parent && store[work.parent]) {
                store[work.parent].childs = store[work.parent].childs && store[work.parent].childs.filter((item) => item !== id);
            }

            let ids = state.ids.filter((item) => item !== id);
            let perform = state.perform.filter((item) => item !== id);
            let monitor = state.monitor.filter((item) => item !== id);

            return {
                ...state,
                store: store,
                ids: ids,
                perform: perform,
                monitor: monitor
            };
        }
        //////////////////
        default:
            return state;
    }
}
