import {
    FETCH_SPACE_USERS, FETCH_SPACE_USERS_SUCCESS, FETCH_SPACE_USERS_FAILURE, RESET_SPACE_USERS,
    ADD_SPACE_USER, ADD_SPACE_USER_SUCCESS, ADD_SPACE_USER_FAILURE,
    DELETE_SPACE_USER, DELETE_SPACE_USER_SUCCESS, DELETE_SPACE_USER_FAILURE,
} from '../actions/action-user';

const INITIAL_STATE = {
    store: {},
    ids: [],
    // list_users: {
    //     users: [],
    //     error: null,
    //     loading: false
    // }
};
export default function(state = INITIAL_STATE, action) {
    var store, ids, users, user, userId;
    switch (action.type) {
         //////////////////        
        case FETCH_SPACE_USERS_SUCCESS: 
            store = {...state.store};
            ids = [];
            users = action.payload;
            users.map((item) => {
                store[item.user._id] = item;
                ids.push(item.user._id);
            });            
            
            return {...state,
                store: store,
                ids: ids                
            };
            
            ////////////////
        case ADD_SPACE_USER_SUCCESS:
            user = action.payload;
            store = {...state.store};
            userId = user.user._id;
            store[userId] = user;
            ids = [...state.ids, userId];

            return {...state,
                store: store,
                ids: ids                
            };
            //////////////////
        case DELETE_SPACE_USER_SUCCESS:
            userId = action.payload.user_id;
            store = {...state.store};
            if(store[userId]) delete store[userId];
            ids = state.ids.filter((item) => item != userId);
            
            return {...state,
                store: store,
                ids: ids                
            };
            //////////////////
        default:
            return state;
    }
}
