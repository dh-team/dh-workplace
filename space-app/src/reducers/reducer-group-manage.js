import {
    FETCH_GROUPS_SUCCESS,
    EDIT_GROUP_SUCCESS,
    ADD_GROUP_SUCCESS,
    UPDATE_GROUP_PARENT_SUCCESS,
    DELETE_GROUP_SUCCESS, 
    SET_ACTIVE_GROUP,
    FETCH_GROUP_USERS_SUCCESS,
    ADD_GROUP_USER_SUCCESS,
    DELETE_GROUP_USER_SUCCESS,
} from '../actions/action-group-manage';

const INITIAL_STATE = {
    store: {},
    ids: [],
    active_group: null    
};
export default function(state = INITIAL_STATE, action) {
    var store, ids, groups, group;
    switch (action.type) {
            //////////////////        
        case FETCH_GROUPS_SUCCESS:
            groups = action.payload;
            store = {...state.store};
            ids = [];
            groups.map(function(item){
                store[item._id] = item;
                ids.push(item._id);
            });
            return {...state,                
                store: store,
                ids: ids
            };            
            
            //////////////////
        case ADD_GROUP_SUCCESS:
            group = action.payload;
            store = {...state.store};
            store[group._id] = group;
            ids = [...state.ids, group._id];
            
            return {...state,                
                store: store,
                ids: ids
            };
            
             //////////////////
        // case EDIT_GROUP_SUCCESS:
        //     group = action.payload;
        //     store = {...state.store};
        //     if(store[group._id]){
        //         store[group._id].name = group.name;
        //         store[group._id].description = group.description;
        //     }
        //     return {...state,                
        //         store: store            
        //     };
             //////////////////
        // case DELETE_GROUP_SUCCESS:
        //     group = action.payload;
        //     store = {...state.store};
        //     if(store[group._id]) delete store[group._id];
        //     ids = state.ids.filter((item) => item !== group._id);

        //     return {...state,                
        //         store: store,
        //         ids: ids
        //     };
            //////////////////
        // case UPDATE_GROUP_PARENT_SUCCESS:
        //     group = action.payload;
        //     store = {...state.store};
        //     if(store[group._id]){
        //         store[group._id].org_info.parent_id = group.org_info.parent_id;                
        //     }
        //     return {...state,                
        //         store: store            
        //     };

            //////////////////
        case SET_ACTIVE_GROUP:
            return {...state,
                active_group: action.payload    
            };
            
            //////////////////        
        case ADD_GROUP_USER_SUCCESS: 
            var userId = action.payload && action.payload.user_id;
            var groupId = action.request_payload && action.request_payload.group_id;
            if(!userId || !groupId) return state;

            store = {...state.store};
            group = store[groupId];
            if(!group) return state;
            if(!group.users){
                group.users = [userId];
            }else if(group.users.indexOf(userId) === -1){                
                group.users = [...group.users, userId];
            }

            return {...state,
                store: store
            };        
            //////////////////
        case DELETE_GROUP_USER_SUCCESS: 
            var userId = action.payload && action.payload.user_id;
            var groupId = action.request_payload && action.request_payload.group_id;
            if(!userId || !groupId) return state;

            store = {...state.store};
            group = store[groupId];
            if(!group || !group.users || group.users.length === 0) return state;
            
            group.users = group.users.filter((item) => item != userId);
            return {...state,
                store: store
            };        
            //////////////////
        default:
            return state;
    }
}

