import {
    FETCH_USER_PROFILE_SUCCESS,
} from '../actions/action-space';

const INITIAL_STATE = {    
    user_profile: {}
};
export default function(state = INITIAL_STATE, action) {    
    switch (action.type) {
        //////////////////        
        case FETCH_USER_PROFILE_SUCCESS:
            return { ...state,
                user_profile: action.payload
            };
        
        default:
            return state;
    }
}
