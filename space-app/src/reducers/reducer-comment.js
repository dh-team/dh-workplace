import {
    FETCH_LIVE_POSTS_SUCCESS
}
from '../actions/action-live-post';

import {
    FETCH_COMMENT_STREAM_SUCCESS,
    ADD_COMMENT_SUCCESS,
    EDIT_COMMENT_SUCCESS,
    DELETE_COMMENT_SUCCESS
}
from '../actions/action-comment';

const INITIAL_STATE = {
    store: {} // { ["stream_id"]: comment_stream }
};

export default function (state = INITIAL_STATE, action) {
    // console.log("dispatch: " + JSON.stringify(action));
    switch (action.type) {
        case FETCH_LIVE_POSTS_SUCCESS: {
            const items = action.payload;
            if (!items || items.length === 0) return state;

            const store = { ...state.store };
            items.map((item) => {
                if(item.comment_stream && item.comment_data){
                    store[item.comment_stream] = Object.assign({}, store[item.comment_stream], item.comment_data);    
                }
            });

            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case FETCH_COMMENT_STREAM_SUCCESS: {
            const stream = action.payload;
            const store = { ...state.store };
            store[stream._id] = Object.assign({}, store[stream._id], stream);
            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case ADD_COMMENT_SUCCESS: {
            const streamId = action.request_payload;
            const comment = action.payload;
            const store = { ...state.store };
            const stream = store[streamId] || { _id: streamId, comments: [], stat: {} };
            const comments = [...stream.comments, comment];
            store[streamId] = {
                ...stream,
                comments: comments,
                stat: {
                    ...stream.stat,
                    comment_count: comments.length
                }
            };

            return {
                ...state,
                store: store
            };
        }

        //////////////////
        case DELETE_COMMENT_SUCCESS: {
            const streamId = action.request_payload.stream_id;
            const commentId = action.request_payload.comment_id;
            const store = { ...state.store };
            const stream = store[streamId];
            if (stream) {
                const comments = stream.comments.filter((item) => item._id !== commentId);
                store[streamId] = {
                    ...stream,
                    comments: comments,
                    stat: {
                        ...stream.stat,
                        comment_count: comments.length
                    }
                };
            }
            return {
                ...state,
                store: store
            };
        }
        //////////////////
        default:
            return state;
    }
}
