import {
    FETCH_TAGS, FETCH_TAGS_SUCCESS, FETCH_TAGS_FAILURE, RESET_TAG, ADD_TAG
} from '../actions/action-tag';

const INITIAL_STATE = {
    store: {},
    by_subject: {} // {subject: []}
};
export default function (state = INITIAL_STATE, action) {

    switch (action.type) {
        //////////////////        
        case FETCH_TAGS_SUCCESS: {
            const subject = action.request_payload;
            const store = { ...state.store };
            const by_subject = { ...state.by_subject };
            by_subject[subject] = [];

            const tags = action.payload;
            tags.map((item) => {
                store[subject + "_" + item.keyword] = item;
                by_subject[subject].push(item.keyword);
            });

            return {
                ...state,
                store: store,
                by_subject: by_subject
            };
        }

        //////////////////
        case ADD_TAG: {
            const { subject, keyword } = action.payload;
            let key = subject + "_" + keyword;
            const store = { ...state.store };
            const by_subject = { ...state.by_subject };

            if (!store[key]) {
                store[key] = { subject, keyword };
                if (!by_subject[subject]) {
                    by_subject[subject] = [keyword];
                } else {
                    by_subject[subject] = [...by_subject[subject], keyword];
                }
            }

            return {
                ...state,
                store: store,
                by_subject: by_subject
            };
        }
        default:
            return state;
    }
}
