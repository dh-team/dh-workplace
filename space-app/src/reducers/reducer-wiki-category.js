import {
    FETCH_WIKI_CATEGORIES_SUCCESS,
    FETCH_WIKI_CATEGORY_SUCCESS,
    SET_ACTIVE_CATEGORY
} from '../actions/action-wiki-category';

const INITIAL_STATE = {
    store: {}, // {[id] : item}
    ids: [], // [id,id,...]
    active_category: null, // id of active category     
};

export default function(state = INITIAL_STATE, action) {
    var categories, store, ids, category;
    switch (action.type) {
        //////////////////
        
        case FETCH_WIKI_CATEGORIES_SUCCESS: 
            categories = action.payload;
            store = {};
            ids = [];
            categories.map(function(item){
                store[item._id] = item;
                ids.push(item._id);
            });
            return {...state,                
                store: store,
                ids: ids
            };
                    
        case FETCH_WIKI_CATEGORY_SUCCESS: 
            category = action.payload;
            store = {...state.store};
            if(category){
                store[category._id] = category;
            }
            return {...state,                
                store: store
            };         
        
         //////////////////
        case SET_ACTIVE_CATEGORY: 
            return {...state,
                active_category: action.payload
            }; 
         
         
        default:
            return state;
    }
}
