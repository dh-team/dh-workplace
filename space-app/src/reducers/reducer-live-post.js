import {
    FETCH_LIVE_POSTS_SUCCESS, FETCH_LIVE_POSTS_RESET,
    FETCH_LIVE_POST_SUCCESS, 
    ADD_LIVE_POST_SUCCESS, 
    EDIT_LIVE_POST_SUCCESS,
    DELETE_LIVE_POST_SUCCESS,
} from '../actions/action-live-post';

const INITIAL_STATE = {
    store: {},
    ids: [], // id
    fetch: {
        cursor: null,
        limit: 5,
        has_more: true
    }
};


export default function(state = INITIAL_STATE, action) {
    // console.log("dispatch: " + JSON.stringify(action));
    var store, ids, posts, post;
    switch (action.type) {
         //////////////////
        case FETCH_LIVE_POSTS_SUCCESS:
            store = {...state.store};
            ids = [...state.ids];
            posts = action.payload;
            posts.map((item) => {
                store[item._id] = item;
                ids.push(item._id);
            });
            
            var nextCursor = ids.length > 0 ? ids[ids.length - 1] : null;
            var hasMore = posts.length === state.fetch.limit;
            return {...state,
                store: store,
                ids: ids,
                fetch: {
                    ...state.fetch,
                    cursor: nextCursor,
                    has_more: hasMore,
                }
            };
        
        case FETCH_LIVE_POSTS_RESET: 
            return {...state,
                store: {},
                ids: [], // id
                fetch: {
                    cursor: null,
                    limit: 5,
                    has_more: true
                }
            };    
             //////////////////
       
        case FETCH_LIVE_POST_SUCCESS:
            post = action.payload;
            return {...state,
                store: {...state.store,
                    [post._id]: post
                }
            };
 
            //////////////////
        case ADD_LIVE_POST_SUCCESS:
            post = action.payload;
            return {...state,
                store: {
                    ...state.store,
                    [post._id]: post
                },
                ids: [post._id, ...state.ids]
            };
             //////////////////
        case EDIT_LIVE_POST_SUCCESS:
            post = action.payload;
            return {...state,
                store: {
                    ...state.store,
                    [post._id]: post
                }
            };
             //////////////////
        case DELETE_LIVE_POST_SUCCESS:
            post = action.payload;
            store = {...state.store};
            delete store[post._id];
            ids = state.ids.filter((item) => item != post._id);
            return {...state,
                store: store,
                ids: ids
           };
            //////////////////
        
        default:
            return state;
    }
}
