import {
    FETCH_OBJECTIVES_SUCCESS,
    ADD_OBJECTIVE_SUCCESS,
    EDIT_OBJECTIVE_SUCCESS,
    DELETE_OBJECTIVE_SUCCESS
} from '../actions/action-objective';

const INITIAL_STATE = {
    store: {}, // {[id] : item}
    ids: [], // [id, id,...]
    active_objective: null, // id of active objective    
};

export default function (state = INITIAL_STATE, action) {
    var objectives, objective, store, ids;
    switch (action.type) {
        //////////////////       
        case FETCH_OBJECTIVES_SUCCESS:
            objectives = action.payload;
            store = { ...state.store };
            ids = [];
            objectives.map(function (item) {
                store[item._id] = item;
                ids.push(item._id);
            });
            return {
                ...state,
                store: store,
                ids: ids
            };
        //////////////////       
        case ADD_OBJECTIVE_SUCCESS:
            objective = action.payload;
            store = { ...state.store };
            store[objective._id] = objective;
            ids = [...state.ids, objective._id];

            return {
                ...state,
                store: store,
                ids: ids
            };
        //////////////////       
        case EDIT_OBJECTIVE_SUCCESS:
            objective = action.payload;
            store = { ...state.store };
            store[objective._id] = objective;

            return {
                ...state,
                store: store
            };
        //////////////////       
        case DELETE_OBJECTIVE_SUCCESS:
            var id = action.payload._id;
            store = { ...state.store };
            if(store[id]) delete store[id];
            ids = state.ids.filter((item) => item !== id);

            return {
                ...state,
                store: store,
                ids: ids
            };
        //////////////////
        default:
            return state;
    }
}
