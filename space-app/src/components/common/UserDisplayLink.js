'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { getUser } from '../../selectors/selector-user';

class UserDisplayLink extends React.Component {
    
    render() {
        const {user, user_id, pre_text} = this.props;
        
        return (
                <span className="user-display-link">
                    {pre_text && <span className="pre-text">{pre_text}</span>} &nbsp;
                    <span className="user-name">
                        {/*{(user && ((user.user && user.user.display_name) || ("@" + user.uname))) || user_id}*/}
                        { (user && ("@" + user.uname)) || user_id }
                    </span>
                </span>
            );    
    }
   
}

const mapStateToProps = (state, ownProps) => {
    if(ownProps.user || !ownProps.user_id) return {};
    return {
        user: getUser(state, ownProps.user_id)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDisplayLink);