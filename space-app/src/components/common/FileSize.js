'use strict';

import React, {Component, PropTypes} from 'react';
import filesize from 'filesize';

export default class FileSize extends Component {

    render() {
        return (
            <span className="filesize">
                {this.props.value > 0 ? filesize(this.props.value) : '--'}
            </span>
        );
    }
}
