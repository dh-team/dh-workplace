'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class CompanyInfoDiv extends Component {
    render() {
        const space = this.props.space_info;
        return (
          <div className="user-panel">
              <div className="pull-left image">
                  <img src="/static/space/img/team.png" className="img-rounded" alt="Logo" />
              </div>
              <div className="pull-left info">
                  <p>{space.short_name}</p>
                  <a href="#"><i className="fa fa-circle text-success"></i> {space.slogan || ""}</a>
              </div>
          </div>
        );
    }
}

export default CompanyInfoDiv;
