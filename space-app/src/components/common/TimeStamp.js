'use strict';

import React, { Component, PropTypes } from 'react';
import numeral from 'numeral';

export default class TimeStamp extends Component {
    render() {
        const { seconds, clock } = this.props;
        var h = Math.floor(seconds / 3600);
        var m = Math.floor((seconds - h * 3600) / 60);
        var s = ((seconds - h * 3600 - m * 60));
        var str = "";
        if (clock) {
            str = numeral(h).format("00") + ":" + numeral(m).format("00") + ":" + numeral(s).format("00");
        } else {
            if (h > 0) str += (h + "h");
            if (m > 0) str += (m + "m");
            if (h === 0 && s > 0) str += (s + "s");
        }
        return (
            <span className="time-stamp">{str}</span>
        );
    }
}
