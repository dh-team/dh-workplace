'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class MenuGroup extends Component {
    render() {
        const { groups, space_info } = this.props;
        return (
            <li className="treeview active">
                <Link to="/space/group/all"><i className="fa fa-briefcase"></i> <span>{space_info.short_name}</span>
                    <span className="pull-right-container">
                        <i className="fa fa-angle-left pull-right"></i>
                    </span>
                </Link>
                <ul className="treeview-menu">
                    {this.renderItem(groups)}
                </ul>
            </li>
        );
    }

    renderItem(groups) {
        return groups.filter(function (item) {
            return !item.org_info || item.org_info.type !== "unique";
        }).map(function (item) {
            return (
                <li key={item._id} ><Link to={"/space/group/" + item._id}><i className="fa fa-circle-o"></i> {item.name}</Link></li>
            );
        });
    }
}

export default MenuGroup;
