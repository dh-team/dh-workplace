'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Messages extends Component {

    render() {
        return (
            <li className="hide dropdown messages-menu">
                {/* Menu toggle button */}
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    <i className="fa fa-envelope-o"></i>
                    <span className="label label-success">0</span>
                </a>
                <ul className="dropdown-menu">
                    <li className="header">You have 4 messages</li>
                    <li>
                        {/* inner menu: contains the messages */}
                        <ul className="menu">
                            <li>
                                {/* start message */}
                                <a href="#">
                                    <div className="pull-left">
                                        {/* User Image */}
                                        <img src="/static/space/img/avatar.png" className="img-circle" alt="User Image" />
                                    </div>
                                    {/* Message title and timestamp */}
                                    <h4>
                                        Support Team
                                        <small><i className="fa fa-clock-o"></i> 5 mins</small>
                                    </h4>
                                    {/* The message */}
                                    <p>Why not buy a new awesome theme?</p>
                                </a>
                            </li>
                            {/* end message */}
                        </ul>
                        {/* /.menu */}
                    </li>
                    <li className="footer"><a href="#">See All Messages</a></li>
                </ul>
            </li>
        );
    }
}

export default Messages;
