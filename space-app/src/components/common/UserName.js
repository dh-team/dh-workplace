'use strict';

import React, {Component, PropTypes} from 'react';

export default class UserName extends Component {

    render() {     
        const {user} = this.props;
        if(!user) return (<span>--</span>);
        return (
            <a className="user-name" href="#">@{this.props.user.uname}</a>
        );
    }
}
