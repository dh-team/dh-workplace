'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { resetMsg } from '../../actions/action-msg';

class ToastMsg extends Component {
    componentWillReceiveProps(nextProps) {
        var toast = nextProps.toast;
        if (toast && toast.type && toast.content) {
            switch(toast.type){
                case "error":
                    toastr.options.timeOut = 10000;
                    toastr.error(toast.content.content, toast.content.title);    
                    break;
                case "warning":
                    toastr.options.timeOut = 7000;
                    toastr.warning(toast.content.content, toast.content.title);    
                    break;
                case "success":
                    toastr.options.timeOut = 3000;
                    toastr.success(toast.content.content, toast.content.title);    
                    break;
                case "info":
                    toastr.options.timeOut = 5000;
                    toastr.info(toast.content.content, toast.content.title);    
                    break;
            }
            // this.props.resetMsg();
        }
    }
    render() {
        return null;
    }
}

const mapStateToProps = (state) => {
    return {
        toast: state.global_msg.toast,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        resetMsg: () => dispatch(resetMsg())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ToastMsg);