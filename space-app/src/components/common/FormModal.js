'use strict';

import React, { Component, PropTypes } from 'react';

export default class NewFolderModal extends Component {
  componentDidMount() {
    const { id, onShow } = this.props;
    $(this.refs.modal).on('shown.bs.modal', function (e) {
      onShow && onShow();
    });
  }

  render() {
    const { id, title, children, okLabel, cancelLabel, disableSubmit, okCallback, cancelCallback } = this.props;
    // remove tabindex="-1" to fix bug select2 not focus
    return (
      <div className="modal fade" id={id} role="dialog" aria-labelledby={id + "_label"} ref="modal">
        <div className="modal-dialog" role="document">
          <form className="modal-content">

            <div className="modal-header">
              <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 className="modal-title" id={id + "_label"}>{title}</h4>
            </div>

            <div className="modal-body">
              {children}
            </div>

            <div className="modal-footer">
              <button type="button" className="btn btn-default" data-dismiss="modal" onClick={cancelCallback} >{cancelLabel || "Cancel"}</button>
              <button type="submit" disabled={disableSubmit} data-dismiss="modal" className="btn btn-primary" onClick={okCallback} >{okLabel || "Ok"}</button>
            </div>

          </form>
        </div>
      </div>
    );
  }
}
