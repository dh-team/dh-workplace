'use strict';

import React, {Component, PropTypes} from 'react';
import moment from 'moment';

export default class DateDisplay extends Component {
    
    render() {
        var {value, format} = this.props;
        if(!value) return (<span className="datetime">--/--/--</span>);
        if(!format) format = "YYYY-MM-DD HH:mm:ss";
        if(format === "date") format = "YYYY-MM-DD";
        return (
            <span className="datetime">
                {moment(value).format(format)}    
            </span>
        );
    }
}
