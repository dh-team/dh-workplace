'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

export default class Box extends Component {

    render() {
        const {title, type, body} = this.props;
        var cls = "box box-" + (type ? type : "info");
        return (
            <div className={cls}>
                { title &&
                    <div className="box-header with-border">
                        <h3 className="box-title">{title}</h3>
                    </div>
                }

                { body &&
                    <div className="box-body">
                        {this.props.children}
                    </div>
                }

                { !body && this.props.children }

            </div>
        );
    }
}
