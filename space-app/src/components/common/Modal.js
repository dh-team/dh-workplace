'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

export default class Modal extends Component {
    componentDidMount() {
        const {id, onShow} = this.props;
        $("#" + id).on('show.bs.modal', function (e) {
            onShow && onShow();
        });
    }

    render() {
        const {id, title, content, children, okLabel, cancelLabel, disableButtons, okCallback, cancelCallback} = this.props;

        var okClick = function(){
            okCallback && okCallback();
        }

        var cancelClick = function(){
            cancelCallback && cancelCallback();
        }

        return (
            <div className="modal fade" id={id} tabIndex="-1" role="dialog" aria-labelledby={id + "_label"}>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 className="modal-title" id={id + "_label"}>{title}</h4>
                        </div>
                      
                        <div className="modal-body">
                            {content}
                            {children}
                        </div>
                      
                      {!disableButtons &&
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal" onClick={cancelClick} >{cancelLabel || "Cancel"}</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={okClick} >{okLabel || "Ok"}</button>
                        </div>    
                      }
                      
                    </div>
                </div>
            </div>
        );
    }
}
