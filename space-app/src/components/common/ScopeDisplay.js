'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import GroupDisplayLink from './GroupDisplayLink';
import UserDisplayLink from './UserDisplayLink';
import { getSpaceInfo } from '../../selectors/selector-space';

class ScopeDisplay extends React.Component {
    
    render() {
        const {scope, pre_text, space} = this.props;
        if(!scope) return null;
        if(scope === "g:all"){
            return (
                <span className="space-display-link">
                    {pre_text && <span>{pre_text}</span>} &nbsp;
                    <Link to={"/space/"}>
                        {space.short_name}
                    </Link>
                </span>
            );
        }
        if(scope.startsWith("u:")){
            return <UserDisplayLink user_id={scope.substr(2)} pre_text={pre_text}/>
        }
        if(scope.startsWith("g:")){
            return <GroupDisplayLink group_id={scope.substr(2)} pre_text={pre_text}/>
        }
        return null;  
    }
   
}

const mapStateToProps = (state, ownProps) => {
    return {
        space: getSpaceInfo(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ScopeDisplay);