'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class SidebarLeft extends Component {

    render() {
        const {header, description, breadcrumb} = this.props;

        var breadcrumbElm = null;
        if(breadcrumb && breadcrumb.length > 0){
            breadcrumbElm = breadcrumb.map(function(item, index){
                if(index == 0){
                    if(index == breadcrumb.length - 1){
                        return <li className="active"><i className="fa fa-dashboard"></i> {item.text}</li>
                    }else{
                        return <li><Link to={item.link}><i className="fa fa-dashboard"></i> {item.text}</Link></li>
                    }
                }
                if(index == breadcrumb.length - 1){
                    return <li className="active">{item.text}</li>
                }
                return <li><Link to={item.link}> {item.text}</Link></li>
            });
        }

        return (
            <section className="content-header">
                <h1>
                    {header}
                    {description &&
                        <small>{description}</small>
                    }
                </h1>
                {breadcrumbElm &&
                    <ol className="breadcrumb">
                        {breadcrumbElm}
                    </ol>
                }
            </section>
        );
    }
}

export default SidebarLeft;
