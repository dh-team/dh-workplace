'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { getGroup } from '../../selectors/selector-group';

class GroupDisplayLink extends React.Component {
    
    render() {
        const {group, group_id, pre_text} = this.props;
        
        return (
                <span className="group-display-link">
                    {pre_text && <span>{pre_text}</span>} &nbsp;
                    <Link to={"/space/group/" + group_id}>
                        {group ? group.name : "Nhóm"}
                    </Link>
                </span>
            );    
    }
   
}

const mapStateToProps = (state, ownProps) => {
    return {
        group: getGroup(state, ownProps.group_id)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDisplayLink);