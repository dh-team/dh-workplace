'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import NotiticationList from '../notification/NotiticationList';
import { fetchNotificationsReset, fetchNotifications, markNotificationRead } from '../../actions/action-notification';
import { getCurrentUser } from '../../selectors/selector-user';
import { getNotifications, getNotificationCount, getFetchNotificationsStatus } from '../../selectors/selector-notification';

class Notifications extends Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
        this.fetchNotifications = this.fetchNotifications.bind(this);
    }
    
    componentDidMount() {
        var elm = $(this.refs.notificationContainer);
        elm.slimScroll({
            // color: '#00f',
            // size: '10px',
            height: '250px',
            alwaysVisible: true
        });
        
        var thisCom = this;
        elm.scroll(function() {
            var maxScrollTop = elm[0].scrollHeight - elm.height();
            // console.log("slimscroll Reached " + elm.scrollTop() + " " + elm[0].scrollHeight + " " + elm.height());
            if (elm.scrollTop() >= maxScrollTop - 3) {
                console.log("slimscroll Reached bottom");
                thisCom.fetchNotifications();
            }
        });
    }
    
    handleClick(){
        this.props.fetchNotificationsReset();
        this.fetchNotifications();
    }
    
    fetchNotifications(){
        const {fetch_status, fetchNotifications} = this.props;
        if (!fetch_status || !fetch_status.loading) {
            fetchNotifications();
        }
    }
    
    render() {
        const {current_user, notifications, fetch_status, notification_count, markNotificationRead} = this.props;
        return (
            <li className="dropdown dropdown-mega notifications-menu">
                {/* Menu toggle button */}
                <a role="button" className="dropdown-toggle" onClick={this.handleClick}>
                    <i className="fa fa-bell-o"></i>
                    {notification_count.unread > 0 && <span className="label label-warning">{notification_count.unread}</span>}
                </a>
                <ul className="dropdown-menu">
                    <li className="header">Thông báo
                        <span title="Tải thêm thông báo" role="button" onClick={this.fetchNotifications} className="notification-load-more pull-right">
                            <i className="fa fa-ellipsis-h"></i>
                        </span>
                    </li>
                    <li>
                        <div ref="notificationContainer">
                            <NotiticationList notifications={notifications} markNotificationRead={markNotificationRead} current_user={current_user} />
                           
                            <div className="loading-area">
                            {fetch_status && fetch_status.loading && 
                                <img src="/static/space/img/loading.gif" alt="loading" />
                            }     
                            </div>
                        </div>
                    </li>
                    <li className="footer"><a href="#">Xem tất cả</a></li>
                </ul>
            </li>
        );
    }
}



const mapStateToProps = (state) => {
    return {
        notifications: getNotifications(state),
        fetch_status: getFetchNotificationsStatus(state),
        notification_count: getNotificationCount(state),
        current_user: getCurrentUser(state),
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchNotificationsReset: () => dispatch(fetchNotificationsReset()),
        fetchNotifications: (params) => dispatch(fetchNotifications(params)),
        markNotificationRead: (params) => dispatch(markNotificationRead(params))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
