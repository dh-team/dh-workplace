'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class Footer extends Component {

    render() {
        return (
            <footer className="main-footer">
                {/* To the right */}
                <div className="pull-right hidden-xs">
                  
                </div>
                {/* Default to the left */}
                <strong>Copyright &copy; 2016 <a href="#">DongHanh</a>.</strong> All rights reserved.
            </footer>
        );
    }
}

export default Footer;
