'use strict';

import React, { Component, PropTypes } from 'react';

export default class ProgressBar extends Component {

    render() {
        const { className } = this.props;
        let progress = this.props.progress;
        if(!progress) progress = 0;
        if(progress > 100) progress = 100;
        progress = Math.round(progress);

        return (
            <div className={"progress progress-xs active " + className}  title={progress + "%"}>
                <div className={"progress-bar progress-bar-striped " + (progress == 100 ? "progress-bar-success" : "progress-bar-warning")} role="progressbar" aria-valuenow={progress} aria-valuemin="0" aria-valuemax="100" style={{ "width": progress + "%" }}>
                    <span className="sr-only">{progress}% Complete</span>
                </div>
            </div>);
    }
}
