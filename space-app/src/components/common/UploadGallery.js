'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import cookie from 'react-cookie';
import { connect } from 'react-redux';

import FileSize from './FileSize';

import { deleteNode } from "../../actions/action-file";
import { openDialog } from '../../actions/action-msg';

const DOWNLOAD_URL = "/fs/download?id=";
const FILE_URL = "/fs/file?id=";

class UploadGallery extends Component {

    constructor(props) {
        super(props);
        this.fu = null;
        this.customHeaders = {
            'Authorization': "Bearer " + cookie.load("space_token"),
        };

        // bind method
        this.beginUpload = this.beginUpload.bind(this);
        this.uploadProgress = this.uploadProgress.bind(this);
        this.uploadSuccess = this.uploadSuccess.bind(this);
        this.uploadError = this.uploadError.bind(this);
        this.removeClick = this.removeClick.bind(this);
        this.removeUploadedClick = this.removeUploadedClick.bind(this);
        this.confirmDeleteUploaded = this.confirmDeleteUploaded.bind(this);
        this.dispatchFilesChanged = this.dispatchFilesChanged.bind(this);

        this.state = {
            uploaded: [], // [{id, name, size, mime_type}] 
            upload: {}, // [id]: {id, name, uploaded_bytes, total_bytes, status, error, response, thumbDrawed}
        }

        this.thumbImg = {};
        this.$gallery = null;
    }

    beginUpload(params) {
        var upload = this.state.upload;
        upload[params.id] = { ...params, status: "uploading" };
        this.setState({
            upload: upload
        });
    }

    uploadProgress(params) {
        if (!this.state.upload[params.id]) return;

        var upload = this.state.upload;
        upload[params.id].uploaded_bytes = params.uploaded_bytes;
        upload[params.id].total_bytes = params.total_bytes;
        this.setState({
            upload: upload
        });
    }

    uploadSuccess(params) {
        if (!this.state.upload[params.id]) return;

        var upload = this.state.upload;
        upload[params.id].status = "success";
        if (params.response.uploaded && params.response.uploaded.length > 0) {
            upload[params.id].file = params.response.uploaded[0];
        }
        this.setState({
            upload: upload
        });

        this.dispatchFilesChanged(this.state.uploaded, upload);
    }

    uploadError(params) {
        if (!this.state.upload[params.id]) return;

        var upload = this.state.upload;
        upload[params.id].status = "error";
        upload[params.id].error = params.error;
        this.setState({
            upload: upload
        });
    }

    removeClick(id) {
        var uploadFile = this.state.upload[id];
        if (!uploadFile) return;

        if (uploadFile.status === "uploading") {
            console.log("cancel upload " + id);
            this.fu.cancel(id);
        }

        // remove from state
        var upload = this.state.upload;
        delete upload[id];
        this.setState({
            upload: upload
        });

        this.dispatchFilesChanged(this.state.uploaded, upload);

        // permanent delete in server
        if (uploadFile.file && uploadFile.file.id) {
            this.props.deleteNode(uploadFile.file.id, true);
        }
    }

    removeUploadedClick(id) {
        var thisCom = this;
        this.props.openDialog({
            title: "Xóa file",
            content: "Bạn chắc chắn muốn xóa file này?",
            confirmCallback: () => thisCom.confirmDeleteUploaded(id)
        });
    }

    confirmDeleteUploaded(id) {
        console.log("delete file", id);

        // remove from state
        var uploaded = this.state.uploaded.filter((item) => {
            return item.id !== id;
        });
        this.setState({
            uploaded: uploaded
        });

        this.dispatchFilesChanged(uploaded, this.state.upload);

        // permanent delete in server
        this.props.deleteNode(id, true);
    }

    dispatchFilesChanged(uploaded, upload) {
        if (!this.props.onFilesChanged) return;

        var files = [...uploaded];
        Object.keys(upload).map((id) => {
            var item = upload[id];
            if (!item || !item.file || item.status !== "success") return;
            files.push({
                id: item.file.id,
                name: item.file.name,
                mime_type: item.file.mime_type,
                size: item.file.size
            });
        });

        this.props.onFilesChanged(files);
    }

    componentDidMount() {
        const { target, display_type, files } = this.props;
        if (files) this.setState({ uploaded: files });
        const thisCom = this;

        // config fine uploader
        this.customHeaders["Args"] = JSON.stringify({
            app: (target && target.app) ? target.app : "LivePost",
            partition: (target && target.partition) ? target.partition : "g:all",
            parent: (target && target.id) ? target.id : null,
            exist_action: "rename"
        });
        this.fu = new qq.FineUploaderBasic({
            text: {
                fileInputTitle: "Tải file lên thư mục này"
            },
            button: this.refs.fileUpload,
            validation: {
                allowedExtensions: display_type === "gallery" ? ["jpg", "jpeg", "png", "gif", "bmp"] : [],
                itemLimit: 20,
                sizeLimit: 10 * 1024 * 1024 // 10MB
            },
            request: {
                inputName: 'file',
                endpoint: '/fs/upload',
                customHeaders: this.customHeaders
            },
            callbacks: {
                onUpload: function (id, name, responseJSON, xhr) {
                    console.log("onUpload: " + id + ", " + name);
                    // thisCom.fu.drawThumbnail(id, thisCom.refs.imgThumb, 120);
                    thisCom.beginUpload({ id, name });

                },
                onComplete: function (id, name, responseJSON, xhr) {
                    console.log("onComplete: " + id + ", " + name + ", " + JSON.stringify(responseJSON));
                    if (responseJSON && responseJSON.success) {
                        thisCom.uploadSuccess({ id: id, response: responseJSON });
                    }
                },
                onAllComplete: function (succeeded, failed) {
                    console.log("onAllComplete: " + JSON.stringify(succeeded) + ", " + JSON.stringify(failed));
                },
                onError: function (id, name, errorReason, xhr) {
                    console.log("onError: " + id + ", " + name + ", " + errorReason);
                    thisCom.uploadError({ id: id, error: errorReason });
                },
                onProgress: function (id, name, uploadedBytes, totalBytes) {
                    console.log("onProgress: " + id + ", " + name + ", " + uploadedBytes + ", " + totalBytes);
                    thisCom.uploadProgress({ id: id, uploaded_bytes: uploadedBytes, total_bytes: totalBytes });
                }
            }
        });


    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.files && JSON.stringify(nextProps.files) !== JSON.stringify(this.props.files)) {
            this.setState({
                uploaded: nextProps.files,
                upload: {}
            });
        }
    }

    componentDidUpdate() {
        if (this.props.display_type !== "gallery") {
            if (this.$gallery) {
                this.$gallery.data('lightGallery').destroy(true);
            } else {
                this.$gallery = $(this.refs.uploadPanel);
            }

            this.$gallery.lightGallery({
                selector: '.upload-box-gallery-item',
                thumbnail: false
            });
            console.log('lightGallery');
            return;
        }

        // draw thumbnail for upload image
        var upload = Object.keys(this.state.upload).map((id) => this.state.upload[id]);
        upload.map((item) => {
            if (!item.thumbDrawed) {
                console.log("draw thumb " + item.id);
                this.fu.drawThumbnail(item.id, this.thumbImg[item.id], 150);
                item.thumbDrawed = true;
            }
        });
    }

    componentWillUnmount() {
        if (this.$gallery) {
            this.$gallery.data('lightGallery').destroy(true);
        }
    }

    isImage(file) {
        if (!file || !file.mime_type) return false;
        return file.mime_type.indexOf('image') !== -1;
    }


    render() {
        const { display_type, read_only } = this.props;
        const { uploaded } = this.state; // uploaded: [{id, name, size, mime_type}] 
        const uploadState = this.state.upload;
        var upload = Object.keys(uploadState).map((id) => uploadState[id]);
        const thisCom = this;


        return (
            <div ref="uploadPanel" id="uploadBox" className={"upload-box upload-box-" + display_type}>

                {uploaded && uploaded.map((item) => {
                    return (
                        <div key={item.id} className={"upload-box-item upload-box-item-uploaded upload-box-item-success"}>


                            {display_type === "gallery" &&
                                <div className="upload-box-item-thumb">
                                    <img src={FILE_URL + item.id} alt={item.name} />
                                </div>
                            }

                            <div className="upload-box-item-caption">
                                <p className="upload-box-item-name">
                                    {thisCom.isImage(item) &&
                                        <a href={FILE_URL + item.id} className="upload-box-gallery-item" >
                                            {item.name}
                                        </a>
                                    }
                                    {!thisCom.isImage(item) &&
                                        <a href={DOWNLOAD_URL + item.id}>
                                            {item.name}
                                        </a>
                                    }
                                </p>
                                <p className="upload-box-item-size">
                                    <FileSize value={item.size} />
                                    {!read_only &&
                                        <a onClick={() => thisCom.removeUploadedClick(item.id)} className="upload-box-item-remove" role="button">
                                            <i className="fa fa-trash"></i>
                                        </a>
                                    }
                                </p>
                            </div>
                        </div>
                    )
                })}

                {upload.map((item) => {
                    var uploadPercent = item.total_bytes > 0 ? Math.round(item.uploaded_bytes * 100 / item.total_bytes) : 0;

                    return (

                        <div key={item.id} className={"upload-box-item upload-box-item-" + item.status}>


                            {display_type === "gallery" &&
                                <div className="upload-box-item-thumb">
                                    <img ref={(elm) => { thisCom.thumbImg[item.id] = elm; }} />
                                </div>
                            }

                            <div className="upload-box-item-caption">
                                <p className="upload-box-item-name">
                                    {item.name}
                                </p>
                                <p className="upload-box-item-size">
                                    <FileSize value={item.total_bytes} />
                                    <a onClick={() => thisCom.removeClick(item.id)} className="upload-box-item-remove" role="button">
                                        <i className="fa fa-trash"></i>
                                    </a>
                                </p>

                                <div className="progress progress-xxs">
                                    <div className={"progress-bar progress-bar-striped " + (item.status === "success" ? "progress-bar-success" : "progress-bar-warning")}
                                        role="progressbar" aria-valuenow={uploadPercent} aria-valuemin="0" aria-valuemax="100" style={{ "width": uploadPercent + "%" }}>
                                        <span className="sr-only">{uploadPercent}% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    )
                })}

                {!read_only &&
                    <div className="upload-box-item-add">
                        <div ref="fileUpload" className="upload-button" title="Upload">
                            <div className="upload-button-text"> <i className="fa fa-plus"></i> Thêm {display_type === "gallery" ? "ảnh" : "file"}</div>
                        </div>
                    </div>
                }

            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        deleteNode: (data) => dispatch(deleteNode(data)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadGallery);
