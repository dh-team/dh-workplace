'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import { getCurrentUser } from '../../selectors/selector-user';

class UserMenu extends Component {

    render() {
        const user = this.props.user;

        return (
            <li className="dropdown user user-menu">
                {/* Menu Toggle Button */}
                <a href="#" className="dropdown-toggle" data-toggle="dropdown">
                    {/* The user image in the navbar*/}
                    <img src="/static/space/img/avatar.png" className="user-image" alt="User Image" />
                    {/* hidden-xs hides the username on small devices so only the image appears. */}
                    <span className="hidden-xs">{user.name}</span>
                </a>
                <ul className="dropdown-menu">
                    {/* The user image in the menu */}
                    <li className="user-header">
                        <img src="/static/space/img/avatar.png" className="img-circle" alt="User Image" />

                        <p>
                            {user.name} - @{user.uname}
                            {user.roles.indexOf("administrator") >= 0 &&
                                <small>Administrator</small>
                            }
                        </p>
                    </li>
                    {/* Menu Body
                    <li className="user-body">
                        <div className="row">
                            <div className="col-xs-4 text-center">
                                <a href="#">Admin</a>
                            </div>
                            <div className="col-xs-4 text-center">
                                <a href="#">Sale Manager</a>
                            </div>
                            <div className="col-xs-4 text-center">
                                <a href="#">Friends</a>
                            </div>
                        </div>
                    </li>
                    */}
                    {/* Menu Footer*/}
                    <li className="user-footer">
                        <div className="pull-left">
                            <Link href="/ws/space/list" className="btn btn-default btn-flat">Chuyển space</Link>
                        </div>
                        <div className="pull-right">
                            <a href="/space/logout" className="btn btn-default btn-flat">Đăng xuất</a>
                        </div>
                    </li>
                </ul>
            </li>
        );
    }


}

const mapStateToProps = (state) => ({
    user: getCurrentUser(state),
});

const mapDispatchToProps = ({
});
export default connect(mapStateToProps, mapDispatchToProps)(UserMenu);
