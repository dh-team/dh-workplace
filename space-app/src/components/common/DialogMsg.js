'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { closeDialog } from '../../actions/action-msg';

class DialogMsg extends Component {
    constructor() {
        super();
        // bind method
        this.dialog = null;
        this.handleCancelClick = this.handleCancelClick.bind(this);
        this.handleConfirmClick = this.handleConfirmClick.bind(this);
    }
    
    componentDidMount() {
        this.dialog = $(this.refs.dialog);
        const {closeDialog} = this.props;
        
        this.dialog.on("hidden.bs.modal", function(){
            closeDialog();
        });
    }
    
    handleCancelClick(){
        const {dialog: {cancelCallback}} = this.props;
        cancelCallback && cancelCallback();
    }
    
    handleConfirmClick(){
        const {dialog: {confirmCallback}} = this.props;
        confirmCallback && confirmCallback();
    }
    
    componentWillReceiveProps(nextProps) {
        var dialog = nextProps.dialog;
        if(dialog && dialog.showing){
            this.dialog.modal('show');
        }
    }
    render() {
        const {dialog} = this.props;
        return (
            <div ref="dialog" className="modal fade" tabIndex="-1" role="dialog" aria-labelledby="globalDialogLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        {dialog.title &&
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 className="modal-title" id="globalDialogLabel">{dialog.title}</h4>
                            </div>
                        }
                        
                        <div className="modal-body">
                            {dialog.content}
                        </div>
                      
                        <div className="modal-footer">
                            <button type="button" className="btn btn-default" data-dismiss="modal" onClick={this.handleCancelClick} >Cancel</button>
                            <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={this.handleConfirmClick} >Ok</button>
                        </div>
                      
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        dialog: state.global_msg.dialog,
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        closeDialog: () => dispatch(closeDialog()),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DialogMsg);