'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import CompanyInfoDiv from './CompanyInfoDiv';
import MenuGroup from './MenuGroup';

class SidebarLeft extends Component {

    render() {
        const { user_profile, space_info, groups } = this.props;
        return (
            <aside className="main-sidebar">

                {/* sidebar: style can be found in sidebar.less */}
                <section className="sidebar">

                    {/* Sidebar company info */}
                    <CompanyInfoDiv space_info={space_info} />

                    {/* Sidebar Menu */}
                    <ul className="sidebar-menu">
                        {/* Optionally, you can add icons to the links */}
                        <li className="active"><Link to="/space/"><i className="fa fa-home"></i> <span>Trang chủ</span></Link></li>
                        <li className="hide"><Link to="/space/profile"><i className="fa fa-user"></i> <span>Profile </span></Link></li>

                        {user_profile.roles.indexOf("administrator") != -1 &&
                            <li className="treeview">
                                <Link to="/space"><i className="fa fa-cog"></i> <span>Space</span>
                                    <span className="pull-right-container">
                                        <i className="fa fa-angle-left pull-right"></i>
                                    </span>
                                </Link>
                                <ul className="treeview-menu">
                                    <li><Link to="/space/setting"><i className="fa fa-circle-o"></i> Thông tin space</Link></li>
                                    <li><Link to="/space/groups"><i className="fa fa-circle-o "></i> Sơ đồ tổ chức</Link></li>
                                    <li><Link to="/space/users"><i className="fa fa-circle-o "></i> Thành viên</Link></li>
                                </ul>
                            </li>
                        }

                        <li className="hide"><Link to="/space/mail"><i className="fa fa-envelope"></i> <span>Email</span>
                            <span className="pull-right-container">
                                <span className="label label-primary pull-right">4</span>
                            </span>
                        </Link>
                        </li>

                        <li className="header">GROUPs</li>
                        
                        <MenuGroup groups={groups} space_info={space_info} />

                        <li className="header">APPs</li>
                        <li><Link to="/space/file"><i className="fa fa-folder"></i> <span>File</span></Link></li>
                        <li><Link to="/space/wiki"><i className="fa fa-book"></i> <span>Wiki</span></Link></li>

                        <li className="treeview active">
                            <a href="#"><i className="fa fa-suitcase"></i> <span>Công việc</span>
                                <span className="pull-right-container">
                                    <i className="fa fa-angle-left pull-right"></i>
                                </span>
                            </a>
                            <ul className="treeview-menu">
                                <li><Link to="/space/objective"><i className="fa fa-crosshairs"></i> <span>OKR</span></Link></li>
                                <li><Link to="/space/work"><i className="fa fa-clock-o"></i> <span>Hiện tại</span></Link></li>
                                <li><Link to="/space/work/all"><i className="fa fa-tasks"></i> <span>Tất cả</span></Link></li>
                                <li><Link to="/space/work/kanban"><i className="fa fa-columns"></i> <span>Kanban</span></Link></li>
                            </ul>
                        </li>


                        <li className="hide"><a href="/space/calendar"><i className="fa fa-calendar"></i> <span>Calendar</span></a></li>

                    </ul>
                    {/* /.sidebar-menu */}
                </section>
                {/* /.sidebar */}
            </aside>
        );
    }
}

export default SidebarLeft;
