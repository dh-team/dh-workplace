'use strict';


import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import Messages from './Messages';
import Notifications from './Notifications';
import UserMenu from './UserMenu';

class Header extends Component {

    render() {
        // const user = this.props.user;

        return (
            <header className="main-header">
            {/* Logo */}
            <a href="/space/" className="logo">
                {/* mini logo for sidebar mini 50x50 pixels */}
                <span className="logo-mini"><b>Up</b></span>
                {/* logo for regular state and mobile devices */}
                <span className="logo-lg"><b>Upspace</b></span>
            </a>

            {/* Header Navbar */}
            <nav className="navbar navbar-static-top" role="navigation">
                {/* Sidebar toggle button*/}
                <a href="#" className="sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span className="sr-only">Toggle navigation</span>
                </a>
                {/* Navbar Right Menu */}
                <div className="navbar-custom-menu">
                    <ul className="nav navbar-nav">
                        {/* Messages: style can be found in dropdown.less*/}
                        <Messages />
                        {/* /.messages-menu */}

                        {/* Notifications Menu */}
                        <Notifications />

                        <UserMenu />
                        {/* Control Sidebar Toggle Button */}
                        <li>
                            <a href="#" data-toggle="control-sidebar"><i className="fa fa-gears"></i></a>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>
        );
    }
}

export default Header;
