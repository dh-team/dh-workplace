'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

class SidebarRight extends Component {

    render() {
        return (
            <aside className="control-sidebar control-sidebar-light">
                
                <ul className="nav nav-tabs nav-justified control-sidebar-tabs">
                    <li className="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i className="fa fa-home"></i></a></li>
                    <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i className="fa fa-gears"></i></a></li>
                </ul>
                
                <div className="tab-content">
                    
                    <div className="tab-pane active" id="control-sidebar-home-tab">
                        <h3 className="control-sidebar-heading">Activities</h3>
                    </div>
                   
                    <div className="tab-pane" id="control-sidebar-settings-tab">
                        <h3 className="control-sidebar-heading">Chat</h3>
                    </div>
                    
                </div>
            </aside>
        );
    }
}

export default SidebarRight;
