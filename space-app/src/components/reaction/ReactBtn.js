'use strict';

import React, {Component, PropTypes} from 'react';
import ReactDOMServer from 'react-dom/server';
import { connect } from 'react-redux';
import UserName from '../common/UserName';
import { userReact, undoUserReact } from '../../actions/action-reaction';
import { getReactionStream } from '../../selectors/selector-reaction';

class ReactBtn extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }
    
    componentDidMount() {
        // const reactContainer = this.refs.reactContainer;
        // $(reactContainer).find(".react-users ").popover();
        var elmID = "#react_users_" + this.props.stream_id;
        if ($(elmID).length > 0) {
            $(elmID).popover();
        }
    }
    
    handleClick(action){
        const {data, current_user, userReact, undoUserReact} = this.props;
        
        let reacted = null;
        if (data.reaction[action]) {
            reacted = data.reaction[action].find((item) => item.issue_user.user_id === current_user.user_id);
        }
        
        const params = {stream_id: data._id, action: action};
        if (reacted) {
            console.log("Undo reaction", params);
            undoUserReact(params);
        }
        else {
            console.log("React", params);
            userReact(params);
        }
    }
    
    render() {      
        const {data, current_user} = this.props;
        if(!data) return null;
        
        let liked = null;
        const likedData = data.reaction.liked;
        if (likedData) {
            liked = likedData.find((item) => item.issue_user.user_id === current_user.user_id);
        }
                             
        return (
            <div className="react-users-container">
                <a className="btn btn-xs btn-warning react-btn" role="button" onClick={() => this.handleClick("liked")}>
                    <i className="fa fa-thumbs-up"></i> {liked ? "Unlike" : "Like"} &nbsp;
                    <span className="badge react-count">{likedData ? likedData.length : 0 }</span>
                </a>
                
                
                {likedData && this.renderUsers(likedData, data._id)}
                
            </div>
        );
    }
    
    renderUsers(reactions, streamId){
        var popoverContent = (
            <div className="react-users-popover">
                <ul>
                    {reactions.map(function(item){
                        return <li key={item.issue_user.user_id}><UserName user={item.issue_user} /></li>;
                    })} 
                </ul>
            </div>);
            
        if(reactions.length <= 2){
            return (
                    <div id={"react_users_" + streamId} className="react-users text-primary" data-toggle="popover" data-html="true" 
                         data-trigger="click" data-placement="top" data-content={ReactDOMServer.renderToString(popoverContent)}>
                        {
                            reactions.map(function(item, index){
                                return <span key={item.issue_user.user_id}>@{item.issue_user.uname}{index < reactions.length - 1 ? ", " : ""} </span>;
                            })
                        }
                    </div>
            );
        }
                             
        return (
            <div id={"react_users_" + streamId} className="react-users text-primary" data-toggle="popover" data-html="true" 
                 data-trigger="click" data-placement="top" data-content={ReactDOMServer.renderToString(popoverContent)}>
                    {
                        reactions.slice(0, 2).map(function(item, index){
                            return <span key={item.issue_user.user_id}>@{item.issue_user.uname}{index === 0 ? ", " : ""} </span>;
                        })
                    }
                    
                    <span> và {reactions.length - 2} người khác</span>
            </div>
        );
    }
    
}

const mapStateToProps = (state, ownProps) => {
    return {
        data: getReactionStream(state, ownProps.stream_id) || {_id: ownProps.stream_id, reaction: {}}
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        userReact: (params) => dispatch(userReact(params)),
        undoUserReact: (params) => dispatch(undoUserReact(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ReactBtn);
