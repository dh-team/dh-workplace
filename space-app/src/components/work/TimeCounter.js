'use strict';

import React, { Component, PropTypes } from 'react';
import numeral from 'numeral';
import TimeStamp from "../common/TimeStamp";

export default class TimeCounter extends Component {
    constructor() {
        super();

        this.state = {
            counter: 0,
            start_time: null
        };
        this.timer = null;
    }

    componentDidMount() {
        const { start_time } = this.props;
        this.setState({ start_time: new Date(start_time) });
        const thisCom = this;
        this.timer = setInterval(function () {
            thisCom.setState({
                counter: Math.round((new Date() - thisCom.state.start_time) / 1000)
            });
        }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timer);
    }

    render() {
        const { value } = this.props;
        return (
            <span className="time-counter"><TimeStamp seconds={this.state.counter} clock /></span>
        );
    }
}
