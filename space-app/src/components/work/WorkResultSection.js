'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';

import valid from '../../utils/validate';
import workUtil from '../../utils/work';

import Editor from '../form/Editor';
import UploadGallery from '../common/UploadGallery';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.detail, 0, 5000)) {
        errors.detail = 'Độ dài tối đa 5000 ký tự';
    }

    return errors;
}

class WorkResultSection extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.submitEditing = this.submitEditing.bind(this);
        this.resetEditing = this.resetEditing.bind(this);
        this.initForm = this.initForm.bind(this);
        this.handleAttachFiles = this.handleAttachFiles.bind(this);

        this.state = {
            attachments: [],
            editing: {}
        };
    }

    componentDidMount() {
        this.initForm(this.props.work);
    }

    componentWillReceiveProps(nextProps) {
        // change work
        if (nextProps.work && this.props.work && nextProps.work._id !== this.props.work._id) {
            console.log("change work id");
            this.initForm(nextProps.work);
        }
    }

    initForm(work) {
        const { initialize, reset } = this.props;
        initialize({
            note: work.result && work.result.note,
        });
        reset();
        // set attachments
        this.setState({ attachments: work.result && work.result.attachments });
    }

    startEdit(field) {
        var editing = {
            ...this.state.editing,
            [field]: true
        };
        this.setState({
            editing: editing
        });
    }

    submitEditing(values, dispatch) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "result",
            id: work._id,
            result: {
                note: values.note,
            }
        };

        editWork(data);
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    resetEditing() {
        this.props.reset();
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    handleAttachFiles(files) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "result",
            id: work._id,
            result: {
                attachments: files
            }
        };

        editWork(data);
    }

    render() {
        const { work, current_user } = this.props;
        const { editing, attachments } = this.state;
        const { handleSubmit, pristine, invalid, submitting } = this.props;

        const enableEdit = work.resource && work.resource.performer === current_user.user_id;

        return (
            <div className="box box-simple">

                <div className="box-header with-border">
                    <h3 className="box-title">Kết quả công việc</h3>

                    {enableEdit &&
                        <div className="box-tools btn-group">
                            <button disabled={submitting} onClick={this.resetEditing} className="btn btn-sm btn-default" title="Hủy bỏ thay đổi">
                                <i className="fa fa-undo"></i>
                            </button>
                            <button disabled={invalid || pristine || submitting} onClick={handleSubmit(this.submitEditing)} className="btn btn-sm btn-default" title="Lưu thay đổi">
                                <i className="fa fa-save"></i>
                            </button>
                        </div>
                    }
                </div>

                <div className="box-body">
                    <div className="box box-flat">
                        <div className="box-header with-border">
                            <h3 className="box-title">
                                Ghi chú: {enableEdit && <a role="button" onClick={() => this.startEdit("note")}><i className="fa fa-edit"></i></a>}
                            </h3>

                            <div className="box-tools">
                                <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div className="box-body">
                            {!editing["note"] &&
                                <div className="work-result-note" dangerouslySetInnerHTML={{ __html: work.result && work.result.note }}>
                                </div>
                            }
                            {editing["note"] &&
                                <Field component={Editor} name="note" />
                            }
                        </div>
                    </div>

                    <div className="box box-flat collapsed-box">
                        <div className="box-header with-border">
                            <h3 className="box-title">
                                File đính kèm: ({(attachments && attachments.length) || 0})
                            </h3>

                            <div className="box-tools">
                                <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div className="box-body">
                            <UploadGallery files={attachments} display_type="list" onFilesChanged={this.handleAttachFiles} read_only={!enableEdit} />
                        </div>
                    </div>

                </div>
            </div>
        );
    }
}

const form = 'editWorkResult';
const fields = ["note"];
export default reduxForm({ form, validate, fields })(WorkResultSection);
