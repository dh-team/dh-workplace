'use strict';

import React, { Component, PropTypes } from 'react';
import GroupDisplayLink from '../group/GroupDisplayLink';
export default class WorkSource extends Component {

    render() {
        const { source, hide_reference } = this.props;
        if(!source) return null;
        var item = <span className="label label-default">Cá nhân</span>;
        switch (source.type) {
            case "personal":
                item = <span className="label label-default">Cá nhân</span>;
                break;
            case "team":
                item = <span className="label label-warning">Nhóm</span>;
                break;
            case "project":
                item = <span className="label label-primary">Dự án</span>;
                break;
            case "business":
                item = <span className="label label-success">Nghiệp vụ</span>;
                break;            
        }
        if(hide_reference || source.type === "personal") return item;
        
        return (
                <span>
                    {item}
                    <GroupDisplayLink group_id={source.reference}/>
                </span>
            )
    }
}
