'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import WorkContentSection from './WorkContentSection';
import WorkStatusSection from './WorkStatusSection';
import WorkTagsSection from './WorkTagsSection';
import WorkResourceSection from './WorkResourceSection';
import WorkPlanningSection from './WorkPlanningSection';
import WorkList from './WorkList';
import WorkDoingSection from './WorkDoingSection';
import WorkResultSection from './WorkResultSection';
import CommentBox from '../comment/CommentBox';
import ProgressBar from '../common/ProgressBar';

import { openDialog } from '../../actions/action-msg';
import { editWork, deleteWork, editWorkTask, deleteWorkTask, addWorkTask } from '../../actions/action-work';
import { getWork, getSubworks, getWorkHierarchy, getDeleteWorkStatus } from '../../selectors/selector-work';
import { getCurrentUser } from '../../selectors/selector-user';
import { getEditWorkTaskStatus } from '../../selectors/selector-work';

import workUtil from '../../utils/work';

class WorkDetail extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.handleDelete = this.handleDelete.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
        const { work } = this.props;
        if (nextProps.delete_status && this.props.delete_status && this.props.delete_status.loading && !nextProps.delete_status.loading && !nextProps.delete_status.error) {
            console.log("delete work success");
            if (work && work.parent) {
                this.context.router.push('/space/work/' + work.parent);
            }
            else {
                this.context.router.push('/space/work');
            }
        }
    }

    handleDelete() {
        this.props.openDialog({
            title: "Xóa công việc",
            content: "Bạn chắc chắn muốn xóa công việc này?",
            confirmCallback: this.confirmDelete
        });
    }

    confirmDelete() {
        const { work_id, deleteWork } = this.props;
        deleteWork(work_id);
    }


    render() {
        const { work, subworks, work_hierarchy, current_user, edit_task_status } = this.props;
        const { editWork, addWorkTask, editWorkTask, deleteWorkTask, openDialog } = this.props;

        if (!work) return null;
        
        const status = work.doing && work.doing.status;
        const showDoing = !work.child_number && workUtil.compareStatus(status, "doing") >= 0;
        const showResult = workUtil.compareStatus(status, "doing") >= 0;        
        const progress = (work.doing && work.doing.progress) || 0;
        
        return (
            <div className="work-detail">
                <div className="row">
                    <div className="col-md-3 col-md-push-9">
                        <WorkStatusSection work={work} current_user={current_user} editWork={editWork} />
                        <WorkTagsSection work={work} current_user={current_user} editWork={editWork} />
                        <WorkPlanningSection work={work} current_user={current_user} editWork={editWork} />

                        {work.source && work.source.type !== "personal" &&
                            <WorkResourceSection work={work} current_user={current_user} editWork={editWork} />
                        }
                    </div>

                    <div className="col-md-9 col-md-pull-3">
                        <WorkContentSection work={work} work_hierarchy={work_hierarchy} current_user={current_user} editWork={editWork} deleteWork={this.handleDelete} />
                        {work && subworks && subworks.length > 0 &&
                            <div className="box box-simple">
                                <div className="box-body no-padding">
                                    <ProgressBar progress={progress} className="work-detail-progress " />                                     

                                    <div className="table-responsive">
                                        <WorkList works={subworks} columns={{ name: true, performer: true, start_date: true, due_date: true, status: true, progress: true }} />
                                    </div>

                                </div>
                            </div>
                        }

                        {showDoing &&
                            <WorkDoingSection work={work} edit_task_status={edit_task_status} current_user={current_user} editWork={editWork} addWorkTask={addWorkTask} editWorkTask={editWorkTask} deleteWorkTask={deleteWorkTask} openDialog={openDialog} />
                        }

                        {showResult &&
                            <WorkResultSection work={work} current_user={current_user} editWork={editWork} />
                        }

                        {work && work.comment_stream &&
                            <CommentBox stream_id={work.comment_stream} />
                        }
                    </div>
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        work: getWork(state, ownProps.work_id),
        subworks: getSubworks(state, ownProps.work_id),
        work_hierarchy: getWorkHierarchy(state, ownProps.work_id),
        delete_status: getDeleteWorkStatus(state),
        edit_task_status: getEditWorkTaskStatus(state),
        current_user: getCurrentUser(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        editWork: (data) => dispatch(editWork(data)),
        deleteWork: (data) => dispatch(deleteWork(data)),
        editWorkTask: (params) => dispatch(editWorkTask(params)),
        deleteWorkTask: (params) => dispatch(deleteWorkTask(params)),
        addWorkTask: (params) => dispatch(addWorkTask(params)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkDetail);
