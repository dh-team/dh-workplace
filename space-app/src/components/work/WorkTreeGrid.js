'use strict';

import React from 'react';
import { Link } from 'react-router';
import DateDisplay from '../common/DateDisplay';
import UserDisplayLink from '../common/UserDisplayLink';
import WorkStatusAction from './WorkStatusAction';
import WorkSource from './WorkSource';

export default class WorkTreeGrid extends React.Component {
    constructor() {
        super();

        // this.buildGridData = 
    }

    componentDidMount() {
        const { works } = this.props;
        const data = this.buildGridData(works);

        var grida = webix.ui({
            container: "workTreeGrid",
            view: "treetable",
            columns: [
                { id: "_id", header: "", css: { "text-align": "right" }, width: 50 },
                {
                    id: "name", header: "Công việc", width: 250,
                    template: "{common.treetable()} #name#"
                },
                { id: "chapter", header: "Mode", width: 200 }
            ],
            autoheight: true,
            autowidth: true,

            data: data
        });
    }

    buildGridData(works) {
        var dict = {};
        var list = [];
        works.map((item) => {
            var dataItem = this.mutableFromDict(item._id, dict);
            Object.assign(dataItem, { _id: item._id, name: item.name, parent: item.parent });
            if (!dataItem.parent) {
                list.push(dataItem);
            } else {
                var parentItem = this.mutableFromDict(dataItem.parent, dict);
                parentItem.data.push(dataItem);
            }
        });
        return list;
    }

    mutableFromDict(id, dict) {
        var item = dict[id];
        if (!item) {
            item = {
                data: []
            }
            dict[id] = item;
        }
        return item;
    }

    render() {
        const { works, columns, current_user, editWork } = this.props;
        const cols = columns || { name: true, source: true, start_date: true, due_date: true, status: true, assignment_response: true };

        return (
            <div id="workTreeGrid"></div>
        );
    }
}
