'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import WorkStatus from './WorkStatus'
// import { connect } from 'react-redux';
// import { editWork} from '../../actions/action-work';
// import { getCurrentUser } from '../../selectors/selector-user';

class WorkStatusAction extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.updateWorkStatus = this.updateWorkStatus.bind(this);
    }

    componentDidMount() {
    }

    updateWorkStatus(status) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "status",
            id: work._id,
            status: status,
        };
        editWork(data);
    }

    render() {
        const { work, current_user, className } = this.props;
        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const status = work.doing && work.doing.status;

        const disable_action = status !== "assigned"  && work.child_number > 0;


        return (
            <div className={"work-status-action " + className}>
                <div className="work-status">
                    <span className="work-status-label">Trạng thái: </span> <WorkStatus value={work.doing && work.doing.status} />
                </div>

                {!disable_action && status === "assigned" && isPerformer &&
                    <div className="work-action">
                        <span>Công việc này được giao cho bạn. </span>
                        <a role="button" title="Đồng ý nhận việc này" className="btn btn-xs" onClick={() => this.updateWorkStatus("assigned_accept")}><i className="fa fa-check"></i> <span>Đồng ý</span></a>
                        <span> hoặc </span>
                        <a role="button" title="Từ chối nhận việc này" className="btn btn-xs" onClick={() => this.updateWorkStatus("assigned_reject")}><i className="fa fa-ban"></i> <span>Từ chối</span></a>
                    </div>
                }

                {!disable_action && status === "assigned_accept" && isPerformer &&
                    <div className="work-action">
                        <span>Công việc này chưa được thực hiện. Chuyển vào Todo</span>
                        <a role="button" title="Chuyển vào Todo trong bảng Kanban" className="btn btn-xs" onClick={() => this.updateWorkStatus("todo")}><i className="fa fa-list-ul"></i> <span>Todo</span></a>
                    </div>
                }

                {!disable_action && status === "todo" && isPerformer &&
                    <div className="work-action">
                        <span>Bắt đầu thực hiện </span>
                        <a role="button" title="Chuyển vào Doing trong bảng Kanban" className="btn btn-xs" onClick={() => this.updateWorkStatus("doing")}><i className="fa fa-play-circle-o"></i> <span>Doing</span></a>
                    </div>
                }

                {!disable_action && status === "doing" && isPerformer &&
                    <div className="work-action">
                        <span>Đánh dấu làm xong </span>
                        <a role="button" title="Chuyển vào Done trong bảng Kanban" className="btn btn-xs" onClick={() => this.updateWorkStatus("done")}><i className="fa fa-check-circle"></i> <span>Done</span></a>
                    </div>
                }

                {!disable_action && status === "done" && isMonitor &&
                    <div className="work-action">
                        <span>Công việc này cần bạn kiểm tra và phê duyệt. Đánh dấu </span>
                        <a role="button" title="Phê duyệt công việc đạt yêu cầu" className="btn btn-xs" onClick={() => this.updateWorkStatus("completed")}><i className="fa fa-check"></i> <span>Đạt</span></a>
                        <span> hoặc </span>
                        <a role="button" title="Công việc chưa đạt cần tiếp tục thực hiện" className="btn btn-xs" onClick={() => this.updateWorkStatus("redo")}><i className="fa fa-ban"></i> <span>Chưa đạt</span></a>
                    </div>
                }

                {!disable_action && status === "completed" && isMonitor &&
                    <div className="work-action">
                        <span>Công việc đã được bạn phê duyệt là hoàn thành. </span>
                        <a role="button" title="Bỏ phê duyệt công việc" className="btn btn-xs" onClick={() => this.updateWorkStatus("done")}><i className="fa fa-rotate-left"></i> <span>Bỏ phê duyệt</span></a>                        
                    </div>
                }

                {!disable_action && status === "redo" && isPerformer &&
                    <div className="work-action">
                        <span>Công việc này chưa được thực hiện. Chuyển vào Todo</span>
                        <a role="button" title="Chuyển vào Todo trong bảng Kanban" className="btn btn-xs" onClick={() => this.updateWorkStatus("todo")}><i className="fa fa-list-ul"></i> <span>Todo</span></a>
                    </div>
                }
            </div>
        );
    }
}

// const mapStateToProps = (state, ownProps) => {
//     return {
//         current_user: getCurrentUser(state),
//     };
// };

// const mapDispatchToProps = (dispatch) => {
//     return {
//         editWork: (data)=> dispatch(editWork(data))
//     };
// };

// export default connect(mapStateToProps, mapDispatchToProps)(WorkStatusAction);

export default WorkStatusAction;