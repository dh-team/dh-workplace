'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { reduxForm, Field, formValueSelector } from 'redux-form';

import FormSelect from '../form/Select';
import UserSelect from '../form/UserSelect';
import DatePicker from '../form/DatePicker';

import { fetchWorks } from '../../actions/action-work';


//client side validation
function validate(values) {
    const errors = {};
    return errors;
}

const INIT_FILTER = {
    source: null,
    status: "unfinished",
    performer: null,
    monitor: null,
    start_date: null,
    due_date: null,
    hierarchy: "top"
};
        
class WorkFilter extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };
    
    constructor() {
        super();
        
        this.applyFilter = this.applyFilter.bind(this);
        this.applyInitFilter = this.applyInitFilter.bind(this);
        this.resetFilter = this.resetFilter.bind(this);
    }

    componentDidMount() {
        const { initialize } = this.props;
        initialize(INIT_FILTER);
    }
    
    applyFilter(values, dispatch){
        console.log("apply filter", values);
        dispatch(fetchWorks(values));
    }
    
    applyInitFilter(values, dispatch){
        console.log("apply init filter", values);
        dispatch(fetchWorks(INIT_FILTER));
    }
    
    resetFilter(){
        const {reset, handleSubmit} = this.props;
        reset();
        handleSubmit(this.applyInitFilter)();
    }
    
    render() {
        const { handleSubmit, pristine, invalid, submitting } = this.props;
        return (
            <div className="work-filter">
                <div className="form-horizontal">
                    <div className="row">
                        <div className="col-md-3">
                            <Field component={FormSelect} name="source" label="" inputWrapCls="col-md-12" >
                                <option value="">-- Nguồn --</option>
                                <option value="project">Dự án</option>
                                <option value="business">Nghiệp vụ</option>
                                <option value="personal">Cá nhân</option>
                                <option value="team">Công việc nhóm</option>
                            </Field>
                        </div>
                        <div className="col-md-3">
                             <Field component={DatePicker} name="start_date"  label="Từ" labelCls="col-sm-3" inputWrapCls="col-sm-9"/>
                        </div>
                        <div className="col-md-4">
                             <Field component={UserSelect} name="performer" label="Thực hiện" labelCls="col-sm-4" inputWrapCls="col-sm-8" />
                        </div>
                        <div className="col-md-2">
                            <Field component={FormSelect} name="hierarchy" label="" inputWrapCls="col-md-12" >
                                <option value="top">Công việc lớn</option>
                                <option value="leaf">Công việc cuối</option>
                                <option value="all">Hiển thị tất cả</option>
                            </Field>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-md-3">
                            <Field component={FormSelect} name="status" label="" inputWrapCls="col-md-12" >
                                <option value="">-- Trạng thái --</option>
                                <option value="unfinished">Chưa kết thúc</option>
                                <option value="created">Khởi tạo</option>
                                <option value="assigned">Đang giao việc</option>
                                <option value="assigned_accept">Chưa làm</option>
                                <option value="assigned_reject">Từ chối nhận việc</option>
                                <option value="closed">Đóng</option>
                                <option value="todo">Chuẩn bị làm</option>
                                <option value="doing">Đang làm</option>
                                <option value="done">Làm xong</option>
                                <option value="completed">Hoàn thành</option>
                            </Field>
                        </div>
                        <div className="col-md-3">
                            <Field component={DatePicker} name="due_date"  label="Đến" labelCls="col-sm-3" inputWrapCls="col-sm-9"/>
                        </div>
                        <div className="col-md-4">
                            <Field component={UserSelect} name="monitor" label="Kiểm tra" labelCls="col-sm-4" inputWrapCls="col-sm-8" />
                        </div>
                        <div className="col-md-2 text-right">
                            <button className="btn btn-sm btn-primary" disabled={submitting} onClick={handleSubmit(this.applyFilter)}><i className="fa fa-search"></i> Lọc</button>
                            <button className="btn btn-sm btn-default" disabled={submitting} onClick={this.resetFilter}><i className="fa fa-rotate-left"></i> Bỏ lọc</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

const form = 'filterWork';
const fields = ["source", "status", "performer", "monitor", "start_date", "due_date", "name", "hierarchy"];
export default reduxForm({ form, validate, fields })(WorkFilter);
