'use strict';

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import FormInput from '../form/Input';
import GroupSelect from '../form/GroupSelect';
import TextArea from '../form/TextArea';
import FormSelect from '../form/Select';
import CheckBox from '../form/CheckBox';
import DatePicker from '../form/DatePicker';
import RadioSelect from '../form/RadioSelect';
import FormModal from '../common/FormModal';
import valid from '../../utils/validate';
import { addWork, editWork } from '../../actions/action-work';
import { getAddWorkStatus, getLastAddedWork } from '../../selectors/selector-work';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.name, 1, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    if (!valid.hasLength(values.detail, 0, 5000)) {
        errors.detail = 'Độ dài tối đa 5000 ký tự';
    }

    return errors;
}

class AddWorkForm extends Component {
    static contextTypes = {
        router: PropTypes.object
    };
    
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }
    
    componentWillReceiveProps(nextProps) {
        const {redirect, last_added_work} = this.props;
        const addSuccess = nextProps.add_status && this.props.add_status && this.props.add_status.loading && !nextProps.add_status.loading && !nextProps.add_status.error;
        if (redirect && addSuccess && last_added_work) {
            console.log("add work success, redirect");
            this.context.router.push('/space/work/' + last_added_work._id);
        }    
    }

    onShow() {
        const { initialize, reset } = this.props;
        initialize({
            name: "",
            detail: "",
            source: {
                type: "personal"
            }
        });
        reset();
    }

    submit(values, dispatch) {
        const { parent_work } = this.props;

        var data = {
            name: values.name,
            detail: values.detail,
        };
        if(parent_work){
            data.parent = parent_work._id;
        }else{
            data.source = values.source;
        }
        
        console.log("add work: " + JSON.stringify(values, null, 4));
        dispatch(addWork(data));
    }

    render() {
        const { id, parent_work, selected_source_type } = this.props;
        const { handleSubmit, pristine, invalid, submitting } = this.props;
        
        var groupSelectLabel = "";
        switch(selected_source_type){
            case "project":
                groupSelectLabel = "Nhóm dự án";
                break;
            case "business":
                groupSelectLabel = "Chọn phòng ban";
                break;
            case "team":
                groupSelectLabel = "Chọn nhóm";
                break;
        }
        
        return (
            <FormModal id={id || "addWorkFormModal"} title="Tạo công việc" disableSubmit={pristine || submitting || invalid}
                okCallback={handleSubmit(this.submit)} onShow={this.onShow} >

                <Field component={FormInput} name="name" type="text" label="Công việc" placeholder="Công việc" />
                <Field component={FormInput} name="detail" type="textarea" label="Mô tả về Công việc" />

                {!parent_work &&
                    <div className="row">
                        <div className="col-md-4">
                            <Field component={FormSelect} name="source.type" label="Thuộc về" >
                                <option value="project">Dự án</option>
                                <option value="business">Nghiệp vụ</option>
                                <option value="personal">Cá nhân</option>
                                <option value="team">Công việc nhóm</option>
                            </Field>
                        </div>
    
                        {(selected_source_type !== "personal") &&
                            <div className="col-md-8">
                                <Field component={GroupSelect} name="source.reference" label={groupSelectLabel} />
                            </div>
                        }                    
    
                    </div>   
                }
            </FormModal>
        );
    }
}

const form = 'AddWorkForm';
const fields = ["name", "detail", "source.type", "source.reference"];
AddWorkForm = reduxForm({ form, validate, fields })(AddWorkForm);
const selector = formValueSelector('AddWorkForm');
const mapStateToProps = (state, ownProps) => {
    return {
        add_status: getAddWorkStatus(state),
        last_added_work: getLastAddedWork(state),
        selected_source_type: selector(state, 'source.type')        
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(AddWorkForm);
