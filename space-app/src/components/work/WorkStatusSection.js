'use strict';

import React from 'react';

import WorkSource from './WorkSource';
import WorkStatusAction from './WorkStatusAction';

export default class WorkStatusSection extends React.Component {
    constructor() {
        super();
        
        this.setPriorityLabel = this.setPriorityLabel.bind(this);
    }

    componentDidMount() {
    }
    
    setPriorityLabel(value) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "content",
            id: work._id,
            priority_label: value
        };
        editWork(data);
    }

    render() {
        const { work, current_user, editWork } = this.props;

        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;
        const enableEdit = isPerformer || isMonitor || isCreator;

        return (
            <div className="box box-danger box-work-status">
                <div className="box-body no-padding">
                    <table className="table table-striped">
                        <tbody>
                            <tr>
                                <td className="work-detail-status">
                                    <WorkStatusAction work={work} current_user={current_user} editWork={editWork} />
                                </td>
                            </tr>

                            <tr>
                                <td className="work-detail-source">
                                    {work.source && <WorkSource source={work.source} />}
                                </td>
                            </tr>
                            <tr>
                                <td className="work-detail-priority-label">
                                    <p className="work-item-label">Xếp hạng</p>
                                    <p>
                                        <button className={"btn btn-sm " + (work.priority_label === "A" ? "btn-primary" : "btn-default")} onClick={() => enableEdit && this.setPriorityLabel("A")}>A</button>
                                        <button className={"btn btn-sm " + (work.priority_label === "B" ? "btn-primary" : "btn-default")} onClick={() => enableEdit && this.setPriorityLabel("B")}>B</button>
                                        <button className={"btn btn-sm " + (work.priority_label === "C" ? "btn-primary" : "btn-default")} onClick={() => enableEdit && this.setPriorityLabel("C")}>C</button>
                                        <button className={"btn btn-sm " + (work.priority_label === "D" ? "btn-primary" : "btn-default")} onClick={() => enableEdit && this.setPriorityLabel("D")}>D</button>
                                        <button className={"btn btn-sm " + (work.priority_label === "E" ? "btn-primary" : "btn-default")} onClick={() => enableEdit && this.setPriorityLabel("E")}>E</button>
                                    </p>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            </div>
        );
    }
}
