'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import WorkItem from './WorkItem';

import { editWork, fetchSubWorks } from '../../actions/action-work';
import { getCurrentUser } from '../../selectors/selector-user';

class WorkList extends React.Component {
    render() {
        const { works, columns, current_user, editWork, fetchSubWorks } = this.props;
        const cols = columns || { name: true, source: true, performer: true, start_date: true, due_date: true, status: true, progress: true};

        return (
            <table className="table work-list">
                <tbody>
                    <tr>
                        {cols.name && <th className="work-item-name-col">Công việc</th>}
                        {cols.source && <th className="work-item-source-col">Thuộc về</th>}
                        {cols.performer && <th className="work-item-performer-col">Thực hiện</th>}
                        {cols.monitor && <th className="work-item-monitor-col">Kiểm tra</th>}
                        {cols.start_date && <th className="work-item-startdate-col">Bắt đầu</th>}
                        {cols.due_date && <th className="work-item-duedate-col">Kết thúc</th>}
                        {cols.status && <th className="work-item-status-col">Trạng thái</th>}
                        {cols.progress && <th className="work-item-progress-col">Tiến độ</th>}
                    </tr>
                    {works && works.map((item) => {
                        return (<WorkItem key={item._id} depth={1} work={item} columns={cols} current_user={current_user} editWork={editWork} fetchSubWorks={fetchSubWorks} />);
                    })}
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = (state) => {
    return {        
        current_user: getCurrentUser(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        editWork: (data) => dispatch(editWork(data)),
        fetchSubWorks: (data) => dispatch(fetchSubWorks(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(WorkList);