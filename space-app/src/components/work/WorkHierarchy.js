'use strict';

import React from 'react';
import { Link } from 'react-router';

class WorkHierarchy extends React.Component {
    constructor() {
        super();
    }

    componentDidMount() {
    }

   
    render() {
        const { works } = this.props;

        return (
            <div className="work-hierarchy">
                {works.map((item, index) => {
                    return (
                        <span className="work-hierarchy-item" key={item._id}>
                            <Link to={"/space/work/" + item._id} className={"btn btn-xs " + (index === (works.length - 1) ? "btn-warning" : "btn-default")}>{item.name}</Link> 
                            {index !== (works.length - 1) && 
                                <span className="work-hierarchy-separator"><i className="fa fa-arrow-right"></i></span>
                            }
                        </span>
                    );
                })}
            </div>
        );
    }
}

export default WorkHierarchy;
