'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import WorkList from './WorkList';

import { getFollowedWorks } from '../../selectors/selector-work';

class FollowedWorks extends React.Component {

    render() {
        const { works, current_user, editWork } = this.props;
        const columns = { name: true, source: true, start_date: true, due_date: true, status: true, performer: true, monitor: true, progress: true };
        return (
            <div className="monitor-works">                
                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Công việc bạn theo dõi</h3>

                        <div className="box-tools">
                          
                        </div>
                    </div>

                    <div className="box-body table-responsive no-padding">
                        <WorkList works={works} columns={columns} />
                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        works: getFollowedWorks(state)        
    };
};

const mapDispatchToProps = (dispatch) => {
    return {       
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FollowedWorks);
