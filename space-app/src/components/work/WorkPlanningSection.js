'use strict';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import DateDisplay from '../common/DateDisplay';
import TimeStamp from "../common/TimeStamp";

// import valid from '../../utils/validate';
import DatePicker from '../form/DatePicker';

//client side validation
function validate(values) {
    const errors = {};
    return errors;
}

class WorkPlanningSection extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.submitEditing = this.submitEditing.bind(this);
        this.resetEditing = this.resetEditing.bind(this);
        this.initForm = this.initForm.bind(this);

        this.state = {
            editing: {}
        };
    }

    componentDidMount() {
        this.initForm(this.props.work);
    }

    componentWillReceiveProps(nextProps) {
        // change work
        if (nextProps.work && this.props.work && nextProps.work._id !== this.props.work._id) {
            this.initForm(nextProps.work);
        }
    }

    initForm(work) {
        const { initialize, reset } = this.props;
        initialize({
            planning: {
                start_date: work.planning && work.planning.start_date,
                due_date: work.planning && work.planning.due_date,
            },
        });
        reset();
    }

    startEdit(field) {
        var editing = {
            ...this.state.editing,
            [field]: true
        };
        this.setState({
            editing: editing
        });
    }

    submitEditing(values, dispatch) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "planning",
            id: work._id,
            planning: values.planning
        };

        editWork(data);
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    resetEditing() {
        this.props.reset();
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    render() {
        const { work, current_user } = this.props;
        const status = (work.doing && work.doing.status) || "created";
        const { editing } = this.state;
        const { handleSubmit, pristine, invalid, submitting } = this.props;

        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;
        const enableEdit = isPerformer || isMonitor || isCreator;

        return (
            <div className="box box-success box-work-planning">
                <div className="box-body no-padding">
                    <table className="table table-striped">
                        <tbody>
                            <tr>
                                <td className="work-item-label">
                                    <label>Bắt đầu vào: </label >
                                </td>
                                <td className="work-item-value">
                                    <div onClick={() => enableEdit && this.startEdit("planning.start_date")} >
                                        {!editing["planning.start_date"] &&
                                            <DateDisplay value={work.planning && work.planning.start_date} format="date" />
                                        }
                                        {editing["planning.start_date"] &&
                                            <Field component={DatePicker} name="planning.start_date" />
                                        }
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td className="work-item-label">
                                    <label>Cần xong trước: </label >
                                </td>
                                <td className="work-item-value">
                                    <div onClick={() => enableEdit && this.startEdit("planning.due_date")}>
                                        {!editing["planning.due_date"] &&
                                            <DateDisplay value={work.planning && work.planning.due_date} format="date" />
                                        }
                                        {editing["planning.due_date"] &&
                                            <Field component={DatePicker} name="planning.due_date" />
                                        }
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td className="work-item-label">
                                    <label>Thời gian thực tế: </label>
                                </td>
                                <td className="work-item-value">
                                    {work.doing && work.doing.spent_time > 0 &&
                                        <TimeStamp seconds={work.doing.spent_time} hide_second />
                                    }
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {enableEdit &&
                    <div className="box-footer">
                        <div className="box-tools pull-right">
                            <button disabled={submitting} onClick={this.resetEditing} className="btn btn-xs btn-default" >
                                <i className="fa fa-undo"></i> Hủy
                        </button>
                            <button disabled={invalid || pristine || submitting} onClick={handleSubmit(this.submitEditing)} className="btn btn-xs btn-primary">
                                <i className="fa fa-save"></i> Lưu
                        </button>
                        </div>
                    </div>
                }
            </div>

        );
    }
}

const form = 'editWorkPlanning';
const fields = ["planning.start_date", "planning.due_date"];
export default reduxForm({ form, validate, fields })(WorkPlanningSection);

