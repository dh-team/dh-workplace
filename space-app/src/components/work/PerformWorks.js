'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import WorkList from './WorkList';
import AddWorkForm from './AddWorkForm';
// import { editWork } from '../../actions/action-work';
// import { getCurrentUser } from '../../selectors/selector-user';
import { getPerformWorks } from '../../selectors/selector-work';

class PerformWorks extends React.Component {

    render() {
        const { works, current_user, editWork } = this.props;
        const columns = { name: true, source: true, start_date: true, due_date: true, status: true, monitor: true, progress: true };
        return (
            <div className="perform-works">

                <AddWorkForm id="addWorkFormModal" redirect />

                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Công việc bạn thực hiện</h3>

                        <div className="box-tools">
                            <button className="btn btn-xs btn-primary btn-add-work" data-toggle="modal" data-target="#addWorkFormModal" title="Tạo công việc"><i className="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div className="box-body table-responsive no-padding">
                        <WorkList works={works} columns={columns} />
                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        works: getPerformWorks(state),
        // current_user: getCurrentUser(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // editWork: (data)=> dispatch(editWork(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PerformWorks);
