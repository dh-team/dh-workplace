'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import moment from 'moment';

import ProgressBar from '../common/ProgressBar';
import WorkTask from './WorkTask';

class WorkDoingSection extends React.Component {
    constructor() {
        super();
        this.sortable = null;
        this.$slider = null;
        this.state = {};

        this.onKeyDown = this.onKeyDown.bind(this);
        this.handleChangeDisplayType = this.handleChangeDisplayType.bind(this);
        
        this.handleAddTask = this.handleAddTask.bind(this);
        this.handleStartStop = this.handleStartStop.bind(this);
        this.handleEditTask = this.handleEditTask.bind(this);      
        this.handleTaskCheck = this.handleTaskCheck.bind(this);
        this.handleTaskMove = this.handleTaskMove.bind(this);
        
        this.handleDeleteTask = this.handleDeleteTask.bind(this);
        this.confirmDeleteTask = this.confirmDeleteTask.bind(this);
    }

    componentDidMount() {
        // List with handle
        const { work, editWork } = this.props;
        const thisCom = this;
        var sortable = this.sortable = Sortable.create(this.refs.tasksList, {
            handle: '.fa-navicon',
            animation: 150,
            // Element dragging ended
            onEnd: function (evt) {
                // console.log("onEnd", evt, evt.oldIndex, evt.newIndex);
                // console.log("onEnd", sortable.toArray());
                thisCom.handleTaskMove(evt.oldIndex, evt.newIndex);
            },

        });
        if (this.props.focus) {
            this.taskInput.focus();
        }
        // init slider
        this.$slider = $(this.progressSlider);
        this.$slider.slider({
            formatter: function (value) {
                return 'Hoàn thành: ' + value + "%";
            }
        }).on("slideStop", (e) => {
            console.log("slideStop", e, e.value);
            const progress = e.value;
            if (work.doing && work.doing.progress === progress) return;
            editWork({
                update_area: "progress",
                id: work._id,
                progress: progress
            });
        });
    }

    componentWillUnmount() {
        this.sortable.destroy();
        this.$slider.slider("destroy");
    }

    componentWillReceiveProps(nextProps) {
        const { work } = this.props;
        if (nextProps.edit_task_status && this.props.edit_task_status && this.props.edit_task_status.loading && !nextProps.edit_task_status.loading && !nextProps.edit_task_status.error) {
            console.log("edit task success");
            if (work.doing && work.doing.tasks) {
                var order1 = this.sortable.toArray();
                var order2 = work.doing.tasks.map((item) => "" + item.display_order);
                if (JSON.stringify(order1) !== JSON.stringify(order2)) {
                    console.log("resort task order", order1, order2);
                    this.sortable.sort(order2);
                }
            }
        }
    }

    onKeyDown(e) {
        if (e.nativeEvent.keyCode === 13 && !e.nativeEvent.shiftKey) {
            e.preventDefault();
            this.handleAddTask();
            return;
        }

        var el = this.taskInput;
        setTimeout(function () {
            el.style.cssText = 'height:34px;';
            if (el.scrollHeight > 34) {
                el.style.cssText = 'height:' + (el.scrollHeight + 10) + 'px';
            }
        }, 0);
    }

    handleAddTask(start) {
        const { work, addWorkTask } = this.props;
        const params = {
            work_id: work._id,
            note: this.taskInput.value.trim()
        };
        if (start) {
            params.status = "running";
            if (!params.note) params.note = "Thực hiện lúc " + moment(new Date).format("YYYY-MM-DD HH:mm");
        } else if (!params.note) {
            return;
        };
        console.log("add task: " + params);
        addWorkTask(params);
        // reset text input
        this.taskInput.value = "";
        this.taskInput.style.cssText = 'height:34px;';
    }

    handleStartStop(task) {
        const { work, editWorkTask } = this.props;
        const newStatus = task.status === "running" ? "stopped" : "running";
        editWorkTask({
            work_id: work._id,
            id: task.display_order,
            status: newStatus
        });
    }

    handleTaskCheck(task) {
        const { work, editWorkTask } = this.props;
        const newStatus = task.status === "completed" ? "created" : "completed";
        editWorkTask({
            work_id: work._id,
            id: task.display_order,
            status: newStatus
        });
    }
    
    handleEditTask(params) {
        const { work, editWorkTask } = this.props;
        const {task, ...rest} = params;
        editWorkTask({
            work_id: work._id,
            id: params.task.display_order,
            ...rest
        });
    }
    
    handleTaskMove(from, to) {
        const { work, editWorkTask } = this.props;
        editWorkTask({
            work_id: work._id,
            id: from + 1,
            display_order: to + 1
        });
    }


    handleDeleteTask(task) {
        this.props.openDialog({
            title: "Xóa công việc",
            content: "Bạn chắc chắn muốn xóa?",
            confirmCallback: () => this.confirmDeleteTask(task.display_order)
        });
    }

    confirmDeleteTask(taskId) {
        const { work, deleteWorkTask } = this.props;
        deleteWorkTask({ work_id: work._id, task_id: taskId });
    }
    
    handleChangeDisplayType() {
        const { work, editWork } = this.props;
        const checklist = work.doing && work.doing.display_type === "checklist";
        editWork({
            update_area: "content",
            id: work._id,
            doing: { display_type: checklist ? "historical" : "checklist" }
        });
    }

    render() {
        const { work, current_user } = this.props;
        const tasks = work.doing && work.doing.tasks;
        const checklist = work.doing && work.doing.display_type === "checklist";
        const progress = (work.doing && work.doing.progress) || 0;

        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        // const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        // const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;
        // const enableEdit = isPerformer || isMonitor || isCreator;

        return (
            <div className="box box-simple">
                <div className="box-header with-border">
                    <h3 className="box-title">
                        Thực hiện  {isPerformer &&
                            <span className="checkbox work-checklist-change">
                                <label><input type="checkbox" onClick={this.handleChangeDisplayType} checked={checklist} /> Checklist</label>
                            </span>
                        }
                    </h3>

                    <div className="box-tools">
                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div className="box-body">
                    <div className="work-tasks-container">
                        <div className={"work-progress-slider " + ((isPerformer && !checklist) ? "" : "hide")}>
                            <input ref={(elm) => { this.progressSlider = elm; }} type="text" data-slider-min="0" data-slider-max="100" data-slider-step="10" data-slider-value={progress} />
                        </div>

                        {(!isPerformer || checklist) &&
                            <ProgressBar progress={progress} className="work-tasks-progress" />                           
                        }

                        {isPerformer &&
                            <div className="work-tasks-add">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <textarea onKeyDown={this.onKeyDown} ref={(input) => { this.taskInput = input; }} className="work-task-input autosize-textarea form-control" placeholder={checklist ? "Thêm task mới" : "Ghi nhật ký hoặc việc cần làm"}>
                                                </textarea>
                                            </td>
                                            <td style={{ width: "50px" }}>
                                                <button className="btn btn-default" title="Tính giờ" onClick={() => this.handleAddTask(true)}><i className={"fa fa-play"}></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        }
                        
                        <div className="work-tasks-list" ref="tasksList">
                            {tasks && tasks.map((item) => {
                                return <WorkTask key={"work-task-" + item.display_order} isPerformer={isPerformer} task={item} checklist={checklist} handleTaskCheck={this.handleTaskCheck} handleStartStop={this.handleStartStop} handleEditTask={this.handleEditTask} handleDeleteTask={this.handleDeleteTask} />;
                            })}
                        </div>

                    </div>
                </div>
            </div>

        );
    }

}

export default WorkDoingSection;