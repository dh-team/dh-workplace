'use strict';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

import UserDisplayLink from '../common/UserDisplayLink';
import DateDisplay from '../common/DateDisplay';

import UserSelect from '../form/UserSelect';
import UserMultiSelect from '../form/UserMultiSelect';

import workUtils from '../../utils/work';

//client side validation
function validate(values) {
    const errors = {};
    return errors;
}

class WorkResourceSection extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.submitEditing = this.submitEditing.bind(this);
        this.resetEditing = this.resetEditing.bind(this);
        this.initForm = this.initForm.bind(this);

        this.state = {
            editing: {}
        };
    }

    componentDidMount() {
        this.initForm(this.props.work);
    }

    componentWillReceiveProps(nextProps) {
        // change work
        if (nextProps.work && this.props.work && nextProps.work._id !== this.props.work._id) {
            this.initForm(nextProps.work);
        }
    }

    initForm(work) {
        const { initialize, reset } = this.props;
        initialize({
            resource: work.resource,
        });
        reset();
    }

    startEdit(field) {
        var editing = {
            ...this.state.editing,
            [field]: true
        };
        this.setState({
            editing: editing
        });
    }

    submitEditing(values, dispatch) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "resource",
            id: work._id,
            resource: values.resource
        };

        editWork(data);
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    resetEditing() {
        this.props.reset();
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    render() {
        const { work, current_user } = this.props;
        const { editing } = this.state;
        const { handleSubmit, pristine, invalid, submitting } = this.props;

        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;
        const status = (work.doing && work.doing.status);
        const enableEdit = (isPerformer || isMonitor || isCreator) && (workUtils.compareStatus(status, "completed") < 0);

        return (
            <div className="box box-info box-work-resource">
                <div className="box-body no-padding">
                    <table className="table table-striped">
                        <tbody>
                            <tr>
                                <td className="work-item-label">
                                    <label>Thực hiện: </label >
                                </td>
                                <td className="work-item-value">
                                    <div onClick={() => isCreator && enableEdit && this.startEdit("resource.performer")}>
                                        {!editing["resource.performer"] &&
                                            <UserDisplayLink user_id={work.resource && work.resource.performer} />
                                        }
                                        {editing["resource.performer"] &&
                                            <Field component={UserSelect} name="resource.performer" />
                                        }
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td className="work-item-label">
                                    <label>Giám sát: </label >
                                </td>
                                <td className="work-item-value">
                                    <div onClick={() => isCreator && enableEdit && this.startEdit("resource.monitor")}>
                                        {!editing["resource.monitor"] &&
                                            <UserDisplayLink user_id={work.resource && work.resource.monitor} />
                                        }
                                        {editing["resource.monitor"] &&
                                            <Field component={UserSelect} name="resource.monitor" />
                                        }
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td className="work-item-label">
                                    <label>Theo dõi: </label >
                                </td>
                                <td className="work-item-value" onClick={() => enableEdit && this.startEdit("resource.followers")}>
                                    <div>
                                        {!editing["resource.followers"] && work.resource && work.resource.followers &&
                                            work.resource.followers.map((item) => {
                                                return (<UserDisplayLink user_id={item} key={item} />);
                                            })
                                        }
                                        {editing["resource.followers"] &&
                                            <Field component={UserMultiSelect} name="resource.followers" />
                                        }

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colSpan={2}>
                                    <div>
                                        <span>Tạo bởi </span> <UserDisplayLink user_id={work.meta && work.meta.create_user && work.meta.create_user.user_id} /> <span> lúc </span> <DateDisplay value={work.meta && work.meta.create_time} />
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {enableEdit &&
                    <div className="box-footer">
                        <div className="box-tools pull-right">
                            <button disabled={submitting} onClick={this.resetEditing} className="btn btn-xs btn-default" >
                                <i className="fa fa-undo"></i> Hủy
                            </button>
                            <button disabled={invalid || pristine || submitting} onClick={handleSubmit(this.submitEditing)} className="btn btn-xs btn-primary">
                                <i className="fa fa-save"></i> Lưu
                            </button>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

const form = 'editWorkResource';
const fields = ["resource.monitor", "resource.performer", "resource.followers"];
export default reduxForm({ form, validate, fields })(WorkResourceSection);

