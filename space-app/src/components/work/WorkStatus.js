'use strict';

import React, { Component, PropTypes } from 'react';

export default class WorkStatus extends Component {

    render() {
        const { value } = this.props;
        var item = <span className="label label-success" title="Công việc được khởi tạo, chưa chọn người thực hiện">Khởi tạo</span>;
        switch (value) {
            case "created":
                item = <span className="label label-default" title="Công việc được khởi tạo, chưa chọn người thực hiện">Khởi tạo</span>;
                break;
            case "assigned":
                item = <span className="label label-warning" title="Đã chọn người thực hiện, người thực hiện chưa xác nhận">Đang giao việc</span>;
                break;
            case "assigned_accept":
                item = <span className="label label-default" title="Công việc trong trạng thái chờ, chưa được thực hiện">Chưa làm</span>;
                break;
            case "assigned_reject":
                item = <span className="label label-danger" title="Người được giao việc từ chối nhận việc">Từ chối nhận việc</span>;
                break;
            case "closed":
                item = <span className="label label-danger" title="Công việc đã đóng">Đóng</span>;
                break;
            case "todo":
                item = <span className="label label-info" title="Todo - Công việc chuẩn bị thực hiện, đã đưa vào todo trong bảng Kanban">Chuẩn bị làm</span>;
                break;
            case "doing":
                item = <span className="label label-warning" title="Doping - Công việc đang được thực hiện">Đang làm</span>;
                break;
            case "redo":
                item = <span className="label label-warning" title="Công việc không được phê duyệt bởi người kiểm tra, cần thực hiện lại">Phải làm lại</span>;
                break;
            case "done":
                item = <span className="label label-primary" title="Done - Công việc đã thực hiện xong, đang đợi kiểm tra">Làm xong</span>;
                break;
            case "completed":
                item = <span className="label label-success" title="Công việc đã hoàn thành và đã được kiểm tra đạt">Hoàn thành</span>;
                break;
        }

        // return <div className="work-status">{item}</div>;
        return item;
    }
}
