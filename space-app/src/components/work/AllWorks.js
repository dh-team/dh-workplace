'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import WorkList from './WorkList';
import AddWorkForm from './AddWorkForm';
import WorkFilter from './WorkFilter';

import { editWork } from '../../actions/action-work';
import { getCurrentUser } from '../../selectors/selector-user';
import { getAllWorks } from '../../selectors/selector-work';

class AllWorks extends React.Component {
    
    render() {
        const { works } = this.props;

        return (
            <div className="all-works">

                <AddWorkForm id="addWorkFormModal" />

                <div className="box">
                    <div className="box-header with-border">
                        <h3 className="box-title">Danh sách công việc</h3>

                        <div className="box-tools">
                            <button className="btn btn-sm btn-primary btn-add-work" data-toggle="modal" data-target="#addWorkFormModal"><i className="fa fa-plus"></i></button>
                        </div>
                    </div>

                    <div className="box-body table-responsive no-padding">
                        <WorkFilter />
                        <WorkList works={works} columns={{ name: true, performer: true, monitor: true, source: true, start_date: true, due_date: true, status: true, progress: true}} />
                    </div>

                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        works: getAllWorks(state),
        current_user: getCurrentUser(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        editWork: (data)=> dispatch(editWork(data)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AllWorks);
