'use strict';

import React from 'react';
import { connect } from 'react-redux';

import { Link } from 'react-router';
import WorkItem from './WorkItem';

import { getWorks } from '../../selectors/selector-work';

class SubWorks extends React.Component {
    constructor() {
        super();

        this.state = {
        };
    }

    render() {
        const { works, columns, depth, current_user, editWork, fetchSubWorks } = this.props;
        return (
            <table className="table work-item-subs">
                <tbody>
                    {works.map((item) => {
                        return <WorkItem depth={depth + 1} work={item} columns={columns} current_user={current_user} editWork={editWork} fetchSubWorks={fetchSubWorks} />
                    })}
                </tbody>
            </table>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    let works = [];
    if (ownProps.subworks && ownProps.subworks.length > 0) {
        works = getWorks(state, ownProps.subworks);
    }
    return {
        works
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        // editWork: (data) => dispatch(editWork(data)),
        // fetchSubWorks: (data) => dispatch(fetchSubWorks(data))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SubWorks);