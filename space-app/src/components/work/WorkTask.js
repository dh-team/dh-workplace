'use strict';

import React from 'react';
import { Link } from 'react-router';
import numeral from 'numeral';

import TimeCounter from './TimeCounter';
import TimeStamp from "../common/TimeStamp";


export default class WorkTask extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.stopEdit = this.stopEdit.bind(this);
        this.handleEditTask = this.handleEditTask.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);

        this.state = {
            editing: false,
            noteDirty: false,
            timeDirty: false,
        };
    }

    componentDidMount() {

    }

    componentWillUnmount() {

    }

    componentWillReceiveProps(nextProps) {
    }

    startEdit() {
        this.setState({
            editing: true,
            noteDirty: false,
            timeDirty: false
        });
        this.taskNoteInput && this.taskNoteInput.focus();
    }

    stopEdit() {
        this.setState({
            editing: false,
            noteDirty: false,
            timeDirty: false
        });
    }

    onKeyDown(e) {
        // if (e.nativeEvent.keyCode === 13 && !e.nativeEvent.shiftKey) {
        //     e.preventDefault();
        //     this.handleEditTask();
        //     this.setState({ editing: false });
        //     return;
        // }
        this.setState({ noteDirty: true });
        var el = this.taskNoteInput;
        setTimeout(function () {
            el.style.cssText = 'height:34px;';
            if (el.scrollHeight > 34) {
                el.style.cssText = 'height:' + (el.scrollHeight + 10) + 'px';
            }
        }, 0);
    }

    handleEditTask() {
        const { task, handleEditTask } = this.props;
        const data = {
            task: task
        };
        if (!this.state.noteDirty && !this.state.timeDirty) return;
        if (this.state.noteDirty) data.note = this.taskNoteInput.value.trim();
        if (this.state.timeDirty) {
            const str = this.taskTimeInput.value.trim();
            const subs = str.split(":");
            if (subs.length === 2) {
                const h = parseInt(subs[0]) || 0;
                const m = parseInt(subs[1]) || 0;
                data.spent_time = h * 3600 + m * 60;
            }
        }

        handleEditTask(data);
        this.setState({
            editing: false,
            noteDirty: false,
            timeDirty: false
        });
    }

    render() {
        const { editing } = this.state;
        const { isPerformer, task, checklist, handleTaskCheck, handleStartStop, handleDeleteTask } = this.props;
        const seconds = task.spent_time || 0;
        const h = Math.floor(seconds / 3600);
        const m = Math.floor((seconds - h * 3600) / 60);

        const spentTime = numeral(h).format("00") + ":" + numeral(m).format("00");

        return (
            <div className={"list-group-item work-task-item work-task-item-" + task.status} data-id={task.display_order}>
                <table>
                    <tbody>
                        <tr>
                            {isPerformer &&
                                <td className="work-task-col-drag">
                                    <span className="fa fa-navicon" aria-hidden="true"></span>
                                </td>
                            }
                            {isPerformer && checklist &&
                                <td className="work-task-col-check">
                                    <input type="checkbox" onClick={() => handleTaskCheck(task)} checked={task.status === "completed"} />
                                </td>
                            }
                            <td className="work-task-col-content">
                                <div className="row">
                                    <div className="col-sm-9">
                                        {!editing && <span className="work-task-note">{task.note}</span>}
                                        {editing &&
                                            <div className="work-task-note-editing">
                                                <textarea defaultValue={task.note} onKeyDown={this.onKeyDown} ref={(input) => { this.taskNoteInput = input; }} className="work-task-input autosize-textarea form-control" placeholder="Nội dung">
                                                </textarea>
                                            </div>
                                        }
                                    </div>
                                    <div className="col-sm-3 text-right work-task-btn">
                                        {task.status === "running" && task.start_time &&
                                            <TimeCounter start_time={task.start_time} />
                                        }

                                        {task.spent_time > 0 && !editing &&
                                            <span className="work-task-time">
                                                <TimeStamp seconds={task.spent_time} hide_second />
                                            </span>
                                        }

                                        {editing &&
                                            <span className="work-task-time">
                                                <input type="text" defaultValue={spentTime} ref={(input) => { this.taskTimeInput = input; }} onChange={() => this.setState({ timeDirty: true })} className="work-task-time-input form-control" placeholder="Giờ:Phút" />
                                            </span>
                                        }

                                        {isPerformer && task.status !== "completed" && !editing &&
                                            <a role="button" className="work-task-start" onClick={() => handleStartStop(task)} title="Bắt đầu">
                                                <i className={"fa fa-lg " + (task.status === "running" ? "fa-pause" : "fa-play")}></i>
                                            </a>
                                        }

                                        {isPerformer && !editing &&
                                            <a role="button" className="work-task-start" onClick={this.startEdit} title="Sửa">
                                                <i className="fa fa-edit"></i>
                                            </a>
                                        }

                                        {editing &&
                                            <a role="button" className="work-task-start" onClick={this.stopEdit} title="Bỏ qua">
                                                <i className="fa fa-undo"></i>
                                            </a>
                                        }

                                        {editing &&
                                            <a role="button" className="work-task-start" onClick={this.handleEditTask} title="Lưu">
                                                <i className="fa fa-save"></i>
                                            </a>
                                        }

                                        {isPerformer &&
                                            <a role="button" className="work-task-start" onClick={() => handleDeleteTask(task)} title="Xóa">
                                                <i className="fa fa-trash"></i>
                                            </a>
                                        }
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        );
    }
}
