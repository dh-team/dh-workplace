'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';

import WorkHierarchy from './WorkHierarchy';
import AddWorkForm from './AddWorkForm';

import valid from '../../utils/validate';
import workUtil from '../../utils/work';

import FormInput from '../form/Input';
import Editor from '../form/Editor';
import UploadGallery from '../common/UploadGallery';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.name, 1, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    if (!valid.hasLength(values.detail, 0, 5000)) {
        errors.detail = 'Độ dài tối đa 5000 ký tự';
    }

    return errors;
}

class WorkContentSection extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.submitEditing = this.submitEditing.bind(this);
        this.resetEditing = this.resetEditing.bind(this);
        this.initForm = this.initForm.bind(this);
        this.handleAttachFiles = this.handleAttachFiles.bind(this);

        this.state = {
            attachments: [],
            editing: {}
        };
    }

    componentDidMount() {
        this.initForm(this.props.work);
    }

    componentWillReceiveProps(nextProps) {
        // change work
        if (nextProps.work && this.props.work && nextProps.work._id !== this.props.work._id) {
            console.log("change work id");
            this.initForm(nextProps.work);
        }
    }

    initForm(work) {
        const { initialize, reset } = this.props;
        initialize({
            name: work.name,
            detail: work.detail,
        });
        reset();
        // set attachments
        this.setState({ attachments: work.attachments });
    }

    startEdit(field) {
        var editing = {
            ...this.state.editing,
            [field]: true
        };
        this.setState({
            editing: editing
        });
    }

    submitEditing(values, dispatch) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "content",
            id: work._id,
            name: values.name,
            detail: values.detail,
        };

        editWork(data);
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    resetEditing() {
        this.props.reset();
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    handleAttachFiles(files) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "attachments",
            id: work._id,
            attachments: files
        };

        editWork(data);
    }

    render() {
        const { work, work_hierarchy, current_user, deleteWork } = this.props;
        const { editing, attachments } = this.state;
        const { handleSubmit, pristine, invalid, submitting } = this.props;
        
        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;

        const enableMenu = isPerformer || isMonitor || isCreator;
        const enableEdit = isMonitor || isCreator;
        // const enableAddSub = workUtil.getPerformerOrCreator(work) === current_user.user_id && workUtil.canAddSubWork(work);
        const enableAddSub = workUtil.canAddSubWork(work);
        const enableDelete = isCreator;

        return (
            <div className="box box-primary">
                <AddWorkForm id="addSubWorkFormModal" parent_work={work} />

                <div className="box-header with-border">
                    <h3 className="box-title">Chi tiết công việc</h3>

                    {enableMenu &&
                        <div className="box-tools btn-group">

                            {enableEdit && 
                                <button disabled={submitting} onClick={this.resetEditing} className="btn btn-sm btn-default" title="Hủy bỏ thay đổi">
                                    <i className="fa fa-undo"></i>
                                </button>
                            }
                            {enableEdit && 
                                <button disabled={invalid || pristine || submitting} onClick={handleSubmit(this.submitEditing)} className="btn btn-sm btn-default" title="Lưu thay đổi">
                                    <i className="fa fa-save"></i>
                                </button>
                            }

                            {enableAddSub &&
                                <button className="btn btn-sm btn-default" data-toggle="modal" data-target="#addSubWorkFormModal" title="Thêm công việc con">
                                    <i className="fa fa-plus"></i>
                                </button>
                            }

                            {enableDelete &&
                                <button disabled={submitting} onClick={deleteWork} className="btn btn-sm btn-default" title="Xóa công việc này">
                                    <i className="fa fa-trash"></i>
                                </button>
                            }
                        </div>
                    }
                </div>

                <div className="box-body">
                    {work_hierarchy.length > 1 && <WorkHierarchy works={work_hierarchy} />}
                    <div className="work-detail-line work-detail-name-elm" onClick={() => enableEdit && this.startEdit("name")}>
                        {!editing.name &&
                            <h4 className="work-detail-name">{work.name}</h4>
                        }
                        {editing.name &&
                            <Field component={FormInput} name="name" type="text" placeholder="Tên công việc" />
                        }
                    </div>

                    <div className="box box-flat">
                        <div className="box-header with-border">
                            <h3 className="box-title">
                                Mô tả công việc: {enableEdit && <a role="button" onClick={() => this.startEdit("detail")}><i className="fa fa-edit"></i></a>}
                            </h3>

                            <div className="box-tools">
                                <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <div className="box-body">
                            {!editing["detail"] &&
                                <div className="work-detail-detail" dangerouslySetInnerHTML={{ __html: work.detail }}>
                                </div>
                            }
                            {editing["detail"] &&
                                <Field component={Editor} name="detail" />
                            }
                        </div>
                    </div>

                    <div className="box box-flat collapsed-box">
                        <div className="box-header with-border">
                            <h3 className="box-title">
                                File đính kèm: ({attachments && attachments.length})
                            </h3>

                            <div className="box-tools">
                                <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-plus"></i>
                                </button>
                            </div>
                        </div>
                        <div className="box-body">
                            <UploadGallery files={attachments} display_type="list" onFilesChanged={this.handleAttachFiles} read_only={!enableEdit} />
                        </div>
                    </div>


                </div>
            </div>
        );
    }
}

const form = 'editWorkContent';
const fields = ["name", "detail"];
export default reduxForm({ form, validate, fields })(WorkContentSection);
