'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import DateDisplay from '../common/DateDisplay';
import UserDisplayLink from '../common/UserDisplayLink';
import ProgressBar from '../common/ProgressBar';
import WorkStatusAction from './WorkStatusAction';
import WorkSource from './WorkSource';
import SubWorks from './SubWorks';

import { getSubworks } from '../../selectors/selector-work';

class WorkItem extends React.Component {
    constructor() {
        super();
        this.expandSubWorks = this.expandSubWorks.bind(this);

        this.state = {
            subworks_loaded: false,
            show_subworks: false
        }
    }

    expandSubWorks() {
        const { subworks_loaded, show_subworks } = this.state;
        const { work, fetchSubWorks } = this.props;
        if (!subworks_loaded) {
            // fetch sub works
            fetchSubWorks(work._id);
            this.setState({ subworks_loaded: true });
        }

        this.setState({ show_subworks: !show_subworks });
    }
    render() {
        const { work, columns, depth, current_user, editWork, fetchSubWorks } = this.props;
        if (!work) return null;
        const { show_subworks } = this.state
        const padding = (depth - 1) * 15;
        const colNumber = Object.keys(columns).reduce((count, curr) => {
            if (columns[curr]) count += 1;
            return count;
        }, 0);
        const progress = (work.doing && work.doing.progress) || 0;

        return (
            <tr>
                <td colSpan={colNumber}>
                    <table className={"table work-item-container " + (work.child_number > 0 ? "work-item-has-childs" : "")} >
                        <tbody>
                            <tr>
                                {columns.name &&
                                    <td className="work-item-name-col">
                                        <span className="work-item-name-padding" style={{ padding: padding + "" }}></span>
                                        {work.child_number > 0 &&
                                            <a className="work-item-expand-btn" role="button" onClick={this.expandSubWorks}>
                                                <i className={show_subworks ? "fa fa-minus-square-o" : "fa fa-plus-square-o"}></i>
                                            </a>
                                        }
                                        <Link className="work-item-name" to={"/space/work/" + work._id}> {work.name}</Link>
                                    </td>
                                }
                                {columns.source && <td className="work-item-source-col"><WorkSource source={work.source} hide_reference /></td>}
                                {columns.performer && <td className="work-item-performer-col"><UserDisplayLink user_id={work.resource && work.resource.performer} /></td>}
                                {columns.monitor && <td className="work-item-monitor-col"><UserDisplayLink user_id={work.resource && work.resource.monitor} /></td>}
                                {columns.start_date && <td className="work-item-startdate-col"><DateDisplay value={work.planning && work.planning.start_date} format="date" /></td>}
                                {columns.due_date && <td className="work-item-duedate-col"><DateDisplay value={work.planning && work.planning.due_date} format="date" /></td>}
                                {columns.status && <td className="work-item-status-col"><WorkStatusAction work={work} current_user={current_user} editWork={editWork} className="hide-text" /></td>}
                                {columns.progress &&
                                    <td className="work-item-progress-col">
                                        <ProgressBar progress={progress} className="work-item-progress" />
                                    </td>
                                }
                            </tr>
                        </tbody>
                    </table>

                    {show_subworks && work.childs && work.childs.length > 0 &&
                        <SubWorks subworks={work.childs} depth={depth} columns={columns} current_user={current_user} editWork={editWork} fetchSubWorks={fetchSubWorks} />
                    }
                </td>
            </tr>
        );
    }
}

export default WorkItem;