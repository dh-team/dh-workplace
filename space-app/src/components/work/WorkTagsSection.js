'use strict';

import React from 'react';
import { reduxForm, Field } from 'redux-form';

// import valid from '../../utils/validate';
import TagInput from '../form/TagInput';

//client side validation
function validate(values) {
    const errors = {};
    return errors;
}

class WorkTagsSection extends React.Component {
    constructor() {
        super();

        this.startEdit = this.startEdit.bind(this);
        this.submitEditing = this.submitEditing.bind(this);
        this.resetEditing = this.resetEditing.bind(this);
        this.initForm = this.initForm.bind(this);

        this.state = {
            editing: {}
        };
    }

    componentDidMount() {
        this.initForm(this.props.work);
    }

    componentWillReceiveProps(nextProps) {
        // change work
        if (nextProps.work && this.props.work && nextProps.work._id !== this.props.work._id) {
            this.initForm(nextProps.work);
        }
    }

    initForm(work) {
        const { initialize, reset } = this.props;
        initialize({
            category_tags: work.category_tags || [],
            knowledge_tags: work.knowledge_tags || []
        });
        reset();
    }

    startEdit(field) {
        var editing = {
            ...this.state.editing,
            [field]: true
        };
        this.setState({
            editing: editing
        });
    }

    submitEditing(values, dispatch) {
        const { work, editWork } = this.props;
        var data = {
            update_area: "content",
            id: work._id,
            category_tags: values.category_tags,
            knowledge_tags: values.knowledge_tags,
        };
        editWork(data);

        // reset editing state
        this.setState({
            editing: {}
        });
    }

    resetEditing() {
        this.props.reset();
        // reset editing state
        this.setState({
            editing: {}
        });
    }

    render() {
        const { work, current_user } = this.props;
        const { editing } = this.state;
        const { handleSubmit, pristine, invalid, submitting } = this.props;

        const isPerformer = work.resource && work.resource.performer === current_user.user_id;
        const isMonitor = work.resource && work.resource.monitor === current_user.user_id;
        const isCreator = work.meta && work.meta.create_user && work.meta.create_user.user_id === current_user.user_id;
        const enableEdit = isPerformer || isMonitor || isCreator;

        return (
            <div className="box box-warning box-work-tags">
                <div className="box-body no-padding">
                    <table className="table table-striped">
                        <tbody>
                            <tr>
                                <td className="work-detail-category-tags">
                                    <p className="work-item-label">
                                        <span role="button" onClick={() => enableEdit && this.startEdit("category_tags")}>
                                            Phân loại {enableEdit && <i className="fa fa-pencil margin-r-5"></i>}
                                        </span>
                                    </p>
                                    {!editing["category_tags"] && work.category_tags &&
                                        work.category_tags.map((item) => {
                                            return (
                                                <span key={item} className="label label-success">{item}</span>
                                            );
                                        })
                                    }

                                    {editing["category_tags"] &&
                                        <Field component={TagInput} subject="work_category" id="workCategoryTags" name="category_tags" />
                                    }
                                </td>
                            </tr>
                            <tr>
                                <td className="work-detail-knowledge-tags">
                                    <p className="work-item-label">
                                        <span role="button" onClick={() => enableEdit && this.startEdit("knowledge_tags")}>
                                            Kiến thức {enableEdit && <i className="fa fa-pencil margin-r-5"></i>}
                                        </span>
                                    </p>
                                    {!editing["knowledge_tags"] && work.knowledge_tags &&
                                        work.knowledge_tags.map((item) => {
                                            return (
                                                <span key={item} className="label label-primary">{item}</span>
                                            );
                                        })
                                    }

                                    {editing["knowledge_tags"] &&
                                        <Field component={TagInput} subject="work_knowledge" id="workKnowledgeTags" name="knowledge_tags" />
                                    }
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                {enableEdit &&
                    <div className="box-footer">
                        <div className="box-tools pull-right">
                            <button disabled={submitting} onClick={this.resetEditing} className="btn btn-xs btn-default" >
                                <i className="fa fa-undo"></i> Hủy
                        </button>
                            <button disabled={invalid || pristine || submitting} onClick={handleSubmit(this.submitEditing)} className="btn btn-xs btn-primary">
                                <i className="fa fa-save"></i> Lưu
                        </button>
                        </div>
                    </div>
                }
            </div>
        );
    }
}

const form = 'editWorkTags';
const fields = ["category_tags", "knowledge_tags"];
export default reduxForm({ form, validate, fields })(WorkTagsSection);
