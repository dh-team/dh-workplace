import React, { Component } from 'react'
import KeyResultsEdit from './KeyResultsEdit';

export default class KeyResultsField extends Component {
    constructor() {
        super();
        // bind method 
    }

    componentDidMount() {
        const { input: { value, onChange } } = this.props;
        console.log("KeyResultsField componentDidMount", value);
    }

    render() {
        const { input: { value, onChange }, meta: { dirty } } = this.props;
        console.log("KeyResultsField render", value);
        return (
            <KeyResultsEdit data={value} dirty={dirty} handleDataChanged={(newData) => onChange(newData)} />
        )
    }
}

