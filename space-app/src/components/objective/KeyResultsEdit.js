import React, { Component } from 'react'
import KeyResultEdit from './KeyResultEdit';

export default class KeyResultsEdit extends Component {
    constructor() {
        super();
        // bind method
        this.handleItemChanged = this.handleItemChanged.bind(this);
        this.handleItemDeleted = this.handleItemDeleted.bind(this);
        this.handleAddItem = this.handleAddItem.bind(this);
        this.changeState = this.changeState.bind(this);
        this.initState = this.initState.bind(this);

        // state
        this.state = {
            count: 0,
            items: []
        }
    }

    componentDidMount() {
        this.initState(this.props.data);
    }

    initState(data) {
        if (!data) data = [];
        var count = 0;
        var items = data.map((item) => {
            return {
                ...item,
                id: ++count
            };
        });
        this.setState({
            items: items,
            count: count
        });
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.data) !== JSON.stringify(this.props.data) && !nextProps.dirty) {
            console.log("KeyResultsEdit update state");
            this.initState(nextProps.data);
        }
    }

    handleItemChanged(editedItem) {
        console.log("handleItemChanged", editedItem);
        var items = this.state.items.map((item) => {
            if (item.id === editedItem.id) return editedItem;
            return item;
        });
        this.changeState({
            items: items
        });
    }

    handleItemDeleted(deletedItem) {
        console.log("handleItemDeleted", deletedItem);
        var items = this.state.items.filter((item) => item.id !== deletedItem.id);
        this.changeState({
            items: items
        });
    }
    handleAddItem() {
        console.log("handleAddItem");
        var items = [...this.state.items];
        var newId = this.state.count + 1;
        items.push({
            id: newId,
            content: "Key result",
            progress: 0
        });
        this.changeState({
            count: newId,
            items: items
        });
    }

    changeState(newState) {
        this.setState(newState);
        if (!newState.items) return;
        var newData = newState.items.map((item) => {
            var newItem = { ...item };
            delete newItem.id;
            return newItem;
        });
        this.props.handleDataChanged(newData);
    }

    render() {
        const items = this.state.items;
        return (
            <div className="objective-results-mgr">
                <div className="row objective-results-header">
                    <div className="col-md-8">
                        <span>Kết quả then chốt</span>
                    </div>
                    <div className="col-md-2">
                        <span>Hoàn thành</span>
                    </div>
                    <div className="col-md-2">
                        <a role="button" className="btn btn-primary btn-xs" onClick={this.handleAddItem}>
                            <i className="fa fa-plus"></i> Thêm
                        </a>
                    </div>
                </div>
                {
                    items.map((item) => {
                        return <KeyResultEdit data={item} handleItemChanged={this.handleItemChanged} handleItemDeleted={this.handleItemDeleted} />
                    })
                }
            </div>
        )
    }
}

