import React, { Component } from 'react'

export default class KeyResultEdit extends Component {
    constructor() {
        super();
        // bind method 
        this.handleTextChanged = this.handleTextChanged.bind(this);
    }

    componentDidMount() {

    }

    handleTextChanged(event, field) {
        console.log("handleTextChanged", event.target.value, field);
        const { data, handleItemChanged } = this.props;
        var changedData = {
            ...data,
            [field]: event.target.value
        };
        handleItemChanged(changedData);
    }

    render() {
        const { data, handleItemDeleted } = this.props;
        return (
            <div className="objective-result-edit">
                <div className="row">
                    <div className="col-md-8">
                        <input type="text" className="form-control input-sm" placeholder="Kết quả then chốt" value={data.content} onChange={(e) => this.handleTextChanged(e, "content")} />
                    </div>
                    <div className="col-md-2">
                        <input type="text" className="form-control input-sm" placeholder="% đạt được" value={data.progress} onChange={(e) => this.handleTextChanged(e, "progress")} />
                    </div>
                    <div className="col-md-2">
                        <a role="button" className="btn btn-xs" onClick={() => handleItemDeleted(data)}><i className="fa fa-minus"></i> Xóa</a>
                    </div>
                </div>
            </div>
        )
    }
}

