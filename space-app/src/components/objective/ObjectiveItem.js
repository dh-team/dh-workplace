'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import DateDisplay from '../common/DateDisplay';
import ScopeDisplay from '../common/ScopeDisplay';
import ProgressBar from '../common/ProgressBar';
import { openDialog } from '../../actions/action-msg';
import { deleteObjective } from '../../actions/action-objective';
import { getAllObjectives } from '../../selectors/selector-objective';

class ObjectiveItem extends React.Component {
    constructor() {
        super();

        this.headerClick = this.headerClick.bind(this);
        this.editClick = this.editClick.bind(this);
        this.deleteClick = this.deleteClick.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);

        this.state = {
            view_detail: false
        };
    }

    headerClick() {
        var viewDetail = this.state.view_detail;
        this.setState({
            view_detail: !viewDetail
        });
    }

    editClick() {
        const { objective, handleEdit } = this.props;
        handleEdit(objective._id);
    }

    deleteClick() {
        this.props.openDialog({
            title: "Xóa Mục tiêu",
            content: "Bạn chắc chắn muốn xóa mục tiêu này?",
            confirmCallback: this.confirmDelete
        });
    }

    confirmDelete() {
        const { objective, deleteObjective } = this.props;
        deleteObjective(objective._id);
    }

    render() {
        const { objective } = this.props;
        if (!objective) return null;

        return (
            <div className="timeline-item">
                <span className="time"><i className="fa fa-crosshairs"></i> <ScopeDisplay scope={objective.owner}/></span>

                <h3 className="timeline-header" onClick={this.headerClick}>
                    {objective.content}
                </h3>

                {this.state.view_detail &&
                    <div className="timeline-body">
                        <p className="objective-detail">{objective.detail}</p>
                        {objective.key_results && objective.key_results.map((result) => {
                            var progress = result.progress;
                            if (progress > 100) progress = 100;
                            if (progress < 0) progress = 0;

                            return (
                                <div className="objective-key-result">
                                    <p className="objective-key-result-content">{result.content} <span className={"label " + (progress == 100 ? "bg-green" : "bg-yellow")}>{progress}%</span>
                                    </p>
                                    <ProgressBar progress={progress} className="objective-key-result-progress" /> 
                                </div>
                            );
                        })}                       
                    </div>
                }

                {this.state.view_detail &&
                    <div className="timeline-footer">
                        <button className="btn btn-primary btn-xs btn-edit" onClick={this.editClick}>Sửa</button>
                        <button className="btn btn-danger btn-xs btn-delete" onClick={this.deleteClick}>Xóa</button>
                    </div>
                }

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        // objectives: getAllObjectives(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        openDialog: (params) => dispatch(openDialog(params)),
        deleteObjective: (params) => dispatch(deleteObjective(params)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectiveItem);
