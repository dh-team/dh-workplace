'use strict';

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, formValueSelector } from 'redux-form';
import FormInput from '../form/Input';
import GroupSelect from '../form/GroupSelect';
import TextArea from '../form/TextArea';
import FormSelect from '../form/Select';
import CheckBox from '../form/CheckBox';
import DatePicker from '../form/DatePicker';
import RadioSelect from '../form/RadioSelect';
import FormModal from '../common/FormModal';
import KeyResultsField from './KeyResultsField';
import valid from '../../utils/validate';
import { addObjective, editObjective } from '../../actions/action-objective';
import { getObjective } from '../../selectors/selector-objective';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.content, 1, 250)) {
        errors.content = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    if (!valid.hasLength(values.detail, 0, 500)) {
        errors.detail = 'Độ dài tối đa 500 ký tự';
    }

    if (values.time && values.time.range === "manual") {
        if (!valid.isDate(values.time.from)) {
            if (!errors.time) errors.time = {};
            errors.time.from = 'Ngày không hợp lệ';
        }
        if (!valid.isDate(values.time.to)) {
            if (!errors.time) errors.time = {};
            errors.time.to = 'Ngày không hợp lệ';
        }
    }

    return errors;
}

class ObjectiveForm extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }

    onShow() {
        const { current_objective, form_type, initialize, reset } = this.props;
        if (form_type === "add") {
            initialize({
                content: "",
                detail: "",
                key_results: [],
                time: {
                    range: "annual", // "monthly" "quarterly", "manual"
                    year: (new Date).getFullYear(),
                    quarter: Math.floor((new Date).getMonth() / 3) + 1
                },
                owner_type: "all", // "all" | "group" | "personal"                
            });
        } else if (current_objective) {
            var initValue = {
                content: current_objective.content,
                detail: current_objective.detail,
                key_results: current_objective.key_results,
                time: current_objective.time
            };
            if (current_objective.owner === "g:all") {
                initValue.owner_type = "all";
            } else if (current_objective.owner && current_objective.owner.startsWith("g:")) {
                initValue.owner_type = "group";
                initValue.owner_group = current_objective.owner.substr(2);
            } else {
                initValue.owner_type = "personal";
            }
            initialize(initValue);
        }
        reset();
    }

    submit(values, dispatch) {
        const { current_objective, form_type } = this.props;

        var data = {
            content: values.content,
            detail: values.detail,
            key_results: values.key_results,
            time: {
                range: values.time.range
            }
        };

        switch (values.time.range) {
            case "annual": {
                data.time.year = values.time.year;
                break;
            }
            case "quarterly": {
                data.time.year = values.time.year;
                data.time.quarter = values.time.quarter;
                break;
            }
            case "manual": {
                data.time.from = values.time.from;
                data.time.to = values.time.to;
                break;
            }
        }
        switch (values.owner_type) {
            case "all": {
                data.owner = "g:all";
                break;
            }
            case "group": {
                data.owner = "g:" + values.owner_group;
                break;
            }
            case "personal": {
                data.owner = "u:own";
                break;
            }
        }
        console.log(form_type + " objective: " + JSON.stringify(values, null, 4));


        if (form_type === "add") {
            dispatch(addObjective(data));
        }
        else {
            data.id = current_objective._id;
            dispatch(editObjective(data));
        }
    }

    render() {
        const { id, form_type, current_objective, selected_time_type, selected_owner_type } = this.props; // form_type = 'add' | 'edit'
        const { error, handleSubmit, pristine, invalid, submitting } = this.props;
        const ownerTypes = [{ value: "all", label: "Doanh nghiệp" }, { value: "group", label: "Nhóm" }, { value: "personal", label: "Cá nhân" }];

        if (form_type === "edit" && !current_objective) {
            return (<p>Dữ liệu không tồn tại!</p>);
        }
        return (
            <FormModal id={id || "objectiveFormModal"} title={form_type === 'add' ? "Tạo mục tiêu" : "Sửa mục tiêu"} disableSubmit={pristine || submitting || invalid}
                okCallback={handleSubmit(this.submit)} onShow={this.onShow} >

                <Field component={FormInput} name="content" type="text" label="Mục tiêu" placeholder="Mục tiêu" />
                <Field component={FormInput} name="detail" type="textarea" label="Mô tả về mục tiêu" />

                <div className="row">
                    <div className="col-md-4">
                        <Field component={FormSelect} name="time.range" value="annual" label="Thời gian" >
                            <option value="annual">Năm</option>
                            <option value="quarterly">Quý</option>
                            <option value="manual">Tùy chọn</option>
                        </Field>
                    </div>

                    {(selected_time_type === "annual" || selected_time_type === "quarterly") &&
                        <div className="col-md-4">
                            <Field component={FormSelect} name="time.year" label="Năm" >
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </Field>
                        </div>
                    }

                    {selected_time_type === "quarterly" &&
                        <div className="col-md-4">
                            <Field component={FormSelect} name="time.quarter" label="Quý" >
                                <option value="1">Q1</option>
                                <option value="2">Q2</option>
                                <option value="3">Q3</option>
                                <option value="4">Q4</option>
                            </Field>
                        </div>
                    }

                    {selected_time_type === "manual" &&
                        <div className="col-md-4">
                            <Field component={DatePicker} id="fromDate" name="time.from" label="Bắt đầu" />
                        </div>
                    }
                    {selected_time_type === "manual" &&
                        <div className="col-md-4">
                            <Field component={DatePicker} id="toDate" name="time.to" label="Kết thúc" />
                        </div>
                    }

                </div>

                <div className="row">
                    <div className="col-md-6">
                        <Field component={RadioSelect} name="owner_type" optionLabelCls="radio-inline" label="Mục tiêu của" options={ownerTypes} />
                    </div>
                    <div className="col-md-6">
                        {selected_owner_type === "group" &&
                            <Field component={GroupSelect} name="owner_group" label="Nhóm" />
                        }
                    </div>
                </div>
                <Field component={KeyResultsField} name="key_results" label="Key results" />
            </FormModal>
        );
    }
}

const form = 'objectiveForm';
const fields = ["content", "detail", "time.range", "time.year", "time.quarter", "time.from", "time.to", "owner_type", "owner_group", "key_results"];
ObjectiveForm = reduxForm({ form, validate, fields })(ObjectiveForm);
const selector = formValueSelector('objectiveForm');
const mapStateToProps = (state, ownProps) => {
    return {
        current_objective: getObjective(state, ownProps.editing_id),
        selected_time_type: selector(state, 'time.range'),
        selected_owner_type: selector(state, 'owner_type'),
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // initializeForm: function(data) {
        //     dispatch(initialize(form, data, fields))
        // }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(ObjectiveForm);
