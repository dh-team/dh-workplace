'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import DateDisplay from '../common/DateDisplay';
import ObjectiveItem from './ObjectiveItem';
import ObjectiveForm from './ObjectiveForm';
import { getAllObjectives, getTimelineObjectives } from '../../selectors/selector-objective';

class ObjectiveTimeline extends React.Component {
    constructor() {
        super();

        this.addClick = this.addClick.bind(this);
        this.editClick = this.editClick.bind(this);
        this.renderObjectivePoint = this.renderObjectivePoint.bind(this);
        this.renderTimePoint = this.renderTimePoint.bind(this);

        this.state = {
            form_type: "add",
            editing_id: null
        };
    }

    addClick() {
        this.setState({
            form_type: "add",
            editing_id: null
        });
        $("#objectiveFormModal").modal('show');
    }

    editClick(id) {
        this.setState({
            form_type: "edit",
            editing_id: id
        });
        $("#objectiveFormModal").modal('show');
    }

    render() {
        const { timeline } = this.props;
        return (
            <div className="objective-timeline">
                <button className="btn btn-primary btn-add-objective" onClick={this.addClick}>Tạo mới</button>
                <ul className="timeline">
                    {
                        timeline.map((point) => {
                            if (point.type === "objective") {
                                return this.renderObjectivePoint(point);
                            } else {
                                return this.renderTimePoint(point);
                            }
                        })
                    }

                    <li>
                        <i className="fa fa-clock-o bg-gray"></i>
                    </li>
                </ul>

                <ObjectiveForm id="objectiveFormModal" form_type={this.state.form_type} editing_id={this.state.editing_id} />
            </div>
        );
    }

    renderTimePoint(point) {
        var bg = "bg-blue";
        switch (point.type) {
            case "annual":
                bg = "bg-red";
                break;
            case "quarterly":
                bg = "bg-yellow";
                break;
            default:
                bg = "bg-green";
                break;
        }
        return (<li key={point.key} className={"time-label time-label-" + point.type}>
            <span className={bg}>{point.text}</span>
        </li>);
    }

    renderObjectivePoint(point) {
        var obj = point.objective;
        var icon = "fa-user";
        if (obj.owner === "g:all") {
            icon = "fa-briefcase";
        } else if (obj.owner && obj.owner.startsWith("g:")) {
            icon = "fa-users";
        }
        return (
            <li key={obj._id}>
                <i className={"fa bg-blue " + icon}></i>
                <ObjectiveItem objective={obj} handleEdit={this.editClick} />
            </li>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        timeline: getTimelineObjectives(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ObjectiveTimeline);
