import React, { Component } from 'react'

export default class RadioSelect extends Component {
    constructor() {
        super();

        this.handleClick = this.handleClick.bind(this);       
    }

    handleClick(value) {
        console.log("RadioSelect handleClick", value);
        const { input: { onChange } } = this.props;
        onChange(value);
    }
    
    render() {
        const { input: { value, onChange }, meta: { touched, error, warning }, options, label, optionLabelCls, inputWrapCls } = this.props;
        const wrapCls =  "form-group " + this.props.wrapCls + (touched && error ? " has-error" : " ");
        const labelCls = "control-label " + this.props.labelCls;

        return (
            <div className={wrapCls}>
                {label && <label className={labelCls}>{label}</label>}
                <div className={inputWrapCls}>
                {
                    options.map((item) => {
                        return (<label className={optionLabelCls} key={item.value}>
                            <input type="radio" value={item.value} checked={value === item.value} onClick={() => this.handleClick(item.value)} /> {item.label}
                        </label>);
                    })
                }

                {touched && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        )
    }
}

