import React, { Component } from 'react'

export default class FormSelect extends Component {
    render() {
        const { id, input, label, meta: { touched, error, invalid, warning } } = this.props;
        const labelCls = "control-label " + (this.props.labelCls || "");  
        const inputCls = "form-control " + (this.props.inputCls || "");
        const inputWrapCls = this.props.inputWrapCls || "";
        const placeholder = this.props.placeholder || "";
        const wrapCls = 'form-group ' + (this.props.wrapCls || ' ') + (touched && error ? 'has-error' : '');
        return (
            <div className={wrapCls}>
                {label && <label className={labelCls}>{label}</label>}
                <div className={inputWrapCls}>
                    <select id={id} {...input}  className={inputCls} placeholder={placeholder}>
                    {this.props.children}
                    </select>
                    {touched && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        )
    }
}

