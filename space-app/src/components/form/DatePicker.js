import React, { Component } from 'react';
import moment from 'moment';

class DatePicker extends Component {
    constructor() {
        super();
        this.setInitValue = this.setInitValue.bind(this);
        this.inputElm = null;
        this.syncState = {
            value: null,
            fireOnChange: true
        };
    }

    componentDidMount() {
        var thisCom = this;
        const { input: { value, onChange } } = this.props;

        this.inputElm = $(this.refs.datePicker);
        this.inputElm.datepicker({
            todayBtn: "linked",
            autoclose: true,
            format: "yyyy-mm-dd",
            todayHighlight: true,
            clearBtn: true
        });

        this.inputElm.datepicker().on("changeDate", function(e) {
            if (!thisCom.syncState.fireOnChange) {
                console.log("DatePicker: ignore change date");
                thisCom.syncState.fireOnChange = true;
                return;
            }
            var dateStr = null;
            if (e.date) {
                var noTimeZoneDate = new Date(e.date);
                // noTimeZoneDate.setMinutes(noTimeZoneDate.getMinutes() - noTimeZoneDate.getTimezoneOffset());           
                dateStr = moment(noTimeZoneDate).format("YYYY-MM-DD");
                console.log("DatePicker: changeDate", dateStr, e.date.toISOString());
            }
            thisCom.syncState.value = dateStr;
            onChange(dateStr);
        });
        
        this.inputElm.datepicker().on("clearDate", function (e) {
            console.log("DatePicker: clearDate");
            thisCom.syncState.value = null;
            onChange(null);
        });

        if (value) {
            console.log("DatePicker: init value", value);
            this.setInitValue(value);
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input && this.props.meta && this.props.meta.pristine && (nextProps.input.value || null) !== this.syncState.value) {
            console.log("DatePicker: update value ", nextProps.input.value, this.syncState.value);
            this.setInitValue(nextProps.input.value);
        }
    }
    
    componentWillUnmount() {
        this.inputElm.datepicker('destroy');
    }
    
    setInitValue(value) {
        this.syncState = {
            value: value,
            fireOnChange: false
        };

        this.inputElm.datepicker("setDate", new Date(value));
    }
    
    render() {
        var { label, meta: { touched, dirty, error, warning }, error_only_dirty, placeholder, labelCls, inputCls, inputWrapCls, wrapCls } = this.props;
        labelCls = "control-label " + (labelCls || "");
        inputCls = "form-control " + (inputCls || "");
        inputWrapCls = inputWrapCls || "";
        placeholder = placeholder || "";
        wrapCls = 'form-group ' + (wrapCls || '') + ((dirty && error) ? ' has-error' : ' ');

        return (
            <div className={wrapCls}>
                {label && <label className={labelCls}>{label}</label>}
                <div className={inputWrapCls}>
                    <div className="input-group date" ref="datePicker">
                        <input type="text" className={inputCls} placeholder={placeholder} />
                        <span className="input-group-addon"><i className="glyphicon glyphicon-calendar"></i></span>
                    </div>
                    {dirty && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        );
    }
}

export default DatePicker;
