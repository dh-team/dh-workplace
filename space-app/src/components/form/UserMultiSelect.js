import React, { Component } from 'react';
import { connect } from 'react-redux';
import FormSelect from './Select';
import { getUsersInSpace } from '../../selectors/selector-user';

class UserMultiSelect extends Component {
    constructor() {
        super();

        this.setInitValue = this.setInitValue.bind(this);

        this.inputElm = null;
        this.select2Elm = null;

        this.syncState = {
            value: null,
            fireOnChange: true
        }
    }

    componentDidMount() {
        const { users, placeholder, input: { value, onChange } } = this.props;
        var data = users.map(function (item) {
            return {
                id: item.user._id,
                text: "@" + item.uname + " - " + item.user.display_name
            }
        });
        this.inputElm = $(this.refs.inputContainer).find("select");
        this.select2Elm = this.inputElm.select2({
            data: data,
            tags: true,
            tokenSeparators: [','],
            multiple: true,
            placeholder: placeholder
        });

        var thisCom = this;
        this.inputElm.on("change", function (e) {
            if (!thisCom.syncState.fireOnChange) {
                console.log("UserMultiSelect: ignore change value");
                thisCom.syncState.fireOnChange = true;
                return;
            }

            var newValue = thisCom.select2Elm.val();
            console.log("UserMultiSelect: change value", newValue);
            if (!newValue) newValue = [];

            // update value
            thisCom.syncState.value = newValue;
            onChange(newValue);
        });

        // set value
        this.setInitValue(value);
    }

    componentWillReceiveProps(nextProps) {
        // value is Array so need JSON.stringify to compare
        if (nextProps.input && this.props.meta && this.props.meta.pristine && JSON.stringify(nextProps.input.value) !== JSON.stringify(this.syncState.value)) {
            console.log("UserMultiSelect: update value", nextProps.input.value, this.syncState.value);
            this.setInitValue(nextProps.input.value);
        }
    }

    componentWillUnmount() {
        this.inputElm.select2('destroy');
    }

    setInitValue(value) {
        var selectValue = value;
        if (!selectValue) selectValue = [];
        this.syncState = {
            value: selectValue,
            fireOnChange: false
        };
        this.select2Elm.val(selectValue).trigger("change");
    }

    render() {
        const { users, input, ...rest } = this.props;
        return (
            <div ref="inputContainer">
                <FormSelect {...rest} />
            </div>

        );
    }
}
const mapStateToProps = (state) => {
    return {
        users: getUsersInSpace(state)
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

export default connect(mapStateToProps, mapDispatchToProps)(UserMultiSelect);
