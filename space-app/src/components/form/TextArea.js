import React, { Component } from 'react'

export default class TextArea extends Component {
    constructor() {
        super();
        // bind method
        this.onKeyDown = this.onKeyDown.bind(this);
    }
    
    componentDidMount() {
        if(this.props.focus){
            this.textarea.focus();    
        }
        this.onKeyDown();
    }
    
    componentWillReceiveProps(nextProps) {
        if(nextProps.input.value !== this.props.input.value){
            // console.log("value changed");
            this.onKeyDown();
        }
    }
    
    onKeyDown() {
        var el = this.textarea;
        setTimeout(function() {
            el.style.cssText = 'height:auto;';
            // console.log("scrollHeight:" + el.scrollHeight);
            el.style.cssText = 'height:' + (el.scrollHeight) + 'px';    
        }, 0);
    }

    render() {
        var { input, id, label, type, meta: { touched, error, dirty, warning }, error_only_dirty,
              placeholder, labelCls, inputCls, inputWrapCls, wrapCls} = this.props;
              
        labelCls = "control-label " + (labelCls || "");
        inputCls = "textarea-autoheight form-control " + (inputCls || "");
        inputWrapCls = inputWrapCls || "";
        placeholder = placeholder || "";
        wrapCls = 'form-group ' + (wrapCls || '') + (touched && (!error_only_dirty || dirty) && error ? ' has-error' : ' ');

        return (
            <div className={wrapCls}>
                <label className={labelCls}>{label}</label>
                <div className={inputWrapCls}>
                    <textarea  ref={(elm) => { this.textarea = elm; }} {...input} id={id} className={inputCls} placeholder={placeholder}></textarea>
                    {this.props.children}
                    {touched && (!error_only_dirty || dirty) && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        )
    }
}

