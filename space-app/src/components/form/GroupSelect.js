import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, Field } from 'redux-form';
import FormSelect from './Select';
import { getAllGroups } from '../../selectors/selector-group';

class GroupSelect extends Component {
    constructor() {
        super();

        this.inputElm = null;
        this.select2Elm = null;
    }

    componentDidMount() {
        const { groups, placeholder, input: { value, onChange } } = this.props;
        var data = groups.map(function (item) {
            return {
                id: item._id,
                text: "@" + item.uname + " - " + item.name
            }
        });
        this.inputElm = $(this.refs.inputContainer).find("select");
        this.select2Elm = this.inputElm.select2({
            data: data,
            multiple: false,
            placeholder: placeholder
        });
        // set value
        if (value) {
            this.select2Elm.val(value).trigger("change");
        }else{
            onChange(this.select2Elm.val());
        }

        var thisCom = this;
        this.inputElm.on("change", function (e) {
            // update value
            onChange(thisCom.select2Elm.val());
        });        
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input && nextProps.input.value && nextProps.input.value !== this.select2Elm.val()) {
            this.select2Elm.val(nextProps.input.value).trigger("change");
        }
    }

    componentWillUnmount() {
        this.inputElm.select2('destroy');
    }

    render() {
        const { groups, input, ...rest } = this.props;
        return (
            <div ref="inputContainer">
                <FormSelect {...rest} />
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        groups: getAllGroups(state)
    }
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupSelect);
