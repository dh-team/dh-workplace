import React, { Component } from 'react';
import { change, Field } from 'redux-form';

import UploadGallery from '../common/UploadGallery';

export default class FileUploader extends Component {
    constructor() {
        super();
        this.handleAttachFiles = this.handleAttachFiles.bind(this);

        this.state = {
            files: []
        };
    }

    componentDidMount() {
        const { input: { value, onChange } } = this.props;
        this.setState({ files: value });

    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input && this.props.meta && this.props.meta.pristine && nextProps.input.value !== this.state.files) {
            console.log("FileUploader: update value", nextProps.input.value);
            this.setState({ files: nextProps.input.value });
        }
    }

    componentWillUnmount() {

    }

    handleAttachFiles(files) {
        const { input: { value, onChange } } = this.props;
        // this.setState({ files: files });
        onChange(files);
    }

    render() {
        const { display_type } = this.props;
        const { files } = this.state;
        return (
            <div ref="inputContainer">
                <UploadGallery files={files} display_type={display_type || "list"} onFilesChanged={this.handleAttachFiles} />
            </div>
        )
    }
}

