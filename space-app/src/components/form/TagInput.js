import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, Field } from 'redux-form';
import FormSelect from './Select';
import { addTag } from '../../actions/action-tag';
import { getTags } from '../../selectors/selector-tag';

class TagInput extends Component {
    constructor() {
        super();

        this.inputElm = null;
        this.select2Elm = null;
        this.state = { count: 0 };
    }

    componentDidMount() {
        const { subject, addTag, tags, id, placeholder, input: { value, onChange } } = this.props;
        var data = tags.map(function (item) {
            return {
                id: item.keyword,
                text: item.keyword,
            }
        });
        this.inputElm = $('#' + id);
        this.select2Elm = this.inputElm.select2({
            data: data,
            tags: true,
            tokenSeparators: [','],
            multiple: true,
            placeholder: placeholder
        });
        // set value
        if (value) {
            this.select2Elm.val(value).trigger("change");
        }

        this.inputElm.on("select2:select", function (e) {
            // console.log("select2:select");
            var keyword = e.params.data.id;
            // console.log(keyword);
            addTag({ subject, keyword });
        });
        var thisCom = this;
        this.inputElm.on("change", function (e) {
            // console.log("select2:change");
            // console.log(thisCom.select2Elm.val());
            // update value
            onChange(thisCom.select2Elm.val());
        });
    }

    componentWillReceiveProps(nextProps) {
        // value is Array so need JSON.stringify to compare
        if (nextProps.input && this.props.meta && this.props.meta.pristine && JSON.stringify(nextProps.input.value) !== JSON.stringify(this.select2Elm.val())) {
            console.log("TagInput: update value", nextProps.input.value, this.select2Elm.val());
            this.select2Elm.val(nextProps.input.value).trigger("change");
        }
    }

    componentWillUnmount() {
        this.inputElm.select2('destroy');
    }

    render() {
        const { id, tags, input, addTag, ...rest } = this.props;
        return (
            <FormSelect id={id || "tagInput"} {...rest} />
        )
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        tags: getTags(state, ownProps.subject)
    }
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        addTag: function (data) {
            dispatch(addTag(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(TagInput);
