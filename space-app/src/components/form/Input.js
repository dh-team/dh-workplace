import React, { Component } from 'react'
import ReactDOM from 'react-dom';

export default class FormInput extends Component {
    componentDidMount(){
        // if(this.props.focus){
        //     ReactDOM.findDOMNode(this.refs.inputElm).focus();
        // }
    }

    render() {
        var { input, id, label, type, meta: { touched, dirty, error, warning }, error_only_dirty, placeholder,
              labelCls, inputCls, inputWrapCls, wrapCls, addonPre, addonPost } = this.props;
        labelCls = "control-label " + (labelCls || "");
        inputCls = "form-control " + (inputCls || "");
        inputWrapCls = inputWrapCls || "";

        placeholder = placeholder || "";
        wrapCls = 'form-group ' + (wrapCls || '') + (touched && (!error_only_dirty || dirty) && error ? ' has-error' : ' ');

        var inputField = <input id={id} {...input} type={type|| "text"} className={inputCls} placeholder={placeholder} />;
        if(type === "textarea"){
            inputField = <textarea id={id} {...input} type="textarea" className={inputCls} placeholder={placeholder}></textarea>
        }

        return (
            <div className={wrapCls}>
                {label && <label className={labelCls}>{label}</label>}
                <div className={inputWrapCls}>
                    {(addonPre || addonPost) ?
                        (<div className="input-group">
                            {addonPre && <div className="input-group-addon">{addonPre}</div>}
                            {inputField}
                            {addonPost && <div className="input-group-addon">{addonPost}</div>}
                        </div>) : inputField
                    }
                    {this.props.children}
                    {touched && (!error_only_dirty || dirty) && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        )
    }
}
