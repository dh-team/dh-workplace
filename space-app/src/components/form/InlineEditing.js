import React, { Component } from 'react'

export default class InlineEditing extends Component {
    constructor() {
        super();

        this.handleChange = this.handleChange.bind(this);      
        
        this.state = {
            newValue: null,
            error: null,
            pristine: true,
        };
    }
    
    componentDidMount() {
        this.setState({newValue: this.props.value});
    }
    
    componentWillReceiveProps(nextProps) {
        if (this.state.pristine && nextProps.value !== this.state.newValue) {
            console.log("int value change");
            this.setState({newValue: nextProps.value});
        }
    }

    handleChange(event) {
        const value = event.target.value;
        console.log("InlineEditing handleChange", value);
        const { onChange } = this.props;
        var error = this.validate(value);
        this.setState({
            newValue: value,
            error: error,
            pristine: false,
        });
        if(!error){
            onChange(value);    
        }
    }
    
    validate(value){
        if(!value) return "Không được để trống";
        return null;
    }
    
    render() {
        const {newValue, error} = this.state;
        const wrapCls = "form-group " + this.props.wrapCls + (error ? " has-error" : " ");
        return (
            <div className={wrapCls}>
                <input type="text" value={newValue} className="form-control" onChange={this.handleChange} />
                {error && <span className='help-block validation-error'>{error}</span>}
            </div>
        );
    }
}

