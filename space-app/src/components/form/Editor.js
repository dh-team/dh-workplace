import React, { Component } from 'react';
import cookie from 'react-cookie';
import FormInput from './Input';

var uploadHeaders = {
    'Authorization': "Bearer " + cookie.load("space_token"),
    'Args': JSON.stringify({app: "Wiki", partition: "g:all"})
};

class Editor extends Component {
    constructor() {
        super();

        this.inputElm = null;
    }

    componentDidMount(){
        var thisCom = this;
        const {id, input: {value, onChange}} = this.props;
        var inputElm = this.inputElm = $(this.refs.editor).find("textarea");
        inputElm.summernote({
            minHeight: 300,
            callbacks: {
                onChange: function(contents, $editable) {
                    onChange(contents);
                },
                onImageUpload: function(files) {
                    console.log("onImageUpload");
                    console.log(files);
                    thisCom.sendFile(files[0], function(err, data){
                        if(err){
                            console.log("upload fail: " + JSON.stringify(err));
                        }else{
                            data = JSON.parse(data);
                            console.log("upload ok: " + JSON.stringify(data));
                            if(data.uploaded){
                                data.uploaded.map(function(file){
                                    inputElm.summernote('insertImage', "/fs/file/?id=" + file.id);
                                });
                            }
                        }
                    });
                }
            }
        });
    }
    
    componentWillUnmount(){
        console.log("summernote destroy");
        this.inputElm.summernote('destroy');
    }
    
    sendFile(file, callback) {
        var data = new FormData();
        data.append("file", file)
        $.ajax({
            url: '/fs/upload',
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            headers: uploadHeaders,
            success: function(data) {
                callback(null, data)
            },
            error: function(jqXHR, textStatus, errorThrown){
                callback(textStatus);
            }
        });
    }
    
    render() {
        const { id, ...rest } = this.props;
        return (
            <div className="html-editor-container" ref="editor">
               <FormInput type="textarea" id={id || "textEditor"} {...rest} />
            </div>
        )
    }
}

export default Editor;
