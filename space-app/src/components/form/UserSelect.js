import React, { Component } from 'react';
import { connect } from 'react-redux';
import { change, Field } from 'redux-form';
import FormSelect from './Select';
import { getUsersInSpace } from '../../selectors/selector-user';

class UserSelect extends Component {
    constructor() {
        super();

        this.setInitValue = this.setInitValue.bind(this);

        this.inputElm = null;
        this.select2Elm = null;
        this.syncState = {
            value: null,
            fireOnChange: true
        }
    }

    componentDidMount() {
        const { users, placeholder, input: { value, onChange }, required } = this.props;
        var data = users.map(function (item) {
            return {
                id: item.user._id,
                text: "@" + item.uname + " - " + item.user.display_name
            }
        });
        if (!required) {
            data.splice(0, 0, { id: "none", text: "-- Chọn --" });
        }
        this.inputElm = $(this.refs.inputContainer).find("select");
        this.select2Elm = this.inputElm.select2({
            data: data,
            multiple: false,
            placeholder: placeholder
        });

        var thisCom = this;
        this.inputElm.on("change", function (e) {
            if (!thisCom.syncState.fireOnChange) {
                console.log("UserSelect: ignore change value");
                thisCom.syncState.fireOnChange = true;
                return;
            }

            var newValue = thisCom.select2Elm.val();
            console.log("UserSelect: change value", newValue);
            if (!newValue || newValue === "none") newValue = null;           
             
            // update value
            thisCom.syncState.value = newValue;
            onChange(newValue);
        });

        console.log("UserSelect: init value", value);
        this.setInitValue(value);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.input && this.props.meta && this.props.meta.pristine && (nextProps.input.value || null) !== this.syncState.value) {
            console.log("UserSelect: update value", nextProps.input.value, this.syncState.value);
            this.setInitValue(nextProps.input.value);
        }
    }

    componentWillUnmount() {
        this.inputElm.select2('destroy');
    }

    setInitValue(value) {
        var stateValue = value
        var selectValue = value;
        if (!value) {
            stateValue = null;
            selectValue = "none";
        } else if (value === "none") {
            stateValue = null;
        }

        this.syncState = {
            value: stateValue,
            fireOnChange: false
        };

        this.select2Elm.val(selectValue).trigger("change");
    }

    render() {
        const { input, ...rest } = this.props;
        return (
            <div ref="inputContainer">
                <FormSelect {...rest} />
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        users: getUsersInSpace(state)
    }
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UserSelect);
