'use strict';

import React, {Component, PropTypes} from 'react';
import { reduxForm, Field, SubmissionError, reset } from 'redux-form';
import FormInput from '../../form/Input';
import valid from '../../../utils/validate';
import { addSpaceUser } from '../../../actions/action-user';


//client side validation
function validate(values) {
    const errors = {};
    if (!valid.isEmail(values.user)) {
        errors.user = 'Email không đúng định dạng';
    }

    if (!values.uname || !valid.hasLength(values.uname, 1, 50)) {
        errors.uname = 'ID không được để trống, độ dài từ 1-50 ký tự';
    }
    if (!valid.isID(values.uname, 1, 50)) {
        errors.uname = 'ID không đúng định dạng';
    }
    return errors;
}

class AddUserForm extends Component {
    componentDidMount() {
        this.submit = this.submit.bind(this);
    }

    submit(values, dispatch) {
        console.log("add user to space: " + JSON.stringify(values, null, 4));
        return dispatch(addSpaceUser(values));
    }

    render() {
        const { error, handleSubmit, pristine, reset, submitting } = this.props;
        const iconMail = <i className="fa fa-envelope"></i>;     
        return (
            <div role="alert" className="add-user-form box box-primary">
                <form onSubmit={handleSubmit(this.submit)} className="box-body row">
                    <Field component={FormInput} name="user" type="text" label="Email người dùng" placeholder="Email người dùng" labelCls="sr-only" wrapCls="col-lg-4" addonPre={iconMail} />
                    <Field component={FormInput} name="uname" type="text" label="ID dùng trong space" placeholder="ID dùng trong space" labelCls="sr-only" wrapCls="col-lg-4" addonPre="@" />
                    <div className="form-group col-lg-4">
                        <button type="submit" disabled={pristine || submitting} className="btn btn-primary">Thêm người dùng</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default reduxForm({
  form: 'addUserForm',  // a unique identifier for this form
  validate
})(AddUserForm)
