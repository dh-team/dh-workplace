'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import SpaceUserItem from './SpaceUserItem';
import { getUsersInSpace } from '../../../selectors/selector-user';

class SpaceUserList extends React.Component {

    render() {
        const {users} = this.props;
        return (
            <div className="space-user-list">
                <ul className="list-group">
                    {this.renderUsers(users)}
                </ul>
            </div>
        );
    }

    renderUsers(users) {
        return users.map((item) => {
            return (
                <li className="list-group-item" key={item.user._id}>
                    <SpaceUserItem data={item}/>
                </li>
            );
        });
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        users: getUsersInSpace(state)
    };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(SpaceUserList)