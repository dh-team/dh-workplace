'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { reduxForm, initialize, Field, SubmissionError } from 'redux-form';
import FormInput from '../form/Input';
import FormSelect from '../form/Select';
import CheckBox from '../form/CheckBox';
import FormModal from '../common/FormModal';
import valid from '../../utils/validate';
import { addGroup, editGroup } from '../../actions/action-group-manage';
import { getActiveGroup } from '../../selectors/selector-group-manage';

//client side validation
function validate(values) {
    const errors = {};
    if (!valid.isID(values.uname, 1, 50)) {
        errors.uname = 'Trường không chỉ cho phép nhập chữ cái, số  và ký tự "-", "_" , độ dài từ 1-50';
    }

    if (!valid.hasLength(values.name, 1, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    if (!valid.hasLength(values.description, 0, 250)) {
        errors.description = 'Độ dài tối đa 250 ký tự';
    }

    return errors;
}

class GroupForm extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }

    onShow(){
        const {current_group, type, initialize, reset} = this.props;
        if (type === "add") {
            initialize({
                uname: "",
                name: "",
                description: "",
                type: "group",
                is_head: false
            });
        } else if (current_group) {
            initialize({
                uname: current_group.uname,
                name: current_group.name,
                description: current_group.description,
                type: current_group.org_info.type || "group",
                is_head: current_group.org_info.is_head || false
            });
        }
        reset();

    }

    submit(values, dispatch) {
        const {current_group, type} = this.props;
        console.log(type +" group: " + JSON.stringify(values, null, 4));
        var data = {
            uname: values.uname,
            name: values.name,
            description: values.description,
            org_info: {
                type: values.type,
                is_head: values.is_head
            }
        };
        if (type === "add") {
            if (current_group && current_group._id && current_group._id != "root") {
                data.org_info.parent_id = current_group._id;
            }
            dispatch(addGroup(data));
        }
        else {
            if (current_group && current_group._id && current_group._id != "root") {
                data.id = current_group._id;
                dispatch(editGroup(data));
            }
        }
    }

    render() {
        const { id, type } = this.props; // type = 'add' | 'edit'
        const { error, handleSubmit, pristine, reset, submitting } = this.props;
        return (
            <FormModal id={id || "groupFormModal"} title={type === 'add' ? "Tạo node" : "Sửa node"} disableSubmit={pristine || submitting}
             okCallback={handleSubmit(this.submit)} onShow={this.onShow} >
                <Field component={FormInput} name="uname" type="text" label="ID" placeholder="group_id" addonPre="@" />
                <Field component={FormInput} name="name" type="text" label="Tên node" placeholder="Tên node" />
                <Field component={FormInput} name="description" type="text" label="Thông tin" />
                <Field component={FormSelect} name="type" value="group" label="Tùy chọn" >
                    <option value="group">Nhóm</option>
                    <option value="unique">Vị trí</option>
                </Field>
                <Field component={CheckBox} name="is_head" label="Head" title="Nhóm/Vị trí quản lý" />
            </FormModal>
        );
    }
}

const form = 'groupForm';
const fields = ["uname", "name", "description", "type", "is_head"];
GroupForm = reduxForm({form, validate, fields})(GroupForm);
// export default GroupForm;

const mapStateToProps = (state, ownProps) => {
    return {
        current_group: getActiveGroup(state)
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        // initializeForm: function(data) {
        //     dispatch(initialize(form, data, fields))
        // }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(GroupForm);
