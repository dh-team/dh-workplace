'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import GroupForm from './GroupForm';
import GroupUsers from './GroupUsers';
import Modal from '../common/Modal';
import { updateGroupParent, setActiveGroup, deleteGroup } from '../../actions/action-group-manage';
import { getSpaceInfo } from '../../selectors/selector-space';
import { getActiveGroupId, getGroups } from '../../selectors/selector-group-manage';

class GroupOrgChart extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.handleDelete = this.handleDelete.bind(this);
        this.buildHierarchy = this.buildHierarchy.bind(this);
        this.state = {func: ''}; // "detail" | 'add' | 'edit'
    }

    componentDidMount() {
        this.buildChart(this.props.groups);
    }

    componentWillReceiveProps(nextProps) {
        if(JSON.stringify(nextProps.groups) != JSON.stringify(this.props.groups)){
            console.log("list groups changed");
            this.buildChart(nextProps.groups);
        }
    }

    handleDelete(){
        const {active_group_id, deleteGroup} = this.props;
        deleteGroup(active_group_id);
    }

    buildHierarchy(groups) {
        const {space_info} = this.props;
        var root = {
            id: 'root',
            name: space_info.short_name || "Root",
            title: space_info.name || 'Company name',
            className: 'org-node-group org-node-root',
            org_type: 'group',
            children: []
        }

        var dict = {};

        function getItemFromDict(id){
            if(!id || id === 'root') return root;

            var item = dict[id];
            if(!item){
                item = {
                    id: id,
                    children: []
                }
                dict[id] = item;
            }
            return item;
        }

        groups.map(function(group, index) {
            var item = getItemFromDict(group._id);
            item.title = group.name;
            item.name = "@" + group.uname;
            item.is_head = group.org_info && group.org_info.is_head;
            item.org_type = "group";
            if (group.org_info && group.org_info.type) {
                item.org_type = group.org_info.type;
            }
            item.className = (item.is_head ? "org-node-head" : "") + " org-node-" + item.org_type;
            getItemFromDict(group.org_info.parent_id).children.push(item);
        });

        return root;
    }

    buildChart(groups) {
        const { updateGroupParent, setActiveGroup, history } = this.props;
        var datasource = this.buildHierarchy(groups);
        const thisCom = this;

        $("#orgChart").empty(); // empty content to rebuild the chart
        $('#orgChart').orgchart({
            'data': datasource,
            'nodeContent': 'title',
            'nodeId': 'id',
            'pan': true,
            'zoom': true,
            'draggable': true,
            'parentNodeSymbol': "fa-plus-square ",
            // 'depth': 4,
            // 'verticalDepth': 4,
            'dropCriteria': function($draggedNode, $dragZone, $dropZone) {
                return $dropZone.hasClass("org-node-group");
            },
            'createNode': function($node, data) {
                $node.on('click', function(event) {
                    if (!$(event.target).is('.edge') && !$(event.target).is('.btn')) { // ".edge" is class name of navigation button
                        setActiveGroup(data.id);
                    }
                });

                if(data.org_type === "unique"){
                    $node.children(".content").append("<i class='fa fa-user pull-right org-node-icon'></i>");
                }

                var menu = $("<div class='org-node-menu'></div>");
                if(data.org_type === "group"){
                    var btnAdd = $('<i> ', {
                        'class': 'btn-add-group btn btn-xs fa fa-plus',
                        'title': 'Thêm node con',
                        click: function() {
                            thisCom.setState({
                                func: 'add'
                            });
                            $("#groupFormModal").modal('show');
                        }
                    });
                    menu.append(btnAdd);
                }

                if (data.id && data.id != "root") { // disable edit, delete at root node
                    var btnEdit = $('<i>', {
                        'class': 'btn-edit-group btn btn-xs fa fa-pencil',
                        'title' : 'Sửa thông tin',
                        click: function() {
                            thisCom.setState({
                                func: 'edit'
                            });
                            $("#groupFormModal").modal('show');
                        }
                    });
                    var btnDelete = $('<i>', {
                        'class': 'btn-delete-group btn btn-xs fa fa-trash',
                        'title' : 'Xóa',
                        'data-toggle' : "modal",
                        'data-target' : "#confirmDeleteModal"
                    });
                     var btnUser = $('<i>', {
                        'class': 'btn-group-user btn btn-xs fa fa-user',
                        'title' : 'Người dùng',
                        'data-toggle' : "modal",
                        'data-target' : "#groupUsersModal"
                        // click: function() {
                        //     // redirect to group page
                        //     // thisCom.context.router.push("/space/group/" + data.id);
                        // }
                    });
                    menu.append(btnAdd).append(btnEdit).append(btnUser).append(btnDelete);
                }

                $node.append(menu);
            }
        }).children('.orgchart').on('nodedropped.orgchart', function(event) {
            var data = {
                id: event.draggedNode.attr('id'),
                parent_id: event.dropZone.attr('id')
            }
            updateGroupParent(data);
        });
    }

    render() {
        return (
            <div>
                <div className="group-org-chart" id="orgChart">
                </div>

                <GroupForm id="groupFormModal" type={this.state.func} />
                <GroupUsers id="groupUsersModal" />
                <Modal id="confirmDeleteModal" title="Xóa node" content="Bạn có chắc chắn muốn xóa node đã chọn?" okCallback={this.handleDelete} />
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        groups: getGroups(state),
        active_group_id: getActiveGroupId(state),
        space_info: getSpaceInfo(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        updateGroupParent: (data) => dispatch(updateGroupParent(data)),
        setActiveGroup: (data) => dispatch(setActiveGroup(data)),
        deleteGroup: (data) => dispatch(deleteGroup(data))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupOrgChart);
