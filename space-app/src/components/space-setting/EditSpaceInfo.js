'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux'
import { reduxForm, Field, SubmissionError } from 'redux-form';
import Box from '../common/Box';
import FormInput from '../form/Input';
import FormSelect from '../form/Select';
import valid from '../../utils/validate';
import { editSpaceInfo } from '../../actions/action-space';
import { getSpaceInfo } from '../../selectors/selector-space';

const submit = function(values, dispatch) {
    console.log("edit space: " + JSON.stringify(values, null, 4));
    return dispatch(editSpaceInfo(values));
}

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.name, 10, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 10-250 ký tự';
    }

    if (!valid.hasLength(values.short_name, 5, 250)) {
        errors.short_name = 'Trường không được để trống, độ dài từ 5-250 ký tự';
    }

    if (!valid.hasLength(values.slogan, 0, 250)) {
        errors.slogan = 'Độ dài tối đa 250 ký tự';
    }

    if (!valid.hasLength(values.phone, 5, 50)) {
        errors.phone = 'Trường không được để trống, độ dài từ 5-50 ký tự';
    }

    if (!valid.hasLength(values.address, 10, 250)) {
        errors.address = 'Trường không được để trống, độ dài từ 10-250 ký tự';
    }

    return errors;
}

class EditSpaceInfo extends Component {     

    render() {
        const inputCls = {
            labelCls: "col-lg-3",
            inputWrapCls:"col-lg-9"
        }
        const { error, handleSubmit, pristine, invalid, submitting } = this.props;
        return (
            <Box title="Cập nhật space doanh nghiệp">
                <form onSubmit={handleSubmit(submit)} className="form-horizontal">
                    <div className="box-body">
                        <Field component={FormInput} {...inputCls} name="name" type="text" label="Tên doanh nghiệp" placeholder="Tên doanh nghiệp" />
                        <Field component={FormInput} {...inputCls} name="short_name" type="text" label="Tên viết tắt" />
                        <Field component={FormInput} {...inputCls} name="slogan" type="text" label="Slogan" placeholder="Slogan" />
                        <Field component={FormInput} {...inputCls} name="phone" type="text" label="Điện thoại" />
                        <Field component={FormInput} {...inputCls} name="address" type="text" label="Địa chỉ liên hệ" placeholder="Địa chỉ liên hệ" />
                    </div>
                    <div className="box-footer">
                        <button type="submit" disabled={invalid || pristine || submitting} className="btn btn-primary pull-right">Cập nhật</button>
                    </div>
                </form>
            </Box>
        );
    }
}


EditSpaceInfo = reduxForm({
    form: 'EditSpaceInfo', // a unique identifier for this form
    validate
})(EditSpaceInfo)

EditSpaceInfo = connect(
    state => ({
        initialValues: getSpaceInfo(state) // pull initial values from account reducer
    }), {} // bind account loading action creator
)(EditSpaceInfo);

export default EditSpaceInfo;
