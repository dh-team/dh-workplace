'use strict';

import React from 'react';
import { Link } from 'react-router';
// import SpaceItem from './SpaceItem';

export default class GroupList extends React.Component {
    
    render() {
        const {groups} = this.props.groupsList;
        return (
            <div className="group-list">
                <ul className="list-group">
                    {this.renderGroups(groups)}
                </ul>
            </div>
        );
    }
    
    renderGroups(groups) {
        return groups.map((item) => {
            return (
                <li className="list-group-item" key={item._id}>
                    <span> {item.name} </span> <span> {item.description} </span>
                </li>
            );
        });
    }
}
