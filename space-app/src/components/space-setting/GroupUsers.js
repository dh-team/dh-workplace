'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import Modal from '../common/Modal';
import AddUserForm from '../group/UserInGroup/AddUserForm';
import GroupUserList from '../group/UserInGroup/GroupUserList';
import { fetchGroupUsers } from '../../actions/action-group-manage';
import { getActiveGroup } from '../../selectors/selector-group-manage';

class GroupUsers extends Component {
    constructor() {
        super();
        // bind method
        this.onShow = this.onShow.bind(this);
    }

    onShow(){
        // const {fetchGroupUsers, active_group} = this.props;
        // fetchGroupUsers(active_group.group_id);
    }

    render() {
        const { id, active_group } = this.props; 
        if(!active_group) return null;
        var groupType = "group";
        if(active_group.org_info && active_group.org_info.type){
            groupType = active_group.org_info.type;
        }
        return (
            <Modal id={id || "groupUsersModal"} title={groupType === "unique" ? "Chọn người dùng cho vị trí" : "Người dùng thuộc nhóm"} onShow={this.onShow} disableButtons={true} >
                { (groupType !== "unique" || active_group.users.length === 0) &&
                    <AddUserForm group_id={active_group._id} />    
                }
                
                <GroupUserList group_id={active_group._id} users={active_group.users} />
            </Modal>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        active_group: getActiveGroup(state)
    };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchGroupUsers: (groupID) => {
            dispatch(fetchGroupUsers(groupID));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupUsers);
