'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect }from 'react-redux';

import FileList from './FileList';
import { fetchDirContent } from "../../actions/action-file";

class FileManage extends Component {
    constructor(props) {
        super(props);
        
        this.fetchDirContent = this.fetchDirContent.bind(this);
        this.reloadCurrentDir = this.reloadCurrentDir.bind(this);
        
        this.state = {
            active_dir: null
        };
    }
    
    componentDidMount() {
        const {start_dir} = this.props;
        this.setState({
            active_dir: start_dir
        });
        
        if(start_dir && start_dir.partition && start_dir.app){
            this.props.fetchDirContent(start_dir);    
        }
    }
    
    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.start_dir) !== JSON.stringify(this.props.start_dir)) {
            console.log('start dir changed: ' + nextProps.start_dir);
            this.setState({
                active_dir: nextProps.start_dir
            });
            nextProps.start_dir && this.props.fetchDirContent(nextProps.start_dir);
        }
    }
    
    fetchDirContent(nodeId){
        var newState = {
            ...this.state.active_dir,
            id: nodeId
        }
        this.setState({active_dir: newState});
        this.props.fetchDirContent(newState);
    }
    
    reloadCurrentDir(){
        const {active_dir} = this.state;
        if(active_dir && active_dir.partition && active_dir.app){
            this.props.fetchDirContent(active_dir);    
        }
    }

    render() {
        const {active_dir} = this.state;
        if(!active_dir) return null;
        return (
            <FileList active_dir={active_dir} fetchDirContent={this.fetchDirContent} reloadCurrentDir={this.reloadCurrentDir}/>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchDirContent: (params) => {
            return dispatch(fetchDirContent(params));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FileManage);