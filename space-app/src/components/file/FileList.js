'use strict';

import React, {Component, PropTypes} from 'react';
import { connect }from 'react-redux';
import { Link } from 'react-router';
import cookie from 'react-cookie';

import FileItem from "./FileItem";
import FileInfo from "./FileInfo";
import NewFolderModal from './NewFolderModal';
import RenameModal from './RenameModal';

import {
    moveNodes, deleteNodes, restoreNodes, emptyTrashCurrent,
    beginUpload, uploadProgress, uploadSuccess, uploadError
} from "../../actions/action-file";
import { openDialog } from '../../actions/action-msg';
import { getNode, getActiveDir, getActiveDirContent, getUploadStatus } from '../../selectors/selector-file';

class FileList extends Component {

    constructor(props) {
        super(props);
        this.fu = null;
        this.customHeaders = {
            'Authorization': "Bearer " + cookie.load("space_token"),
        };
        
        // bind method
        this.toggleSelectAll = this.toggleSelectAll.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
        this.handleDeletePermanent = this.handleDeletePermanent.bind(this);
        this.confirmDeletePermanent = this.confirmDeletePermanent.bind(this);
        this.handleEmptyTrash = this.handleEmptyTrash.bind(this);
        this.handleRestore = this.handleRestore.bind(this);
        this.gotoParent = this.gotoParent.bind(this);
        this.gotoDir = this.gotoDir.bind(this);
        this.setActiveNode = this.setActiveNode.bind(this);
        this.updateCustomHeaders = this.updateCustomHeaders.bind(this);
        this.overwriteClick = this.overwriteClick.bind(this);
        this.uploadClick = this.uploadClick.bind(this);
        
        this.toggleSelectedNode = this.toggleSelectedNode.bind(this);
        this.selectNodes = this.selectNodes.bind(this);
        this.clearSelectedNodes = this.clearSelectedNodes.bind(this);
        
        this.handleMove = this.handleMove.bind(this);
        this.handlePaste = this.handlePaste.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        
        this.state = {
            active_node : null,
            selected_nodes: [], // selected node ids,
            moving: false
        };
    }
    
    componentDidMount() {
        const {active_dir, reloadCurrentDir, beginUpload, uploadProgress, uploadSuccess, uploadError} = this.props;
      
        // config fine uploader
        this.customHeaders["Args"] = JSON.stringify({
            app: active_dir.app,
            partition: active_dir.partition,
            parent: active_dir.id,
            exist_action: "rename"
        });
        this.fu = new qq.FineUploaderBasic({
            text: {
                fileInputTitle: "Tải file lên thư mục này"
            },
            button: this.refs.fileUpload,
            request: {
                inputName: 'file',
                endpoint: '/fs/upload',
                customHeaders: this.customHeaders
            },
            callbacks: {
                onUpload: function(id, name, responseJSON, xhr){
                    console.log("onUpload: " + id + ", " + name);
                    beginUpload({id, name});
                },
                onComplete: function(id, name, responseJSON, xhr){
                    console.log("onComplete: " + id + ", " + name + ", " + JSON.stringify(responseJSON));
                    if(responseJSON && responseJSON.success){
                        uploadSuccess({id});
                        reloadCurrentDir();
                    }
                },
                onAllComplete: function(succeeded, failed){
                    console.log("onAllComplete: " + JSON.stringify(succeeded) + ", "  + JSON.stringify(failed));
                },
                onError: function(id, name, errorReason, xhr){
                    console.log("onError: " + id + ", " + name + ", " + errorReason);
                    uploadError({id: id, error: errorReason});
                },
                onProgress: function(id, name, uploadedBytes, totalBytes){
                    console.log("onProgress: " + id + ", " + name + ", " + uploadedBytes + ", " + totalBytes);
                    uploadProgress({id: id, uploaded_bytes: uploadedBytes, total_bytes: totalBytes});
                }
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        if (this.fu && JSON.stringify(nextProps.active_dir) !== JSON.stringify(this.props.active_dir)) {
            console.log('active dir changed: ' + nextProps.active_dir);
        }
    }

    componentDidUpdate() {
    }
    
    toggleSelectedNode(nodeId) {
        var nodes = [...this.state.selected_nodes];
        var i = nodes.indexOf(nodeId);
        if (i < 0) {
            nodes.push(nodeId);
        }
        else {
            nodes.splice(i, 1);
        }
        this.setState({
            selected_nodes: nodes
        });
    }
    
    clearSelectedNodes(){
        this.setState({
            selected_nodes: []
        });
    }
    
    selectNodes(nodeIds){
        var nodes = [...this.state.selected_nodes];
        nodeIds.map(function(id) {
            if (nodes.indexOf(id) < 0) nodes.push(id);
        });
        this.setState({
            selected_nodes: nodes
        });
    }
    
    toggleSelectAll() {
        const {active_dir_content} = this.props;
        const isSelectedAll = this.state.selected_nodes.length >= active_dir_content.length;
        if (isSelectedAll) {
            this.clearSelectedNodes();
        }
        else {
            this.selectNodes(active_dir_content.map((item) => item.id));
        }
    }
    
    handleDelete(){
        this.props.openDialog({
            title:"Xóa file",
            content:"Bạn chắc chắn muốn xóa các file và thư mục đã chọn?",
            confirmCallback: this.confirmDelete
        });
    }
    
    confirmDelete() {
        const {deleteNodes} = this.props;
        deleteNodes(this.state.selected_nodes);
        this.clearSelectedNodes();
    }
    
    handleDeletePermanent(){
        this.props.openDialog({
            title:"Xóa file",
            content:"Bạn chắc chắn muốn xóa các file và thư mục đã chọn? Các file bị xóa sẽ không thể khôi phục được nữa.",
            confirmCallback: this.confirmDeletePermanent
        });
    }
    
    confirmDeletePermanent() {
        const {deleteNodes} = this.props;
        deleteNodes(this.state.selected_nodes, true);
        this.clearSelectedNodes();
    }
    
    handleEmptyTrash(){
        const {openDialog, emptyTrash} = this.props;
        openDialog({
            title:"Làm trống thùng rác",
            content:"Bạn chắc chắn muốn xóa tất cả file và thư mục trong thùng rác? Các file bị xóa sẽ không thể khôi phục được nữa.",
            confirmCallback: emptyTrash
        });
    }
    
    handleRestore() {
        const {restoreNodes} = this.props;
        restoreNodes(this.state.selected_nodes);
        this.clearSelectedNodes();
    }
    
    gotoParent(e) {
        e.preventDefault();
        this.gotoDir(this.props.active_dir_meta.parent);
    }
    
    gotoDir(nodeId){
        // clear active node
        this.setState({active_node: null});
        
        // clear selected nodes
        if(!this.state.moving){
            this.clearSelectedNodes();
        }
        
        const {fetchDirContent} = this.props;
        fetchDirContent(nodeId || null);
    }
      
    setActiveNode(nodeId) {
        this.setState({
            active_node: nodeId
        });
    }
    
    overwriteClick(e){
        this.updateCustomHeaders("overwrite");
        // trigger file browser
        $(this.refs.fileUpload).find('input[type="file"]').click();
    }
    
    uploadClick(e){
        this.updateCustomHeaders("rename");
        // trigger file browser
        $(this.refs.fileUpload).find('input[type="file"]').click();
    }
    
    updateCustomHeaders(existAction){
        var args = JSON.parse(this.customHeaders["Args"]);
        const {active_dir} = this.props;
        args = {
            app: active_dir.app,
            partition: active_dir.partition,
            parent: active_dir.id,
            exist_action: existAction || args.exist_action 
        };
        this.customHeaders["Args"] = JSON.stringify(args);
        this.fu.setCustomHeaders(this.customHeaders);
    }
    
    handleMove(){
        this.setState({
            moving: true
        });
    }
    
    handlePaste(){
        console.log("handlePaste");
        const selectNodes = this.state.selected_nodes;
        const {active_dir, moveNodes} = this.props;
        if (selectNodes.length > 0) {
            moveNodes({
                target: active_dir.id,
                ids: selectNodes
            });
        }
       
        this.setState({
            moving: false,
            selected_nodes: []
        });
    }
    
    handleCancel(){
        this.setState({
            moving: false
        });        
    }
        
    render() {
        const thisCom = this;
        const {
            active_dir, active_dir_meta, active_dir_content, reloadCurrentDir, 
        } = this.props;
        
        const selectedNodes = this.state.selected_nodes;
        const moving = this.state.moving;

        var selectCount = selectedNodes.length;
        var isSelectedAll = active_dir_content && selectCount >= active_dir_content.length;
        var lastSelectedNode = selectCount > 0 ? selectedNodes[selectCount - 1] : null;
        var nodeInfoId = this.state.active_node || active_dir.id;
        
        var enableDelete, enableCopy, enableMove;
        enableDelete = enableCopy = enableMove = selectCount > 0 && !moving;
        var enableRename = !moving && selectedNodes && selectCount === 1;
        var enableToolbar = active_dir.app && active_dir.partition && !active_dir.trash;
        var enableTrashToolbar = active_dir.app && active_dir.partition && active_dir.trash;
        var enablePaste = moving;
        var enableCancel = moving;
        
        var dirName = "";
        if (active_dir_meta.id) {
            dirName = active_dir_meta.name;
        }
        else {
            dirName = active_dir.trash ? "Thùng rác" : active_dir.app;
        }
        
        var viewMode = "default";
        if(moving) viewMode = "moving";
        if(active_dir.trash) viewMode = "trash";
        
        return (
            <div className="row">
                <div className="col-md-9">
                    <div className="box box-primary">

                        <div className="box-header with-border">
                            <h3 className="box-title">
                                <i className="fa fa-folder-open"></i> &nbsp;
                                {active_dir_meta.id && <span><a role="button" onClick={this.gotoParent}>...</a> /  </span>}
                                {dirName}
                            </h3>
                            <div ref="fileUpload" className="hide" title="Upload"></div>
                            <div className={ "pull-right box-tools btn-group " + (enableToolbar ? "" : "hide") }>
                                
                                <button className="btn btn-default btn-sm" title="Tải file lên thư mục này" onClick={this.uploadClick}>
                                    <i className="fa fa-upload"></i>
                                </button>
                                <button type="button" className="btn btn-default btn-sm" onClick={reloadCurrentDir} title="Refresh">
                                    <i className="fa fa-refresh"></i>
                                </button>
        
                                { enableDelete &&
                                    <button type="button" className="btn btn-default btn-sm" title="Xóa" onClick={this.handleDelete}>
                                        <i className="fa fa-trash"></i>
                                    </button>
                                }
        
                                <div className="btn-group">
                                    <button type="button" className="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown" title="Thêm chức năng">
                                        <i className="fa fa-bars"></i>
                                    </button>
                                    <ul className="dropdown-menu pull-right" role="menu">
                                        <li><a href="#" data-toggle="modal" data-target="#newFolderModal"><i className="fa fa-plus"></i> Tạo thư mục</a></li>
                                        <li className="divider"></li>
                                        { enableRename && <li><a href="#" data-toggle="modal" data-target="#renameModal"><i className="fa fa-edit"></i> Sửa tên</a></li> }
                                        { enableCopy && <li className="hide"><a href="#"><i className="fa fa-copy"></i> Copy</a></li> }
                                        { enableMove && <li><a onClick={this.handleMove} role="button"><i className="fa fa-cut"></i> Move</a></li> }
                                        { enablePaste && <li><a onClick={this.handlePaste} role="button"><i className="fa fa-paste"></i> Paste</a></li> }
                                        { enableCancel && <li><a onClick={this.handleCancel} role="button"><i className="fa fa-times-circle-o"></i> Cancel</a></li> }
                                        <li><a onClick={this.overwriteClick} role="button"><i className="fa fa-arrow-circle-up"></i> Tải lên và ghi đè file</a></li>
                                        <li className="hide"><a href="#"><i className="fa fa-download"></i> Tải xuống</a></li>
                                    </ul>
                                </div>
        
                            </div>
        
                            { enableTrashToolbar &&
        
                                <div className="pull-right box-tools btn-group">
                                    <button type="button" className="btn btn-default btn-sm" title="Khôi phục" onClick={this.handleRestore}>
                                        <i className="fa fa-rotate-left"></i>
                                    </button>
                                    <button type="button" className="btn btn-default btn-sm" onClick={this.handleDeletePermanent}>
                                        <i className="fa fa-remove"></i>
                                    </button>
                                    <button type="button" className="btn btn-default btn-sm" onClick={this.handleEmptyTrash}>
                                        <i className="fa fa-trash-o"></i>
                                    </button>
                                </div>
                            }
                        </div>
        
        
                        <div className="box-body no-padding">
                          <table id="tbFiles" className="table table-hover table-file">
                            <tbody>
                                <tr>
                                    <th style={{width: "40px"}}> <input type="checkbox" checked={isSelectedAll} onClick={this.toggleSelectAll} /></th>
                                    <th style={{"min-width": "180px"}}>Tên</th>
                                    <th>Người tạo</th>
                                    <th>Ngày sửa cuối</th>
                                    <th>Kích thước</th>
                                    <th style={{width: "40px"}}></th>
                                </tr>
        
                                {
                                    active_dir_content && active_dir_content.map(function(child, index) {
                                        if(!child) return null;
                                        var isSelected = selectedNodes.indexOf(child.id) >= 0;
                                        var isActive = thisCom.state.active_node === child.id;
                                        
                                        return <FileItem key={child.id} data={child} active={isActive} selected={isSelected} view_mode={viewMode} goto={thisCom.gotoDir} setActive={thisCom.setActiveNode} toggleSelect={thisCom.toggleSelectedNode} />;
                                    })
                                }
        
                            </tbody>
                          </table>
        
                        </div>
        
                        <NewFolderModal id="newFolderModal" active_dir={active_dir}/>
                        <RenameModal id="renameModal" node_id={lastSelectedNode} />
        
                    </div>
                </div>
                <div className="col-md-3">
                    <FileInfo node_id={nodeInfoId}/>
                </div>
            </div>
            
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    var node = null;
    if(ownProps.active_dir && ownProps.active_dir.app && ownProps.active_dir.partition){
        var activeDirId = ownProps.active_dir.id || (ownProps.active_dir.partition + "_" + ownProps.active_dir.app);
        node = getNode(state, activeDirId);    
    }
    
    return {
        active_dir_meta: (node && node.meta) || {},
        active_dir_content: (node && node.content) || [],
        upload: getUploadStatus(state),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {        
        moveNodes: (params) => dispatch(moveNodes(params)),
        deleteNodes: (nodes, permanent) => dispatch(deleteNodes(nodes, permanent)),
        beginUpload: (data) => dispatch(beginUpload(data)),
        uploadProgress: (data) => dispatch(uploadProgress(data)),
        uploadSuccess: (data) => dispatch(uploadSuccess(data)),
        uploadError: (data) => dispatch(uploadError(data)),
        restoreNodes: (nodes) => dispatch(restoreNodes(nodes)),
        emptyTrash: () => dispatch(emptyTrashCurrent()),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FileList);
