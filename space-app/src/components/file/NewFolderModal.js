'use strict';

import React, {Component, PropTypes} from 'react';
import { connect }from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import FormModal from '../common/FormModal';
import FormInput from '../form/Input';
import valid from '../../utils/validate';
import { createDir } from "../../actions/action-file";


//client side validation
function validate(data) {
    const errors = {};
    if (!valid.hasLength(data.name, 1, 250)) {
        errors.name = 'Độ dài hợp lệ từ 1-250 ký tự';
    }
    return errors;
}

class NewFolderModal extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }

    onShow(){
        this.props.reset();
    }
    
    submit(values, dispatch) {
        const {active_dir} = this.props;
        var newDir = {
            parent: active_dir.id,
            app: active_dir.app,
            partition: active_dir.partition,
            name: values.name
        }
        dispatch(createDir(newDir));
    }

    render() {
        const { id, title } = this.props;
        const { error, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <FormModal id={id || "newFolderModal"} title={title || "Tạo thư mục mới"} disableSubmit={pristine || submitting}
                okCallback={handleSubmit(this.submit)} onShow={this.onShow}>

                <Field component={FormInput} name="name" type="text" label="Tên thư mục" focus={true} />

            </FormModal>
        );
    }
}

NewFolderModal = reduxForm({
  form: 'newFolderModal',  // a unique identifier for this form
  validate,
  initialValues: {name : ""},
  fields: ["name"]
})(NewFolderModal);

export default NewFolderModal;

