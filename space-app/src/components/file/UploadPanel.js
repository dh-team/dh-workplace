'use strict';
import filesize from 'filesize';
import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect }from 'react-redux';
import { clearUpload } from "../../actions/action-file";

class UploadPanel extends Component {
    componentDidMount() {
    }

    render() {
        const {data, clearUpload} = this.props;
        if(!data || Object.keys(data).length === 0) return null;
        return (
            <div className="box box-warning upload-panel">
              <div className="box-header with-border">
                <h3 className="box-title">File tải lên</h3>

                <div className="box-tools">
                  <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i></button>
                  <button type="button" className="btn btn-box-tool" onClick={clearUpload}><i className="fa fa-remove"></i></button>
                </div>
              </div>
              <div className="box-body table-responsive no-padding">
                  <table id="tbFiles" className="table table-hover table-file">
                    <tbody>
                  {
                      data && Object.keys(data).map(function(key, index){
                          const item = data[key];
                          var textCls = "";
                          if(item.status === 'success'){
                              textCls = "text-light-blue";
                          }else if(item.status === 'error'){
                              textCls = "text-red";
                          }

                          const percent = item.total_bytes > 0 ? Math.round(item.uploaded_bytes * 100 / item.total_bytes) : 0;
                          return (
                              <tr key={key}>
                                  <td className="col-file-info">
                                        <p className={textCls}><i className="fa fa-file-o"></i> {item.name} - {filesize(item.uploaded_bytes)}</p>
                                        {item.status === 'error' &&
                                            <p className={textCls + " text-right upload-status"}>Lỗi: {item.error}</p>
                                        }
                                        {item.status === 'success' &&
                                            <p className={textCls + " text-right upload-status"}>Hoàn tất</p>
                                        }
                                   </td>
                                  <td className="col-percentage">
                                      <div className={"c100 small p" + percent}>
                                          <span>{percent}%</span>
                                          <div className="slice">
                                              <div className="bar"></div>
                                              <div className="fill"></div>
                                          </div>
                                      </div>
                                  </td>
                              </tr>
                          );
                      })
                  }
                  </tbody>
                </table>
              </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        data: state.file.upload
    };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        clearUpload: () => {
            dispatch(clearUpload());
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(UploadPanel)
