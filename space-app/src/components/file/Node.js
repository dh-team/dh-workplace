'use strict';

import React, {Component, PropTypes} from 'react';
import { connect }from 'react-redux';
import { Link } from 'react-router';
import { changeStartDir } from "../../actions/action-file";
import { getStartDir } from '../../selectors/selector-file';

class Node extends Component {
    constructor() {
        super();
        // bind method
        this.handleClick = this.handleClick.bind(this);
        this.handleTrashClick = this.handleTrashClick.bind(this);
        this.fetch = this.fetch.bind(this);
    }
    
    handleClick(e){
        e && e.stopPropagation();
        this.fetch(false);
    }
    
    handleTrashClick(e){
        e && e.stopPropagation();
        this.fetch(true);
    }
    
    fetch(trash){
        const {partition, app, changeStartDir} = this.props;
        changeStartDir({
            app: app,
            partition: partition,
            id: null,
            trash: trash
        });
    }

    render() {
        const {partition, app, name, icon, children, start_dir} = this.props;
        var menuActive = false, trashActive = false;
        if (start_dir && partition === start_dir.partition && app === start_dir.app) {
            menuActive = true;
            if(start_dir.trash) trashActive = true;
        }

        return (
            <li className={menuActive ? "active" : ""}>
                <a role="button" onClick={this.handleClick}>
                    {icon && <i className={icon}></i>}
                    {name}
                    <a className={"btn-view-trash pull-right " + (trashActive ? "active" : "")} role="button" onClick={this.handleTrashClick}> 
                        <i className={"fa " + (trashActive ? "fa-trash" : "fa-trash-o")}></i> 
                    </a>
                </a>

                {children &&
                    <ul> {children} </ul>
                }
            </li>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        start_dir: getStartDir(state)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeStartDir: (params) => {
            dispatch(changeStartDir(params));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Node);
