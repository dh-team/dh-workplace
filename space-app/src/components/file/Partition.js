'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import Node from './Node';

export default class Partition extends Component {
    componentDidMount() {
    }

    render() {
        const {title, partition} = this.props;
        return (
            <div className="box box-solid">
                <div className="box-header with-border">
                {title &&
                    <h3 className="box-title">{title}</h3>
                }
                
                    <div className="box-tools">
                        <button type="button" className="btn btn-box-tool" data-widget="collapse"><i className="fa fa-minus"></i>
                        </button>
                    </div>
                    
                </div>
                <div className="box-body no-padding">
                    <ul className="nav nav-pills nav-stacked">
                        <Node name="File" partition={partition} app="FileManager" icon="fa fa-folder"/>
                        <Node name="Wiki" partition={partition} app="Wiki" icon="fa fa-book" />
                        <Node name="Post" partition={partition} app="LivePost" icon="fa fa-globe" />
                    </ul>
                </div>
            </div>
        );
    }
}
