'use strict';

import React, {Component, PropTypes} from 'react';
import { publishUrl } from '../../../conf.json';

export default class CopyLinkBtn extends Component {
    constructor() {
        super();
        // bind method
        this.clipboard = null;
    }
    
    componentDidMount() {
        const btnLink = this.refs.btnLink;
        $(btnLink).popover();
        
        const url = publishUrl + "/fs/download?id=" + this.props.file.id;
        const txtElmId = "fileLinkText_" + this.props.file.id;
        this.clipboard = new Clipboard('#' + txtElmId, {
            text: function(trigger) {
                return url;
            }
        });
    }
    
    componentWillUnmount(){
        this.clipboard && this.clipboard.destroy();
    }

    render() {
        const url = publishUrl + "/fs/download?id=" + this.props.file.id;
        const txtElmId = "fileLinkText_" + this.props.file.id;
        return (
            <div className="file-link-popover">
                <a className="file-link-btn" role="button" ref="btnLink" id={txtElmId}
                   data-toggle="popover" data-html="true" data-trigger="click" data-placement="left"
                   data-content={"<div><input id='fileLinkText' class='form-control input-sm file-link-text' type='text' readonly value='"+ url +"'/></div>"}>
                    <i className="fa fa-link"></i>
                </a>
                <input type="hidden" id={txtElmId} value={url} />
            </div>
        );
    }
}
