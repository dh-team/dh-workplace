'use strict';

import React, {Component, PropTypes} from 'react';
import { connect }from 'react-redux';
import { reduxForm, Field, initialize } from 'redux-form';
import FormModal from '../common/FormModal';
import FormInput from '../form/Input';
import valid from '../../utils/validate';
import { rename } from "../../actions/action-file";
import { getNode } from '../../selectors/selector-file';

//client side validation
function validate(data) {
    const errors = {};
    if (!valid.hasLength(data.name, 1, 250)) {
        errors.name = 'Độ dài hợp lệ từ 1-250 ký tự';
    }
    return errors;
}

class RenameModal extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }

    onShow(){
        const {current_node, reset, initializeForm} = this.props;
        if(current_node){
            initializeForm({
                name: current_node.name
            });
            reset();
        }
    }

    submit(values, dispatch){
        const {current_node} = this.props;
        var data = {
            id: current_node.id,
            new_name: values.name
        }
        dispatch(rename(data));
    }

    render() {
        const { id, title } = this.props;
        const { error, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <FormModal id={id || "renameModal"} title={title || "Sửa tên"} disableSubmit={pristine || submitting}
                okCallback={handleSubmit(this.submit)} onShow={this.onShow}>

                <Field component={FormInput} name="name" type="text" label="Tên mới" focus={true} />

            </FormModal>
        );
    }
}

const form = 'renameModal';  // a unique identifier for this form
const fields = ["name"];
RenameModal = reduxForm({form, validate, fields})(RenameModal);

const mapStateToProps = (state, ownProps) => {
    var node = getNode(state, ownProps.node_id);
    return {
        current_node: node && node.meta
    };
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        initializeForm: function(data) {
            dispatch(initialize(form, data, fields))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(RenameModal)
