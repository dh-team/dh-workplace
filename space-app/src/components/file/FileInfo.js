'use strict';

import React, {Component, PropTypes} from 'react';
import { connect }from 'react-redux';
import { Link } from 'react-router';
import filesize from 'filesize';
import { publishUrl } from '../../../conf.json';
import DateDisplay from '../common/DateDisplay';
import FileSize from '../common/FileSize';
import UserName from '../common/UserName';
import { getNode } from '../../selectors/selector-file';

class FileInfo extends Component {
    constructor() {
        super();
        // bind method
    }
    
    isImage(mime){
        if(!mime) return false;
        return mime.toLowerCase().startsWith("image/");
    }

    render() {
        const {node} = this.props;
        if(!node) return null;
        const {meta} = node;
        
        return (
            <div className="box box-simple file-info">
                <div className="box-header with-border">
                    <h3 className="box-title"> <i className={"fa " + (meta.type === "file" ? "fa-file" : "fa-folder")}></i> {meta.name}</h3>
                </div>
                <div className="box-body no-padding">
                    {this.isImage(meta.mime_type) &&
                        <div className="file-info-img-preview">
                            <img src={"/fs/file?id=" + meta.id} alt={meta.name} />
                        </div>
                    }
                   
                    <table className="table file-info-table">
                        <tbody>
                            <tr>
                                <td className="file-info-col-field">Id</td>
                                <td className="file-info-col-value">{meta.id}</td>
                            </tr>
                            <tr>
                                <td className="file-info-col-field">Loại</td>
                                <td className="file-info-col-value">{meta.type === "file" ? "Tệp" : "Thư mục"}</td>
                            </tr>
                            {meta.type === "file" &&
                                <tr>
                                    <td className="file-info-col-field">Kích thước</td>
                                    <td className="file-info-col-value"><FileSize value={meta.size}/></td>
                                </tr>
                            }
                            <tr>
                                <td className="file-info-col-field">Người tạo</td>
                                <td className="file-info-col-value">
                                {meta.create_user && 
                                    <UserName user={meta.create_user} />
                                }
                                </td>
                            </tr>
                            <tr>
                                <td className="file-info-col-field">Ngày tạo</td>
                                <td className="file-info-col-value">
                                {meta.create_time &&
                                    <DateDisplay value={meta.create_time} /> 
                                }
                                </td>
                            </tr>
                            <tr>
                                <td className="file-info-col-field">Ngày cập nhật</td>
                                <td className="file-info-col-value">
                                {meta.edit_time &&
                                    <DateDisplay value={meta.edit_time} /> 
                                }
                                </td>
                            </tr>
                           
                            {meta.type === "file" &&
                                <tr>
                                    <td colSpan="2" className="file-info-col-value"><i className="fa fa-link"></i> <a href={publishUrl + "/fs/download?id=" + meta.id}>{publishUrl + "/fs/download?id=" + meta.id}</a></td>
                                </tr>
                            }
                        </tbody>
                    </table>
                    
                    
                    {meta.versions && meta.versions.length > 0 && 
                        
                        <div className="file-info-versions">
                            <h4>Các phiên bản</h4>
                            <table className="table table-striped file-info-table">
                                <tbody>
                                    <tr>
                                        <th></th>
                                        <th>Kích thước</th>
                                        <th>Ngày tạo</th>
                                        <th></th>
                                    </tr>
                                    
                                    {meta.versions.map((version) => {
                                        return (
                                            <tr key={version.version_number}>
                                                <td>v.{version.version_number}</td>
                                                <td><FileSize value={version.size}/></td>
                                                <td>{version.create_time && <DateDisplay value={version.create_time} />}</td>
                                                <td> <a href={"/fs/file?id=" + meta.id + "&version=" + version.version_number}><i className="fa fa-download"></i></a></td>
                                            </tr>
                                        );
                                    })}
                                </tbody>
                            </table>
                        </div>
                        
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        node: ownProps.node_id ? getNode(state, ownProps.node_id) : null
    };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(FileInfo)
