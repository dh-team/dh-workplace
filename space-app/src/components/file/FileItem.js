'use strict';

import filesize from 'filesize';
import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import CopyLinkBtn from './CopyLinkBtn';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';

export default class FileItem extends Component {
    constructor() {
        super();
        // bind method
        this.goToChild = this.goToChild.bind(this);
        this.selectClick = this.selectClick.bind(this);
        this.itemClick = this.itemClick.bind(this);
        this.downloadClick = this.downloadClick.bind(this);
    }
    
    componentDidMount() {
    }
    
    goToChild(e) {
        e && e.stopPropagation();    
        e && e.preventDefault();
        
        const {data, goto} = this.props;
        goto(data.id);
    }
     
    selectClick(e) {
        e && e.stopPropagation();    
        // e && e.preventDefault();
        
        const {data, toggleSelect} = this.props;
        toggleSelect(data.id);
    }
    
    itemClick(e) {
        e && e.stopPropagation();    
        e && e.preventDefault();
        
        const {data, setActive} = this.props;
        setActive(data.id);
    }
    
    downloadClick(e) {
        e && e.stopPropagation();    
        e && e.preventDefault();
        
        window.open("/fs/download?id=" + this.props.data.id);
    }
        
    render() {
        const {view_mode, data, selected, active} = this.props;

        return (
            <tr onClick={this.itemClick} className={active ? "active" : ""}>
                <td><input type="checkbox" className="checkbox" onClick={this.selectClick} checked={selected} /></td>
                {data.type === 'dir' &&
                    <td>
                        <i className="fa fa-folder" />&nbsp; 
                        {(view_mode === "trash" || (view_mode === "moving" && selected)) ? <span>{data.name}</span> : <a role="button" onClick={this.goToChild}>{data.name}</a>}
                    </td>    
                }
                
                {data.type === 'file' &&
                    <td>
                        <i className="fa fa-file-o" />&nbsp;
                        {view_mode !== "trash" ? <a role="button" onClick={this.downloadClick}>{data.name}</a> : <span>{data.name}</span>}
                    </td>    
                }
                
                <td>
                {data.create_user && 
                    <UserName user={data.create_user} />
                }
                </td>
                <td> 
                {data.create_time &&
                    <DateDisplay value={data.create_time} /> 
                }
                </td>
                <td><span className="label label-success">{ data.type === 'dir'? '--' : filesize(data.size) }</span></td>
                <td>{data.type === "file" && view_mode !== "trash" && <CopyLinkBtn file={data}/>}</td>
            </tr>
        );
    }
}
