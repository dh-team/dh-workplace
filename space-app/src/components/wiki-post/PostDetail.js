'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';
import Modal from '../common/Modal';
import ReactBtn from '../reaction/ReactBtn';
import CommentBox from '../comment/CommentBox';
import { deletePost } from "../../actions/action-wiki-post";
import { setActiveCategory } from "../../actions/action-wiki-category";
import { getActivePost, getDeletePostStatus } from '../../selectors/selector-wiki-post';
import { getCategory, getActiveCategoryId } from '../../selectors/selector-wiki-category';
import { getCurrentUser } from '../../selectors/selector-user';

class PostDetail extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };
    
    constructor(props) {
        super(props);
        this.handleDelete = this.handleDelete.bind(this);
    }
    
    componentDidMount() {
        const {active_category_id, category, setActiveCategory} = this.props;
        if(category && active_category_id !== category._id){
            setActiveCategory(category._id);
        }
    }
    
    componentWillReceiveProps(nextProps) {
        const {category} = this.props;
        if (nextProps.delete_status && this.props.delete_status && this.props.delete_status.loading && !nextProps.delete_status.loading && !nextProps.delete_status.error) {
            console.log("delete wiki post success");
            if (category) {
                this.context.router.push('/space/wiki/category/' + category._id);
            }
            else {
                this.context.router.push('/space/wiki');
            }
        }
    }
    
    handleDelete(){
        const {post, deletePost} = this.props;
        deletePost(post._id);
    }
        
    render() {
        const {post, category, current_user} = this.props;
        if(!post) return null;
       
        const isOwner = post.meta && post.meta.create_user && post.meta.create_user.user_id === current_user.user_id;
        
        return (
            <div className="wiki-post-detail">
                {!post &&
                   <div><p className="text-red">Bài viết không tồn tại</p></div>
                }
               
                {post &&
                    <div className="box box-simple" key={post._id}>
                    
                        <div className="box-header with-border">
                            <h3 className="box-title"> <i className="fa fa-folder-o"></i> <Link to={"/space/wiki/category/" + category._id}> {category.name} </Link> </h3>
                            
                            {isOwner && 
                                <div className="box-tools btn-group">
                                    <div className="btn-group">
                                        <Link className="btn btn-default btn-sm" title="Sửa" to={"/space/wiki/post/edit/" + post._id}>
                                            <i className="fa fa-pencil"></i>
                                        </Link>
                                        <button type="button" className="btn btn-default btn-sm" data-toggle="modal" data-target="#confirmDeletePostModal" title="Xóa">
                                            <i className="fa fa-trash"></i>
                                        </button>
                                    </div>
                                </div>
                            }
                        </div>
                    
                    
                        <div className="box-body">
                            <div className="wiki-post-title"> <h3> {post.title} </h3> </div>
                            <div className="wiki-post-meta">
                                    <span> Đăng bởi <UserName user={post.meta.create_user}/>  vào lúc </span>
                                    <DateDisplay value={post.meta.create_time} />
                            </div>
                            <div className="wiki-post-content" dangerouslySetInnerHTML={{__html: post.content}}></div>                            
                            
                            <div className="wiki-post-tags">
                                <span> <i className="fa fa-tags"></i> Tags: </span>
                                <ul>
                                { post.tags && 
                                    post.tags.map(function(tag){
                                        return  <li> <Link className="label label-primary" to={"/space/wiki/tag/" + tag}> {tag} </Link> </li>;
                                    })
                                }
                                </ul>
                            </div>
                        </div>
                        
                        <div className="box-footer">
                            <ReactBtn stream_id={post.reaction_stream} current_user={current_user} />
                        </div>
                        <Modal id="confirmDeletePostModal" title="Xóa bài" content="Bạn có chắc chắn muốn xóa bài viết này không?" okCallback={this.handleDelete} />

                    </div>
               }
            
                {post && post.comment_stream && 
                    <CommentBox stream_id={post.comment_stream} />
                }
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    var post = getActivePost(state);
    var category = null;
    if(post && post.categories && post.categories.length > 0){
        category = getCategory(state, post.categories[0]);    
    }
    
    return {
        post: post,
        category: category,
        delete_status: getDeletePostStatus(state),
        current_user: getCurrentUser(state),
        active_category_id: getActiveCategoryId(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        deletePost: (id) => {
            dispatch(deletePost(id));
        },
        setActiveCategory: (id) => {
            dispatch(setActiveCategory(id));
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail);
