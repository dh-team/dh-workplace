'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import FormSelect from '../form/Select';
import { Field } from 'redux-form';
import { getSortedCategories } from '../../selectors/selector-wiki-category';

class CategorySelect extends React.Component {
        
    render() {
        const {categories, field_props} = this.props;
        return (
            <Field component={FormSelect}  {...field_props}>
                {
                    categories.map(function(item){
                        return <option value={item._id} sort_key={item.sort_key}>{item.display_field}</option>;
                    })
                }
            </Field>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        categories: getSortedCategories(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(CategorySelect);
