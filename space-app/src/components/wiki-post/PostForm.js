'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux'
import { reduxForm, Field, SubmissionError } from 'redux-form';
import Box from '../common/Box';
import FormInput from '../form/Input';
import Editor from '../form/Editor';
import TagInput from '../form/TagInput';
import CategorySelect from './CategorySelect';
import valid from '../../utils/validate';
import { addPost, editPost } from '../../actions/action-wiki-post';
import { getActivePost, getAddPostStatus, getEditPostStatus } from '../../selectors/selector-wiki-post';
import { getActiveCategory } from '../../selectors/selector-wiki-category';

//client side validation
function validate(values) {
    const errors = {};
   
    if (!valid.hasLength(values.title, 1, 250)) {
        errors.title = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    // if (!valid.hasLength(values.content, 5)) {
    //     errors.content = 'Trường không được để trống, độ dài từ 5 ký tự';
    // }
    // console.log("PostForm validate ", values, errors);
    return errors;
}

class PostForm extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
    }

    componentWillMount() {
        const {active_category, active_post, type, initialize, reset} = this.props;
        
        if (type === "add") {            
            initialize({
                title: "",
                content: "",
                tags: "",
                category_id: (active_category && active_category._id) ? active_category._id : ""
            });
        }
        else {            
            var selectedCat = (active_category && active_category._id) ? active_category._id : "";
            if (active_post.categories.length > 0)
                selectedCat = active_post.categories[0];
        
            initialize({
                title: active_post.title,
                content: active_post.content,
                tags: active_post.tags,
                category_id: selectedCat
            });
        }
        // reset();
    }

    componentWillReceiveProps(nextProps) {
        const {type, active_category, active_post} = this.props;
        if(type === "add"){
            if (nextProps.add_status && this.props.add_status && this.props.add_status.loading && !nextProps.add_status.loading && !nextProps.add_status.error) {
                console.log("add ok redirect")
                this.context.router.push('/space/wiki/category/' + active_category._id);
            }    
        }else{
            if (nextProps.edit_status && this.props.edit_status && this.props.edit_status.loading && !nextProps.edit_status.loading && !nextProps.edit_status.error) {
                console.log("edit ok redirect")
                this.context.router.push('/space/wiki/post/' + active_post._id);
            } 
        }
    }
    
    handleCancel(){
        const {type, active_category, active_post} = this.props;
        if (type === "add") {
            this.context.router.push('/space/wiki/category/' + active_category._id);
        }
        else {
            this.context.router.push('/space/wiki/post/' + active_post._id);
        }
    }
    

    submit(values, dispatch) {
        const {active_post, type} = this.props;
        
        console.log(type + " post: " + JSON.stringify(values, null, 4));
        if (type === "add") {
            dispatch(addPost(values));
        }
        else {
            values.id = active_post._id;
            dispatch(editPost(values));
        } 
    }

    render() {
        const { type, error, handleSubmit, pristine, invalid, submitting } = this.props;
        return (
            <Box type="simple" title={type === "add" ? "Tạo bài viết" : "Sửa bài viết"}>
                <form onSubmit={handleSubmit(this.submit)} >
                    <div className="box-body">
                        <CategorySelect field_props={{name:"category_id", label:"Chủ đề"}} />
                        <Field component={FormInput} name="title" type="text" label="Tiêu đề" placeholder="Tiêu đề" />
                        <Field component={Editor} id="postContentEditor" name="content" label="Nội dung" />
                        <Field component={TagInput} subject="post" id="postTagInput" name="tags" label="Tag" placeholder="Tags" />
                    </div>
                    <div className="box-footer">
                        <button type="submit" disabled={invalid || pristine || submitting} className="btn btn-primary pull-right">{type === "add" ? "Tạo" : "Cập nhật"}</button>
                        <button type="button" className="btn btn-default pull-right" onClick={this.handleCancel}>Đóng</button>
                    </div>
                </form>
            </Box>
        );
    }
}

const form = 'PostForm';
const fields = ["title", "content", "tags", "category_id"];
PostForm = reduxForm({form, validate, fields})(PostForm);

const mapStateToProps = (state) => {
    return {
        active_category: getActiveCategory(state),
        active_post: getActivePost(state),
        add_status: getAddPostStatus(state),
        edit_status: getEditPostStatus(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(PostForm);
