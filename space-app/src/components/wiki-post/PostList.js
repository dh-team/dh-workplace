'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import DateDisplay from '../common/DateDisplay';
import { getPosts } from '../../selectors/selector-wiki-post';

class PostList extends React.Component {

    render() {
        const {posts} = this.props;
        return (
            <div className="wiki-post-list">
                {posts && this.listPosts(posts)}
            </div>
        );
    }

    listPosts(list) {
        return list.map((item) => {
            return (
                <div className="box box-simple wiki-post-item" key={item._id}>
                    { item.title &&
                        <div className="box-header with-border">
                            <h3 className="box-title"><Link to={"/space/wiki/post/" + item._id}> {item.title}</Link></h3>
                            <div className="wiki-post-meta pull-right">
                                <span> Đăng bởi <a href="#"> @{item.meta.create_user.uname}</a> vào lúc </span>
                                <DateDisplay value={item.meta.create_time} />
                            </div>
                        </div>
                    }

                    { item.content &&
                        <div className="box-body">
                            <span className="wiki-post-preview-content" dangerouslySetInnerHTML={{__html: item.content}}></span>                            
                            <Link className="wiki-post-read-more" to={"/space/wiki/post/" + item._id}>Xem tiếp</Link>
                        </div>
                    }

                </div>
            );
        });
    }
}

const mapStateToProps = (state) => {
    return {
        posts: getPosts(state)
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(PostList);
