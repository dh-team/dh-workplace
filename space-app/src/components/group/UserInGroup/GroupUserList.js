'use strict';

import React from 'react';
import { Link } from 'react-router';
import GroupUserItem from './GroupUserItem';

export default class GroupUserList extends React.Component {

    render() {
        const {users} = this.props;
        return (
            <div className="space-user-list">
                <ul className="list-group">
                    {this.renderUsers(users)}
                </ul>
            </div>
        );
    }
    

    renderUsers(users) {
        return users.map((item) => {
            if(!item) return null;
            return (
                <li className="list-group-item" key={item.user._id}>
                    <GroupUserItem data={item} group_id={this.props.group_id}/>
                </li>
            );
        });
    }
}
