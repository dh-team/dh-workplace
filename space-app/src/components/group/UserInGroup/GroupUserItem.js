'use strict';

import React from 'react';
import { connect }from 'react-redux';
import { Link } from 'react-router';
import { deleteGroupUser } from "../../../actions/action-group-manage";

class GroupUserItem extends React.Component {
    render() {
        const {data, deleteGroupUser} = this.props;
        const isAdmin = data.roles && data.roles.indexOf("administrator") > -1 ;
        return (
            <div className="space-user-item row">
                <div className="space-user-item-detail col-md-11">
                    <img src="/static/space/img/avatar.png" alt="" className="user-avatar img-circle" />
                    <span className="user-name">{"@" + data.uname} - {data.user.display_name}</span>
                    {isAdmin && <span className="user-role label label-danger">administrator</span>}
                    {!data.is_active && <span className="user-status label label-default">chờ xác nhận</span>}
                </div>
                
                <div className="space-user-item-fn col-md-1"> 
                    {<button onClick={deleteGroupUser} className="btn btn-xs btn-default pull-right"><i className="glyphicon glyphicon-remove"></i> loại bỏ</button>}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
}
const mapDispatchToProps = (dispatch, ownProps) => {
    var userID = ownProps.data.user._id;
    var groupID = ownProps.group_id;
    return {
        deleteGroupUser: () => {
            dispatch(deleteGroupUser({group_id: groupID, user_id: userID}));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupUserItem)
