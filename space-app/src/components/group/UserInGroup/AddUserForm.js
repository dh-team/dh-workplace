'use strict';

import React, {Component, PropTypes} from 'react';
import { reduxForm, Field, SubmissionError, reset } from 'redux-form';
import FormInput from '../../form/Input';
import valid from '../../../utils/validate';
import { addGroupUser } from '../../../actions/action-group-manage';


//client side validation
function validate(values) {
    const errors = {};
    if (!values.uname || !valid.hasLength(values.uname, 1, 50)) {
        errors.uname = 'ID không được để trống, độ dài từ 1-50 ký tự';
    }
    if (!valid.isID(values.uname, 1, 50)) {
        errors.uname = 'ID không đúng định dạng';
    }
    return errors;
}

class AddUserForm extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        // this.handleCancel = this.handleCancel.bind(this);
    }
    componentDidMount() {
        this.submit = this.submit.bind(this);
    }

    submit(values, dispatch) {
        values.group_id = this.props.group_id;
        console.log("add user to group: " + JSON.stringify(values, null, 4));
        return dispatch(addGroupUser(values));
    }

    render() {
        const { error, handleSubmit, pristine, reset, submitting } = this.props;

        return (
            <div className="add-user-form box box-solid">
                <form onSubmit={handleSubmit(this.submit)} className="box-body row">
                    <Field component={FormInput} name="uname" type="text" label="ID người dùng trong space" placeholder="ID người dùng trong space" labelCls="sr-only" wrapCls="col-lg-8" addonPre="@" />
                    <div className="form-group col-lg-4">
                        <button type="submit" disabled={pristine || submitting} className="btn btn-primary"> Thêm người dùng</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default reduxForm({
  form: 'addUserForm',  // a unique identifier for this form
  validate
})(AddUserForm)
