'use strict';

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import WorkColumn from "./WorkColumn";

import { openDialog } from '../../actions/action-msg';
import { editWork } from '../../actions/action-work';
import { getTodoWorks, getDoingWorks, getDoneWorks } from '../../selectors/selector-work';

class KanbanTable extends Component {
    constructor() {
        super();

        this.updateWorkStatus = this.updateWorkStatus.bind(this);

        this.sortableTodo = null;
        this.sortableDoing = null;
        this.sortableDone = null;

        this.state = {
            shoud_update: true
        }
    }

    componentDidMount() {
        const thisCom = this;
        this.sortableTodo = Sortable.create(document.getElementById('todoList'), {
            group: {
                name: 'todo',
                put: ["doing"]
            },
            animation: 100,
            draggable: ".kanban-work-item",
            onAdd: function (evt) {
                var workId = evt.item.getAttribute("data-id");
                console.log("onAdd todo", workId);
                thisCom.updateWorkStatus(workId, "todo");
            }
        });

        this.sortableDoing = Sortable.create(document.getElementById('doingList'), {
            group: {
                name: 'doing',
                put: ["todo", "done"]
            },
            animation: 100,
            draggable: ".kanban-work-item",
            onAdd: function (evt) {
                var workId = evt.item.getAttribute("data-id");
                console.log("onAdd doing", workId);
                thisCom.updateWorkStatus(workId, "doing");
            }
        });

        this.sortableDone = Sortable.create(document.getElementById('doneList'), {
            group: {
                name: 'done',
                put: ['doing']
            },
            animation: 100,
            draggable: ".kanban-work-item",
            onAdd: function (evt) {
                var workId = evt.item.getAttribute("data-id");
                console.log("onAdd done", workId);
                thisCom.updateWorkStatus(workId, "done");
            }
        });
    }

    componentWillUnmount() {
        this.sortableTodo.destroy();
        this.sortableDoing.destroy();
        this.sortableDone.destroy();
    }

    shouldComponentUpdate() {
        return this.state.shoud_update;
    }

    updateWorkStatus(workId, status, shoud_update) {
        this.setState({ shoud_update: false });
        const { work, editWork } = this.props;
        var data = {
            update_area: "status",
            id: workId,
            status: status,
        };
        editWork(data);
    }

    render() {
        const { todo, doing, done } = this.props;
        return (
            <div className="kanban-table row">
                <div className="kanban-column col-md-4">
                    <WorkColumn works={todo} updateWorkStatus={this.updateWorkStatus} title="Todo" className="box-default" id="todoList" />
                </div>
                <div className="kanban-column col-md-4">
                    <WorkColumn works={doing} updateWorkStatus={this.updateWorkStatus} title="Doing" className="box-primary" id="doingList" />
                </div>
                <div className="kanban-column col-md-4">
                    <WorkColumn works={done} updateWorkStatus={this.updateWorkStatus} title="Done" className="box-success" id="doneList" />
                </div>

            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        todo: getTodoWorks(state),
        doing: getDoingWorks(state),
        done: getDoneWorks(state),
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        editWork: (data) => dispatch(editWork(data)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(KanbanTable);