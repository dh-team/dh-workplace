'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';

import TimeStamp from "../common/TimeStamp";
import WorkSource from '../work/WorkSource';

export default class WorkItem extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        const { work, updateWorkStatus } = this.props;
        const status = work.doing && work.doing.status;
        return (
            <div className="kanban-work-item" data-id={work._id}>
                <div className="kanban-work-name">
                    <Link to={"/space/work/" + work._id}>{work.name}</Link>
                </div>
                <div className="kanban-work-detail">
                    {work.source && <WorkSource source={work.source} />}
                </div>
                {status === "todo" && 
                    <a role="button" className="remove-from-todo" title="Bỏ khỏi todo" onClick={() => updateWorkStatus(work._id, "assigned_accept", true)}><i className="fa fa-rotate-left"></i></a>
                }

            </div>
        );
    }
}
