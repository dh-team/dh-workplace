'use strict';

import React, { Component, PropTypes } from 'react';
import WorkItem from "./WorkItem";

export default class WorkColumn extends Component {
    constructor() {
        super();
    }

    componentDidMount() {
        
    }

    componentWillUnmount() {
        
    }

    render() {
        const { id, works, title, className, updateWorkStatus } = this.props;
        return (
            <div className={"box " + className}>
                <div className="box-header with-border">
                    <h3 className="box-title">{title} ({works.length})</h3>
    
                    <div className="box-tools">
                    </div>
                </div>
    
                <div className="box-body">
                    <div className="kanban-work-list" id={id}>
                        {works.map((item) => {
                            return (<WorkItem key={"kanban_item_" + item._id} work={item} updateWorkStatus={updateWorkStatus} />);
                        })}
                    </div>
                </div>
    
            </div>
        );
    }
}
