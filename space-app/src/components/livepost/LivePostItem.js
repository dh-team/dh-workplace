'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import LivePostCreate from './LivePostCreate';
import LivePostGallery from './LivePostGallery';
import LivePostFiles from './LivePostFiles';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';
import ReactBtn from '../reaction/ReactBtn';
import CommentList from '../comment/CommentList';
import CommentCountBtn from '../comment/CommentCountBtn';
import AddComment from '../comment/AddComment';
import GroupDisplayLink from '../group/GroupDisplayLink';

export default class LivePostItem extends React.Component {
    constructor() {
        super();
        // bind method
        this.handleCommentClick = this.handleCommentClick.bind(this);
        this.state = {
            show_comment: false
        }
    }
    
    handleCommentClick(){
        this.setState({
            ...this.state,
            show_comment: true
        });
    }
    
    render() {
        const {post, user_profile, editing, display_scope, startEdit, endEdit, deletePost} = this.props;
        
        var isOwner = post.meta.create_user.user_id === user_profile.user_id;
        var content = post.content.replace(/\n/g, "<br/>");
        
        var postInGroup = null;
        if(display_scope){
            post.scope.view_scope.map(function(item){
                if(item !== "g:all" && item.startsWith("g:")){
                    postInGroup = item.substr(2);
                }
            });
        }
        
        return (
            <div className={"box box-simple live-post-item " + (isOwner ? "live-post-item-own" : "")} key={post._id}>
                   
                <div className="box-body">
                    <div className="media live-post-header">
                        <div className="media-left">
                            <a href="#">
                                <img className="media-object img-circle live-post-user-img" src="/static/space/img/avatar.png" alt="user avatar" />
                            </a>
                        </div>
                        
                        <div className="media-body">
                        
                            <div className="pull-left">
                                <div className="live-post-username"><UserName user={post.meta.create_user}/></div>
                                <div className="live-post-time"><DateDisplay value={post.meta.create_time} /></div>
                            </div>
                            
                            <div className="pull-right">
                                {postInGroup &&
                                    <div className="live-post-scope"><GroupDisplayLink group_id={postInGroup} pre_text="đăng trong" /></div>
                                }
                                
                                {isOwner &&
                                    <div className="live-post-function">
                                        <a role="button" className="dropdown-toggle" data-toggle="dropdown">
                                            <i className="fa fa-angle-down"></i>
                                        </a>
                                        <ul className="dropdown-menu pull-right" role="menu">
                                            <li><a role="button" onClick={() => startEdit(post._id)}><i className="fa fa-pencil"></i> Sửa</a></li>
                                            <li><a role="button" onClick={() => deletePost(post._id)}><i className="fa fa-trash-o"></i> Xóa</a></li>
                                        </ul>
                                    </div>
                                }
                            </div>
                            
                        </div>
                    </div>
                    
                    {!editing &&
                        <div className="live-post-content" dangerouslySetInnerHTML={{__html: content}}></div>    
                    }
                    
                    {editing &&
                        <LivePostCreate type="edit" form="editLivePostForm" onCancel={endEdit} onCompleted={endEdit} post={post} />
                    }
                    
                    {post.attachments && post.attachments.display_type === "gallery" &&
                        <LivePostGallery files={post.attachments.files}/>
                    }
                    
                    {post.attachments && post.attachments.display_type === "list" &&
                        <LivePostFiles files={post.attachments.files}/>
                    }
                    <div className="live-post-meta">
                        <div className="live-post-meta-reaction pull-left">
                            <ReactBtn stream_id={post.reaction_stream} current_user={user_profile} />
                        </div>
                        <div className="live-post-meta-comment pull-right">
                            <CommentCountBtn onBtnClick={this.handleCommentClick} stream_id={post.comment_stream || "1"} />
                        </div>
                    </div>
                    
                </div>
                
                {this.state.show_comment &&
                    <div className="box-footer">
                        <CommentList stream_id={post.comment_stream} />
                        <AddComment stream_id={post.comment_stream} focus />
                    </div>    
                }
            </div>
        );
    }
}

