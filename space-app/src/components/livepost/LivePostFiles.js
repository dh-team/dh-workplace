'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import FileSize from '../common/FileSize';

export default class LivePostGallery extends Component {
    constructor() {
        super();
        // bind method
        // this.render1 = this.render1.bind(this);
    }
    
    componentDidMount() {
       
    }
    
    componentWillUnmount() {
    }
    
    
    render() {
        const {files} = this.props;
        const url = "/fs/download?id=";
        
        if(!files || files.length === 0) return null;
       
        return (
            <div className="live-post-file-list" ref="gallery">
                {files.map((item, index) => {
                    return (
                        <div key={item.id} className="media live-post-file"> 
                            <div className="media-left"> 
                                <i className="fa fa-file"></i>
                            </div> 
                            <div className="media-body"> 
                                <div className="live-post-file-name">
                                    <a href={url + item.id} ><span>{item.name}</span></a>
                                </div>
                                <div className="live-post-item-meta">
                                    <FileSize value={item.size} />
                                </div>
                            </div> 
                        </div>
                    );
                })}
            </div>
        );
    }
}
