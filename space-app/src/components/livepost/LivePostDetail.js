'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import LivePostGallery from './LivePostGallery';
import LivePostFiles from './LivePostFiles';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';
import ReactBtn from '../reaction/ReactBtn';
import CommentBox from '../comment/CommentBox';

import { getPost } from '../../selectors/selector-live-post';
import { getCurrentUser } from '../../selectors/selector-user';

class LivePostDetail extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };
    
    constructor(props) {
        super(props);
    }
    
    componentWillReceiveProps(nextProps) {
    }
        
    render() {
        const {post} = this.props;
        if(!post) return null;
        var content = post.content.replace(/\n/g, "<br/>");
        
        return (
            <div className="live-post-detail">
                <div className="box box-simple live-post-item" key={post._id}>
                
                    <div className="box-body">
                        <div className="media live-post-header">
                            <div className="media-left">
                                <a href="#">
                                    <img className="media-object img-circle live-post-user-img" src="/static/space/img/avatar.png" alt="user avatar" />
                                </a>
                            </div>
                            <div className="media-body">
                                <div><UserName user={post.meta.create_user}/></div>
                                <div><DateDisplay value={post.meta.create_time} /></div>
                            </div>
                        </div>
                        
                        <div className="live-post-content" dangerouslySetInnerHTML={{__html: content}}></div>
                        
                        {post.attachments && post.attachments.display_type === "gallery" &&
                            <LivePostGallery files={post.attachments.files}/>
                        }
                    
                        {post.attachments && post.attachments.display_type === "list" &&
                            <LivePostFiles files={post.attachments.files}/>
                        }
                        
                    </div>
                    
                    <div className="box-footer">
                        <ReactBtn stream_id={post.reaction_stream} current_user={this.props.user_profile} />
                    </div>
                    
                </div>
            
                {post && post.comment_stream && 
                    <CommentBox stream_id={post.comment_stream} />
                }
            </div>    
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        post: getPost(state, ownProps.post_id),
        user_profile: getCurrentUser(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LivePostDetail);
