'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import LivePostItem from './LivePostItem';

import { fetchCommentStream } from '../../actions/action-comment';
import { fetchLivePosts, setActiveLivePost, deleteLivePost } from '../../actions/action-live-post';
import { openDialog } from '../../actions/action-msg';

import { getPosts, getFetchPostsStatus } from '../../selectors/selector-live-post';
import { getCurrentUser } from '../../selectors/selector-user';

class LivePostList extends React.Component {
    constructor() {
        super();
        // bind method
        this.startEdit = this.startEdit.bind(this);
        this.endEdit = this.endEdit.bind(this);
        this.handleDelete= this.handleDelete.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
        this.loadMore = this.loadMore.bind(this);
        
        this.state = {
            selected_to_edit: null,
            selected_to_delete: null
        };
    }
    
    startEdit(postID){
        const {setActiveLivePost} = this.props;
        setActiveLivePost(postID);
        this.setState({
            ...this.state,
            selected_to_edit: postID
        });
    }
    
    endEdit(){
        this.setState({
            ...this.state,
            selected_to_edit: null
        });
    }
    
    handleDelete(postID){
        console.log("delete live post " + postID);
        this.setState({
            ...this.state,
            selected_to_delete: postID
        });
        this.props.openDialog({
            title:"Xóa bài viết",
            content:"Bạn có chắc chắn muốn xóa bài viết này không?",
            confirmCallback: this.confirmDelete
        });
    }
    
    confirmDelete(){
        console.log("do delete live post " + this.state.selected_to_delete);
        this.props.deleteLivePost(this.state.selected_to_delete);
    }
    
    loadMore(){
        const {group_id, fetch_status, fetchLivePosts} = this.props;
        if(!fetch_status.loading){
            fetchLivePosts(group_id);    
        }
    }
    
    render() {
        const {display_scope, posts, fetch_status, user_profile, fetchCommentStream} = this.props;
        var thisCom = this;
        return (
            <div className="live-post-list">
                {posts && 
                    posts.map(function(item){
                        return <LivePostItem key={item._id} post={item} user_profile={user_profile} editing={item._id === thisCom.state.selected_to_edit} display_scope={display_scope}
                        fetchCommentStream={fetchCommentStream} startEdit={thisCom.startEdit} endEdit={thisCom.endEdit} deletePost={thisCom.handleDelete} />;
                    })
                }
                <div className="live-post-more">
                    {fetch_status.loading && 
                        <img src="/static/space/img/loading.gif" alt="loading" />
                    }
                    {!fetch_status.loading && 
                        <button className="btn btn-default btn-sm" onClick={this.loadMore}>
                            Tải thêm
                        </button>
                    }
                </div>
            </div>
            
        );
    }
   
}

const mapStateToProps = (state) => {
    return {
        posts: getPosts(state),
        fetch_status: getFetchPostsStatus(state),
        user_profile: getCurrentUser(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchLivePosts: (params) => dispatch(fetchLivePosts(params)),
        fetchCommentStream: (params) => dispatch(fetchCommentStream(params)),
        deleteLivePost: (id) => dispatch(deleteLivePost(id)),
        setActiveLivePost: (id) => dispatch(setActiveLivePost(id)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LivePostList);
