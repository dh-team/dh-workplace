'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import UploadGallery from '../common/UploadGallery';
import TextArea from '../form/TextArea';
import valid from '../../utils/validate';

import { addLivePost, addLivePostReset, editLivePost, editLivePostReset } from '../../actions/action-live-post';
import { getPost, getAddPostStatus, getEditPostStatus } from '../../selectors/selector-live-post';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.content, 1)) {
        errors.content = 'Bạn cần nhập nội dung';
    }

    return errors;
}

const INIT_STATE = {
    attach_type: "none", // "none" | "image" | "file",
    attach_image: {
        render: false,
        files: [], // [ file ], file: {id, name, size, mime_type}
    },
    attach_file: {
        render: false,
        files: [], // [ file ], file: {id, name, size, mime_type}
    }
};

class LivePostCreate extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.handleCancel = this.handleCancel.bind(this);
        this.handleAttachFiles = this.handleAttachFiles.bind(this);
        this.changeAttachType = this.changeAttachType.bind(this);
        
        this.state = INIT_STATE;
    }

    componentWillMount() {
        const {post, type, addLivePostReset, editLivePostReset, initialize, reset} = this.props;
        if (type === "add") {
            addLivePostReset();
            initialize({
                content: ""
            });
        }
        else {
            editLivePostReset();
            initialize({
                content: post.content
            });
        }
        reset();
    }

    componentWillReceiveProps(nextProps) {
        const {type, add_status, edit_status, onCompleted, reset} = this.props;
        if(type === "add"){
            if (add_status && nextProps.add_status && add_status.loading && !nextProps.add_status.loading && !nextProps.add_status.error) {
                // addLivePostReset();
                reset();
                this.setState(INIT_STATE);
            }    
        }else{
            if (edit_status && nextProps.edit_status && edit_status.loading && !nextProps.edit_status.loading && !nextProps.edit_status.error) {
                // editLivePostReset();
                onCompleted();
            } 
        }
    }
    
    handleCancel(){
    }
    
    handleAttachFiles(type, files){
        if(type === "image"){
            this.setState({
                attach_image: {
                    ...this.state.attach_image,
                    files: files
                }
            });
        }else{
            this.setState({
                attach_file: {
                    ...this.state.attach_file,
                    files: files
                }
            });
        }
    }

    submit(values, dispatch) {
        console.log("live post: " + JSON.stringify(values, null, 4));
        console.log("state: " + JSON.stringify(this.state, null, 4));
        
        const {group_id, post, type} = this.props;
        if (type === "add") {
            var newPost = {
                content: values.content
            };
            if(group_id){
                newPost.group_id = group_id;
            }
            const {attach_type, attach_image, attach_file} =  this.state;
            if (attach_type === "image" && attach_image.files.length > 0) {
                newPost.attachments = {
                    display_type: "gallery",
                    files: attach_image.files
                };
            }
            else if (attach_type === "file" && attach_file.files.length > 0) {
                newPost.attachments = {
                    display_type: "list",
                    files: attach_file.files
                };
            }
            dispatch(addLivePost(newPost));
        }
        else {
            values.id = post._id;
            dispatch(editLivePost(values));
        } 
    }
    
    changeAttachType(type){
        var updateState = {attach_type: type};
        if (type === "image" && !this.state.attach_image.render) {
            updateState.attach_image = {
                ...this.state.attach_image,
                render: true
            };
        }
        else if (type === "file" && !this.state.attach_file.render) {
            updateState.attach_file = {
                ...this.state.attach_file,
                render: true
            };
        }
        this.setState(updateState);
    }

    render() {
        const { type, error, handleSubmit, invalid, pristine, reset, submitting, onCancel } = this.props;
        return (
            <div className="live-post-create">
                <div className="box box-simple">
                    {type === "add" && 
                        <div className="box-header with-border">
                            <ul className="live-post-create-menu">
                                <li><a role="button" onClick={() => this.changeAttachType("none")}><i className="fa fa-pencil"></i> Chia sẻ</a></li>
                                <li><a role="button" onClick={() => this.changeAttachType("image")}><i className="fa fa-camera"></i> Ảnh</a></li>
                                <li><a role="button" onClick={() => this.changeAttachType("file")}><i className="fa fa-file-text-o"></i> File</a></li>
                            </ul>
                        </div>
                    }
                    <form onSubmit={handleSubmit(this.submit)} >
                        <div className="box-body">
                            <Field component={TextArea} name="content" label="" placeholder="Viết chia sẻ" labelCls="sr-only" focus={type === "edit"} error_only_dirty />
                            
                            {this.state.attach_image.render && 
                                <div className={this.state.attach_type === "image" ? "" : "hide"}>
                                    <UploadGallery onFilesChanged={(files) => this.handleAttachFiles("image", files)} display_type="gallery" />    
                                </div>
                            }
                            
                            {this.state.attach_file.render && 
                                <div className={this.state.attach_type === "file" ? "" : "hide"}>
                                    <UploadGallery onFilesChanged={(files) => this.handleAttachFiles("file", files)} display_type="list" />    
                                </div>
                            }
                            
                        </div>
                        <div className="box-footer">
                            <button type="submit" disabled={invalid || pristine || submitting} className="btn btn-xs btn-primary pull-right">{type === "add" ? "Đăng" : "Cập nhật"}</button>
                            {type === "edit" && 
                                <button type="button" className="btn btn-xs pull-right" onClick={onCancel}>Bỏ qua</button>    
                            }
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

const form = 'LivePostCreate';
const fields = ["content"];
LivePostCreate = reduxForm({form, validate, fields})(LivePostCreate);

const mapStateToProps = (state, ownProps) => {
    return {
        // post: ownProps.post_id ? getPost(state, ownProps.post_id) : null,
        add_status: getAddPostStatus(state),
        edit_status: getEditPostStatus(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addLivePostReset: () => dispatch(addLivePostReset()),
        editLivePostReset: () => dispatch(editLivePostReset()),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(LivePostCreate);
