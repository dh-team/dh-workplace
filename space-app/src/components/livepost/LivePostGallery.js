'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';

export default class LivePostGallery extends Component {
    constructor() {
        super();
        // bind method
        this.render1 = this.render1.bind(this);
        this.render2 = this.render2.bind(this);
        this.render3 = this.render3.bind(this);
        this.render4 = this.render4.bind(this);
    }
    
    componentDidMount() {
        if (!this.refs.gallery) return;
        
        $(this.refs.gallery).lightGallery({
            selector: '.live-post-gallery-image'
        });
        
        // console.log("LivePostGallery componentDidMount");
        // console.log(this.refs.gallery);
        // console.log(this.refs.gallery.clientHeight);
        
        const {files} = this.props;
        if(!files || files.length === 0) return;
        
        if(files.length === 1) {
            this.render1();
        }else if(files.length === 2) {
            this.render2();
        }else if(files.length === 3) {
            this.render3();
        }else if(files.length >= 4) {
            this.render4();
        }
    }
    
    componentWillUnmount() {
        if (this.refs.gallery) {
            // $(this.refs.gallery).lightGallery().destroy(true);
        }
    }
    
    render1(){
        
    }
    
    render2() {
        var wrapper = this.refs.gallery;
        var jWrapper = $(wrapper);
        var wrapperWidth = wrapper.clientWidth;
        
        this.iterator(jWrapper.find(".item-1, .item-2"), (item) => {
            item.style.height = "" + (wrapperWidth / 2) + "px";  
        });
        
        this.fitImgs(jWrapper.find(".live-post-gallery-thumb"));
    }
    
    render3(){
        var wrapper = this.refs.gallery;
        var jWrapper = $(wrapper);
        var wrapperWidth = wrapper.clientWidth;
        
        this.iterator(jWrapper.find(".item-1"), (item) => {
            item.style.height = "" + (wrapperWidth * 2 / 3) + "px";
        });
        
        this.iterator(jWrapper.find(".item-2, .item-3"), (item) => {
            item.style.height = "" + (wrapperWidth / 3) + "px";
        });
       
        this.fitImgs(jWrapper.find(".live-post-gallery-thumb"));
    }
    
    render4(){
        var wrapper = this.refs.gallery;
        var jWrapper = $(wrapper);
        var wrapperWidth = wrapper.clientWidth;
        
        this.iterator(jWrapper.find(".item-1"), (item) => {
            item.style.height = "" + (wrapperWidth * 2 / 3) + "px";
        });
        
        this.iterator(jWrapper.find(".item-2, .item-3, .item-4"), (item) => {
            item.style.height = "" + (wrapperWidth / 3) + "px";
        });
       
        this.fitImgs(jWrapper.find(".live-post-gallery-thumb"));
        this.fitImgs(jWrapper.find(".thumb-1"), 2 / 3);
    }
    
    iterator(selector, f){
        for(var i = 0; i < selector.length; i++){
            f(selector[i]);
        }
    }
    
    fitImgs(imgs, ratio){
        for(var i = 0; i < imgs.length; i++){
            this.fitImg(imgs[i], ratio);
        }
    }
    
    fitImg(img, ratio){
        if(!ratio) ratio = 1;
        if(img.clientHeight < img.clientWidth * ratio) {
            img.style.height = "100%";
            img.style.width = "auto";
        }else{
            img.style.height = "auto";
            img.style.width = "100%";
        }
    }
    
    
    render() {
        const {files} = this.props;
        const url = "/fs/file?id=";
        
        if(!files || files.length === 0) return null;
        var cls = "live-post-gallery";
        if(files.length <= 4) cls += " gallery-" + files.length;
        if(files.length > 4) cls += " gallery-4 has-more";
        return (
            <div className={cls} ref="gallery">
                {files.map((item, index) => {
                    return (
                        <div key={item.id} className={"live-post-gallery-item item-" + (index > 3 ? "hide" : (index + 1))}>
                            <a href={url + item.id} className="live-post-gallery-image">
                                {index > 3 ? <img src={url + item.id} alt={item.name} width="0" height="0" /> : <img className={"live-post-gallery-thumb thumb-" + (index + 1)} alt={item.name} src={url + item.id} />}
                                
                                {index === 3 && files.length > 4 && 
                                    <div className="live-post-gallery-more">
                                        <div className="display-table"><div className="display-cell">+{files.length - index}</div></div>
                                    </div>
                                }
                            </a>
                        </div>
                    );
                })}
            </div>
        );
    }
}
