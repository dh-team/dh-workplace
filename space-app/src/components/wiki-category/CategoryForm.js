'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { reduxForm, initialize, Field, SubmissionError } from 'redux-form';
import FormInput from '../form/Input';
import FormModal from '../common/FormModal';
import valid from '../../utils/validate';
import { addPostCategory, editPostCategory } from '../../actions/action-wiki-category';
import { getActiveCategory } from '../../selectors/selector-wiki-category';

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.name, 1, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 1-250 ký tự';
    }

    if (!valid.hasLength(values.description, 0, 250)) {
        errors.description = 'Độ dài tối đa 250 ký tự';
    }

    return errors;
}

class CategoryForm extends Component {
    constructor() {
        super();
        // bind method
        this.submit = this.submit.bind(this);
        this.onShow = this.onShow.bind(this);
    }

    onShow(){
        const {active_category, type, initialize, reset} = this.props;
        if (type === "add") {
            initialize({
                name: "",
                description: ""
            });
        } else if (active_category) {
            initialize({
                name: active_category.name,
                description: active_category.description
            });
        }
        reset();
    }

    submit(values, dispatch) {
        const {active_category, type} = this.props;
        console.log(type +" category: " + JSON.stringify(values, null, 4));
        var data = {
            name: values.name,
            description: values.description
        };
        if (type === "add") {
            if (active_category && active_category._id && active_category._id != "root") {
                data.parent = active_category._id;
            }
            dispatch(addPostCategory(data));
        }
        else {
            if (active_category && active_category._id && active_category._id != "root") {
                data.id = active_category._id;
                dispatch(editPostCategory(data));
            }
        }
    }

    render() {
        const { id, type } = this.props; // type = 'add' | 'edit'
        const { error, handleSubmit, pristine, reset, submitting } = this.props;
        return (
            <FormModal id={id || "categoryFormModal"} title={type === 'add' ? "Tạo chủ đề" : "Sửa chủ đề"} disableSubmit={pristine || submitting}
             okCallback={handleSubmit(this.submit)} onShow={this.onShow} >
                <Field component={FormInput} name="name" type="text" label="Tên chủ đề" placeholder="Tên chủ đề" />
                <Field component={FormInput} name="description" type="text" label="Thông tin" />
            </FormModal>
        );
    }
}

const form = 'postCategoryForm';
const fields = ["name", "description"];
CategoryForm = reduxForm({form, validate, fields})(CategoryForm);

const mapStateToProps = (state, ownProps) => {
    return {
        active_category: getActiveCategory(state),
    };
};
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryForm);
