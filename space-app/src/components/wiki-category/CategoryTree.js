'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import CategoryForm from './CategoryForm';
import { fetchPostCategories, updateCategoryParent, setActiveCategory, deleteCategory } from '../../actions/action-wiki-category';
import { openDialog } from '../../actions/action-msg';
import { getAllCategories, getActiveCategory, getActiveCategoryId } from '../../selectors/selector-wiki-category';
import { getCurrentUser } from '../../selectors/selector-user';

class CategoryTree extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.jCatTree = null;
        this.handleManageMode = this.handleManageMode.bind(this);
        this.handleViewMode = this.handleViewMode.bind(this);
        this.handleDeleteClick = this.handleDeleteClick.bind(this);
        this.handleAddClick = this.handleAddClick.bind(this);
        this.handleEditClick = this.handleEditClick.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
        this.confirmMove = this.confirmMove.bind(this);
        this.cancelMove = this.cancelMove.bind(this);

        this.buildSource = this.buildSource.bind(this);
        this.buildTree = this.buildTree.bind(this);
        this.buildTreeNormalUser = this.buildTreeNormalUser.bind(this);
        this.refreshTree = this.refreshTree.bind(this);
        this.setSelected = this.setSelected.bind(this);
        this.registerChange = this.registerChange.bind(this);

        this.state = {
            func: '', // "detail" | 'add' | 'edit'
            move: { // move node
                id: '',
                parent: ''
            },
            mode: "view", // "view" | "manage",
            selected_id: null
        };
        
    }

    componentDidMount() {
        this.jCatTree = $(this.refs.categoryTree);
        const {current_user, categories} = this.props;
        if (current_user.roles.indexOf("administrator") != -1) {
            this.buildTree();
        }
        else {
            this.buildTreeNormalUser();
        }
        this.registerChange();
        this.refreshTree(categories);
    }

    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.categories) != JSON.stringify(this.props.categories)) {
            console.log("list categories changed");
            this.refreshTree(nextProps.categories);
        }
        
        if(nextProps.active_category_id !== this.props.active_category_id && nextProps.active_category_id !== this.state.selected_id){
            console.log("selected id changed: " + this.state.selected_id + " " + nextProps.active_category_id);
            this.setSelected(nextProps.active_category_id);
        }
    }
    
    componentDidUpdate(){
    }
    
    setSelected(id){
        console.log("select tree node " + id);
        this.setState({selected_id: id});
        this.jCatTree.jstree(true).deselect_all();    
        this.jCatTree.jstree(true).select_node(id);    
    }
    
    buildTree() {
        this.jCatTree.jstree({
            "core": {
                "multiple": false,
                "check_callback": true
            },
            "plugins": ["dnd"],
            "dnd": {
                is_draggable: function() {
                    return false;
                }
            }
        });
        
        const thisCom = this;
        this.jCatTree.bind("move_node.jstree", function (e, data) {
            console.log("move");
            console.log(data);
            var currentID = data.node.id;
            var newParentID = data.parent;
            if(newParentID === "#") newParentID = "root";
            var oldParentID = data.old_parent;

            if(oldParentID !== newParentID){
                console.log("move: " + currentID + " to " + newParentID);
                thisCom.setState({
                    move: { // move node
                        id: currentID,
                        parent: newParentID
                    }
                });
                thisCom.props.openDialog({
                    title: "Di chuyển chủ đề",
                    content: "Bạn có chắc chắn muốn di chuyển chủ đề đã chọn?",
                    confirmCallback: thisCom.confirmMove,
                    cancelCallback: thisCom.cancelMove
                });
            }
        });
    }
    
    buildTreeNormalUser() {
        this.jCatTree.jstree({
            "core": {
                "multiple": false,
                "check_callback" : true
            }
        });
    }

    registerChange(){
        const thisCom = this;
        
        this.jCatTree.bind('activate_node.jstree', function(e, data) {
            console.log("activate_node node changed");
            console.log(data);
            // console.log(data.selected);
            thisCom.jCatTree.jstree(true).open_node(data.node.id);
            thisCom.setState({selected_id: data.node.id});
            
            if (data.node.id !== "root") {
                thisCom.context.router.push("/space/wiki/category/" + data.node.id);
            }
            else {
                thisCom.context.router.push("/space/wiki");
            }
        });
    }

    buildSource(categories) {
        var source = categories.map(function(item) {
            var node = {
                id: item._id,
                parent: item.parent || "root",
                text: item.name + " (" + item.stat.total_post_count + ")",
                icon: "fa fa-folder-o",
                a_attr: {
                    title: "Bài viết thuộc chủ đề: " + item.stat.post_count
                }
            }
            return node;
        });
        var totalPostCount = categories.reduce(function(accumulator, currentValue) {
            return currentValue.parent ? accumulator : accumulator + currentValue.stat.total_post_count;
        }, 0);
        var node = {
            id: "root",
            parent: "#",
            text: "Tất cả" + " (" + totalPostCount + ")",
            icon: "fa fa-folder",
            state: {opened: true}
        }
        source.push(node);
        return source;
    }
   
    refreshTree(categories){
        var source = this.buildSource(categories);
        const thisCom = this;
        
        this.jCatTree.jstree(true).settings.core.data = source;
        this.jCatTree.unbind('refresh.jstree');
        this.jCatTree.bind('refresh.jstree', function() {
            thisCom.setSelected(thisCom.props.active_category_id);
        });
        this.jCatTree.jstree(true).refresh();
    }

    handleManageMode() {
        this.setState({mode: "manage"});
        this.jCatTree.jstree(true).settings.dnd.is_draggable = function() {
            return true;
        };
    }
    
    handleViewMode() {
        this.setState({mode: "view"});
        this.jCatTree.jstree(true).settings.dnd.is_draggable = function() {
            return false;
        };
    }

    handleAddClick() {
        this.setState({
            func: 'add'
        });
        $("#postCategoryModal").modal('show');
    }

    handleEditClick() {
        this.setState({
            func: 'edit'
        });
        $("#postCategoryModal").modal('show');
    }

    handleDeleteClick(){
        this.props.openDialog({
            title:"Xóa chủ đề",
            content:"Bạn có chắc chắn muốn xóa chủ đề đã chọn?",
            confirmCallback: this.confirmDelete
        });
    }
    
    confirmDelete() {
        const {active_category_id, deleteCategory} = this.props;
        deleteCategory(active_category_id);
    }

    confirmMove() {
        // console.log(this.state);
        if(this.state.move && this.state.move.id && this.state.move.parent){
            this.props.updateCategoryParent({
                id: this.state.move.id,
                parent_id: this.state.move.parent
            });
        }
    }

    cancelMove() {
        this.refreshTree(this.props.categories);
    }

    render() {
        const {current_user, active_category_id, fetchPostCategories} = this.props;
        const isAdmin = current_user.roles.indexOf("administrator") != -1;
        return (
            <div className="box box-primary">
                <div className="box-header with-border">
                    <h3 className="box-title">Chủ đề</h3>
                    {this.state.mode === "view" &&
                        <div className="box-tools btn-group">
                        
                            {isAdmin && 
                                <button type="button" className="btn btn-default btn-sm" onClick={this.handleManageMode} title="Quản lý chủ đề">
                                    <i className="fa fa-wrench"></i>
                                </button>
                            }
                            
                            <button type="button" className="btn btn-default btn-sm" onClick={fetchPostCategories} title="Làm mới">
                                <i className="fa fa-refresh "></i>
                            </button>
                        </div>
                    }
                    
                    {this.state.mode === "manage" &&
                        <div className="box-tools btn-group">
                            
                            <button type="button" className="btn btn-default btn-sm" onClick={this.handleViewMode} title="Chế độ xem">
                                <i className="fa fa-eye"></i>
                            </button>
                            
                            <button type="button" className="btn btn-default btn-sm" onClick={this.handleAddClick} title="Thêm chủ đề">
                                <i className="fa fa-plus"></i>
                            </button>
                            
                             <button type="button" className="btn btn-default btn-sm" onClick={this.handleEditClick} title="Sửa">
                                <i className="fa fa-pencil"></i>
                            </button>
                            
                            <button type="button" className="btn btn-default btn-sm" onClick={this.handleDeleteClick} title="Xóa">
                                <i className="fa fa-trash"></i>
                            </button>
                            
                            <button type="button" className="btn btn-default btn-sm" onClick={fetchPostCategories} title="Làm mới">
                                <i className="fa fa-refresh "></i>
                            </button>
                        </div>
                    }
                    
                </div>
                <div className="box-body no-padding">
                    <div id="postCategoryTree" className="post-categories-tree" ref="categoryTree"></div>
                    <CategoryForm id="postCategoryModal" type={this.state.func} />
                </div>
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return {
        categories: getAllCategories(state),
        active_category_id: getActiveCategoryId(state),
        current_user: getCurrentUser(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchPostCategories: () => dispatch(fetchPostCategories()),
        updateCategoryParent: (data) => dispatch(updateCategoryParent(data)),
        setActiveCategory: (data) => dispatch(setActiveCategory(data)),
        deleteCategory: (data) => dispatch(deleteCategory(data)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoryTree);
