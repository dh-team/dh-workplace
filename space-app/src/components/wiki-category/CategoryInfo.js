'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import { getActiveCategory } from '../../selectors/selector-wiki-category';

class CategoryInfo extends React.Component {
    
    render() {
        const {active_category} = this.props;
        if(!active_category || !active_category._id) return null;
        
        return (
            <div className="box box-simple post-category-info">
                <div className="box-header with-border">
                    <h3 className="box-title">Chủ đề: {active_category.name}</h3>
                    <div className="box-tools btn-group">
                        <Link className="btn btn-primary btn-sm" to={"/space/wiki/post/new?category=" + active_category._id}>
                            <i className="fa fa-plus"></i> Tạo bài viết
                        </Link>
                    </div>
                </div>
                <div className="box-body">
                    <h4 className="post-category-name text-light-blue"></h4>
                    <p className="post-category-description">{active_category.description}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        active_category: getActiveCategory(state),
    };
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryInfo);
