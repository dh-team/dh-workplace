'use strict';

import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
// import valid from '../../utils/validate';
import { addComment } from '../../actions/action-comment';

class AddComment extends Component {
    constructor() {
        super();
        // bind method
        this.onKeyDown = this.onKeyDown.bind(this);
        this.handlePostComment = this.handlePostComment.bind(this);
    }
    
    componentDidMount() {
        if(this.props.focus){
            this.commentInput.focus();    
        }
    }
    
    onKeyDown(e) {
        if (e.nativeEvent.keyCode === 13) {
            if (e.nativeEvent.shiftKey) {
                console.log("Shift + Enter");
            }
            else {
                console.log("Enter");
                e.preventDefault();
                this.handlePostComment();
                return;
            }
        }
        var el = this.commentInput;
        setTimeout(function() {
            el.style.cssText = 'height:34px;';
            // console.log("scrollHeight:" + el.scrollHeight);
            if(el.scrollHeight > 34){
                el.style.cssText = 'height:' + (el.scrollHeight + 10) + 'px';    
            }
        }, 0);
    }
    
    handlePostComment(){
        var content = this.commentInput.value.trim();
        if(!content) return;
        const {stream_id, reply_to, addComment} = this.props;
        
        console.log("add comment: " + content);
        addComment({
            stream_id,
            reply_to,
            content: content
        });
        this.commentInput.value = "";
        this.commentInput.style.cssText = 'height:34px;';
    }

    render() {
        return (
            <div className="add-comment-box">
                <textarea onKeyDown={this.onKeyDown} ref={(input) => { this.commentInput = input; }} className="comment-input form-control" placeholder="Viết bình luận, SHIFT + ENTER để xuống dòng">
                </textarea>
            </div>
        );
    }
}


const mapStateToProps = (state, ownProps) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        addComment: (params) => dispatch(addComment(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(AddComment);
