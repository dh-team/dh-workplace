'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import CommentList from './CommentList';
import AddComment from './AddComment';

import { fetchCommentStream } from '../../actions/action-comment';

class CommentBox extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };
    
    constructor() {
        super();
        // bind method
        this.handleLoad = this.handleLoad.bind(this);
        this.state = {
            commentHighlight: null
        };
    }

    componentDidMount(){
        this.handleLoad();
    }
    
    componentWillReceiveProps(nextProps) {
        if (JSON.stringify(nextProps.stream_id) !== JSON.stringify(this.props.stream_id)) {
            console.log("comment stream_id change, reload " + JSON.stringify(nextProps.stream_id));
            this.props.fetchCommentStream(nextProps.stream_id);
        }  
    }
    
    handleLoad(){
        this.props.fetchCommentStream(this.props.stream_id);
    }
    
    componentDidUpdate() {
        
        // scroll to and highlight comment in query string
        // do it one time
        // var commentHighlight = this.context.router.location.query.goto_comment;
        // console.log("componentDidUpdate: commentHighlight " + commentHighlight);
        // if (!commentHighlight || (commentHighlight === this.state.commentHighlight)) return;

        // var commentElm = "#comment_" + commentHighlight;
        // if ($(commentElm).length > 0) {
        //     $('html, body').animate({
        //         scrollTop: $(commentElm).offset().top - $(".main-header").height() - 10
        //     }, 1200);

        //     $(commentElm).pulsate({
        //         color: '#EB6D6D', // set the color of the pulse
        //         reach: 10, // how far the pulse goes in px
        //         speed: 1000, // how long one pulse takes in ms
        //         pause: 0, // how long the pause between pulses is in ms
        //         glow: true, // if the glow should be shown too
        //         repeat: 3, // will repeat forever if true, if given a number will repeat for that many times
        //         onHover: false // if true only pulsate if user hovers over the element
        //     });

        //     this.setState({
        //         commentHighlight: commentHighlight
        //     });
        // }
    }
    
    render() {
        const {stream_id} = this.props;
        
        return (
            <div className="box box-simple comment-box">
                <div className="box-header with-border">
                    <h3 className="box-title"> <i className="fa fa-comments"></i> Bình luận </h3>
                    <div className="box-tools btn-group">
                        <div className="btn-group">
                            <button type="button" className="btn btn-default btn-sm" title="Làm mới" onClick={this.handleLoad}>
                                <i className="fa fa-refresh"></i>
                            </button>
                        </div>
                    </div>
                </div>
                <div className="box-body">
                    <CommentList stream_id={stream_id} />
                    <AddComment stream_id={stream_id} />
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCommentStream: (params) => dispatch(fetchCommentStream(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentBox);
