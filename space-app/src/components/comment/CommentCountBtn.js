'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { fetchCommentStream } from '../../actions/action-comment';
import { getCommentStream } from '../../selectors/selector-comment';

class CommentCountBtn extends React.Component {
    constructor() {
        super();
        this.handleClick = this.handleClick.bind(this);
    }
    
    handleClick(){
        const {stream_id, data, fetchCommentStream, onBtnClick} = this.props;
        if(!(data && data.loading)){
            // avoid fetch multi times
            fetchCommentStream(stream_id);    
        }
        
        onBtnClick && onBtnClick();
    }
    
    
    render() {
        const {data} = this.props;
        let commentCount = 0;
        if(data && data.stat && data.stat.comment_count > 0){
            commentCount = data.stat.comment_count;
        }
        return (
            <a role="button" onClick={this.handleClick}>
                <i className="fa fa-comments"></i> {commentCount} bình luận
            </a>
        );
    }
   
}

const mapStateToProps = (state, ownProps) => {
    return {
        data: getCommentStream(state, ownProps.stream_id) || {_id: ownProps.stream_id, comments: [], stat: {}}
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCommentStream: (params) => dispatch(fetchCommentStream(params)),
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentCountBtn);