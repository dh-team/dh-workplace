'use strict';

import React from 'react';
import { Link } from 'react-router';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';
import AddComment from './AddComment';
import ReactBtn from '../reaction/ReactBtn';

class CommmentItem extends React.Component {
    constructor() {
        super();
    }
    
    render() {
        const {comment, settings} = this.props;
        if(comment.visible){
            return this.renderNormal(comment, settings)
        }else{
            return this.renderDeleted(comment, settings)
        }
    }
    
    renderDeleted(comment, settings) {
        return (
            <div className="media comment-item comment-item-deleted" >
                <div className="media-left">
                    <a href="#">
                        <img className="media-object img-rounded comment-user-img" src="/static/space/img/avatar.png" alt="user avatar" />
                    </a>
                </div>
                <div className="media-body comment-content">
                    <span> Bình luận đã bị xóa!</span>
                    <div className="clearfix"></div>
                    
                    {comment.replies &&
                        comment.replies.map(function(reply){
                            return <CommmentItem key={reply._id} comment={reply} settings={{...settings, disable_reply: true}} />;
                        })
                    }
                    
                </div>
            </div>                
        );
    }
    
    renderNormal(comment, settings) {
        // const {comment, settings} = this.props;
        const {stream_id, selected_to_reply, current_user, disable_reply, handleDelete, handleReply} = settings;
        const isOwner = comment.meta && comment.meta.create_user && comment.meta.create_user.user_id === current_user;
        var content = comment.content;
        if(content) content = content.replace(/\n/g, "<br/>");

        return (
            <div className="media comment-item" id={"comment_" + comment._id} >
                <div className="media-left">
                    <a href="#">
                        <img className="media-object img-rounded comment-user-img" src="/static/space/img/avatar.png" alt="user avatar" />
                    </a>
                </div>
                <div className="media-body comment-content">
                     <UserName user={comment.meta.create_user} /> <span dangerouslySetInnerHTML={{__html: content}}></span>
                     <div className="comment-meta">
                        <ReactBtn stream_id={comment.reaction_stream} current_user={{user_id: current_user}} />
                        
                        {!disable_reply &&
                            <a title="Trả lời bình luận" role="button" onClick={() => handleReply(comment._id)} >Trả lời</a>    
                        }
                        
                        {isOwner && 
                            <a title="Xóa bình luận" role="button" onClick={() => handleDelete(comment._id)} >Xóa</a>
                        }
                        <DateDisplay value={comment.meta.create_time} />
                    </div>
                    
                    <div className="clearfix"></div>
                    
                    {comment.replies &&
                        comment.replies.map(function(reply){
                            return <CommmentItem key={reply._id} comment={reply} settings={settings} />;
                        })
                    }
                    
                    {selected_to_reply === comment._id && 
                        <AddComment stream_id={stream_id} reply_to={comment._id} focus />
                    }
                </div>
            </div>                
        );
        
    }
}

export default CommmentItem;
