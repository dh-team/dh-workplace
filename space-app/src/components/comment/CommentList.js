'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import Modal from '../common/Modal';
import CommentItem from './CommentItem';
import { deleteComment } from '../../actions/action-comment';
import { openDialog } from '../../actions/action-msg';

import { getCommentStream } from '../../selectors/selector-comment';
import { getCurrentUser } from '../../selectors/selector-user';

class CommentList extends React.Component {
    constructor() {
        super();
        this.handleReply = this.handleReply.bind(this);
        this.handleDelete = this.handleDelete.bind(this);
        this.confirmDelete = this.confirmDelete.bind(this);
        this.handleEdit = this.handleEdit.bind(this);
        this.buildCommentStruct = this.buildCommentStruct.bind(this);
        
        this.state = {
            selected_to_delete: null,
            selected_to_reply: null,
        };
    }
    
    handleReply(commentID){
        console.log("reply comment " + commentID);
        this.setState({
            selected_to_reply: commentID
        });
    }
    
    handleDelete(commentID){
        console.log("delete comment " + commentID);
        this.setState({
            selected_to_delete: commentID
        });
        // $("#confirmDeleteCommentModal").modal('show');
        this.props.openDialog({
            title:"Xóa bình luận",
            content:"Bạn có chắc chắn muốn xóa bình luận này không?",
            confirmCallback: this.confirmDelete
        });
    }
    
    confirmDelete(){
        console.log("do delete comment " + this.state.selected_to_delete);
        const {stream_id, deleteComment} = this.props;
        deleteComment({
            stream_id,
            comment_id: this.state.selected_to_delete
        });
    }
    
    handleEdit(){
    }
    
    render() {
        const {data, stream_id, user_profile} = this.props;
        var comments = (data && data.comments) ? data.comments : [];
        return (
            <div className={"comment-list " + (comments.length === 0 ? "comment-list-empty" : "")}>
                {comments && 
                    this.buildCommentStruct(comments).map((item) => {
                        return <CommentItem key={item._id} comment={item} settings={{
                            stream_id: stream_id, 
                            current_user: user_profile.user_id, 
                            selected_to_reply: this.state.selected_to_reply, 
                            handleLike: this.handleLike,
                            handleDelete: this.handleDelete, 
                            handleReply: this.handleReply
                        }} />;
                    })
                }
                
                <Modal id="confirmDeleteCommentModal" title="Xóa bình luận" content="Bạn có chắc chắn muốn xóa bình luận này không?" okCallback={this.confirmDelete} />
            </div>
        );
    }
    
    buildCommentStruct(comments){
        var dict = {};
        var list = [];
        function getItemFromDict(id, dict) {
            var item = dict[id];
            if (!item) {
                item = {
                    id: id,
                    replies: []
                };
                dict[id] = item;
            }
            return item;
        }
        
        comments.map(function(item){
            var thisItem = getItemFromDict(item._id, dict); 
            Object.assign(thisItem, item);
            thisItem.visible = true;
            
            if(thisItem.reply_to){
                var thisParent = getItemFromDict(item.reply_to, dict);
                thisParent.replies.push(thisItem);    
            }
            else{
                list.push(thisItem);
            }
        });
        
        // put deleted comment
        var cmId = 1;
        Object.keys(dict).map(function(key){
            var item = dict[key];
            if(!item.visible && !item.reply_to){
                item._id = cmId++;
                list.push(item);
            }
        });
        return list;
    }
}

const mapStateToProps = (state, ownProps) => {
    const streamId = ownProps.stream_id;
    return {
        data: getCommentStream(state, streamId) || {_id: streamId, comments: [], stat: {}},
        user_profile: getCurrentUser(state)
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteComment: (params) => dispatch(deleteComment(params)),
        openDialog: (params) => dispatch(openDialog(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(CommentList);