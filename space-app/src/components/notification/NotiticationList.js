'use strict';

import React from 'react';
import NotiticationItem from './NotiticationItem';

class NotiticationList extends React.Component {
    constructor() {
        super();
    }
    
    render() {
        const {notifications, current_user, markNotificationRead} = this.props;
        return (
            <div className="notification-list">
                {notifications && notifications.map((item) => {
                        return <NotiticationItem key={item._id} notification={item} markNotificationRead={markNotificationRead} current_user={current_user} />
                    })
                }
            </div>
        );
    }
}

export default NotiticationList;
