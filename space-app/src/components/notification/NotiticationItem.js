'use strict';

import React, { Component, PropTypes } from 'react';
import { Link } from 'react-router';
import DateDisplay from '../common/DateDisplay';
import UserName from '../common/UserName';
import GroupDisplayLink from '../group/GroupDisplayLink';

class NotificationItem extends React.Component {
    static contextTypes = {
        router: PropTypes.object
    };

    constructor() {
        super();
        this.handleMarkRead = this.handleMarkRead.bind(this);
        this.handleItemClick = this.handleItemClick.bind(this);
    }

    handleMarkRead(e) {
        e && e.stopPropagation();
        const { notification, markNotificationRead } = this.props;
        markNotificationRead(notification._id);
    }

    handleItemClick() {
        const { notification: { is_read, type, ref_data } } = this.props;
        if (!is_read) {
            this.handleMarkRead();
        }

        switch (type) {
            case "post_cat_new":
            case "post_cat_modify":{
                if (!ref_data.category) return;
                this.context.router.push('/space/wiki/category/' + ref_data.category.id);
                break;
            }
            case "post_new":
            case "post_modify":{
                if (!ref_data.post) return;
                this.context.router.push(this.getPostLink(ref_data.post));
                break;
            }
            case "comment_new": {
                if (!ref_data.comment) return;
                const objId = ref_data.comment.ref_id;
                const commentId = ref_data.comment.comment_id;

                if (ref_data.comment.ref_type === "post") {
                    let postUrl = (ref_data.comment.ref_obj && ref_data.comment.ref_obj.type === "archive") ? "/space/wiki/post" : "/space/post";
                    this.context.router.push(postUrl + "/" + objId + "?goto_comment=" + commentId);
                } else if (ref_data.comment.ref_type === "work") {
                    this.context.router.push("/space/work/" + objId + "?goto_comment=" + commentId);
                }
                break;
            }
            case "work_assign_performer":
            case "work_assign_monitor":
            case "work_change_status":{
                if (!ref_data.work) return;
                this.context.router.push('/space/work/' + ref_data.work.id);
                break;
            }
            default:
                break;
        }
    }

    getPostLink(post) {
        if (post.type === "archive") {
            return '/space/wiki/post/' + post.id;
        }
        let groupID = null;
        if (post.view_scope.length === 1 && post.view_scope[0].startsWith("g:")) {
            groupID = post.view_scope[0].substr(2);
            if (groupID === "all") groupID = null;
        }
        if (groupID) return '/space/group/' + groupID + "/post/" + post.id;

        return '/space/post/' + post.id;
    }

    getContent(notification, currentUser) {
        let content = "";
        let post;
        switch (notification.type) {
            case "post_cat_new":
                content = <span>tạo chủ đề mới</span>;
                break;
            case "post_cat_modify":
                content = <span>cập nhật chủ đề</span>;
                break;

            case "post_new":
                post = notification.ref_data.post;
                if (post && post.type === "live") {
                    content = (post.view_scope.length > 0 && post.view_scope[0].startsWith("g:") && post.view_scope[0] !== "g:all") ? <span>đăng trong <GroupDisplayLink group_id={post.view_scope[0].substr(2)} /></span> : <span>đăng bài viết mới</span>;
                } else if (post && post.type === "archive") {
                    content = <span>đăng bài viết mới trong Wiki</span>;
                }
                break;

            case "post_modify":
                post = notification.ref_data.post;
                if (post && post.type === "live") {
                    content = (post.view_scope.length > 0 && post.view_scope[0].startsWith("g:") && post.view_scope[0] !== "g:all") ? <span>cập nhật bài viết trong <GroupDisplayLink group_id={post.view_scope[0].substr(2)} /></span> : <span>cập nhật bài viết</span>;
                } else if (post && post.type === "archive") {
                    content = <span>cập nhật bài viết trong Wiki</span>;
                }
                break;

            case "work_assign_performer":
                content = "giao cho bạn thực hiện công việc";
                break;

            case "work_assign_monitor":
                content = "giao cho bạn kiểm tra công việc";
                break;

            case "work_change_status":
                let work = notification.ref_data && notification.ref_data.work;
                if (work) content = this.notifyContentByWorkStatus(work.new_status, work.old_status);
                break;

            case "comment_new":
                content = this.buildNotifyCommentContent(notification, currentUser);
                break;

            default:
                break;
        }
        return content;
    }

    buildNotifyCommentContent(notification, currentUser) {
        const cm = notification.ref_data.comment;
        let content = "";
        switch (cm.ref_type) {
            case "post":
                if (cm.ref_obj && cm.ref_obj.create_user === currentUser.user_id) {
                    content = "bình luận trong bài viết của bạn";
                } else {
                    content = "bình luận trong bài viết bạn theo dõi";
                }
                break;
            case "work":
                if (cm.ref_obj && cm.ref_obj.resource && cm.ref_obj.resource.performer === currentUser.user_id) {
                    content = "bình luận trong công việc của bạn";
                } else if (cm.ref_obj && cm.ref_obj.resource && cm.ref_obj.resource.monitor === currentUser.user_id){
                    content = "bình luận trong công việc bạn kiểm tra";
                } else{
                    content = "bình luận trong công việc bạn theo dõi";
                }
                break;
        }
        return content;
    }

    notifyContentByWorkStatus(status, oldStatus) {
        switch (status) {
            case "assigned_accept":
                return "đồng ý nhận việc";
            case "assigned_reject":
                return "từ chối nhận việc";
            case "closed":
                return "đóng công việc";
            case "todo":
                return "chuẩn bị làm";
            case "doing":
                return "đang làm";
            case "redo":
                return "không phê duyệt công việc";
            case "done":
                return "thực hiện xong công việc";
            case "completed":
                return "phê duyệt công việc";
        }
        return "";
    }

    render() {
        const { notification, current_user } = this.props;
        let content = this.getContent(notification, current_user);

        return (
            <div onClick={this.handleItemClick} className={"media notification-item " + (notification.is_read ? "notification-item-read" : "notification-item-unread")}>
                <div className="media-left">
                    <a href="#">
                        <img className="media-object img-rounded notification-user-img" src="/static/space/img/avatar.png" alt="user avatar" />
                    </a>
                </div>
                <div className="media-body notification-content">
                    <UserName user={notification.ref_data.issue_user} />
                    <span> {content}</span>
                    {notification.ref_data.preview_content &&
                        <span>: &quot;{notification.ref_data.preview_content}&quot;</span>
                    }
                    <div className="notification-meta">
                        <DateDisplay value={notification.ref_data.issue_time} />
                        {!notification.is_read &&
                            <a className="pull-right" title="Đánh dấu là đã đọc" role="button" onClick={this.handleMarkRead}>Đã đọc</a>
                        }
                    </div>

                    <div className="clearfix"></div>
                </div>
            </div>
        );

    }
}

export default NotificationItem;
