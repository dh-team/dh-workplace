'use strict';

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import SpaceLayout from './containers/Layout';
import NotFoundPage from './containers/NotFoundPage';
import SpaceIndexPage from './containers/IndexPage';
import PostDetailPage from './containers/PostDetailPage';

import SpaceSettingPage from './containers/space/SettingPage';
import GroupsPage from './containers/space/GroupsPage';
import UsersPage from './containers/space/UsersPage';
import FileManagePage from './containers/file/FileManagePage';

import GroupMainPage from './containers/group/MainPage';
import GroupDiscussionPage from './containers/group/DiscussionPage';
import GroupFilesPage from './containers/group/FilesPage';
import GroupUsersPage from './containers/group/UsersPage';
import GroupSettingsPage from './containers/group/SettingsPage';
import GroupPostDetailPage from './containers/group/PostDetailPage';

import WikiMainPage from './containers/wiki/MainPage';
import WikiDefaultPage from './containers/wiki/DefaultPage';
import WikiCategoryPage from './containers/wiki/CategoryPage'; 
import WikiAddPostPage from './containers/wiki/AddPostPage'; 
import WikiEditPostPage from './containers/wiki/EditPostPage'; 
import WikiPostDetailPage from './containers/wiki/PostDetailPage'; 

import ObjectivePage from './containers/objective/ObjectivePage'; 

import WorkMainPage from './containers/work/WorkMainPage'; 
import AllWorksPage from './containers/work/AllWorksPage'; 
import MyWorksPage from './containers/work/MyWorksPage';
import KanbanPage from './containers/work/KanbanPage';
import WorkDetailPage from './containers/work/WorkDetailPage'; 

const routes = (
    <Route path="/space" component={SpaceLayout}>
        <IndexRoute component={SpaceIndexPage}/>
        <Route path="/space/setting" component={SpaceSettingPage}/>
        <Route path="/space/groups" component={GroupsPage}/>
        <Route path="/space/users" component={UsersPage}/>
        <Route path="/space/file" component={FileManagePage}/>
        <Route path="/space/post/:post_id" component={PostDetailPage}/>
        
        <Route path="/space/group/:group_id" component={GroupMainPage}>
            <IndexRoute component={GroupDiscussionPage}/>
            <Route path="/space/group/:group_id/files" component={GroupFilesPage}/>
            <Route path="/space/group/:group_id/members" component={GroupUsersPage}/>
            <Route path="/space/group/:group_id/settings" component={GroupSettingsPage}/>
            <Route path="/space/group/:group_id/post/:post_id" component={GroupPostDetailPage}/>
        </Route>
        
        <Route path="/space/wiki" component={WikiMainPage}>
            <IndexRoute component={WikiDefaultPage}/>
            <Route path="/space/wiki/category/:category_id" component={WikiCategoryPage}/>
            <Route path="/space/wiki/post/new" component={WikiAddPostPage}/>
            <Route path="/space/wiki/post/edit/:post_id" component={WikiEditPostPage}/>
            <Route path="/space/wiki/post/:post_id" component={WikiPostDetailPage}/>
        </Route>
        
        <Route path="/space/objective" component={ObjectivePage}/>
        
         <Route path="/space/work" component={WorkMainPage}>
            <IndexRoute component={MyWorksPage}/>
            <Route path="/space/work/all" component={AllWorksPage}/>
            <Route path="/space/work/kanban" component={KanbanPage}/>
            <Route path="/space/work/:work_id" component={WorkDetailPage}/>
        </Route>
        
        <Route path="/space/*" component={NotFoundPage}/>
    </Route>
);

export default routes;
