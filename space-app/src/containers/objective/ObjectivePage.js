'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';

import ObjectiveTimeline from '../../components/objective/ObjectiveTimeline';
import ContentHeader from '../../components/common/ContentHeader';
import { fetchObjectives } from '../../actions/action-objective';
import { getFetchObjectivesStatus } from '../../selectors/selector-objective';

class ObjectivePage extends React.Component {
    render() {
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/objective", text: "OKR"}
        ]
        return (
            <div >
                <ContentHeader header="OKR" description="Thiết lập và quản lý Mục tiêu theo OKR" breadcrumb={breadcrumb} />

                <section className="content">
                    <ObjectiveTimeline />
                </section>
            </div>

        );
    }
}
const asyncProps = [    
    {
        promise: ({store: {dispatch, getState}, params}) => {            
            var status = getFetchObjectivesStatus(getState());
            if(!status || !status.loaded){
                return dispatch(fetchObjectives());
            }
        }
    }
];

export default asyncConnect(asyncProps)(ObjectivePage);

