import React, { Component, PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { ReduxAsyncConnect } from 'redux-connect';
import routes from '../routes';

class Root extends Component {
    render() {
        return (
            <Provider store={this.props.store}>
                <Router render={(props) => <ReduxAsyncConnect {...props}/>} history={this.props.history} routes={routes} onUpdate={() => window.scrollTo(0, 0)}/>
            </Provider>
        );
    }
    static propTypes = {
        store: PropTypes.object.isRequired,
        history: PropTypes.object.isRequired
    }
}
export default Root
