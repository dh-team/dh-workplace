'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';
import LivePostCreate from '../components/livepost/LivePostCreate';
import LivePostList from '../components/livepost/LivePostList';
import { fetchLivePosts, fetchLivePostsReset } from '../actions/action-live-post';

class IndexPage extends React.Component {
  render() {
    return (
        <div>
            <section className="content">
                <div className="row">
                    <div className="col-md-9">
                       <LivePostCreate type="add" />
                       <LivePostList display_scope />
                    </div>
                    <div className="col-md-3">
                    </div>
                </div>
            </section>
        </div>
    );
  }
}

const asyncProps = [
    {
        promise: ({store: {dispatch, getState}, params}) => {
            dispatch(fetchLivePostsReset());
            return dispatch(fetchLivePosts());
        }
    }
];

export default asyncConnect(asyncProps)(IndexPage)