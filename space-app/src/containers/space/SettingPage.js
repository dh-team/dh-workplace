'use strict';

import React from 'react';
import EditSpaceInfo from '../../components/space-setting/EditSpaceInfo';
import ContentHeader from '../../components/common/ContentHeader';

class SettingPage extends React.Component {
    render() {
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/setting", text: "Space"}
        ]
        return (
            <div >
                <ContentHeader header="Setting" description="Cập nhật thông tin space" breadcrumb={breadcrumb} />

                <section className="content">
                    <EditSpaceInfo />
                </section>
            </div>

        );
    }
}

export default SettingPage;
