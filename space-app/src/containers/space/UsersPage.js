'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';

import ContentHeader from '../../components/common/ContentHeader';
import AddUserForm from '../../components/space-setting/UserInSpace/AddUserForm';
import SpaceUserList from '../../components/space-setting/UserInSpace/SpaceUserList';
import { fetchSpaceUsers } from '../../actions/action-user';
import { getUsersInSpaceStatus } from '../../selectors/selector-user';

class UsersPage extends React.Component {
    render() {
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/users", text: "Users"}
        ]

        return (
            <div >
                <ContentHeader header="Users" description="Nhân sự trong doanh nghiệp" breadcrumb={breadcrumb} />

                <section className="content">
                    <div className="space-user-container">
                        <AddUserForm />
                        <SpaceUserList />
                    </div>
                </section>
            </div>
             
        );
    }
}

const asyncProps = [{
    promise: ({store: {dispatch, getState}, params}) => {
        var status = getUsersInSpaceStatus(getState());
        if(!status || !status.loaded){
            return dispatch(fetchSpaceUsers());   
        }
    }
}];

export default asyncConnect(asyncProps)(UsersPage)