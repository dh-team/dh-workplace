'use strict';

import React from 'react';
import GroupOrgChart from '../../components/space-setting/GroupOrgChart';
import ContentHeader from '../../components/common/ContentHeader';

import { asyncConnect } from 'redux-connect';
import { fetchGroups } from '../../actions/action-group-manage';


class GroupsPage extends React.Component {
    render() {
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/groups", text: "Groups"}
        ]

        return (
            <div>
                <ContentHeader header="Groups" description="Cấu hình nhóm trong doanh nghiệp" breadcrumb={breadcrumb} />

                <section className="content">
                    <GroupOrgChart />
                </section>
            </div>

        );
    }
}

const asyncProps = [{
    promise: ({store: {dispatch, getState}}) => {
        return dispatch(fetchGroups());
    }
}];

export default asyncConnect(asyncProps)(GroupsPage);
