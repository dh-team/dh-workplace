'use strict';

import React from 'react';
import { Link } from 'react-router';

class UsersPage extends React.Component {
    render() {

        return (
            <div className="box box-solid">
                <div className="box-body">
                    <p className="text-muted">Members here</p>
                </div>
            </div>
        );
    }
}

export default UsersPage;

