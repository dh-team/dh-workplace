'use strict';

import React from 'react';
import { Link } from 'react-router';


class SettingsPage extends React.Component {
    render() {

        return (
            <div className="box box-solid">
                <div className="box-body">
                    <p className="text-muted">Settings here</p>
                </div>
            </div>
        );
    }
}

export default SettingsPage;

