'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';
import LivePostCreate from '../../components/livepost/LivePostCreate';
import LivePostList from '../../components/livepost/LivePostList';
import { fetchLivePosts, fetchLivePostsReset } from '../../actions/action-live-post';
import { getGroup } from '../../selectors/selector-group';

class DiscussionPage extends React.Component {
    render() {

        return (
            <div className="row">
                <div className="col-md-9">
                    <div className="group-discussion-container">
                        <LivePostCreate type="add" group_id={this.props.params.group_id} />
                        <LivePostList group_id={this.props.params.group_id} />
                    </div>
                </div>
                <div className="col-md-3">
                    <div className="box box-primary">
                        <div className="box-header with-border">
                            <h3 className="box-title">Thông tin</h3>
                        </div>
    
                        <div className="box-body">
                            <p className="text-muted">{this.props.group.description}</p>
                        </div>
                    </div>
                </div>
            </div>
            
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        group: getGroup(state, ownProps.params.group_id)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

const asyncProps = [
    {
        promise: ({store: {dispatch, getState}, params}) => {
             dispatch(fetchLivePostsReset());
            return dispatch(fetchLivePosts(params.group_id));
        }
    }
];

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(DiscussionPage);

