'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';

import ContentHeader from '../../components/common/ContentHeader';
import { fetchGroupInfo } from '../../actions/action-group-display';
import { getGroup, getFetchGroupStatus } from '../../selectors/selector-group';

class MainPage extends React.Component {

    render() {
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/group", text: "Group"}
        ]
        const {children, group, location} = this.props;
        function activeMenu(menu) {
            if (!location || !location.pathname) return "";
            var subs = location.pathname.split("/").filter(function(item) {
                return item;
            });
            if (!menu) {
                return subs.length === 3 ? "active" : "";
            }
            return (subs.length > 3 && subs[3] === menu) ? "active" : "";
        }
        var des = <span><i className="fa fa-fw fa-lock"></i> Secret Group</span>;
        switch(group.privacy){
            case 'public':
                des = <span><i className="fa fa-fw fa-globe"></i> Public Group</span>;
                break;
            case 'closed':
                des = <span><i className="fa fa-fw fa-lock"></i> Closed Group</span>;
                break;
            case 'secret':
                des = <span><i className="fa fa-fw fa-lock"></i> Secret Group</span>;
                break;
        }
        return (
            <div >
                <ContentHeader header={group.name} description={des} breadcrumb={breadcrumb} />
                <section className="content">
                    <div className="group-header">
                        <div className="nav-tabs-custom">
                            <ul className="nav nav-tabs">
                                <li className={activeMenu()} ><Link to={"/space/group/" + group._id} >Hoạt động</Link></li>
                                <li className={activeMenu("files")} ><Link to={"/space/group/" + group._id + "/files"} >Dữ liệu</Link></li>
                                <li className={activeMenu("members")} ><Link to={"/space/group/" + group._id + "/members"} >Thành viên</Link></li>
                                <li className={activeMenu("settings")} ><Link to={"/space/group/" + group._id + "/settings"} >Thiết lập</Link></li>
                            </ul>
                        </div>
                    </div>
                    
                    {children}
                    
                </section>
            </div>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        group: getGroup(state, ownProps.params.group_id)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {};
};

const asyncProps = [{
    promise: ({store: {dispatch, getState}, params}) => {
        var status = getFetchGroupStatus(getState());
        if(!status || !status.loaded){
            return dispatch(fetchGroupInfo(params.group_id));
        }        
    }
}];

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(MainPage);
