'use strict';

import React from 'react';
import { Link } from 'react-router';
import { connect }from 'react-redux';

import Partition from '../../components/file/Partition';
import FileManage from '../../components/file/FileManage';
import UploadPanel from '../../components/file/UploadPanel';

import { changeStartDir } from '../../actions/action-file';
import { getStartDir } from '../../selectors/selector-file';

class FilesPage extends React.Component {
    componentDidMount(){
        this.props.changeStartDir(null);
    }
    render() {
        const {start_dir} = this.props;
        return (
            <div className="row">
                <div className="col-md-2">
                    <Partition title="Chia sẻ trong group" partition={"g:" + this.props.params.group_id} />
                </div>
                <div className="col-md-10">
                     <FileManage start_dir={start_dir}/>
                </div>
                <UploadPanel />
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        start_dir: getStartDir(state),
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeStartDir: (params) => dispatch(changeStartDir(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FilesPage);



