'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';

import CategoryInfo from '../../components/wiki-category/CategoryInfo';
import PostList from '../../components/wiki-post/PostList';

import { setActiveCategory } from '../../actions/action-wiki-category';
import { fetchPosts } from '../../actions/action-wiki-post';

class CategoryPage extends React.Component {

    render() {
        return (
            <div>
                <CategoryInfo />
                <PostList />
            </div>
        );
    }
}

const asyncProps = [
    // {
    //     promise: ({store: {dispatch, getState}, params}) => {
    //         return dispatch(fetchPostCategory(params.category_id));
    //     }    
    // },
    {
        promise: ({store: {dispatch, getState}, params}) => {
            dispatch(setActiveCategory(params.category_id));
            return dispatch(fetchPosts(params.category_id));
        }
    }
];


const mapStateToProps = (state) => {
    return {};
};

const mapDispatchToProps = (dispatch) => {
    return {
        setActiveCategory: (data) => dispatch(setActiveCategory(data)),
    };
};
export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(CategoryPage)
