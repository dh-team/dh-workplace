'use strict';

import React from 'react';
import { Link } from 'react-router';
import PostForm from '../../components/wiki-post/PostForm';

export default class EditPostPage extends React.Component {

    render() {
        return (
            <div>
                <PostForm type="edit" />
            </div>
        );
    }
}
