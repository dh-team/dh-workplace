'use strict';

import React from 'react';
import { connect } from 'react-redux';
import { setActiveCategory } from '../../actions/action-wiki-category';

class DefaultPage extends React.Component {
    componentDidMount() {
        this.props.setActiveCategory(null);
    }

    render() {
        return (
            <div></div>
        );
    }
}

const mapStateToProps = (state) => {
    return {};
}

const mapDispatchToProps = (dispatch) => {
    return {
        setActiveCategory: (params) => {
            dispatch(setActiveCategory(params));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
