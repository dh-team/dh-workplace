'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';
import PostDetail from '../../components/wiki-post/PostDetail';
import { setActivePost, fetchPost } from '../../actions/action-wiki-post';

class PostDetailPage extends React.Component {
    componentDidUpdate(){
        console.log("PostDetailPage did update");
    }
    render() {
        
        return (
            <div>
                <PostDetail />
            </div>
        );
    }
}

const asyncProps = [
    {
        promise: ({store: {dispatch, getState}, params}) => {
            dispatch(setActivePost(params.post_id));
            return dispatch(fetchPost(params.post_id));
        }    
    }
];

export default asyncConnect(asyncProps)(PostDetailPage)

