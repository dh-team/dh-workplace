'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';

import ContentHeader from '../../components/common/ContentHeader';
import CategoryTree from '../../components/wiki-category/CategoryTree';
import { fetchPostCategories } from '../../actions/action-wiki-category';
import { fetchTags } from '../../actions/action-tag';
import { getFetchCategoriesStatus } from '../../selectors/selector-wiki-category';
import { getFetchTagsStatus } from '../../selectors/selector-tag';



class MainPage extends React.Component {

    render() {
        const breadcrumb = [
            { link: "/space/", text: "Trang chủ" },
            { link: "/space/wiki", text: "Wiki" }
        ]

        return (
            <div>
                <ContentHeader header="Wiki" description="Tri thức doanh nghiệp" breadcrumb={breadcrumb} />
                <section className="content">
                    <div className="row">
                        <div className="col-md-3">
                            <CategoryTree />
                        </div>
                        <div className="col-md-9">
                            {this.props.children}
                        </div>
                    </div>

                </section>
            </div>
        );
    }
}

const asyncProps = [
    {
        promise: ({ store: { dispatch, getState }, params }) => {
            var status = getFetchCategoriesStatus(getState());
            if (!status || !status.loaded) {
                return dispatch(fetchPostCategories());
            }
        }
    },
    {
        promise: ({ store: { dispatch, getState }, params }) => {
            var status = getFetchTagsStatus(getState(), "post");
            if (!status || !status.loaded) {
                return dispatch(fetchTags("post"));
            }
        }
    }
];

// export default MainPage;
export default asyncConnect(asyncProps)(MainPage)
