'use strict';

import React from 'react';
import { connect }from 'react-redux';

import ContentHeader from '../../components/common/ContentHeader';
import Partition from '../../components/file/Partition';
import FileManage from '../../components/file/FileManage';
import UploadPanel from '../../components/file/UploadPanel';

import { changeStartDir } from '../../actions/action-file';
import { getStartDir } from '../../selectors/selector-file';
import { getCurrentUser } from '../../selectors/selector-user';

class FileManagePage extends React.Component {
    componentDidMount(){
        this.props.changeStartDir(null);
    }
    render() {
        const {current_user, start_dir} = this.props;
        const breadcrumb = [
            {link: "/space/", text: "Trang chủ"},
            {link: "/space/file", text: "File"}
        ];
        return (
            <div >
                <ContentHeader header="File" description="Quản lý file" breadcrumb={breadcrumb} />

                <section className="content">
                    <div className="row">
                        <div className="col-md-2">
                            <Partition title="Cá nhân" partition={"u:" + current_user.user_id} />
                            <Partition title="Space" partition="g:all" />
                        </div>
                        <div className="col-md-10">
                            <FileManage start_dir={start_dir}/>
                        </div>
                    </div>
                </section>

                <UploadPanel />
            </div>

        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {
        start_dir: getStartDir(state),
        current_user: getCurrentUser(state)
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        changeStartDir: (params) => dispatch(changeStartDir(params))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(FileManagePage);

