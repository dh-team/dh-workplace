'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';
import LivePostDetail from '../components/livepost/LivePostDetail';
import { fetchLivePost } from '../actions/action-live-post';

class PostDetailPage extends React.Component {
    componentDidUpdate(){
    }
    render() {
        
        return (
            <div>
                <section className="content">
                    <div className="row">
                        <div className="col-md-9">
                            <LivePostDetail post_id={this.props.params.post_id}/>
                        </div>
                        <div className="col-md-3">
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}

const asyncProps = [
    {
        promise: ({store: {dispatch, getState}, params}) => {
            return dispatch(fetchLivePost(params.post_id));
        }    
    }
];

export default asyncConnect(asyncProps)(PostDetailPage);

