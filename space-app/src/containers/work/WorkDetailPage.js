'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';

import WorkDetail from '../../components/work/WorkDetail';
import { fetchSubWorks, fetchParentWorks, fetchWork } from '../../actions/action-work';
// import { getFetchWorksStatus } from '../../selectors/selector-work';

class WorkDetailPage extends React.Component {
    render() {
        return (
            <div >
                <WorkDetail work_id={this.props.params.work_id} />
            </div>

        );
    }
}
const asyncProps = [    
    {
        promise: ({store: {dispatch, getState}, params}) => {
            return dispatch(fetchWork(params.work_id));
        }
    },
    // {
    //     promise: ({store: {dispatch, getState}, params}) => {
    //         return dispatch(fetchSubWorks(params.work_id));
    //     }
    // },
    // {
    //     promise: ({store: {dispatch, getState}, params}) => {
    //         return dispatch(fetchParentWorks(params.work_id));
    //     }
    // }
];

export default asyncConnect(asyncProps)(WorkDetailPage);

