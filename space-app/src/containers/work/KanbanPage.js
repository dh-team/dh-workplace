'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';

import KanbanTable from '../../components/kanban/KanbanTable';
import { fetchPerformWorks } from '../../actions/action-work';
import { getFetchPerformWorksStatus } from '../../selectors/selector-work';

class KanbanPage extends React.Component {
    render() {
        return (
            <div>
                <KanbanTable />
            </div>
        );
    }
}

const asyncProps = [    
    {
        promise: ({store: {dispatch, getState}, params}) => {            
            var status = getFetchPerformWorksStatus(getState());
            if(!status || !status.loaded){
                return dispatch(fetchPerformWorks());
            }
        }
    }
];

export default asyncConnect(asyncProps)(KanbanPage);

