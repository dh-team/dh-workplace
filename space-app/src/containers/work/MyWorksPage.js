'use strict';

import React from 'react';
import { asyncConnect } from 'redux-connect';

import PerformWorks from '../../components/work/PerformWorks';
import MonitorWorks from '../../components/work/MonitorWorks';
import FollowedWorks from '../../components/work/FollowedWorks';
import { fetchPerformWorks, fetchMonitorWorks, fetchFollowedWorks } from '../../actions/action-work';
import { getFetchPerformWorksStatus, getFetchMonitorWorksStatus } from '../../selectors/selector-work';

class MyWorksPage extends React.Component {
    render() {
        return (
            <div >
                <PerformWorks />
                <MonitorWorks />
                <FollowedWorks />
            </div>

        );
    }
}

const asyncProps = [    
    {
        promise: ({store: {dispatch, getState}, params}) => { 
            return dispatch(fetchPerformWorks());
            // var status = getFetchPerformWorksStatus(getState());
            // if(!status || !status.loaded){
            //     return dispatch(fetchPerformWorks());
            // }
        }
    },
    {
        promise: ({store: {dispatch, getState}, params}) => {            
            return dispatch(fetchMonitorWorks());            
        }
    },
    {
        promise: ({store: {dispatch, getState}, params}) => {            
            return dispatch(fetchFollowedWorks());            
        }
    }
];

export default asyncConnect(asyncProps)(MyWorksPage);

