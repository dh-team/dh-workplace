'use strict';

import React from 'react';
// import { asyncConnect } from 'redux-connect';

import AllWorks from '../../components/work/AllWorks';
// import { fetchWorks } from '../../actions/action-work';
// import { getFetchWorksStatus } from '../../selectors/selector-work';

class AllWorksPage extends React.Component {
    render() {
        return (
            <div >
                <AllWorks />
            </div>

        );
    }
}

// const asyncProps = [    
//     {
//         promise: ({store: {dispatch, getState}, params}) => {            
//             var status = getFetchWorksStatus(getState());
//             if(!status || !status.loaded){
//                 return dispatch(fetchWorks());
//             }
//         }
//     }
// ];

// export default asyncConnect(asyncProps)(AllWorksPage);

export default AllWorksPage;
 