'use strict';

import React from 'react';
import { Link } from 'react-router';
import { asyncConnect } from 'redux-connect';

import ContentHeader from '../../components/common/ContentHeader';

import { fetchTags } from '../../actions/action-tag';
import { getFetchTagsStatus } from '../../selectors/selector-tag';

class WorkMainPage extends React.Component {

    render() {
        const breadcrumb = [
            { link: "/space/", text: "Trang chủ" },
            { link: "/space/Work", text: "Công việc" }
        ];

        return (
            <div>
                <ContentHeader header="Công việc" description="Quản lý công việc" breadcrumb={breadcrumb} />
                <section className="content">
                    {this.props.children}
                </section>
            </div>
        );
    }
}

const asyncProps = [
    {
        promise: ({ store: { dispatch, getState }, params }) => {
            var status = getFetchTagsStatus(getState(), "work_category");
            if (!status || !status.loaded) {
                return dispatch(fetchTags("work_category"));
            }
        }
    },
    {
        promise: ({ store: { dispatch, getState }, params }) => {
            var status = getFetchTagsStatus(getState(), "work_knowledge");
            if (!status || !status.loaded) {
                return dispatch(fetchTags("work_knowledge"));
            }
        }
    }
];

export default asyncConnect(asyncProps)(WorkMainPage);