import React, { Component, PropTypes } from 'react';
// import { connect } from 'react-redux';
import { asyncConnect } from 'redux-connect';
import ToastMsg from '../components/common/ToastMsg';
import DialogMsg from '../components/common/DialogMsg';
import Header from '../components/common/Header';
import SidebarLeft from '../components/common/SidebarLeft';
import SidebarRight from '../components/common/SidebarRight';
import Footer from '../components/common/Footer';

import { fetchSpaceInfo, fetchUserProfile } from '../actions/action-space';
import { fetchGroupsOfUser } from '../actions/action-group-display';
import { countNotification } from '../actions/action-notification';
import { fetchSpaceUsers } from '../actions/action-user';

import { getAllGroups, getFetchGroupsStatus } from '../selectors/selector-group';
import { getSpaceInfo, getFetchSpaceInfoStatus } from '../selectors/selector-space';
import { getCurrentUser, getFetchUserProfileStatus, getUsersInSpaceStatus } from '../selectors/selector-user';


class Layout extends Component {
    render() {
        const {children, space_info, user_profile, groups} = this.props;
        return (
                <div className="wrapper">
                    <ToastMsg />
                    <DialogMsg />
                    <Header />
                    <SidebarLeft space_info={space_info} user_profile={user_profile} groups={groups} />
                    <div className="content-wrapper">
                        {children}
                    </div>
                    <Footer />
                    <SidebarRight />
                    <div className="control-sidebar-bg"></div>
                </div>
        );
    }
}

const mapStateToProps = (state) => ({
    // user: state.user,
    space_info: getSpaceInfo(state),
    user_profile: getCurrentUser(state),
    groups: getAllGroups(state)
});

const mapDispatchToProps = (dispatch) => {
    return {}
}

const asyncProps = [
    {
        promise: ({store: {dispatch, getState}}) => {            
            var status = getFetchSpaceInfoStatus(getState());
            if(!status || !status.loaded){
                return dispatch(fetchSpaceInfo());    
            }
        }
    },
    {
        promise: ({store: {dispatch, getState}}) => {
            var status = getFetchUserProfileStatus(getState());
            if(!status || !status.loaded){
               return dispatch(fetchUserProfile());   
            }
        }
    },
    {
        promise: ({store: {dispatch, getState}}) => {
            var status = getFetchGroupsStatus(getState());
            if(!status || !status.loaded){
                return dispatch(fetchGroupsOfUser());   
            }
        }
    },
    {
        promise: ({store: {dispatch, getState}}) => {
            var status = getUsersInSpaceStatus(getState());
            if(!status || !status.loaded){
                return dispatch(fetchSpaceUsers());   
            }
        }
    },
    {
        promise: ({store: {dispatch, getState}}) => {
             return dispatch(countNotification());
        }
    }
];

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(Layout);
