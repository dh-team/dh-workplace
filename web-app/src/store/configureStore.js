import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
// import promise from 'redux-promise';
import clientMiddleware from './clientMiddleware';
import rootReducer from '../reducers';

const configureStore = (preloadedState, client) => {
    const middlewares = client? [clientMiddleware(client), thunkMiddleware] : [thunkMiddleware];
    const enhancer = compose(
        applyMiddleware(...middlewares)
    );
    
    const store = createStore(
        rootReducer,
        preloadedState,
        enhancer
        // applyMiddleware(promise)
    );
    
    // const createStoreWithMiddleware = applyMiddleware(thunk)(createStore);
    // const store = createStoreWithMiddleware(rootReducer,preloadedState);

    console.log(store.getState());
    let unsubscribe = store.subscribe(() =>
        console.log(store.getState())
    )

    if (module.hot) {
        // Enable Webpack hot module replacement for reducers
        module.hot.accept('../reducers', () => {
            const nextRootReducer = require('../reducers').default
            store.replaceReducer(nextRootReducer)
        })
    }

    return store;
}

export default configureStore
