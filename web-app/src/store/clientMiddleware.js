export default function clientMiddleware(client) {
    return ({dispatch, getState}) => {
        return next => action => {
            if(typeof action !== 'function' && action.promise){
                const { promise, type, ...rest } = action;
                if(type){
                    next({type, ...rest});
                }
                return promise(client, dispatch, getState);
            }
            return next(action);
        };
    };
}