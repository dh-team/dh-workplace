'use strict';

import React from 'react'
import { Route, IndexRoute } from 'react-router'

import IndexPage from './components/IndexPage';
import NotFoundPage from './components/NotFoundPage';
// spaces manage
import SpacesManageLayout from './containers/spaces-manage/Layout';
import CreateSpacePage from './containers/spaces-manage/CreateSpacePage';
import SpaceListPage from './containers/spaces-manage/SpaceListPage';
import SpaceEditPage from './containers/spaces-manage/SpaceEditPage';

const routes = (
    <Route path="/ws" component={SpacesManageLayout}>
        <IndexRoute component={IndexPage}/>
        <Route path="/ws/space/create" component={CreateSpacePage}/>
        <Route path="/ws/space/list" component={SpaceListPage}/>
        <Route path="/ws/space/edit/:space_id" component={SpaceEditPage}/>
        <Route path="/ws/*" component={NotFoundPage}/>
    </Route>
);

export default routes;
