import React, {Component, PropTypes} from 'react';
import { connect } from 'react-redux';
import { asyncConnect } from 'redux-connect';
import { fetchSpaces } from '../../actions/action-space';
import SpaceList from '../../components/spaces-manage/SpaceList';


class SpaceListPage extends Component {
    
    componentDidMount() {
        // this.props.fetchSpaces();    
    }

    render() {
        return (
            <SpaceList spacesList={this.props.spacesList} />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        spacesList: state.spaces.spacesList
    };
}

const mapDispatchToProps = (dispatch) => {
    return {
        fetchSpaces: () => dispatch(fetchSpaces())
    }
}

const asyncProps = [{
    promise: ({store: {dispatch, getState}}) => {
        return dispatch(fetchSpaces());
    }
}];

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(SpaceListPage)