import React, { Component, PropTypes }from 'react';
import { connect }from 'react-redux';
// import { asyncConnect as connect } from 'redux-connect';

import CreateSpaceForm from '../../components/spaces-manage/CreateSpaceForm'
import { resetNewSpace } from '../../actions/action-space';

const mapStateToProps = (state) => ({
    newSpace: state.spaces.newSpace
});

const mapDispatchToProps = (dispatch) => {
    return {
        resetNewSpace: () => dispatch(resetNewSpace())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CreateSpaceForm)
