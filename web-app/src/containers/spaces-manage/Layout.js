import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
// import { asyncConnect as connect } from 'redux-connect';

import { resetMsg } from '../../actions/action-msg';
import Menu from '../../components/spaces-manage/common/Menu'
import GlobalMsg from '../../components/GlobalMsg'

class Layout extends Component {
    render() {
        return (
            <div>
                <GlobalMsg msg={this.props.global_msg} resetMsg={this.props.resetMsg} />
                <Menu user={this.props.user} />
                <div className="container page-content">
                    {this.props.children}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    user: state.user,
    global_msg: state.global_msg
});

const mapDispatchToProps = ({
    resetMsg: resetMsg
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout)
