import React, { Component, PropTypes }from 'react';
// import { connect }from 'react-redux';
import { asyncConnect } from 'redux-connect';

import AddUserForm from '../../components/spaces-manage/AddUserForm'
import SpaceUserList from '../../components/spaces-manage/SpaceUserList'
import { fetchSpaceUsers } from '../../actions/action-space';

class SpaceEditPage extends Component {
    componentWillMount() {
        // this.props.fetchSpaceUsers();
    }
    
    render() {
        return (
            <div className="space-user-container">
                <AddUserForm spaceID={this.props.params.space_id} />
                <SpaceUserList users={this.props.activeSpace.users_in_space} />
            </div>    
        );
    }
}

const mapStateToProps = (state, ownProps) => {
  return { 
    activeSpace: state.spaces.activeSpace
  };
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        fetchSpaceUsers: () => {
            dispatch(fetchSpaceUsers(ownProps.params.space_id));
        }
    }
}

const asyncProps = [{
    promise: ({store: {dispatch}, params}) => {
        // console.log("asyncProps:" + JSON.stringify(params));
        return dispatch(fetchSpaceUsers(params.space_id));
    }
}];

export default asyncConnect(asyncProps, mapStateToProps, mapDispatchToProps)(SpaceEditPage)
