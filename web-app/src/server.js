'use strict';

var path = require("path");
var https = require('https');
var http = require('http');
var fs = require('fs');

import Express from 'express';
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var login = require("connect-ensure-login");

import React from 'react';
import { Provider } from 'react-redux';
import { renderToString } from 'react-dom/server';
import { match, RouterContext } from 'react-router';
import { ReduxAsyncConnect, loadOnServer } from 'redux-connect';
import routes from './routes';
import NotFoundPage from './components/NotFoundPage';
import configureStore from './store/configureStore'

var config = require("./config");
var auth = require('./auth/auth');
import axios from 'axios';

//Pull in the mongo store if we're configured to use it
//else pull in MemoryStore for the session configuration
var sessionStorage;
if (config.session.type === 'MongoStore') {
    var MongoStore = require('connect-mongo')({
        session: expressSession
    });
    console.log('Using MongoDB for the Session');
    sessionStorage = new MongoStore({
        db: config.session.dbName
    });
}
else if (config.session.type === 'MemoryStore') {
    var MemoryStore = expressSession.MemoryStore;
    console.log('Using MemoryStore for the Session');
    sessionStorage = new MemoryStore();
}
else {
    //We have no idea here
    throw new Error("Within config/index.js the session.type is unknown: " + config.session.type);
}
// initialize the server and configure support for ejs templates
const app = new Express();

app.set('view engine', 'ejs');
app.set('views', path.join(__dirname, 'views'));

app.use(cookieParser());
//Session Configuration
app.use(expressSession({
    saveUninitialized: true,
    resave: true,
    secret: config.session.secret,
    store: sessionStorage,
    key: "webapp.sid",
    cookie: {
        maxAge: config.session.maxAge
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(passport.initialize());
app.use(passport.session());

// static resources are provided by proxy server
app.use("/static", Express.static(path.join(__dirname, 'static')));

app.get("/ws/auth", passport.authenticate('oauth2'));
// handle login
app.get("/ws/auth/handle", passport.authenticate('oauth2'), function(req, res) {
    // save token to cookie
    console.log("authInfo: " + JSON.stringify(req.authInfo));
    res.cookie(config.cookies.accessTokenKey, req.authInfo.accessToken, {
        maxAge: config.cookies.maxAge,
        httpOnly: false // cookie can be accessed in javascript
    });
    res.redirect(req.session.returnTo || "/ws");
});
app.get('/ws/logout', function(req, res) {
    // logout
    req.logout();
    res.redirect("/as/logout?redirect=/ws");
});

// app.get('/ws', function(req, res) {
//     res.redirect('/ws/');
// });
app.get('/', function(req, res) {
    res.redirect('/ws/');
});

// universal routing and rendering
var handleSite = [
    login.ensureLoggedIn({
        redirectTo: "/ws/auth"
    }),
    (req, res) => {
        // console.log("url: " + req.url);
        var template = "index";
        // console.log("Request cookies: " + JSON.stringify(req.cookies));
        var client = axios.create({
            baseURL: config.proxyServer.url,
            headers: {'Authorization': "Bearer " + req.cookies.access_token}
        });
        const store = configureStore({user: req.user}, client);
        match({
                routes,
                location: req.url
            },
            (err, redirectLocation, renderProps) => {

                // in case of error display the error message
                if (err) {
                    return res.status(500).send(err.message);
                }

                // in case of redirect propagate the redirect to the browser
                if (redirectLocation) {
                    return res.redirect(302, redirectLocation.pathname + redirectLocation.search);
                }

                // generate the React markup for the current route
                let markup;

                if (renderProps) {
                    // if the current route matched we have renderProps
                    // console.log("begin loadOnServer");
                    // async load and render
                    loadOnServer({ ...renderProps, store}).then(() => {
                        // console.log("callback loadOnServer");
                        // console.log(store.getState());
                        markup = renderToString(
                            <Provider store={store} key="provider">
                                <ReduxAsyncConnect {...renderProps} />
                            </Provider>);
                        // console.log("render markup ok");
                        // render the index template with the embedded React markup
                        return res.render(template, {
                            markup: markup,
                            initState: JSON.stringify(store.getState()).replace(/</g, '\\x3c')
                        });
                    });
                }
                else {
                    // otherwise we can render a 404 page
                    markup = renderToString(<NotFoundPage/>);
                    res.status(404);
                    // render the index template with the embedded React markup
                    return res.render(template, {
                        markup,
                        initState: JSON.stringify(store.getState()).replace(/</g, '\\x3c')
                    });
                }
            }
        );
    }
];

app.get('/ws', handleSite);
app.get('/ws/*', handleSite);

// Create HTTP | HTTPS server
if (config.webServer.type === 'http') {
    http.createServer(app).listen(config.webServer.port, "0.0.0.0");
}
else {
    var options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.webServer.port, "0.0.0.0");
}
console.log("Web Server started on port " + config.webServer.port);
