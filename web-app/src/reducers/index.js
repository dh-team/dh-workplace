import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux';
import { reducer as reduxFormReducer } from 'redux-form';
import { reducer as reduxAsyncConnect} from 'redux-connect';
import spaces from './reducer-space';
import global_msg from './reducer-msg';

const user = (state = null, action) => {
  return state
}

const rootReducer = combineReducers({
    form: reduxFormReducer, // mounted under "form"
    routing: routerReducer,
    reduxAsyncConnect,
    user: user,
    spaces: spaces,
    global_msg: global_msg
});

export default rootReducer;
