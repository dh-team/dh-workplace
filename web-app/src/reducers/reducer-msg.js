import {
    SET_MSG,
    RESET_MSG
}
from '../actions/action-msg';

const INITIAL_STATE = {
    type: null, // error, warning, info, success
    content: null
};

export default function(state = INITIAL_STATE, action) {
    switch (action.type) {
        case SET_MSG:
            return {...state,
                type: action.msg.type,
                content: action.msg.content,
            }

        case RESET_MSG:
            return {...state,
                type: null,
                content: null
            }
        default:
            return state;
    }
}
