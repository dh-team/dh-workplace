import { 
    CREATE_SPACE, CREATE_SPACE_SUCCESS, CREATE_SPACE_FAILURE, RESET_NEW_SPACE,
    FETCH_SPACES, FETCH_SPACES_SUCCESS, FETCH_SPACES_FAILURE, RESET_SPACES,
    // FETCH_SPACE, FETCH_SPACE_SUCCESS, FETCH_SPACE_FAILURE, RESET_SPACE,
    FETCH_SPACE_USERS, FETCH_SPACE_USERS_SUCCESS, FETCH_SPACE_USERS_FAILURE, RESET_SPACE_USERS,
    ADD_SPACE_USER, ADD_SPACE_USER_SUCCESS, ADD_SPACE_USER_FAILURE,
    USER_JOIN_SPACE, USER_JOIN_SPACE_SUCCESS, USER_JOIN_SPACE_FAILURE,
    USER_UNJOIN_SPACE, USER_UNJOIN_SPACE_SUCCESS, USER_UNJOIN_SPACE_FAILURE,
} from '../actions/action-space';

const INITIAL_STATE = {
    spacesList: {
        spaces: [],
        error: null,
        loading: false,
        loaded: false // for server rendering
    },
    newSpace: {
        space: null,
        error: null,
        loading: false
    },
    activeSpace: {
        space: null, // space info
        users_in_space: [], // users in space
        error: null,
        loading: false
    },
};
export default function(state = INITIAL_STATE, action) {
    let error;
    switch (action.type) {
        case CREATE_SPACE:
            return {...state,
                newSpace: {...state.newSpace,
                    loading: true
                }
            }
        case CREATE_SPACE_SUCCESS:
            return {...state,
                newSpace: {
                    space: action.payload,
                    error: null,
                    loading: false
                }
            }
        case CREATE_SPACE_FAILURE:
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                newSpace: {
                    space: null,
                    error: error,
                    loading: false
                }
            }
        case RESET_NEW_SPACE:
            return {...state,
                newSpace: {
                    space: null,
                    error: null,
                    loading: false
                }
            }
            //////////////////
        case FETCH_SPACES: // start fetching spaces and set loading = true
            return {...state,
                spacesList: {
                    spaces: [],
                    error: null,
                    loading: true,
                    loaded: false
                }
            };
        case FETCH_SPACES_SUCCESS: // return list of spaces and make loading = false
            return {...state,
                spacesList: {
                    spaces: [...action.payload],
                    error: null,
                    loading: false,
                    loaded: true
                }
            };
        case FETCH_SPACES_FAILURE: // return error and make loading = false
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                spacesList: {
                    spaces: [],
                    error: error,
                    loading: false,
                    loaded: false
                }
            };
        case RESET_SPACES: // reset to initial state
            return {...state,
                spacesList: {
                    spaces: [],
                    error: null,
                    loading: false,
                    loaded: false
                }
            };
            //////////////////
        case FETCH_SPACE_USERS: // start fetching spaces and set loading = true
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    users_in_space: [],
                    error: null,
                    loading: true
                }
            };
        case FETCH_SPACE_USERS_SUCCESS: // return list of spaces and make loading = false
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    users_in_space: action.payload,
                    error: null,
                    loading: false
                }
            };
        case FETCH_SPACE_USERS_FAILURE: // return error and make loading = false
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    users_in_space: [],
                    error: error,
                    loading: false
                }
            };
        case RESET_SPACE_USERS: // reset to initial state
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    users_in_space: [],
                    error: null,
                    loading: false
                }
            };
            //////////////////
        case ADD_SPACE_USER:
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    error: null,
                    loading: true
                }
            };
        case ADD_SPACE_USER_SUCCESS:
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    users_in_space: [action.payload, ...state.activeSpace.users_in_space],
                    error: null,
                    loading: false
                }
            };
        case ADD_SPACE_USER_FAILURE:
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                activeSpace: {
                    ...state.activeSpace,
                    error: error,
                    loading: false
                }
            };
            //////////////////
        case USER_JOIN_SPACE:
            return {...state,
                spacesList: {
                    ...state.spacesList,
                    error: null,
                    loading: true
                }
            };
        case USER_JOIN_SPACE_SUCCESS:
            var spaces = [...state.spacesList.spaces];
            
            for(var i = 0; i < spaces.length; ++i){
                if(spaces[i].space._id === action.payload.space_id){
                    spaces[i].is_active = true;
                    break;
                }
            }
            return {...state,
                spacesList: {
                    spaces: state.spacesList.spaces.map(item => {
                        if (item.space._id !== action.payload.space_id) {
                            return item;
                        }
                        return {
                            ...item,
                            is_active: true
                        }
                    }),
                    error: null,
                    loading: false
                }
            };
        case USER_JOIN_SPACE_FAILURE:
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                spacesList: {
                    ...state.spacesList,
                    error: error,
                    loading: false
                }
            };
        //////////////////
        case USER_UNJOIN_SPACE:
            return {...state,
                spacesList: {
                    ...state.spacesList,
                    error: null,
                    loading: true
                }
            };
        case USER_UNJOIN_SPACE_SUCCESS:
            return {...state,
                spacesList: {
                    spaces: state.spacesList.spaces.filter(function(item){
                        return item.space._id != action.payload.space_id;
                    }),
                    error: null,
                    loading: false
                }
            };
        case USER_UNJOIN_SPACE_FAILURE:
            error = action.payload || {
                message: action.payload.message
            }; //2nd one is network or server down errors
            return {...state,
                spacesList: {
                    ...state.spacesList,
                    error: error,
                    loading: false
                }
            };    
        default:
            return state;
    }
}
