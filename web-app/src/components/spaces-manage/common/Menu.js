'use strict';

import React, { Component, PropTypes } from 'react'
import { Link } from 'react-router'
import UserMenu from './UserMenu'

export default class Menu extends Component {
    static propTypes = {
        user: PropTypes.object.isRequired,
    }

    render() {
        const user = this.props.user;

        return (
            <nav className="navbar navbar-inverse navbar-fixed-top">
            <div className="container">
                <div className="navbar-header">
                    <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span className="sr-only">Toggle navigation</span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                        <span className="icon-bar"></span>
                    </button>
                    <Link to="/ws/" className="navbar-brand" href="/">Upspace</Link>
                </div>
                <div id="navbar" className="collapse navbar-collapse">
                    <ul className="nav navbar-nav">
                    </ul>
                    {!user && 
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to="/ws/login">Đăng nhập</Link></li>
                            <li><Link to="/ws/register">Đăng ký</Link></li>
                        </ul>
                    }
                    
                    {user && 
                        <ul className="nav navbar-nav navbar-right">
                            <UserMenu user={user} />
                        </ul>
                    }
                </div>
            </div>
        </nav>
        );
    }
}
