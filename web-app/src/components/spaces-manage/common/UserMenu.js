'use strict';

import React, {Component, PropTypes} from 'react';
import { Link } from 'react-router';
class UserMenu extends Component {

    render() {
        const user = this.props.user;

        return (
            <li className="dropdown">
                <a href="#" className="dropdown-toggle" ref="dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {user.display_name} <span className="caret"></span>
                </a>
                <ul className="dropdown-menu" role="menu">
                    <li><Link to="/ws/space/list"><i className="glyphicon glyphicon-briefcase"></i> Spaces</Link></li>
                    <li><Link to="/ws/edit-profile"><i className="glyphicon glyphicon-edit"></i> Sửa profile</Link></li>
                    <li role="separator" className="divider"></li>
                    <li><a href="/ws/logout"><i className="glyphicon glyphicon-log-out"></i> Đăng xuất</a></li>
                </ul>
            </li>
        );
    }

    static propTypes = {
        user: PropTypes.shape({
            display_name: PropTypes.string.isRequired
        }).isRequired
    }

}

export default UserMenu;
