'use strict';

import React, {Component, PropTypes} from 'react';
import { reduxForm, Field, SubmissionError, reset } from 'redux-form';
import FormInput from '../form/Input';
import valid from '../../utils/validate';
import { addSpaceUser } from '../../actions/action-space';

const submit = function(values, dispatch) {
    // console.log("add user to space: " + JSON.stringify(values, null, 4));
    return dispatch(addSpaceUser(values));
    // .then(result => {
    //     dispatch(reset("addUserForm"));
    // });
}

//client side validation
function validate(values) {
    const errors = {};
    if (!valid.isEmail(values.user)) {
        errors.user = 'Email không đúng định dạng';
    }
    return errors;
}

class AddUserForm extends Component {
    componentDidMount() {
        // console.log("AddUserForm: spaceID " + this.props.spaceID);
        this.props.initialize({
            "space_id": this.props.spaceID
        });
    }
    
    render() {
        const { error, handleSubmit, pristine, reset, submitting } = this.props;
        
        return (
            <div role="alert" className="add-user-form alert alert-info"> 
                <form onSubmit={handleSubmit(submit)} className="row form-inline">
                    <Field component="input" type="hidden" name="space_id" />
                    <Field component={FormInput} name="user" type="text" label="Email người dùng" placeholder="Email người dùng" labelCls="sr-only" wrapCls="col-lg-4" />
                    <div className="form-group col-lg-4">
                        <button type="submit" disabled={submitting} className="btn btn-primary">Thêm người dùng</button>
                    </div>
                </form>
            </div>
        );
    }
}

export default reduxForm({
  form: 'addUserForm',  // a unique identifier for this form
  validate
})(AddUserForm)
