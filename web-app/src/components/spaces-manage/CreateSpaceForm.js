'use strict';

import React, {Component, PropTypes} from 'react';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import FormInput from '../form/Input';
import FormSelect from '../form/Select';
import valid from '../../utils/validate';
import { createSpace } from '../../actions/action-space';

const submit = function(values, dispatch) {
    // console.log("create space: " + JSON.stringify(values, null, 4));
    return dispatch(createSpace(values));
}

//client side validation
function validate(values) {
    const errors = {};

    if (!valid.hasLength(values.name, 10, 250)) {
        errors.name = 'Trường không được để trống, độ dài từ 10-250 ký tự';
    }

    if (!valid.hasLength(values.short_name, 5, 250)) {
        errors.short_name = 'Trường không được để trống, độ dài từ 5-250 ký tự';
    }

    if (!valid.hasLength(values.phone, 5, 50)) {
        errors.phone = 'Trường không được để trống, độ dài từ 5-50 ký tự';
    }

    if (!valid.hasLength(values.address, 10, 250)) {
        errors.address = 'Trường không được để trống, độ dài từ 10-250 ký tự';
    }

    if (!valid.required(values.size)) {
        errors.size = 'Trường không được để trống';
    }

    if (!valid.required(values.type)) {
        errors.type = 'Trường không được để trống';
    }

    if (!values.uname || !valid.hasLength(values.uname, 3, 50)) {
        errors.uname = 'Trường không được để trống, độ dài từ 3-50 ký tự';
    }
    if (!valid.isID(values.uname, 3, 50)) {
        errors.uname = 'Trường không đúng định dạng, chỉ cho phép chữ, số , dấu - và _ ';
    }

    if (!values.username || !valid.hasLength(values.username, 1, 50)) {
        errors.username = 'Trường không được để trống, độ dài từ 1-50 ký tự';
    }
    if (!valid.isID(values.username, 1, 50)) {
        errors.username = 'Trường không đúng định dạng, chỉ cho phép chữ, số , dấu - và _ ';
    }

    return errors;
}

class CreateSpaceForm extends Component {
    static contextTypes = {
        router: PropTypes.object
    };

    componentWillMount() {
        this.props.resetNewSpace();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.newSpace.space && !nextProps.newSpace.error) {
            // create space success, redirect to list of spaces
            if (this.context && this.context.router)
                this.context.router.push('/ws/space/list');
        }
    }

    render() {
        const inputCls = {
            labelCls: "col-lg-3",
            inputWrapCls:"col-lg-9"
        }

        const { error, handleSubmit, pristine, reset, submitting } = this.props;
        return (
            <div className="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2">
                <div className="panel panel-info">
                    <div className="panel-heading">
                        <div className="panel-title">Tạo space cho doanh nghiệp của bạn</div>
                    </div>
                    <div className="panel-body">
                    {error &&
                        <div className="alert alert-danger" role="alert">
                            <strong>Error!</strong> {error}
                        </div>
                    }

                        <div className="signup-container">
                            <form onSubmit={handleSubmit(submit)} className="form-horizontal">
                                <Field component={FormInput} {...inputCls} name="name" type="text" label="Tên doanh nghiệp" placeholder="Tên doanh nghiệp" />
                                <Field component={FormInput} {...inputCls} name="short_name" type="text" label="Tên viết tắt" />
                                <Field component={FormInput} {...inputCls} name="phone" type="text" label="Điện thoại" />
                                <Field component={FormInput} {...inputCls} name="address" type="text" label="Địa chỉ liên hệ" placeholder="Địa chỉ liên hệ" />
                                <Field component={FormSelect} name="size" value="50" label="Quy mô nhân sự" labelCls="col-lg-3" inputWrapCls="col-lg-4" >
                                    <option value="">-- Chọn --</option>
                                    <option value="10">0-10</option>
                                    <option value="50">10-50</option>
                                    <option value="100">50-100</option>
                                    <option value="500">100-500</option>
                                    <option value="1000">500-1000</option>
                                    <option value="10000">>1000</option>
                                </Field>
                                <Field component={FormSelect} name="type" label="Lĩnh vực hoạt động" labelCls="col-lg-3" inputWrapCls="col-lg-4" >
                                    <option value="">-- Chọn --</option>
                                    <option value="1">Công nghệ thông tin</option>
                                    <option value="0">Lĩnh vực khác</option>
                                </Field>
                                <Field component={FormInput} name="uname" type="text" label="URL" placeholder="donghanhjsc" labelCls="col-lg-3" inputWrapCls="col-lg-9 form-inline" inputCls="space-url-input">
                                    <span className="space-url-postfix">.upspace.vn</span>
                                </Field>
                                <Field component={FormInput} {...inputCls} name="username" type="text" label="Tài khoản người dùng" placeholder="Tài khoản người dùng trong space" />
                                <div className="form-buttons form-group">
                                    <div className="col-lg-offset-3 col-lg-9">
                                        <button type="submit" disabled={submitting} className="btn btn-primary">Tạo Space</button>
                                        <button type="button" disabled={pristine || submitting} className="btn btn-default">Clear</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}

export default reduxForm({
  form: 'createSpaceForm',  // a unique identifier for this form
  validate
})(CreateSpaceForm)
