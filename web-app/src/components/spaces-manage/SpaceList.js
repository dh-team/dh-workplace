'use strict';

import React from 'react';
import { Link } from 'react-router';
import SpaceItem from './SpaceItem';

export default class SpaceList extends React.Component {
    
    render() {
        const {spaces} = this.props.spacesList;
        return (
            <div className="space-list">
                <div role="alert" className="alert alert-info"> 
                    <Link to="/ws/space/create" className="btn btn-sm btn-info"><i className="glyphicon glyphicon-plus"></i> Tạo space</Link> 
                    <span> Tạo space cho doanh nghiệp của bạn!</span> 
                </div>
                <ul className="list-group">
                    {this.renderSpaces(spaces)}
                </ul>
            </div>
        );
    }
    
    renderSpaces(spaces) {
        return spaces.map((item) => {
            return (
                <li className="list-group-item" key={item.space._id}>
                    <SpaceItem data={item}/>
                </li>
            );
        });
    }
}
