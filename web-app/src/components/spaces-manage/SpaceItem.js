'use strict';

import React, { Component, PropTypes }from 'react'
import { connect }from 'react-redux';
import { Link } from 'react-router';
import { userJoinSpace, userUnjoinSpace } from "../../actions/action-space";

class SpaceItem extends Component {
   
    render() {
        const data = this.props.data;
        const isAdmin = data.roles && data.roles.indexOf("administrator") > -1 ;
        return (
            <div className="space-item">
                <span className="space-name"><i className="glyphicon glyphicon-briefcase"></i> {data.space.name}</span>
                <div className="space-item-fn pull-right"> 
                    {!data.is_active &&
                        <div className="btn-group" role="group">
                            <button onClick={this.props.handleJoin} className="btn btn-xs btn-info"><i className="glyphicon glyphicon-thumbs-up"></i> Tham gia</button>
                            <button onClick={this.props.handleUnjoin} className="btn btn-xs btn-warning"><i className="glyphicon glyphicon-thumbs-down"></i> Từ chối</button>  
                        </div>
                    }
                    {data.is_active && 
                        <a href={"/space/open?space_id=" + data.space._id} className="btn btn-xs btn-primary">
                            <i className="glyphicon glyphicon-share-alt"></i> Mở space
                        </a>
                    }
                    {isAdmin && 
                        <Link to={"/ws/space/edit/" + data.space._id} className="btn btn-xs btn-info">
                            <i className="glyphicon glyphicon-pencil"></i> Sửa
                        </Link>
                    }
                    {!isAdmin && data.is_active && 
                        <button onClick={this.props.handleUnjoin} className="btn btn-xs btn-danger">
                            <i className="glyphicon glyphicon-log-out"></i> Rời space
                        </button>
                    }
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state, ownProps) => {
    return {};
}
const mapDispatchToProps = (dispatch, ownProps) => {
    var spaceID = ownProps.data.space._id;

    return {
        handleJoin: () => {
            // console.log("join " + spaceID);
            dispatch(userJoinSpace({space_id: spaceID}));
        },
        handleUnjoin: () => {
            // console.log("unjoin " + spaceID);
            dispatch(userUnjoinSpace({space_id: spaceID}));
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(SpaceItem)
