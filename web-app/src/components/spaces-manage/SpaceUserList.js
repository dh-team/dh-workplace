'use strict';

import React from 'react';
import { Link } from 'react-router';
import SpaceUserItem from './SpaceUserItem';

export default class SpaceUserList extends React.Component {
    
    render() {
        const users = this.props.users;
        return (
            <div className="space-user-list">
                <ul className="list-group">
                    {this.renderUsers(users)}
                </ul>
            </div>
        );
    }
    
    renderUsers(users) {
        return users.map((item) => {
            return (
                <li className="list-group-item" key={item.user._id}>
                    <SpaceUserItem data={item}/>
                </li>
            );
        });
    }
}
