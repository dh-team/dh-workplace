'use strict';

import React from 'react';
import { Link } from 'react-router';

export default class SpaceUserItem extends React.Component {
    render() {
        const data = this.props.data;
        const isAdmin = data.roles && data.roles.indexOf("administrator") > -1 ;
        return (
            <div className="space-user-item row">
                <div className="space-user-item-detail col-md-11">
                    <img src="/static/space/img/avatar.png" alt="" className="user-avatar img-circle" />
                    <span className="user-name">{data.user.display_name}</span>
                    {isAdmin && <span className="user-role label label-danger">administrator</span>}
                    {!data.is_active && <span className="user-status label label-default">chờ xác nhận</span>}
                </div>
                
                <div className="space-user-item-fn col-md-1"> 
                    {<button className="btn btn-xs btn-default pull-right"><i className="glyphicon glyphicon-remove"></i> loại bỏ</button>}
                </div>
            </div>
        );
    }
}
