import React, { Component } from 'react'

export default class FormInput extends Component {
    render() {
        const { input, label, type, meta: { touched, error, invalid, warning } } = this.props;
        const labelCls = "control-label " + (this.props.labelCls || "");  
        const inputCls = "form-control " + (this.props.inputCls || "");
        const inputWrapCls = this.props.inputWrapCls || "";
        const placeholder = this.props.placeholder || "";
        const wrapCls = 'form-group ' + (this.props.wrapCls || '') + (touched && error ? ' has-error' : ' ');
        return (
            <div className={wrapCls}>
                <label className={labelCls}>{label}</label>
                <div className={inputWrapCls}>
                    <input {...input}  type={type|| "text"} className={inputCls} placeholder={placeholder} />
                    {this.props.children}
                    {touched && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
                </div>
            </div>
        )
    }
}

