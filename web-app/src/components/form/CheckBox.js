import React, { Component } from 'react'

export default class CheckBox extends Component {
    render() {
        const { input, label, meta: { touched, error, invalid, warning }, labelCls, inputCls } = this.props;
        const wrapCls = 'checkbox ' + (this.props.wrapCls || '') + (touched && error ? ' has-error' : ' ');
        return (
            <div className={wrapCls}>
                <label className={labelCls}>
                    <input {...input}  type="checkbox" className={inputCls} checked={input.value == true} /> {label}
                </label>
                {touched && ((error && <span className='help-block validation-error'>{error}</span>) || (warning && <span>{warning}</span>))}
            </div>
        )
    }
}

