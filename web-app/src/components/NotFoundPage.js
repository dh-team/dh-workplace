'use strict';

import React from 'react';
import { Link } from 'react-router';

export default class NotFoundPage extends React.Component {
    render() {
        return (
            <div className="not-found">
                <h1>404</h1>
                <h2>Không tìm thấy trang!</h2>
                <p>
                  <Link to="/ws/">Trở về trang chủ</Link>
                </p>
            </div>
        );
    }
}
