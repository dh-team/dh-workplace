'use strict';

import React, {Component, PropTypes} from 'react';

class GlobalMsg extends Component {
    componentWillReceiveProps(nextProps) {
        var msg = nextProps.msg;
        if (msg && msg.type && msg.content) {
            switch(msg.type){
                case "error":
                    toastr.error(msg.content.content, msg.content.title);    
                    break;
                case "warning":
                    toastr.warning(msg.content.content, msg.content.title);    
                    break;
                case "success":
                    toastr.success(msg.content.content, msg.content.title);    
                    break;
                case "info":
                    toastr.info(msg.content.content, msg.content.title);    
                    break;
            }
            this.props.resetMsg();
        }
    }
    render() {
        return null;
    }
}

export default GlobalMsg;
