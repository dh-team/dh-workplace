import React from 'react'
import { render } from 'react-dom'
import { browserHistory } from 'react-router'
import { syncHistoryWithStore } from 'react-router-redux'
import cookie from 'react-cookie';
import axios from 'axios';
import Root from './containers/Root';
import configureStore from './store/configureStore';
import { publishUrl } from '../conf.json';

console.log("token in cookie: " + cookie.load("access_token"));
console.log("publishUrl: " + publishUrl);
var client = axios.create({
    baseURL: publishUrl,
    headers: {
        'Authorization': "Bearer " + cookie.load("access_token")
    }
});

const preloadedState = window.__PRELOADED_STATE__;
const store = configureStore(preloadedState, client);
const history = syncHistoryWithStore(browserHistory, store);

// axios.defaults.headers.common['Authorization'] = "Bearer " + cookie.load("access_token");

render(
    <Root store={store} history={history} />,
    document.getElementById('main')
)

