import {setMsg} from './action-msg';

export const CREATE_SPACE = 'CREATE_SPACE';
export const CREATE_SPACE_SUCCESS = 'CREATE_SPACE_SUCCESS';
export const CREATE_SPACE_FAILURE = 'CREATE_SPACE_FAILURE';
export const RESET_NEW_SPACE = 'RESET_NEW_SPACE';

//Space list
export const FETCH_SPACES = 'FETCH_SPACES';
export const FETCH_SPACES_SUCCESS = 'FETCH_SPACES_SUCCESS';
export const FETCH_SPACES_FAILURE = 'FETCH_SPACES_FAILURE';
export const RESET_SPACES = 'RESET_SPACES';
// List users of space
export const FETCH_SPACE_USERS = 'FETCH_SPACE_USERS';
export const FETCH_SPACE_USERS_SUCCESS = 'FETCH_SPACE_USERS_SUCCESS';
export const FETCH_SPACE_USERS_FAILURE = 'FETCH_SPACE_USERS_FAILURE';
export const RESET_SPACE_USERS = 'RESET_SPACE_USERS';
// Add user to space
export const ADD_SPACE_USER = 'ADD_SPACE_USER';
export const ADD_SPACE_USER_SUCCESS = 'ADD_SPACE_USER_SUCCESS';
export const ADD_SPACE_USER_FAILURE = 'ADD_SPACE_USER_FAILURE';
// Join/Unjoin space
export const USER_JOIN_SPACE = 'USER_JOIN_SPACE';
export const USER_JOIN_SPACE_SUCCESS = 'USER_JOIN_SPACE_SUCCESS';
export const USER_JOIN_SPACE_FAILURE = 'USER_JOIN_SPACE_FAILURE';

export const USER_UNJOIN_SPACE = 'USER_UNJOIN_SPACE';
export const USER_UNJOIN_SPACE_SUCCESS = 'USER_UNJOIN_SPACE_SUCCESS';
export const USER_UNJOIN_SPACE_FAILURE = 'USER_UNJOIN_SPACE_FAILURE';

///////////////
export function createSpace(props) {
    return {
        type: CREATE_SPACE,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/create-space", props).then(function(response) {
                dispatch(createSpaceSuccess(response.data));
                dispatch(setMsg("success", {
                    title: "Tạo space thành công",
                    content: "Tạo space " + response.data.name + " thành công"
                }));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                dispatch(createSpaceFailure(err));
                dispatch(setMsg("error", {
                    title: "Tạo space lỗi",
                    content: err.error
                }));
            });
        }
    };
}

export function createSpaceSuccess(newSpace) {
    return {
        type: CREATE_SPACE_SUCCESS,
        payload: newSpace
    };
}

export function createSpaceFailure(error) {
    return {
        type: CREATE_SPACE_FAILURE,
        payload: error
    };
}

export function resetNewSpace() {
    return {
        type: RESET_NEW_SPACE
    }
}
///////////////
export function fetchSpaces() {
    return {
        type: FETCH_SPACES,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/list-spaces").then(function(response) {
                // console.log("fetchSpaces:" + JSON.stringify(response.data));
                dispatch(fetchSpacesSuccess(response.data));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                // console.log("fetchSpaces:" + JSON.stringify(err));
                dispatch(fetchSpacesFailure(err));
                dispatch(setMsg("error", {
                    content: JSON.stringify(err)
                }));
            });
        }
    };
}

export function fetchSpacesSuccess(spaces) {
    return {
        type: FETCH_SPACES_SUCCESS,
        payload: spaces
    };
}

export function fetchSpacesFailure(error) {
    return {
        type: FETCH_SPACES_FAILURE,
        payload: error
    };
}
///////////////
export function fetchSpaceUsers(spaceID) {
    return {
        type: FETCH_SPACE_USERS,
        promise: function(client, dispatch, getState) {
            return client.get("/api/space/list-users?space_id=" + spaceID).then(function(response) {
                dispatch(fetchSpaceUsersSuccess(response.data));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                dispatch(fetchSpaceUsersFailure(err));
                dispatch(setMsg("error", {
                    content: JSON.stringify(err)
                }));
            });
        }
    };
}

export function fetchSpaceUsersSuccess(users) {
    return {
        type: FETCH_SPACE_USERS_SUCCESS,
        payload: users
    };
}

export function fetchSpaceUsersFailure(error) {
    return {
        type: FETCH_SPACE_USERS_FAILURE,
        payload: error
    };
}

export function resetSpaceUsers() {
    return {
        type: RESET_SPACE_USERS
    }
}
///////////////
export function addSpaceUser(props) {
    return {
        type: ADD_SPACE_USER,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/add-user", props).then(function(response) {
                dispatch(addSpaceUserSuccess(response.data));
                dispatch(setMsg("success", {
                    content: "Thêm người dùng thành công"
                }));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                dispatch(addSpaceUserFailure(err));
                dispatch(setMsg("error", {
                    title: "Thêm người dùng lỗi",
                    content: JSON.stringify(err)
                }));
            });
        }
    };
}

export function addSpaceUserSuccess(newUser) {
    return {
        type: ADD_SPACE_USER_SUCCESS,
        payload: newUser
    };
}

export function addSpaceUserFailure(error) {
    return {
        type: ADD_SPACE_USER_FAILURE,
        payload: error
    };
}
///////////////
export function userJoinSpace(props) {
    return {
        type: USER_JOIN_SPACE,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/join", props).then(function(response) {
                dispatch(userJoinSpaceSuccess(response.data));
                dispatch(setMsg("success", {
                    content: "Bạn đã tham gia space"
                }));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                dispatch(userJoinSpaceFailure(err));
                dispatch(setMsg("error", {
                    title: "Tham gia space lỗi",
                    content: JSON.stringify(err)
                }));
            });
        }
    };
}

export function userJoinSpaceSuccess(data) {
    return {
        type: USER_JOIN_SPACE_SUCCESS,
        payload: data
    };
}

export function userJoinSpaceFailure(error) {
    return {
        type: USER_JOIN_SPACE_FAILURE,
        payload: error
    };
}
///////////////
export function userUnjoinSpace(props) {
    return {
        type: USER_UNJOIN_SPACE,
        promise: function(client, dispatch, getState) {
            return client.post("/api/space/unjoin", props).then(function(response) {
                dispatch(userUnjoinSpaceSuccess(response.data));
                dispatch(setMsg("success", {
                    content: "Bạn đã rời khỏi space"
                }));
            }).catch(function(error) {
                var err = error.response ? error.response.data : error.message;
                dispatch(userUnjoinSpaceFailure(err));
                dispatch(setMsg("error", {
                    title: "Rời khỏi space lỗi",
                    content: JSON.stringify(err)
                }));
            });
        }
    };
}

export function userUnjoinSpaceSuccess(data) {
    return {
        type: USER_UNJOIN_SPACE_SUCCESS,
        payload: data
    };
}

export function userUnjoinSpaceFailure(error) {
    return {
        type: USER_UNJOIN_SPACE_FAILURE,
        payload: error
    };
}
