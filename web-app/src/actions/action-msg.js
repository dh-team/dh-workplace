export const SET_MSG = 'SET_MSG';
export const RESET_MSG = 'RESET_MSG';

export function setMsg(type, content) {
    return {
        type: SET_MSG,
        msg: {
            type: type,
            content: content
        }
    };
}

export function resetMsg() {
    return {
        type: RESET_MSG
    }
}
