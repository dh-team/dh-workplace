/*jslint node: true */
'use strict';

var request = require('request');
var passport = require('passport');
var OAuth2Strategy = require('passport-oauth2').Strategy;
var config = require('../config');

class MyStrategy extends OAuth2Strategy {
    // override userProfile method of OAuth2Strategy
    userProfile(accessToken, done) {
        // get user's profile
        request.get(config.authServer.userProfileUrl, {
            headers: {
                Authorization: 'Bearer ' + accessToken
            }
        }, function(err, res, body) {
            if (err) return done(err);
            var profile = JSON.parse(body);
            if (res.statusCode === 200) {
                return done(null, profile);
            }

            return done(null, false); // get user's profile fail
        });
    }
}

passport.use(new MyStrategy({
        authorizationURL: config.authServer.authorizationUrl,
        tokenURL: config.authServer.tokenUrl,
        clientID: config.authServer.clientId,
        clientSecret: config.authServer.clientSecret,
        callbackURL: config.authServer.callbackUrl
    },
    function(accessToken, refreshToken, profile, cb) {
        console.log("accessToken:" + accessToken);
        console.log("refreshToken:" + refreshToken);
        console.log(profile);
        var authInfo = {
            accessToken: accessToken,
            refreshToken: refreshToken
        };
        cb(null, profile.user, authInfo);
    }
));

passport.serializeUser(function(user, done) {
    done(null, user);
});

passport.deserializeUser(function(user, done) {
    done(null, user);
});
