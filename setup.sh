dir=$(pwd)

cp common/default-conf.json common/conf.json

# create dir for logging
mkdir -p log
#create dir for data
mkdir -p data/rethinkdb

# create symbolic links
cd $dir/auth-server
ln -sf ../common/mongodb
ln -sf ../common/utils
ln -sf ../common/conf.json

cd $dir/core-api-server
ln -sf ../common/mongodb
ln -sf ../common/utils
ln -sf ../common/resource-auth
ln -sf ../common/conf.json

cd $dir/file-server
ln -sf ../common/resource-auth
ln -sf ../common/conf.json

cd $dir/web-app
ln -sf ../common/conf.json
cd $dir/web-app/src
ln -sf ../../common/utils

cd $dir/space-app
ln -sf ../common/conf.json
cd $dir/space-app/src
ln -sf ../../common/utils

cd $dir/proxy-server
ln -sf ../common/conf.json
