const esr = require("escape-string-regexp");
const p = require("path");
var config = require("../config");
var re = require("./rethink-db");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

// merge input object with default file object
function nodeObj(obj) {
    var defaultObj = {
        space: null,
        app: null,
        partition: null,

        device: "local",
        location: null,

        type: "file",
        node_type: null,
        name: null,
        parent: null,

        create_user: null,
        create_time: new Date(),
        access_time: new Date(),
        edit_time: null,

        size: null,
        mime_type: null,
        description: null,
        tags: [],
        attributes: null,

        versions: [],

        is_deleted: false
    };

    return Object.assign(defaultObj, obj);
}

// get node by ID
function getNodeByID(id, callback) {
    re.table(tbNode).get(id).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.deleted) return callback(null, null);
        callback(null, r);
    });
}

// filter nodes by condition and sort by fieldname
function filterNodes(cond, sort, callback) {
    if (typeof(sort) === "function" && !callback) {
        callback = sort;
    }
    var strFilter = null;
    if (cond.string_filter) {
        strFilter = cond.string_filter;
        delete cond.string_filter;
    }

    var query = re.table(tbNode).filter(cond);

    if (strFilter && strFilter.length > 0) {
        strFilter.map(function(item) {
            if (item.cond === "startswith") {
                query = query.filter(re.row(item.field).match("^" + esr(item.value)));
            }
        });
    }
    if (sort) {
        query = query.orderBy(sort);
    }

    query.run(callback);
}

// filter and get first node with condition
function getSingleNode(condition, callback) {
    re.table(tbNode).filter(condition).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.length == 0) return callback(null, null);
        return callback(null, r[0]);
    });
}

// get child node by name
function getChildByName(parent, childName, callback) {
    var cond = {
        parent: parent.hasOwnProperty("id") ? parent.id : parent,
        name: childName
    };
    getSingleNode(cond, callback);
}

// update node by ID
function updateNodeByID(id, node, callback) {
    re.table(tbNode).get(id).update(node).run(callback);
}

/**
 * add version of file
 *  currentNode
 *  newFile
 *  callback: function(err, updatedFile)
 */ 
function addFileVersion(currentNode, newFile, callback) {

    // add new version
    var version = {
        version_number: currentNode.versions.length + 1,
        size: currentNode.size,
        create_time: currentNode.edit_time || currentNode.create_time,
        location: currentNode.location,
        description: newFile.description
    };

    log.debug("add version: " + JSON.stringify(version) + " for " + currentNode.id);

    re.table(tbNode).get(currentNode.id).update({
        location: newFile.location,
        size: newFile.size,
        edit_time: new Date(),
        mime_type: newFile.mime_type,
        "versions": re.row("versions").append(version)
    }, {
        returnChanges: true
    }).run(function(err, r) {
        if (err) return callback(err);
        if (r && r.changes.length > 0) {
            return callback(null, r.changes[0].new_val);
        }
        return callback(null, null);
    });
}

module.exports = {
    nodeObj: nodeObj,
    filterNodes: filterNodes,
    getNodeByID: getNodeByID,
    getSingleNode: getSingleNode,
    getChildByName: getChildByName,
    updateNodeByID: updateNodeByID,
    addFileVersion: addFileVersion
}
