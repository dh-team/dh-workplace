const asyn = require("async");
const config = require("../config");
const re = require("./rethink-db");

const tbNode = config.db_table.tbNode;


// delete permanent a node, return deleted node
function deletePermanentNode(id, callback) {
    return re.table(tbNode).get(id).delete({
        returnChanges: true
    }).run(function(err, r) {
        callback(err, (r && r.changes && r.changes.length > 0) ? r.changes[0].old_val : null);
    });
}

// delete permanent node recursive
function deletePermanentNodeR(id, childDeletedCallback, completeCallback) {
    // delete all files in dir first
    re.table(tbNode).filter({
        parent: id,
        type: "file"
    }).delete({
        returnChanges: true
    }).run(function(err, r) {
        if (r && r.changes) {
            r.changes.map(function(item) {
                childDeletedCallback(item.old_val);
            });
        }
        if (err) return completeCallback(err);

        re.table(tbNode).filter({
            parent: id,
            type: "dir"
        }).run(function(err, r) {
            if (err) return completeCallback(err);

            if (!r || r.length == 0) return deletePermanentNode(id, completeCallback); // current node has no childs

            var tasks = r.map(function(item) {
                return function(cb) {
                    deletePermanentNodeR(item.id, childDeletedCallback, function(err, deletedNode) {
                        if (deletedNode) childDeletedCallback(deletedNode);
                        cb(err);
                    });
                }
            });

            asyn.parallel(tasks, function(err) {
                if (err) return completeCallback(err);
                // all childs deleted -> delete current node
                return deletePermanentNode(id, completeCallback);
            });
        });
    });
}

// delete permanent file by id
function deletePermanentFileByID(id, callback) {
    return deletePermanentNode(id, callback);
}

// delete permanent dir by id
function deletePermanentDirByID(id, childDeletedCallback, completeCallback) {
    deletePermanentNodeR(id, childDeletedCallback, completeCallback);
}


module.exports = {
    deletePermanentFileByID: deletePermanentFileByID,
    deletePermanentDirByID: deletePermanentDirByID
}
