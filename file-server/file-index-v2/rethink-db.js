var config = require('../config');
var log = require("../logger");
var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;
var re = require('rethinkdbdash')({
    servers: [config.rethinkdb]
});

function init(callback) {
    re.table(tbNode).indexWait("name").run().then(function(result) {
        log.debug("Table and index are available");
        callback(null, result);
    }).error(function() {
        log.debug("Error, init db for the first time.");
        re.dbCreate(config.rethinkdb.db).run()
            .finally(function() {
                log.debug("Create tb " + tbNode);
                return re.tableCreate(tbNode).run(); // create table
            }).finally(function() {
                log.debug("Create index 'name' for tb " + tbNode);
                return re.table(tbNode).indexCreate("name").run(); // create index
            }).finally(function(result) {
                log.debug("Wait index 'name' for tb " + tbNode);
                return re.table(tbNode).indexWait("name").run()
            }).finally(function() {
                log.debug("Create tb " + tbIndex);
                return re.tableCreate(tbIndex).run(); // create table
            }).finally(function() {
                log.debug("Create index 'fullname' for tb " + tbIndex);
                return re.table(tbIndex).indexCreate("fullname").run(); // create index
            }).finally(function(result) {
                log.debug("Wait index 'fullname' for tb " + tbIndex);
                return re.table(tbIndex).indexWait("fullname").run()
            }).then(function(result) {
                log.debug("Table and index are available.");
                callback(null, result);
            }).error(function(err) {
                if (err) {
                    callback(err);
                }
            });
    });
}

init(function(err) {
    if (err)
        log.debug("init connection to rethinkdb error: " + JSON.stringify(err));
    else
        log.debug("init connection to rethinkdb successfully");
});
module.exports = re;
