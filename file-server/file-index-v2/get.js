var config = require("../config");
var cm = require("./common");

// get dir by id
function getDirByID(id, callback) {
    cm.getNodeByID(id, function(err, r) {
        if (err) return callback(err);
        if (!r || r.type !== "dir") return callback(null, null);

        return callback(null, r);
    });
}

// get dir by id
function getFileByID(id, callback) {
    cm.getNodeByID(id, function(err, r) {
        if (err) return callback(err);
        if (!r || r.type !== "file") return callback(null, null);

        return callback(null, r);
    });
}

module.exports = {
    getDirByID: getDirByID,
    getFileByID: getFileByID
}
