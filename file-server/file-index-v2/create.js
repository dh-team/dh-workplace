var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var log = require("../logger");

var tbNode = config.db_table.tbNode;

/**
 * Create new node 
 *  node: object
 *  callback: function(err, created node)
 */
function createNode(node, callback) {
    var newNode = cm.nodeObj(node);
    log.debug("create node " + JSON.stringify(newNode));

    re.table(tbNode).insert(newNode).run(function(err, r) {
        if (err) return callback(err);
        if (!r.generated_keys || r.generated_keys.length == 0) return callback();
        newNode.id = r.generated_keys[0];
        callback(null, newNode);
    });
}

module.exports = {
    createNode: createNode
}
