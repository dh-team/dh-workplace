const p = require('path');
var httpUtils = module.exports = require("./utils/http");

function extractScope(partition) {
    var subs = partition.split(":");
    if (!subs || subs.length < 2) return null;
    if (subs[0] !== "g" && subs[0] !== "u") return null;
    if (!subs[1] || !subs[1].trim()) return null;
    return {
        type: subs[0],
        id: subs[1].trim()
    }
}

function splitPath(path) {
    var i = path.lastIndexOf('/');
    if (i == -1) {
        return {
            parent: null,
            name: path
        };
    }

    var pPath = path.substr(0, i).trim();
    var fName = path.substr(i + 1).trim();
    return {
        parent: pPath,
        name: fName
    };
}

module.exports.splitPath = splitPath;
module.exports.extractScope = extractScope;

