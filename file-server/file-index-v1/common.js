const esr = require("escape-string-regexp");
const p = require("path");
var config = require("../config");
var re = require("./rethink-db");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

// check to table config.db_table.tbNode create if not exist
function init(callback) {
    re.table(tbNode).indexWait("name").run().then(function(result) {
        log.debug("Table and index are available");
        callback(null, result);
    }).error(function() {
        log.debug("Error, init db for the first time.");
        re.dbCreate(config.rethinkdb.db).run()
            .finally(function() {
                log.debug("Create tb " + tbNode);
                return re.tableCreate(tbNode).run(); // create table
            }).finally(function() {
                log.debug("Create index 'name' for tb " + tbNode);
                return re.table(tbNode).indexCreate("name").run(); // create index
            }).finally(function(result) {
                log.debug("Wait index 'name' for tb " + tbNode);
                return re.table(tbNode).indexWait("name").run()
            }).finally(function() {
                log.debug("Create tb " + tbIndex);
                return re.tableCreate(tbIndex).run(); // create table
            }).finally(function() {
                log.debug("Create index 'fullname' for tb " + tbIndex);
                return re.table(tbIndex).indexCreate("fullname").run(); // create index
            }).finally(function(result) {
                log.debug("Wait index 'fullname' for tb " + tbIndex);
                return re.table(tbIndex).indexWait("fullname").run()
            }).then(function(result) {
                log.debug("Table and index are available.");
                callback(null, result);
            }).error(function(err) {
                if (err) {
                    callback(err);
                }
            });
    });
}

// merge input object with default file object
function nodeObj(obj) {
    var defaultObj = {
        //id: String, // auto generated id
        device: "local", // device that stores this file (Local Disk, Cloud Storage: Google Drive, Dropbox, Amazon,...)
        location: "", // location in storage device

        type: "file", // "file"|"dir"
        name: null, // name of file
        parent: null, // id of parent node

        space: null, // id of organization
        partition: null, // "[type]/[id]" type: "g" (group) | "u" (user), id: id of group or user, example "g/qwertttyuioopasdfghjkl", special "g/all" - partition for this space, all users can access

        size: null, // size of file in bytes
        permision: "7000", // permision for user (position), group, organization, all user

        create_user: null, // user who created file - {user_id, uname}
        create_time: new Date(), // created time
        access_time: new Date(), // access time
        edit_time: null, // modify content time

        mime_type: null,
        description: "",
        tags: [],
        attributes: null,
        node_type: "user" // "system"|"user", "system" : node is system node, user can not see, modify, delete it, "user" : node is user node
    };

    return Object.assign(defaultObj, obj);
}

// get node by ID
function getNodeByID(id, callback) {
    re.table(tbNode).get(id).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.deleted) return callback(null, null);
        callback(null, r);
    });
}

// filter and get first node with condition
function getSingleNode(condition, callback) {
    re.table(tbNode).filter(condition).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.length == 0) return callback(null, null);
        return callback(null, r[0]);
    });
}

// get child node by name
function getChildByName(parent, childName, callback) {
    var cond = {
        parent: parent.hasOwnProperty("id") ? parent.id : parent,
        name: childName
    };
    getSingleNode(cond, callback);
}

// get node index by nodeID
function getNodeIndexByID(nodeID, callback) {
    return re.table(tbIndex).get(nodeID).run(function(err, r) {
        if (err) return callback(err);
        callback(null, r);
    });
}

// get node index by path
function getNodeIndexByPath(path, callback) {
    return re.table(tbIndex).filter({
        fullname: path
    }).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.length == 0) return callback(null, null);
        callback(null, r[0]);
    });
}

// get parents of a node
function getNodeParents(node, arr, callback) {
    if (!node.parent) return callback(null, arr);
    getNodeByID(node.parent, function(err, r) {
        if (err) return callback(err);
        arr.push(r);
        getNodeParents(r, arr, callback);
    });
}

// find target trash for deleting a node
function targetTrash(node, callback) {
    return getNode({
        name: "trash",
        space: node.space,
        partition: node.partition
    }, callback);
}

// find home dir
function targetHome(node, callback) {
    return getNode({
        name: "home",
        space: node.space,
        partition: node.partition
    }, callback);
}

function getNode(node, callback) {
    if (node.parent && node.parent != "*" && node.name) {
        return getChildByName(node.parent, node.name, callback);
    }
    else if (node.name && node.space && node.partition) {
        var cond = {
            space: node.space,
            partition: node.partition,
            name: node.name
        };
        if (node.parent != "*")
            cond.parent = null;
            
        return getSingleNode(cond, callback);
    }
}

// find thumb dir
function targetThumb(node, callback) {
    return getNode({
        name: "thumb",
        space: node.space,
        partition: node.partition
    }, callback);
}

function hook(input, opt, returnCb, continueCb) {
    if (opt && opt.hook) {
        var err = opt.hook(input);
        if (err) return returnCb(err);
    }
    if (opt && opt.asyncHook) {
        opt.asyncHook(input, function(err) {
            if (err) return returnCb(err);
            return continueCb();
        });
    }
    else {
        return continueCb();
    }
}

// update node by ID
function updateNodeByID(id, node, callback) {
    re.table(tbNode).get(id).update(node).run(callback);
}

// rename exist node name: filename -> filename-1, ...
function autoRename(childName, parentID, callback) {
    getChildByName(parentID, childName, function(err, r) {
        if (err) return callback(err);
        if (!r) return callback(null, childName);

        var ext = p.extname(childName);
        var basename = p.basename(childName, ext);

        re.table(tbNode).filter({
            parent: parentID
        }).filter(
            re.row("name").match("^" + esr(basename))
        ).run(function(err, r) {
            if (err) return callback(err);
            var maxNum = 0;
            var tmp;
            r.map(function(item) {
                tmp = getNum(item.name);
                if (tmp > maxNum) maxNum = tmp;
            });
            return callback(null, basename + "-" + (++maxNum) + ext);
        });
    });
}

// filename-1.ext, filename-2.ext
function getNum(filename) {
    var basename = p.basename(filename, p.extname(filename));
    var idx = basename.lastIndexOf("-");
    if (idx < 0) return 0;
    var num = parseInt(basename.substr(idx + 1));
    return num >= 0 ? num : 0;
}

module.exports = {
    init,
    nodeObj,
    getNodeByID,
    getSingleNode,
    getChildByName: getChildByName,
    getNodeIndexByID,
    getNodeIndexByPath,
    targetTrash,
    targetHome,
    targetThumb,
    getNodeParents,
    hook,
    updateNodeByID,
    autoRename: autoRename,
    getNode: getNode
}
