var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var helper = require("../helper");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

// get dir by id
function getDirByID(id, callback) {
    cm.getNodeByID(id, function(err, r) {
        if (err) return callback(err);
        if (!r || r.type !== "dir") return callback(null, null);

        return callback(null, r);
    });
}

// get dir by id
function getFileByID(id, callback) {
    cm.getNodeByID(id, function(err, r) {
        if (err) return callback(err);
        if (!r || r.type !== "file") return callback(null, null);

        return callback(null, r);
    });
}

// get dir by path
function getDirByPath(path, callback) {
    cm.getNodeIndexByPath(path, function(err, r) {
        if (err) return callback(err);
        if (!r) return callback(null, null);

        return cm.getNodeByID(r.id, callback);
    });
}

// get file by path
function getFileByPath(path, callback) {
    var spl = helper.splitPath(path);
    if (!spl.parent) {
        return process.nextTick(function() {
            callback("param_invl");
        });
    }

    // get parent node
    getDirByPath(spl.parent, function(err, pDir) {
        if (err) return callback(err);
        if (!pDir) return callback("not_exist");

        cm.getChildByName(pDir, spl.name, function(err, r) {
            if (err) return callback(err);
            if (!r || r.type != "file") return callback("not_exist");

            callback(null, r);
        });
    });
}

// get dir"s content by path
function getDirContentByPath(path, callback) {
    getDirByPath(path, function(err, dir) {
        if (err) return callback(err);
        if (!dir) return callback("not_exist");

        addDirContent(dir, callback);
    });
}

// get dir"s content by id
function getDirContentByID(id, callback) {
    cm.getNodeByID(id, function(err, node) {
        if (err) return callback(err);
        if (!node || node.type !== "dir") return callback("not_exist");
        addDirContent(node, callback);
    });
}

function getRootDirContent(name, space, partition, callback) {
    cm.getNode({
        name,
        space,
        partition
    }, function(err, node) {
        if (err) return callback(err);
        if (!node || node.type !== "dir") return callback("not_exist");
        addDirContent(node, callback);
    });
}

function addDirContent(dir, callback) {
    re.table(tbNode).filter({
        parent: dir.id
    }).orderBy('name').run(function(err, r) {
        if (err) return callback(err);
        callback(null, Object.assign(dir, {
            content: r
        }));
    });
}


module.exports = {
    getDirByID: getDirByID,
    getFileByID: getFileByID,
    getDirByPath: getDirByPath,
    getFileByPath: getFileByPath,
    getDirContentByID: getDirContentByID,
    getDirContentByPath: getDirContentByPath,
    getRootDirContent: getRootDirContent
}
