var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var idx = require("./index-mgr");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

/* update node's name
    param: {
        id: false,
        new_name
    }
*/

function renameNode(param, opt, callback) {
    cm.getNodeByID(param.id, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        // check same name
        cm.getChildByName(node.parent, param.new_name, function(err, r) {
            if (err) return callback(err);
            if (r) return callback("already_exist");

            cm.hook(node, opt, callback, function() {
                if (node.type === 'dir') {
                    renameDir(param, node, callback);
                }
                else {
                    updateNodeName(param, node, callback);
                }
            });
        });
    });
}

function renameDir(param, node, callback) {
    return updateNodeName(param, node, function(err) {
        if (err) return callback(err);
        idx.updateNodeName(param.id, param.new_name, callback);
    });
}

function updateNodeName(param, node, callback) {
    re.table(tbNode).get(param.id).update({
        name: param.new_name
    }).run(function(err, r) {
        if (err) return callback(err);
        callback(null);
    });
}


module.exports = {
    renameNode
}
