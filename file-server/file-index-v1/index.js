module.exports = Object.assign(
    {},
    require('./common'),
    require('./create'),
    require('./get'),
    require('./trash'),
    require('./rename')
);