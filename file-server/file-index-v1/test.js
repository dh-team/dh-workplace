var re = require('./rethink-db');
var config = require('../config');
var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

// default callback
var defaultCallback = function(err, r) {
    if (err) console.log(JSON.stringify(err));
    if (r) console.log('ok!' + JSON.stringify(r));
};


// helper method
function reset(callback) {
    if (!callback) {
        callback = defaultCallback;
    }
    re.dbDrop(config.rethinkdb.db).run()
        .then(function() {
            callback(null);
        })
        .error(function(err) {
            callback(err);
        });
}

function listFiles(user, path, callback) {
    if (!callback) {
        callback = defaultCallback;
    }
    re.table(tbNode).filter({
        user: user
    }).run().then(function(r) {
        callback(null, r);
    }).error(function(err) {
        callback(err);
    });
}

function listAllNodes(callback) {
    if (!callback) {
        callback = defaultCallback;
    }
    re.table(tbNode).run().then(function(r) {
        callback(null, r);
    }).error(function(err) {
        callback(err);
    });
}

function listAllIndexes(callback) {
    if (!callback) {
        callback = defaultCallback;
    }
    re.table(tbIndex).orderBy("fullname").run().then(function(r) {
        callback(null, r);
    }).error(function(err) {
        callback(err);
    });
}

function deleteAll(callback) {
    if (!callback) {
        callback = defaultCallback;
    }
    re.table(tbNode).delete().run().then(function(r) {
        callback(null, r);
    }).error(function(err) {
        callback(err);
    });
}

module.exports = {
    reset,
    listAllNodes,
    listAllIndexes
}
