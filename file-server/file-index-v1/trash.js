var asyn = require("async");
const esr = require("escape-string-regexp");
var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var gt = require("./get");
var idx = require("./index-mgr");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

// delete node (move node to .trash folder and delete all indexes)
function deleteNode(node, callback) {
    // find trash
    cm.targetTrash(node, function(err, trash) {
        if (err) return callback(err);
        if (!trash) return callback("trash_not_exist");

        re.table(tbNode).get(node.id).update({
            parent: trash.id,
            restore_parents: [node.parent]
        }).run(function(err) {
            if (err) return callback(err);

            if (node.type === "file") return callback(null);

            // delete index recursive
            idx.deleteRelatedIndexByID(node.id, function(err) {
                if (err) log.debug("delete related index error: " + JSON.stringify(err));
                callback(null);
            });
        });
    });
}

/* delete node by node id
    opt: {
        hook: function,
        asyncHook: function
    }
*/

function deleteNodeByID(nodeID, opt, callback) {
    cm.getNodeByID(nodeID, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        cm.hook(node, opt, callback, function() {
            deleteNode(node, callback);
        });
    });
}

// delete dir by path
function deleteDirByPath(path, opt, callback) {
    gt.getDirByPath(path, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        cm.hook(node, opt, callback, function() {
            deleteNode(node, callback);
        });
    });
}

// delete file by path
function deleteFileByPath(path, opt, callback) {
    gt.getFileByPath(path, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        cm.hook(node, opt, callback, function() {
            deleteNode(node, callback);
        });
    });
}

// delete permanent a node, return deleted node
function deletePermanentNode(id, callback) {
    return re.table(tbNode).get(id).delete({
        returnChanges: true
    }).run(function(err, r) {
        callback(err, (r && r.changes && r.changes.length > 0) ? r.changes[0].old_val : null);
    });
}

// delete permanent node recursive
function deletePermanentNodeR(id, childDeletedCallback, completeCallback) {
    // delete all files in dir first
    re.table(tbNode).filter({
        parent: id,
        type: "file"
    }).delete({
        returnChanges: true
    }).run(function(err, r) {
        if (r && r.changes) {
            r.changes.map(function(item) {
                childDeletedCallback(item.old_val);
            });
        }
        if (err) return completeCallback(err);

        re.table(tbNode).filter({
            parent: id,
            type: "dir"
        }).run(function(err, r) {
            if (err) return completeCallback(err);

            if (!r || r.length == 0) return deletePermanentNode(id, completeCallback); // current node has no childs

            var tasks = r.map(function(item) {
                return function(cb) {
                    deletePermanentNodeR(item.id, childDeletedCallback, function(err, deletedNode) {
                        if (deletedNode) childDeletedCallback(deletedNode);
                        cb(err);
                    });
                }
            });

            asyn.parallel(tasks, function(err) {
                if (err) return completeCallback(err);
                // all childs deleted -> delete current node
                return deletePermanentNode(id, completeCallback);
            });
        });
    });
}

// delete permanent file by id
function deletePermanentFileByID(id, callback) {
    return deletePermanentNode(id, callback);
}

// delete permanent dir by id
function deletePermanentDirByID(id, childDeletedCallback, completeCallback) {
    deletePermanentNodeR(id, childDeletedCallback, function(err, deletedDir) {
        if (err) return completeCallback(err);
        // delete all index
        idx.deleteRelatedIndexByID(id, function(err) {
            if (err) log.debug("deletePermanentDirByID: " + err);
            completeCallback(null, deletedDir);
        });
    });
}

// delete permanent file by path
function deletePermanentFileByPath(path, callback) {


    gt.getFileByPath(path, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        return deletePermanentFileByID(node.id, callback);
    });
}

// delete permanent dir by path
function deletePermanentDirByPath(path, callback) {


    gt.getDirByPath(path, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        return deletePermanentDirByID(node.id, callback);
    });
}

// restore a file in trash
function restoreNodeByID(nodeID, opt, callback) {
    cm.getNodeByID(nodeID, function(err, node) {
        if (err) return callback(err);
        if (!node) return callback("not_exist");

        cm.hook(node, opt, callback, function() {
            restoreNode(node, callback);
        });
    });
}

// restore a file in trash
function restoreNode(node, callback) {
    var parentID;
    if (node.restore_parents && node.restore_parents.length > 0) {
        parentID = node.restore_parents[0];
    }

    if (!parentID) return restoreNodeToHome(node, callback);

    cm.getNodeIndexByID(parentID, function(err, parentIndex) {
        if (err) return callback(err);

        if (!parentIndex) return restoreNodeToHome(node, callback); // parent index not exist ~ may be parent node deleted -> restore to home

        // check parent node exist
        cm.getNodeByID(parentIndex.id, function(err, parentNode) {
            if (err || !parentNode) return restoreNodeToHome(node, callback); // parent node not exist -> restore to home

            return restoreNodeToParent(node, parentNode, callback);
        });
    });
}

// restore a file in trash to home node
function restoreNodeToHome(node, callback) {
    // find home
    cm.targetHome(node, function(err, home) {
        if (err) return callback(err);
        if (!home) return callback("target_not_exist");
        restoreNodeToParent(node, home, callback);
    });
}

// restore a file in trash to parent node
function restoreNodeToParent(node, parentNode, callback) {
    cm.autoRename(node.name, parentNode.id, function(err, newName) {
        if (err) return callback(err);

        if (newName) node.name = newName;
        node.parent = parentNode.id;
        node.restore_parents = [];

        re.table(tbNode).get(node.id).update({
            name: node.name,
            parent: node.parent,
            restore_parents: node.restore_parents
        }).run(function(err) {
            if (err) return callback(err);
            if (node.type === "file") return callback(null);

            // build index recursive for this dir
            idx.buildIndex(node, function(err) {
                if (err) log.error("buildIndex: " + JSON.stringify(err));
                callback(null);
            });
        });
    });
}


module.exports = {
    deleteNodeByID: deleteNodeByID,
    // deleteDirByPath: deleteDirByPath,
    // deleteFileByPath: deleteFileByPath,
    // deletePermanentDirByPath: deletePermanentDirByPath,
    // deletePermanentFileByPath: deletePermanentFileByPath,
    deletePermanentFileByID: deletePermanentFileByID,
    deletePermanentDirByID: deletePermanentDirByID,
    restoreNodeByID: restoreNodeByID
}
