var p = require("path");
var asyn = require("async");
const esr = require("escape-string-regexp");
var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;
var insertIdxOpt = {
    conflict: "update"
};

// get all index of node and it's childs by path
function getRelatedIndex(parentPath, callback) {
    parentPath = p.join(parentPath, "/");
    re.table(tbIndex).filter(
        re.row("fullname").match("^" + esr(parentPath))
    ).run(function(err, r) {
        if (err) return callback(err);
        return callback(null, r);
    });
}

// delete all index of node and it's childs by path
function deleteRelatedIndex(parentPath, callback) {
    parentPath = p.join(parentPath, "/");
    re.table(tbIndex).filter(
        re.row("fullname").match("^" + esr(parentPath))
    ).delete().run(function(err, r) {
        if (err) return callback(err);
        return callback(null, r);
    });
}

// delete all index of node and it's childs by node id
function deleteRelatedIndexByID(nodeID, callback) {
    cm.getNodeIndexByID(nodeID, function(err, parentIndex) {
        if (err) return callback(err);
        if (!parentIndex) return callback(null);
        deleteRelatedIndex(parentIndex.fullname, callback);
    })
}

// insert an index for a node
function createIndex(idx, callback) {
    log.debug("create index " + JSON.stringify(idx));
    re.table(tbIndex).insert(idx, insertIdxOpt).run(function(err, r) {
        if (err) return callback(err);
        callback(null, idx);
    });
}

// insert an index for a node
function createIndexForNode(node, callback) {
    log.debug("create index for node " + JSON.stringify(node));
    // build node fullname
    if (!node.parent) {
        var idx = {
            id: node.id,
            fullname: p.join("/", node.space, node.partition, node.name, "/")
        }
        return createIndex(idx, callback);
    }

    cm.getNodeIndexByID(node.parent, function(err, parentIndex) {
        if (err) return callback(err);
        if (!parentIndex) return callback("parent_index_not_found");
        var idx = {
            id: node.id,
            fullname: p.join(parentIndex.fullname, node.name, "/")
        }
        return createIndex(idx, callback);
    });
}

// build all index for a node and it's childs
function buildIndex(node, callback) {
    createIndexForNode(node, function(err, nodeIndex) {
        if (err) return callback(err);
        // index all child
        indexChildsR(nodeIndex, function(err) {
            if (err) return callback(err);
            return callback(null, nodeIndex);
        });
    });
}

// build index for child nodes recursive
function indexChildsR(parentIndex, callback) {
    // get childs
    re.table(tbNode).filter({
        parent: parentIndex.id
    }).run(function(err, r) {
        if (err) return callback(err);
        if (!r || r.length == 0) return callback(null);

        var childs = r.map(function(item) {
            return {
                id: item.id,
                fullname: p.join(parentIndex.fullname, item.name, "/")
            };
        });

        re.table(tbIndex).insert(childs, insertIdxOpt).run(function(err) {
            if (err) return callback(err);
            asyn.each(childs, indexChildsR, function(err) {
                if (err) return callback(err);
                return callback(null);
            });
        });
    });
}


function editFullName(input, fullname, newName) {
    // "/587b1f95ebf7361b0030bce6/u/5876ef55d0d0a9121ab7d03f/home/d1/"
    var parent = p.dirname(fullname);
    return input.replace(fullname, p.join(parent, newName, "/"));
}


function updateNodeName(nodeID, newName, callback) {
    cm.getNodeIndexByID(nodeID, function(err, idx) {
        if (err) return callback(err);
        if (!idx) return callback(null);

        getRelatedIndex(idx.fullname, function(err, r) {
            if (err) return callback(err);
            asyn.map(r, function(item, cb) {
                // console.log("rename: " + JSON.stringify(item));
                re.table(tbIndex).get(item.id).update({
                    fullname: editFullName(item.fullname, idx.fullname, newName)
                }).run(cb);
            }, function(err) {
                if (err) return callback(err);
                return callback(null);
            });
        });
    });
}

module.exports = {
    getRelatedIndex,
    deleteRelatedIndex,
    deleteRelatedIndexByID,
    createIndex,
    createIndexForNode,
    buildIndex,
    updateNodeName
}
