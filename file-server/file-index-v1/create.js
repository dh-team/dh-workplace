var config = require("../config");
var re = require("./rethink-db");
var cm = require("./common");
var idx = require("./index-mgr");
var log = require("../logger");

var tbNode = config.db_table.tbNode;
var tbIndex = config.db_table.tbIndex;

/* insert node
    opt: {
        exist_action: 'ignore' | 'error' | 'rename' | 'overwrite'
        ignore_exist: false,
        auto_rename: false  // rename when exist
    },
    callback(err, createdID)
*/

function createNode(node, opt, callback) {
    var newNode = cm.nodeObj(node);
    log.debug("create node " + JSON.stringify(newNode));

    // check node exist
    cm.getNode(newNode, function(err, r) {
        if (err) return callback(err);
        if (!r) {
            return (newNode.type === "file") ? doInsertNode(newNode, callback) : doInsertDir(newNode, callback);
        }

        if (opt && opt.exist_action === "ignore") return callback(null, r.id);
        if (opt && opt.exist_action === "rename") {
            return cm.autoRename(newNode.name, newNode.parent, function(err, newName) {
                if (err) return callback(err);
                log.debug("node name: " + newNode.name + " is exist, change to: " + newName);
                newNode.name = newName;
                return (newNode.type === "file") ? doInsertNode(newNode, callback) : doInsertDir(newNode, callback);
            });
        }
        if (newNode.type === "file" && opt && opt.exist_action === "overwrite") {
            doOverwriteFile(r, newNode, function(err) {
                if (err) return callback(err);
                return callback(null, r.id);
            });
        }
        return callback("already_exist");
    });
}

function doInsertNode(node, callback) {
    re.table(tbNode).insert(node).run(function(err, r) {
        if (err) return callback(err);
        if (!r.generated_keys || r.generated_keys.length == 0) return callback("db_insert_err");

        callback(null, r.generated_keys[0]);
    });
}

function doInsertDir(node, callback) {
    doInsertNode(node, function(err, insertedID) {
        if (err) return callback(err);
        // set generated id for this node
        node.id = insertedID;
        log.debug("inserted dir node " + JSON.stringify(node));
        idx.createIndexForNode(node, function(err) {
            if (err) return callback(err);
            callback(null, insertedID);
        });
    });
}

function doOverwriteFile(currentNode, newFile, callback) {

    // add new version
    var version = {
        version_number: currentNode.versions.length + 1,
        size: currentNode.size,
        create_time: new Date(),
        location: currentNode.location,
        description: newFile.description
    };

    log.debug("add version: " + JSON.stringify(version) + " for " + currentNode.id);

    re.table(tbNode).get(currentNode.id).update({
        size: newFile.size,
        edit_time: new Date(),
        mime_type: newFile.mime_type,
        "versions": re.row("versions").append(version)
    }).run(callback);
}


module.exports = {
    createNode: createNode
}
