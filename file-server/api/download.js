var contentDisposition = require('content-disposition');
const p = require("path");
var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function download(req, res) {
    var params = helper.extractObject(req, ['id', 'version']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] download: " + JSON.stringify(params));

    if (params.id) {
        fm.getFileByID(params, auth, function(err, r) {
            response(res, err, r);
        });
    }
    else {
        return response(res, {
            code: 'param_invalid',
            message: 'Request must provide "id"'
        });
    }
}

function response(res, err, r) {
    if (err) {
        return helper.responseInvalid(res, err);
    }
    
    var fileName = r.file.name;
    if (r.file.selected_version) {
        var ext = p.extname(fileName);
        var basename = p.basename(fileName, ext);
        fileName = basename + "-v" + r.file.selected_version.version_number + ext;
    }

    res.setHeader('Content-Length', r.file.size);
    res.setHeader('Content-Type', r.file.mime_type);
    res.setHeader('Content-Disposition', contentDisposition(fileName));
    r.stream.on('error', function(err) {
        return helper.responseInvalid(res, err);
    });
    r.stream.pipe(res);
}

module.exports = {
    download: [auth.spaceAuthSuccess, download]
}
