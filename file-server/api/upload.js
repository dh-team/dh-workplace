var asyn = require("async");
var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

var multiparty = require('multiparty');


function upload(req, res) {
    res.set("Content-Type", "text/plain"); // text/plain is required to ensure support for IE9 and older

    var auth = helper.extractAuthInfo(req);
    try {
        var params = JSON.parse(req.headers["args"]);
    }
    catch (err) {
        return helper.responseInvalid(res, err);
    }

    log.debug("[api] upload file, context:" + JSON.stringify(auth) + ", Args: " + JSON.stringify(params));

    var form = new multiparty.Form();
    form.parse(req, function(err, fields, files) {
        if (err) return helper.responseInvalid(res, err);
        log.debug("uploaded: " + JSON.stringify(fields) + ", " + JSON.stringify(files));

        if (files.file) {
            handleSimpleUpload(auth, params, files.file, res);
        }
        else {
            res.json({
                error: "invalid_input_name"
            })
        }
    });
}

function handleSimpleUpload(auth, params, files, res) {
    asyn.map(files, function(item, cb) {
        handleUploadedFile(auth, params, item, cb);
    }, function(err, r) {
        var responseData = {
            success: false
        };
        if (err) {
            log.debug("add file error: " + JSON.stringify(err));
            responseData.error = err;
            res.send(responseData);
        }
        else {
            log.debug("add file success: " + JSON.stringify(r));
            responseData.success = true;
            responseData.uploaded = r;
            res.send(responseData);
        }
    });
}

function handleUploadedFile(auth, params, file, callback) {
    // create node
    fm.addFile({
        file: {
            name: file.originalFilename,
            mime_type: file.headers["content-type"],
            size: file.size,
            tmp_path: file.path
        },
        target: {
            app: params.app,
            partition: params.partition,
            parent: params.parent
        },
        opt: {
            exist_action: params.exist_action || "rename"
        }
    }, auth, callback)
}

module.exports = {
    upload: [auth.spaceAuthSuccess, upload]
}
