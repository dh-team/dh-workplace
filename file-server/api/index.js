module.exports = Object.assign(
    {},
    require("./dir-content"),
    require("./create-dir"),
    require("./trash"),
    require("./upload"),
    require("./rename"),
    require("./download"),
    require("./get"),
    require("./move")
);
