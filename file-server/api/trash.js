var asyn = require("async");
var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function getTrash(req, res) {
    var params = helper.extractObject(req, ["app", "partition"]);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] get trash content: " + JSON.stringify(params));

    fm.getTrash(params, auth, function(err, r) {
        return helper.response(res, err, r);
    });
}

function deleteNode(req, res) {
    var params = helper.extractObject(req, ['id', 'ids']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] delete node, param:" + JSON.stringify(params));

    if (params.id) {
        fm.deleteNodeByID(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else if (params.ids) {
        asyn.map(params.ids, function(id, cb) {
            fm.deleteNodeByID({
                id
            }, auth, cb);
        }, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        return helper.responseInvalid(res, {
            code: 'param_invalid',
            message: 'Request must provide "id" or "ids"'
        });
    }
}

function deletePermanentNode(req, res) {
    var params = helper.extractObject(req, ['id', 'ids']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] delete permanent node, param:" + JSON.stringify(params));

    if (params.id) {
        fm.deletePermanentNodeByID(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else if (params.ids) {
        asyn.map(params.ids, function(id, cb) {
            fm.deletePermanentNodeByID({
                id
            }, auth, cb);
        }, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        return helper.responseInvalid(res, {
            code: 'param_invalid',
            message: 'Request must provide "id" or "ids"'
        });
    }
}

function restoreNode(req, res) {
    var params = helper.extractObject(req, ['id', 'ids']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] restore node, param:" + JSON.stringify(params));

    if (params.id) {
        fm.restoreNodeByID(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else if (params.ids) {
        asyn.map(params.ids, function(id, cb) {
            fm.restoreNodeByID({
                id
            }, auth, cb);
        }, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        return helper.responseInvalid(res, {
            code: 'param_invalid',
            message: 'Request must provide "id" or "ids"'
        });
    }
}

function emptyTrash(req, res) {
    var params = helper.extractObject(req, ['app', 'partition']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] empty trash :" + JSON.stringify(auth.location));

    fm.emptyTrash(params, auth, function(err) {
        return helper.response(res, err, {
            success: true
        });
    });
}


module.exports = {
    getTrash: [auth.spaceAuthSuccess, getTrash],
    deleteNode: [auth.spaceAuthSuccess, deleteNode],
    deletePermanentNode: [auth.spaceAuthSuccess, deletePermanentNode],
    restoreNode: [auth.spaceAuthSuccess, restoreNode],
    emptyTrash: [auth.spaceAuthSuccess, emptyTrash]
}
