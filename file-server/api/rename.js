var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function rename(req, res) {
    var params = helper.extractObject(req, ['id', 'new_name']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] rename, param:" + JSON.stringify(params));

    if (params.id && params.new_name) {
        fm.rename(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        return helper.responseInvalid(res, {
            code: 'param_invalid',
            message: 'Request must provide "id" and "new_name"'
        });
    }
}


module.exports = {
    rename: [auth.spaceAuthSuccess, rename]
}
