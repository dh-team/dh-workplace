var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function createDir(req, res) {
    const param = helper.extractObject(req, ['name', 'parent', 'app', 'partition']);
    const auth = helper.extractAuthInfo(req);

    log.debug("[api] create dir, param:" + JSON.stringify(param));

    if (param.parent) {
        // create new dir in parent dir
        fm.createDir(param, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        // create new dir in root dir
        fm.createDirInRoot(param, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
}

module.exports = {
    createDir: [auth.spaceAuthSuccess, createDir]
}
