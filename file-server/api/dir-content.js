var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function getDirContent(req, res) {
    var params = helper.extractObject(req, ["id", "app", "partition"]);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] get dir content: " + JSON.stringify(params));

    if (params.id) {
        fm.getDirContentByID(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        fm.getRootDirContent(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
}

module.exports = {
    getDirContent: [auth.spaceAuthSuccess, getDirContent]
}
