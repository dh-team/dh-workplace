var asyn = require("async");
var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function move(req, res) {
    const params = helper.extractObject(req, ['target', 'ids', 'id']);
    const auth = helper.extractAuthInfo(req);

    log.debug("[api] move, param:" + JSON.stringify(params));

    if (params.id) {
        fm.moveNodeById(params, auth, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else if (params.ids) {
        asyn.map(params.ids, function(id, cb) {
            fm.moveNodeById({
                target: params.target,
                id: id
            }, auth, cb);
        }, function(err, r) {
            return helper.response(res, err, r);
        });
    }
    else {
        return helper.responseInvalid(res, {
            code: 'param_invalid',
            message: 'Request must provide "id" or "ids"'
        });
    }
}

module.exports = {
    move: [auth.spaceAuthSuccess, move]
}
