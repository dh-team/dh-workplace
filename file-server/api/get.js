var auth = require("../resource-auth");
var fm = require('../file-manage');
var helper = require("../helper");
var log = require("../logger");

function getFile(req, res) {
    var params = helper.extractObject(req, ['id']);
    var auth = helper.extractAuthInfo(req);
    log.debug("[api] get: " + JSON.stringify(params));

    if (params.id) {
        fm.getFileByID(params, auth, function(err, r) {
            response(res, err, r);
        });
    }
    else {
        return response(res, {
            code: 'param_invalid',
            message: 'Request must provide "id"'
        });
    }
}

/**
 * fileData { file: file, stream: stream } 
 */
function response(res, err, fileData) {
    if (err) {
        return helper.responseInvalid(res, err);
    }

    res.setHeader('Content-Length', fileData.file.size);
    res.setHeader('Content-Type', fileData.file.mime_type);
    fileData.stream.on('error', function(err) {
        return helper.responseInvalid(res, err);
    });
    fileData.stream.pipe(res);
}

module.exports = {
    getFile: [auth.spaceAuthSuccess, getFile]
}
