var p = require("path");
var config = require("../../config");

function absPath(path) {
    return p.join(config.fs.path, path || "");
}

// fileParams {space, app, partition, location}
function filePath(fileParams) {
    return p.join(fileParams.space, fileParams.app, fileParams.location);
}

function absFilePath(fileParams) {
    return absPath(filePath(fileParams));
}


module.exports = {
    absPath: absPath,
    filePath: filePath,
    absFilePath: absFilePath
};
