var asyn = require("async");
const uuidV4 = require('uuid/v4');
const fse = require("fs-extra");
var fs = require('fs');
const p = require("path");
const constant = require("../../constant");
var pr = require('./path-resolve');

/**
 * fileParams {space, app, partition, location} 
 */

/**
 * create storage for space
 *  space:  id of space
 *  callback: function(err, success)
 *      success: true if creation successfully
 */
function createStorage(space, callback) {
    asyn.eachSeries(["user", "system"],
        function(item, cb) {
            createDir(p.join(space, item), cb);
        },
        function(err, results) { // final callback
            if (err) return callback(err);
            callback(null, true);
        });
}

// create dir recursive
function createDir(path, callback) {
    var fullPath = pr.absPath(path);
    fse.mkdirs(fullPath, function(err) {
        if (err) return callback(err);
        return callback(null, path);
    });
}

/**
 * add file from temporary file, remove this temporary file when finished
 *  tmpFile:  abs path of temp file
 *  fileParams: object      
 *  callback: function(err, location)
 *      location: location of added file
 */
function addFileFromTmp(tmpFile, fileParams, callback) {
    if (!fileParams.location) fileParams.location = uuidV4();
    var dstPath = pr.absFilePath(fileParams);
    fse.move(tmpFile, dstPath, function(err) {
        if (err) return callback(err);
        return callback(null, fileParams.location);
    });
}

/**
 * remove file
 *  fileParams: object
 *  callback: function(err, location)
 *      location: location of removed file
 */
function removeFile(fileParams, callback) {
    var dstPath = pr.absFilePath(fileParams);
    fse.remove(dstPath, function(err) {
        if (err) return callback(err);
        return callback(null, fileParams.location);
    });
}


/**
 * get file
 *  fileParams: object
 *  callback: function(err, stream)
 *      stream: ReadStream of file
 */
function getFile(fileParams, callback) {
    var absPath = pr.absFilePath(fileParams);
    fs.exists(absPath, function(exists) {
        if (!exists) return callback(constant.err.not_exist);
        callback(null, fs.createReadStream(absPath));
    });
}

module.exports = {
    createStorage: createStorage,
    addFileFromTmp: addFileFromTmp,
    removeFile: removeFile,
    getFile: getFile
}
