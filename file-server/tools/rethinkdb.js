const fse = require("fs-extra");
const asyn = require("async");
const p = require("path");
var config = require('../config');
var re = require('rethinkdbdash')({
    servers: [config.rethinkdb]
});
var tbNode = config.db_table.tbNode;

var defaultCallback = function(err, r) {
    if (err) {
        console.log(JSON.stringify(err));
    }
    else {
        // console.log(JSON.stringify(r));
        console.log(r);
    }
}

module.exports.tableList = function(callback) {
    if (!callback) callback = defaultCallback;
    re.tableList().run(callback);
}

module.exports.dbDrop = function(callback) {
    if (!callback) callback = defaultCallback;
    re.dbDrop(config.rethinkdb.db).run(callback);
}

module.exports.listNodes = function(callback) {
    if (!callback) callback = defaultCallback;
    re.table(tbNode).filter({}).run(callback);
}

function migrateTableNodes() {
    re.table(tbNode).filter({
        app: null
    }, {
        default: true
    }).run(function(err, r) {
        if (err) return console.log(err);

        console.log("found " + r.length + " old nodes");
        var sysDirs = {};
        r.map(function(node) {
            if (node.type == "dir" && node.node_type == "system") {
                sysDirs[node.id] = node;
            }
            if (node.type == "file") {
                var oldFile = p.join(config.fs.path, node.space, node.partition.substr(0, 1), node.partition.substr(2), node.node_type, node.id + p.extname(node.name));
                console.log("file: " + oldFile);
            }
        });

        asyn.eachSeries(r, function(node, cb) {
            updateNode(node, sysDirs, cb);
        }, function(err) {
            if (err) return console.log(err);
            console.log("success!!!");
        })
    });
}

function updateNode(node, sysDirs, callback) {
    var update;
    if (node.type == "dir") {
        if (node.node_type == "system") {
            update = {
                is_deleted: true // mark to delete
            }
        }
        else {
            // user created dir
            update = {
                app: "FileManager",
                partition: node.partition.replace("/", ":"),
                parent: node.parent,

                node_type: null,
                permission: null,
                is_deleted: false
            }
            if (update.parent && sysDirs[update.parent]) update.parent = null;
        }
        re.table(tbNode).get(node.id).update(update).run(function(err) {
            if (err) return callback(err);
            console.log("updated dir " + node.id);
            callback();
        });
    }
    else if (node.type == "file") {
        update = {
            app: node.node_type == "system" ? "Wiki" : "FileManager",
            partition: node.partition.replace("/", ":"),
            parent: node.parent,

            location: node.id,

            node_type: null,
            permission: null,
            versions: [],
            is_deleted: false
        }
        if (update.parent && sysDirs[update.parent]) update.parent = null;

        // copy file
        var oldFile = p.join(config.fs.path, node.space, node.partition.substr(0, 1), node.partition.substr(2), node.node_type, node.id + p.extname(node.name));
        var newFile = p.join(config.fs.path, node.space, update.app, node.id);
        fse.mkdirs(p.dirname(newFile), function(err) {
            if (err) return callback(err);

            fse.copy(oldFile, newFile, function(err) {
                if (err) console.log(err);
                console.log("copy: " + oldFile + " to: " + newFile);
                // update node
                re.table(tbNode).get(node.id).update(update).run(function(err) {
                    if (err) return callback(err);
                    console.log("updated file: " + node.id);
                    callback();
                });
            });
        });
    }
    else {
        console.log("node type invalid: " + node.type);
        return process.nextTick(function() {
            callback();
        });
    }
}

function removeDeleted() {
    re.table(tbNode).filter({
        "is_deleted": true
    }).delete().run(function(err, r) {
        if (err) return console.log(err);
        console.log(r);
    })
}

module.exports.defaultCallback = defaultCallback;
module.exports.migrateTableNodes = migrateTableNodes;
module.exports.removeDeleted = removeDeleted;
