/*jslint node: true */
'use strict';

var p = require("path");

module.exports = require("./conf.json");
//
// The configuration options of the server
//

/**
 * Session configuration
 *
 * type - The type of session to use.  MemoryStore for "in-memory",
 * or MongoStore for the mongo database store
 * maxAge - The maximum age in milliseconds of the session.  Use null for
 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
 * for a year.
 * secret - The session secret that you should change to what you want
 * dbName - The database name if you're using Mongo
 */
module.exports.session = {
    type: "MemoryStore",
    maxAge: 3600000 * 24 * 7 * 52,
    //TODO You need to change this secret to something that you choose for your secret
    secret: "PxGZT2bJpGyrFddDphNW",
    dbName: "dh-file-server-session"
};

module.exports.fs = {
    path: p.resolve(__dirname, "./content")
};

module.exports.rethinkdb = {
    host: "localhost",
    port: 28015,
    authKey: "",
    db: "file_db"
};

module.exports.db_table = {
    tbNode: "nodes",
    tbIndex: "nodes_idx"
};
