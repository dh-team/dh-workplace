'use strict';

var https = require('https');
var http = require('http');
var path = require('path');
var fs = require('fs');

var express = require('express');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var config = require('./config');
var auth = require('./resource-auth');
auth.configure({
    authServerUrl: config.authServer.url
});


var fapi = require("./api");

// Express configuration
var app = express();
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Routing
app.post('/fs/dir-content', fapi.getDirContent);
app.get('/fs/dir-content', fapi.getDirContent);

app.post('/fs/create-dir', fapi.createDir);
app.get('/fs/create-dir', fapi.createDir);

app.post('/fs/rename', fapi.rename);
app.get('/fs/rename', fapi.rename);

app.post('/fs/delete', fapi.deleteNode);
app.get('/fs/delete', fapi.deleteNode);

app.post('/fs/delete-permanent', fapi.deletePermanentNode);
app.get('/fs/delete-permanent', fapi.deletePermanentNode);

app.post('/fs/restore', fapi.restoreNode);
app.get('/fs/restore', fapi.restoreNode);

app.post('/fs/upload', fapi.upload);

app.post('/fs/download', fapi.download);
app.get('/fs/download', fapi.download);

app.post('/fs/empty-trash', fapi.emptyTrash);
app.get('/fs/empty-trash', fapi.emptyTrash);

app.post('/fs/trash', fapi.getTrash);
app.get('/fs/trash', fapi.getTrash);

app.post('/fs/file', fapi.getFile);
app.get('/fs/file', fapi.getFile);

app.post('/fs/move', fapi.move);
app.get('/fs/move', fapi.move);

// Create HTTP | HTTPS server
if (config.fileServer.type === 'http') {
    http.createServer(app).listen(config.fileServer.port, "0.0.0.0");
}
else {
    var options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.fileServer.port, "0.0.0.0");
}
console.log("File Server started on port " + config.fileServer.port);
