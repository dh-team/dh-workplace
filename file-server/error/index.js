function handleError(err, req, res, next) {
    res.status(err.httpStatus || 500);
    return res.json({
        status: 'error',
        error: err.error || convertError(err)
    });
};

function convertError(sysErr) {
    var retErr = {};
    switch (sysErr.code) {
        case 'ENOENT':
            retErr = {
                code: 'resource_not_exist',
                message: 'Resource not exist'
            }
            break;

        default:
            // code
    }

    retErr.trace = sysErr;
    return retErr;
}

module.exports.handleError = handleError;
