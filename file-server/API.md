            File Server API         
************************************
- "/get-meta"
- "/dir-content"
- "/copy"
- "/move"
- "/delete"
- "/restore"
- "/delete-permanent"
- "/empty-trash"
- "/upload"
- "/search"
