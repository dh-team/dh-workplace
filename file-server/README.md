                File Server
******************************************

1. High level design

# Modules
    + API: REST api for managing file in space
    + File Manage: do auth, business
    + File Index: manage meta data, indexing of file, using Rethinkdb
    + File Storage: store real file in storage, storage can be local disk or Amazon S3

2. File Index desgin

# table "nodes": store files and directories metadata
    {
        id: String, // auto generated id
        
        space: String, // id of space
        app: String, // name of application that create file ("FileManager" | "Wiki" | "LivePost" | "FilePreview" | ...), default is "FileManager"
        partition: String, // partition that file belong to, "g:GROUP_ID" - partition of group, "u:USER_ID" - partition of user, "g:all" - partition of this space
        
        device: String, // device that stores this file, "local" - Local Disk, "amazons3" - Amazon S3
        location: String, // location in device
        
        type: String, // 'file' | 'dir',
        node_type: String, // "system" | "user", "system" - mark node is system node, user can not see, modify, delete it
        name: String, // name of file
        parent: String, // id of parent node
        
        permision: {
            access_scope: {allow: [], deny: []}, // ["g:GROUP_ID", "u:USER_ID", ...]
            modify_scope: {allow: [], deny: []},
            manage_scope: {allow: [], deny: []}
        }, // access control for this file, default: null - creator can modify and manage, users of partition can access  
        
        create_user: String, // user who created file - {user_id, uname}
        create_time: Date, // created time
        access_time: Date, // access time
        edit_time: Date, // modify content time
        
        size: Number, // in byte
        mime_type: String,
        description: String,
        tags: ['tag1', 'tag2'],
        attributes: JSON, // user's custom attributes
        
        versions: [{
            version_number: Number,
            size: Number, 
            create_time: Date, 
            location: String, 
            description: String
        }]
        
        is_deleted: Boolean // true - mark this node is deleted (do not display in view, display in trash)
    }


2. File Storage design

# Local device:
    + ROOT
        |-- SPACE_ID
        |----- APP
        |---------- File key
        |---------- File key
        |---------- ...
        |----- ...
        |-- ...
        
# Amazon S3    

3. File Manage design

# Create storage:
    + create storage for space
    
# Create file:
    + check permission to access partition 
    + get parent node
    + check parent node exist
    + check permision to 'modify' parent node
    + store file in storage: return file location
    + check file name exist: exist action
        - Ignore: return exist file 
        - Rename: rename uploaded file to not exist name
        - Overwrite: add new version of file, set uploaded file to current version
    + insert file to table "nodes"
    
# Delete file:
    + get file node
    + check file exist
    + check permision to 'manage' file
    + Set "is_deleted" = true

# Restore node:
    + get file node
    + check file exist
    + check permision to 'manage' file
    + Set "is_deleted" = false
    + Check file name exist: exist action - Rename this restored file

# Delete permanent node:
    + get file node
    + check file exist
    + check permision to 'manage' file
    + delete permanent file node in table "nodes"
    + delete permanent file in storage

4. API design
- Get home page
curl http://localhost:8082/ -H "Authorization: Bearer Rh9crOGqzvfpZb48sftUOJcFS96nWkr86Mf1nfB5pn719RDeQaR4G5Sl2Lymf9Lc" | jq .

# List dir content
    + URL: /fs/dir-content
    + Method: POST | GET
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    Get root:
        - app
        - partition
        
    By ID:    
        - id: 'DIR_ID'

    + Returns: [node]
    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Request resource is not exist'
        }

        {
            error: 'deleted',
            error_detail: 'Request resource is deleted'
        }
    + Example: 

# Create dir
    + URL: /fs/create-dir
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    To root dir:
        - app
        - partition
        - name
    To parent dir:
        - parent // id of parent dir 
        - name

    + Returns: created node 
        
    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Parent directory is not found'
        }

        {
            error: 'name_invalid',
            error_detail: "Directory's name is not valid"
        }

        {
            error: 'already_exist',
            error_detail: "Directory is exist"
        }

# Get metadata of a file or directory
    + URL: /fs/get-meta
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    By ID:
        - id: 'DIR_ID|FILE_ID'

    + Returns: found node
    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Request resource is not exist'
        }


# Delete file or directory
    + URL: /fs/delete
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    By ID:
        - id: 'DIR_ID|FILE_ID'

    + Returns: deleted node
    + Errors:
        {
            error: 'not_exist',
            error_detail: 'File or directory is not found'
        }

# Restore deleted file or directory
    + URL: /fs/restore
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    By ID:
        - id: 'DIR_ID|FILE_ID'

    + Returns: restored node

    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Source file or directory is not found'
        }

# Delete file or directory permanently
    + URL: /fs/delete-permanent
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters:
    By ID:
        - id: 'DIR_ID|FILE_ID'

    + Returns:
        {
            deleted_count: Integer
        }

    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Source file or directory is not found'
        }

# Empty trash
    + URL: /fs/empty-trash
    + Method: GET
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
    + Parameters: NO

    + Returns:
        {
            deleted_count: Integer
        }

    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Source file or directory is not found'
        }

# Upload file to directory
    + URL: /fs/upload
    + Method: POST
    + Header:
        - Authorization: Bearer SPACE_ACCESS_TOKEN
        - Args: '{}'
    + Parameters:
    Args:
        To root dir:
            - app
            - partition
        To parent dir:
            - parent: id of parent dir
        - name: 'example-file.txt'
        - exist_action: 'ignore' | 'error' | 'rename' | 'overwrite'
        
    Data Binary: local file

    + Returns: created file meta

    + Errors:
        {
            error: 'not_exist',
            error_detail: 'Parent directory is not found'
        }
        
        {
            error: 'name_invalid',
            error_detail: "File's name is not valid"
        }

        {
            error: 'already_exist',
            error_detail: "File is exist"
        }

5. Reference
- Linux inode: "http://teaching.idallen.com/cst8207/13w/notes/450_file_system.html"
- Linux inode structure
    {
        dev: 2114,
        ino: 48064969,
        mode: 33188,
        nlink: 1,
        uid: 85,
        gid: 100,
        rdev: 0,
        size: 527,
        blksize: 4096,
        blocks: 8,
        atime: Mon, 10 Oct 2011 23:24:11 GMT,
        mtime: Mon, 10 Oct 2011 23:24:11 GMT,
        ctime: Mon, 10 Oct 2011 23:24:11 GMT,
        birthtime: Mon, 10 Oct 2011 23:24:11 GMT
    }

6. Examples
curl http://localhost:8082/fs/get/path?childs=true -H "Authorization: Bearer uJ9WcBpWOEOKo5Hv3yZp3tKlQHXWiCh7l5kbqopKHxjFf8rqL1ifqEa5lcO0cgDn" | jq .
curl http://localhost:8082/fs/create_dir -d "path=d1/d11" -H "Authorization: Bearer 32ssbv1MZI0tbhH09MArRnfSJ1I1CjAUmPrKyVpHDUfFgFOV72llV65YFUk13s6c" | jq .

- Get dir content:
curl http://localhost:8080/fs/dir-content?path=/ -H "Authorization: Bearer fviRseGb7WuXvonXSv7RikH6w5Jat8F9GP7NTK2DolZFC3DZEp6hs3qW7oVqrZ6u" | jq .

- Create dir:
curl http://localhost:8080/fs/create-dir?path=d1 -H "Authorization: Bearer Y98V0vYdMM2t2jHqCXPVe8ZoV2cEoWOUWiShHpI6WjkA2J0IXNWQjeEF8jX9Q2L6" | jq .

- Delete dir:
curl http://localhost:8080/fs/delete?id=b07a74e7-9382-4a6f-829b-0bb4416645b0 -H "Authorization: Bearer fviRseGb7WuXvonXSv7RikH6w5Jat8F9GP7NTK2DolZFC3DZEp6hs3qW7oVqrZ6u" | jq .
