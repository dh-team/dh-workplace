module.exports = {
    err: {
        not_exist: "not_exist",
        already_exist: "already_exist",
        permission_deny: "permission_deny",
        deleted: "deleted"
    },
    act: {
        access: "access",
        modify: "modify",
        manage: "manage"
    }
}
