const asyn = require("async");
const constant = require("../constant");
const fin = require("../file-index");
const fst = require("../file-storage");
const cm = require("./common");
const log = require("../logger");

// param: {app, partition}
function getTrash(params, auth, callback) {
    // check permission to access partition
    var pErr = cm.checkInScope(params.partition, auth);
    if (pErr) return callback(pErr);

    // get content
    fin.filterNodes({
        space: auth.space_id,
        app: params.app,
        partition: params.partition,
        is_deleted: true
    }, "name", function(err, childs) {
        if (err) return callback(err);
        callback(null, cm.auditNodes(childs));
    });
}

// param: {id, opt}
function deleteNodeByID(params, auth, callback) {
    // get node
    cm.getByIdAndValidate({
        id: params.id,
        auth: auth,
        permission: constant.act.manage
    }, function(err, foundNode) {
        if (err) return callback(err);
        if (foundNode.is_deleted) return callback(null, foundNode);

        // all thing valid, mark node is deleted
        fin.updateNodeByID(foundNode.id, {
            is_deleted: true
        }, function(err) {
            if (err) return callback(err);

            foundNode.is_deleted = true;
            return callback(null, cm.auditNode(foundNode));
        });
    });
}

// param: {id, opt}
function deletePermanentNodeByID(params, auth, callback) {
    // get node
    cm.getByIdAndValidate({
        id: params.id,
        auth: auth,
        permission: constant.act.manage
    }, function(err, foundNode) {
        if (err) return callback(err);
        // all thing valid, do delete
        doDeletePermanentNode(foundNode, callback);
    });
}

function doDeletePermanentNode(node, callback) {
    if (node.type === "file") {
        fin.deletePermanentFileByID(node.id, function(err, deleted) {
            if (err || !deleted) return callback(err, deleted);
            log.debug("deleted file: " + JSON.stringify(node));
            // delete file in storage
            fst.removeFile(node, function(err, deletedFile) {
                if (err) return callback(err);
                log.debug("removed file in storage: " + deletedFile);
                return callback(null, node);
            });
        });
    }
    else if (node.type === "dir") {
        fin.deletePermanentDirByID(node.id, function(deletedChild) {
            log.debug("deleted child: " + JSON.stringify(deletedChild));
            if (deletedChild.type === "file") {
                fst.removeFile(deletedChild, function(err, deletedFile) {
                    if (err) log.debug("remove file in storage error: " + deletedChild.location + ", " + JSON.stringify(err));
                    else log.debug("remove file in storage: " + deletedFile);
                });
            }
        }, function(err, deleted) {
            if (err || !deleted) return callback(err, deleted);
            log.debug("deleted dir: " + JSON.stringify(deleted));
            return callback(null, node);
        });
    }
}

// param: {id, opt}
function restoreNodeByID(params, auth, callback) {
    // get node
    cm.getByIdAndValidate({
        id: params.id,
        auth: auth,
        permission: constant.act.manage
    }, function(err, foundNode) {
        if (err) return callback(err);
        if (!foundNode.is_deleted) return callback(null, foundNode);

        // get available name
        cm.getAvailableName(foundNode, function(err, availableName) {
            if (err) return callback(err);

            // all thing valid, mark node is not deleted
            fin.updateNodeByID(foundNode.id, {
                name: availableName,
                is_deleted: false
            }, function(err) {
                if (err) return callback(err);

                foundNode.name = availableName;
                foundNode.is_deleted = false;
                return callback(null, cm.auditNode(foundNode));
            });
        });
    });
}

function emptyTrash(params, auth, callback) {
    // check permission to manage partition
    var pErr = cm.checkInScope(params.partition, auth);
    if (pErr) return callback(pErr);

    // get deleted item
    fin.filterNodes({
        space: auth.space_id,
        app: params.app,
        partition: params.partition,
        is_deleted: true
    }, null, function(err, childs) {
        if (err) return callback(err);
        if (!childs || childs.length === 0) return callback(null, 0);
        log.debug("found " + childs.length + " items in trash, delete now");
        asyn.each(childs, function(item, cb) {
            if (cm.checkPermission(constant.act.manage, item, auth)) return cb(null);
            doDeletePermanentNode(item, cb);
        }, function(err) { // final callback
            callback(err);
        });
    });
}

module.exports = {
    getTrash: getTrash,
    deleteNodeByID: deleteNodeByID,
    deletePermanentNodeByID: deletePermanentNodeByID,
    restoreNodeByID: restoreNodeByID,
    emptyTrash: emptyTrash
}
