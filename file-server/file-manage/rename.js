const constant = require("../constant");
const fin = require('../file-index');
const cm = require("./common");

// param: {id, new_name}
function rename(params, auth, callback) {
    // get node
    cm.getByIdAndValidate({
        id: params.id,
        not_deleted: true,
        permission: constant.act.manage,
        auth: auth
    }, function(err, node) {
        if (err) return callback(err);

        // set new name
        node.name = params.new_name;
        
        // check new name existed
        cm.checkNameDuplicate(node, function(err) {
            if (err) return callback(err);
            // all thing valid, do rename
            fin.updateNodeByID(node.id, {
                name: params.new_name
            }, function(err) {
                if (err) return callback(err);
                return callback(null, cm.auditNode(node));
            });
        });
    });
}

module.exports = {
    rename
}
