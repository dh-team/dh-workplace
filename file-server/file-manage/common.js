const p = require("path");
const helper = require("../helper");
const constant = require("../constant");
const fin = require('../file-index');

function auditNode(node) {
    // remove restricted properties
    delete node["device"];
    delete node["location"];
    delete node["node_type"];
    return node;
}

function auditNodes(nodes) {
    return nodes.map(function(item) {
        return auditNode(item);
    })
}

function checkInScope(scope, auth) {
    var grant = helper.extractScope(scope);
    if (grant.type === "u" && grant.id !== auth.user_id) {
        return constant.err.permission_deny;
    }

    if (grant.type === "g" && grant.id !== "all" && auth.groups.indexOf(grant.id) < 0) {
        return constant.err.permission_deny;
    }

    return null;
}

function checkAccessPermission(node, auth) {
    return checkInScope(node.partition, auth);
}

function checkModifyPermission(node, auth) {
    if (node.create_user && node.create_user.user_id !== auth.user_id) {
        return constant.err.permission_deny;
    }

    return null;
}

function checkPermission(action, node, auth) {
    if (node.space && (node.space !== auth.space_id)) {
        return constant.err.permission_deny;
    }

    switch (action) {
        case constant.act.access:
            return checkAccessPermission(node, auth);
        case constant.act.modify:
        case constant.act.manage:
            return checkModifyPermission(node, auth);

    }

    return null;
}

function getByIdAndValidate(validation, callback) {
    // get node
    fin.getNodeByID(validation.id, function(err, foundNode) {
        if (err) return callback(err);

        // check node exist
        if (!foundNode) return callback(constant.err.not_exist);

        // check type (file or dir)
        if (validation.type && foundNode.type != validation.type) return callback(constant.err.not_exist);

        // check node exist
        if (validation.not_deleted && !foundNode) return callback(constant.err.not_exist);

        // check permission
        if (validation.permission) {
            // check manage permission
            var pErr = checkPermission(validation.permission, foundNode, validation.auth);
            if (pErr) return callback(pErr);
        }

        callback(null, foundNode);
    });
}

function checkNameDuplicate(node, callback) {
    // check same name 
    var cond = makeFindByNameCond(node);
    fin.getSingleNode(cond, function(err, existedNode) {
        if (err) return callback(err);
        if (existedNode) return callback(constant.err.already_exist, existedNode);
        callback();
    });
}

/**
 * find node has same name
 *  node: {name, parent } | {name, space, app, partition }
 */ 
function getNodeByName(node, callback) {
    // check same name 
    var cond = makeFindByNameCond(node);
    fin.getSingleNode(cond, callback);
}

function makeFindByNameCond(node) {
    var cond = {
        is_deleted: false,
        name: node.name
    };
    if (node.parent) {
        cond.parent = node.parent;
    }
    else {
        cond.space = node.space;
        cond.app = node.app;
        cond.partition = node.partition;
        cond.parent = null;
    }
    return cond;
}

// rename exist node name: filename -> filename-1, ...
function getAvailableName(node, callback) {
    var cond = makeFindByNameCond(node);
    var name = node.name;
    fin.getSingleNode(cond, function(err, r) {
        if (err) return callback(err);
        if (!r) return callback(null, name); // this input name not exist

        var ext = p.extname(name);
        var basename = p.basename(name, ext);

        delete cond.name;
        cond.string_filter = [{
            field: "name",
            cond: "startswith",
            value: basename
        }]

        fin.filterNodes(cond, null, function(err, r) {
            if (err) return callback(err);
            var maxNum = 0;
            var tmp;
            r.map(function(item) {
                tmp = getNum(item.name);
                if (tmp > maxNum) maxNum = tmp;
            });
            return callback(null, basename + "-" + (++maxNum) + ext);
        });

    });
}

// filename-1.ext, filename-2.ext
function getNum(filename) {
    var basename = p.basename(filename, p.extname(filename));
    var idx = basename.lastIndexOf("-");
    if (idx < 0) return 0;
    var num = parseInt(basename.substr(idx + 1));
    return num >= 0 ? num : 0;
}


module.exports = {
    auditNode: auditNode,
    auditNodes: auditNodes,
    checkInScope: checkInScope,
    checkPermission: checkPermission,
    getByIdAndValidate: getByIdAndValidate,
    checkNameDuplicate: checkNameDuplicate,
    getNodeByName: getNodeByName,
    getAvailableName: getAvailableName
}
