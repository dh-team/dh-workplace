/*
 context: {
    location: { // location of file
        space,
        partition,
        root_dir
    },
    auth: { // auth info of current user
        user_id,
        space_id,
        uname,        
        groups,
        roles
    }
 }
 */

module.exports = Object.assign(
    {},
    require("./dir-content"),
    require("./create-dir"),
    require("./trash"),
    require("./add-file"),
    require('./rename'),
    require('./get'),
    require('./move')
);
