var fin = require('../file-index');
var cm = require("./common");
var log = require("../logger");
const constant = require("../constant");

/**
 * Create new dir in parent dir
 *  params: {name, parent}
 *  callback: function(err, createdDir)
 */
function createDir(params, auth, callback) {
    log.debug("[fm] create dir with name: " + JSON.stringify(params));

    // get parent dir
    cm.getByIdAndValidate({
        id: params.parent,
        type: "dir",
        not_deleted: true,
        permission: constant.act.access, // TODO: fix here, should be 'modify'
        auth: auth
    }, function(err, parentNode) {
        if (err) return callback(err);

        // check child name exist
        cm.getNodeByName({
            parent: params.parent,
            name: params.name
        }, function(err, existedNode) {
            if (err) return callback(err);
            if (existedNode) return callback(constant.err.already_exist);

            // all thing valid, create node
            doCreateDir(parentNode, params, auth, callback);
        });
    });
}

/**
 * Create new dir in root dir
 *  params: {name, app, partition}
 *  callback: function(err, createdDir)
 */
function createDirInRoot(params, auth, callback) {
    log.debug("[fm] create dir with name: " + JSON.stringify(params));

    // check permission to access partition
    var pErr = cm.checkInScope(params.partition, auth);
    if (pErr) return callback(pErr);

    // check child name exist
    cm.getNodeByName({
        space: auth.space_id,
        app: params.app,
        partition: params.partition,
        name: params.name
    }, function(err, existedNode) {
        if (err) return callback(err);
        if (existedNode) return callback(constant.err.already_exist);
        // all thing valid, create node
        doCreateDir({}, params, auth, callback);
    });
}


function doCreateDir(parentNode, params, auth, callback) {
    var newDir = {
        type: 'dir',
        name: params.name,
        parent: parentNode.id || null,
        space: parentNode.space || auth.space_id,
        app: parentNode.app || params.app,
        partition: parentNode.partition || params.partition,
        create_user: {
            user_id: auth.user_id,
            uname: auth.uname,
        }
    }

    fin.createNode(newDir, function(err, createdDir) {
        if (err) return callback(err);
        callback(null, createdDir);
    });
}

module.exports = {
    createDir: createDir,
    createDirInRoot: createDirInRoot
}
