const p = require("path");
const constant = require("../constant");
const cm = require("./common");
const fin = require("../file-index");

// param: {id}
function getDirContentByID(params, auth, callback) {
    // get dir
    fin.getDirByID(params.id, function(err, dir) {
        if (err) return callback(err);

        // check dir exist
        if (!dir) return callback(constant.err.not_exist);

        // check dir deleted
        if (dir.is_deleted) return callback(constant.err.deleted);

        // check access permission
        var pErr = cm.checkPermission(constant.act.access, dir, auth);
        if (pErr) return callback(pErr);

        // get dir content
        fin.filterNodes({
            parent: params.id,
            is_deleted: false
        }, "name", function(err, childs) {
            if (err) return callback(err);
            callback(null, cm.auditNodes(childs));
        });
    });
}

// param: {app, partition}
function getRootDirContent(params, auth, callback) {
    // check permission to access partition
    var pErr = cm.checkInScope(params.partition, auth);
    if (pErr) return callback(pErr);

    // get content
    fin.filterNodes({
        space: auth.space_id,
        app: params.app,
        partition: params.partition,
        parent: null,
        is_deleted: false
    }, "name", function(err, childs) {
        if (err) return callback(err);
        callback(null, cm.auditNodes(childs));
    });
}


module.exports = {
    getDirContentByID: getDirContentByID,
    getRootDirContent: getRootDirContent
}
