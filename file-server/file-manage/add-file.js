var p = require("path");
var fin = require('../file-index');
var fst = require('../file-storage');
var ip = require("./init-partition");
var cm = require("./common");
var log = require("../logger");
var helper = require("../helper");
const constant = require("../constant");

/**
 * add new file
 *   params: {
 *       file:{ name, mime_type, size, tmp_path },
 *       target: { parent, app, partition }
 *       opt: {exist_action: 'error' | 'overwrite' | 'rename'}
 *   }
 *   
 *  callback: function(err, newFile)
 */

function addFile(params, auth, callback) {
    log.debug("[fm] add new file: " + JSON.stringify(params));
    if (params.target.parent) {
        addFileToParentDir(params, auth, callback);
    }
    else {
        addFileToRootDir(params, auth, callback);
    }
}

function addFileToParentDir(params, auth, callback) {
    // find parent dir
    cm.getByIdAndValidate({
        id: params.target.parent,
        type: "dir",
        not_deleted: true,
        permission: constant.act.access, // TODO: fix here, should be 'modify'
        auth: auth
    }, function(err, targetDir) {
        if (err) return callback(err);

        var newFile = {
            parent: targetDir.id,
            space: targetDir.space,
            app: targetDir.app,
            partition: targetDir.partition
        }

        doAddFile(newFile, params, auth, callback);
    });
}

function addFileToRootDir(params, auth, callback) {
    // check permission to access partition
    var pErr = cm.checkInScope(params.target.partition, auth);
    if (pErr) return callback(pErr);

    var newFile = {
        space: auth.space_id,
        app: params.target.app,
        partition: params.target.partition
    }

    doAddFile(newFile, params, auth, callback);
}

function doAddFile(newFile, params, auth, callback) {
    // validate ok, insert file
    newFile.type = 'file';
    newFile.node_type = 'user';

    newFile.name = params.file.name;
    newFile.mime_type = params.file.mime_type;
    newFile.size = params.file.size;

    newFile.create_user = {
        user_id: auth.user_id,
        uname: auth.uname
    };

    // create file in storage
    fst.addFileFromTmp(params.file.tmp_path, newFile, function(err, location) {
        if (err) return callback(err);

        // update file location
        newFile.location = location;

        // check file name exist
        cm.getNodeByName(newFile, function(err, existedNode) {
            if (err) return callback(err);
            if (!existedNode) return fin.createNode(newFile, callback);

            // file name exist
            if (params.opt && params.opt.exist_action === "rename") {
                // create file with new name
                log.debug("create file with new name: " + JSON.stringify(newFile));
                cm.getAvailableName(newFile, function(err, availableName) {
                    if (err) return callback(err);
                    newFile.name = availableName;
                    return fin.createNode(newFile, callback);
                })
            }
            else {
                // create version of file
                log.debug("create version of file: " + JSON.stringify(newFile));
                fin.addFileVersion(existedNode, newFile, function(err, updated) {
                    if (err) return callback(err);
                    callback(null, updated);
                });
            }
        });
    });
}

module.exports = {
    addFile: addFile
}
