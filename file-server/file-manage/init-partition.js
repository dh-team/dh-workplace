var asyn = require("async");
var fin = require('../file-index');
var fst = require('../file-storage');
var cm = require("./common");
var log = require("../logger");

var createDirOpt = {
    ignore_exist: true
}
var createNodeOpt = {
    ignore_exist: true
}

function initPartition(location, callback) {
    asyn.series([
            function(cb) {
                createSystemDirsStorage(location, cb);
            },
            function(cb) {
                createSystemDirsIndex(location, cb);
            }
        ],
        // final callback
        function(err, results) {
            if (err) return callback(err);
            callback(null, true);
        });
}

function createSystemDirsStorage(location, callback) {
    fin.createStorage(location.space, callback);
}

function createSystemDirsIndex(location, callback) {
    asyn.eachSeries(["home", "trash", "app/app_wiki"],
        function(item, cb) {
            createSystemDirIndex(item, location, cb);
        },
        function(err, results) { // final callback
            if (err) return callback(err);
            callback(null, true);
        });
}

function createSystemDirIndex(dirName, location, done) {
    var dirs = dirName.split("/");
    asyn.reduce(dirs, null, function(parentID, item, cb) {
        doCreateSystemDirIndex(item, parentID, location, cb);
    }, done);
}

function doCreateSystemDirIndex(dirName, parentID, location, callback) {
    console.log("doCreateSystemDirIndex, name: " + dirName + ", parent: " + parentID)
    var newDir = {
        node_type: "system",
        type: 'dir',
        name: dirName,
        parent: parentID,
        space: location.space,
        partition: location.partition
    };
    fin.createNode(newDir, createNodeOpt, function(err, newDirID) {
        if (err) return callback(err);
        callback(null, newDirID);
    });
}

module.exports = {
    initPartition: initPartition,
    createSystemDirIndex: createSystemDirIndex,
    createSystemDirsIndex: createSystemDirsIndex
}
