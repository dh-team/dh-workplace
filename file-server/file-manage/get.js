const p = require("path");
const constant = require("../constant");
const fin = require('../file-index');
const fst = require('../file-storage');
const cm = require("./common");

/**
 * get file by id
 *  params: {id, version}
 *  callback: function(err, {file, stream})
 */
function getFileByID(params, auth, callback) {
    // get file
    fin.getFileByID(params.id, function(err, file) {
        if (err) return callback(err);

        // check file exist
        if (!file) return callback(constant.err.not_exist);

        // check file deleted: allow view deleted file
        // if (file.is_deleted) return callback(constant.err.deleted);

        // check access permission
        var pErr = cm.checkPermission(constant.act.access, file, auth);
        if (pErr) return callback(pErr);

        var fileParams = {
            space: file.space,
            app: file.app,
            partition: file.partition,
            location: file.location
        };
        // find location of older version
        if (params.version && file.versions) {
            var v = file.versions.find(function(item) {
                return item.version_number == params.version;
            });
            
            if (v) {
                fileParams.location = v.location;
                file.selected_version = v;
            }
        }

        if (file.device === "local") {
            fst.getFile(fileParams, function(err, stream) {
                if (err) return callback(err);
                callback(null, {
                    file: file,
                    stream: stream
                });
            });
        }
        else {
            return callback("device_not_support");
        }
    });
}

module.exports = {
    getFileByID: getFileByID
}
