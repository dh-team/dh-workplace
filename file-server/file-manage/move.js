const constant = require("../constant");
const fin = require('../file-index');
const cm = require("./common");

// params: {id, target}
function moveNodeById(params, auth, callback) {
    // find current node
    cm.getByIdAndValidate({
        id: params.id,
        not_deleted: true,
        permission: constant.act.manage,
        auth: auth
    }, function(err, node) {
        if (err) return callback(err);
        
        // find target
        
        // update parent field
        node.parent = params.target;
        
        // check name
        cm.getAvailableName(node, function(err, availableName) {
            if (err) return callback(err);
            
            node.name = availableName;
            fin.updateNodeByID(node.id, {
                parent: node.parent,
                name: node.name
            }, function(err){
                if (err) return callback(err);
                return callback(null, cm.auditNode(node));
            });
        })
        
    });

}

module.exports = {
    moveNodeById: moveNodeById
}
