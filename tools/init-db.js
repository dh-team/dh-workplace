var mongo = require("../common/tools/mongodb");
var rethink = require("../file-server/tools/rethinkdb");

mongo.initData(function(err) {
    if (err) console.log(JSON.stringify(err));
    console.log('++++ init mongodb data ok!');
});

rethink.dbDrop(function(err){
    if (err) console.log(JSON.stringify(err));
    console.log('++++ init rethinkdb data ok!');
})
