        Core API Server
********************************

1. Database design: mongodb

# "spaces"
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    name: {
        type: String,
        required: true
    },
    short_name: {
        type: String,
        required: false
    },
    url: {
        type: Array, // [String]
        required: true
    },
    country: {
        type: String,
        required: false
    },
    address: {
        type: String,
        required: false
    },
    size: {
        type: String,
        required: false
    },
    phone: {
        type: String,
        required: false
    },
    logo_url: {
        type: String,
        required: false
    },
    is_verified: {
        type: Boolean,
        required: true
    },
    is_active: {
        type: Boolean,
        required: true
    }
}

# Table 'groups'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    parent_id: {
        type: String,
        required: false
    },
    all_parents: {
        type: Array, // [String] - array of parent' ids, from bottom
        required: false
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    is_user_group: {
        type: Boolean,
        required: false
    },
    is_closed_group: {
        type: Boolean,
        required: false
    },
    is_active: {
        type: Boolean,
        required: false
    },
    meta: {
        create_user: String,
        create_time: Date,
        modify_user: String,
        modify_time: Date
    },
    is_head: {
        type: Boolean,
        required: false
    },
    is_position: {
        type: Boolean,
        required: false
    }
}

# Table 'positions'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    group_id: {
        type: String,
        required: true
    },
    manager_id: {
        type: String, // id of manager position
        required: false
    },
    user_id: {
        type: String, // id of user in this position
        required: false
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
    type: {
        type: String,
        required: false
    },
    is_active: {
        type: Boolean,
        required: false
    }
}

# Table 'post'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    view_scope: {
        type: Array, // [String], example [ "g:GROUP_ID", "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    interact_scope: {
        type: Array, // [String], example [ "g:GROUP_ID",  "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    modify_scope: {
        type: Array, // [String], example [ "g:GROUP_ID", "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    notify_scope: {
        type: Array, // [String], example [ "g:GROUP_ID", "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    tags: {
        type: [String],
        required: true
    },
    categories: {
        type: [String],
        required: false
    },
    meta: {
        create_user: String,
        create_time: Date,
        modify_user: String,
        modify_time: Date,
    },
    stat: {
        read_count: Integer,
        like_count: Integer,
        comment_count: Integer,
        last_interact_time: Date,
    },
    is_active: {
        type: Boolean,
        required: false
    },
    allow_comment: {
        type: Boolean,
        required: true
    }
}

# Table 'post_category'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space: {
        type: String,
        required: true
    },
    parent: {
        type: String,
        required: false
    },
    all_parents: {
        type: [String],
        required: false
    },
    view_scope: {
        type: Array, // [String], example [ "g:GROUP_ID", "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    interact_scope: {
        type: Array, // [String], example [ "g:GROUP_ID",  "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    modify_scope: {
        type: Array, // [String], example [ "g:GROUP_ID", "p:POSITION_ID", "u:USER_ID" ]
        required: true
    },
    name: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: true
    },
    meta: {
        create_user: String,
        create_time: Date,
        modify_user: String,
        modify_time: Date,
    },
    is_active: {
        type: Boolean,
        required: false
    }
}

# Table 'comment'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    post_id: {
        type: String,
        required: true
    },
    reply_to: { // reply to another comment
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    meta: {
        create_user: String,
        create_time: Date,
        modify_time: Date
    },
    stat: {
        like_count: Integer,
        comment_count: Integer,
        last_interact_time: Date,
    },
    is_active: {
        type: Boolean,
        required: false
    }
}

# Table 'like'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    content_id: {
        type: String, // "p:POST_ID", "c:COMMENT_ID"
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    issue_time: {
        type: Date,
        required: true
    },
}

# Table 'notification'
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    user_id: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    issue_time: {
        type: Date,
        required: true
    },
    is_read: {
        type: Boolean,
        required: true
    },
    display_priority: {
        type: Integer,
        required: false
    }
}

2. API design

#  Get space's info
+ URL: /core/space-info
+ Method: POST | GET
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:

+ Returns:
    {
        space_id: String,
        name: String,
        short_name: String,
        logo_url: String,
        user_groups: [
            {
                id: String,
                name: String,
                parent_id: String,
                is_user_group: Boolean,
            }
        ],
        user_positions: [
            {
                id: String,
                name: String,
                group_id: String
            }
        ],
    }

+ Errors:

#  Get list of groups in space
+ URL: /core/list-groups
+ Method: POST
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:
    - include_user_groups: false
    - tree_format: false
+ Returns:
    {
        entries: [
            {
                group_id: String,
                parent_id: String,
                name: String,
                description: String,
                is_user_group: Boolean,
                is_closed_group: Boolean,
                meta: {
                    create_user: String,
                    create_time: Date,
                    modify_user: String,
                    modify_time: Date,
                },
                child_groups: [GroupEntry], // if tree_format = true
            }
        ]
    }

+ Errors:

#  Get list of positions in space
+ URL: /core/list-positions
+ Method: POST
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:
    - group_id: String  // filter by group
+ Returns:
    {
        entries: [
            {
                position_id: String,
                group_id: String,
                manager_id: String,
                user_id: String,
                name: String,
                description: String,
                type: String,
                user_info: {
                    username: String,
                    fullname: String,
                    avatar_url: String
                }
            }
        ]
    }

+ Errors:

#  Get list of users of space with online status
+ URL: /core/list-users
+ Method: POST
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:
    - group_id: String  // filter by group
+ Returns:
    {
        entries: [
            {
                user_id: String,
                position: {
                    position_id: String,
                    position_name: String    
                },
                group: {
                    group_id: String,
                    group_name: String
                },
                username: String,
                fullname: String,
                avatar_url: String,
                online_status: String
            }
        ]
    }

+ Errors:

#  Get list of post categories
+ URL: /core/list-categories
+ Method: POST
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:
    - tree_format: false
+ Returns:
    {
        entries: [
            {
                category_id: String,
                parent_id: String,
                view_scope: Array,
                interact_scope: Array,
                modify_scope: Array,
                name: String,
                description: String,
                meta: {
                    create_user: String,
                    create_time: Date,
                    modify_user: String,
                    modify_time: Date,
                },
                childs: []
            }
        ]
    }

+ Errors:

#  Get list of post by category
+ URL: /core/list-posts
+ Method: POST
+ Header:
    - Authorization: Bearer SPACE_ACCESS_TOKEN
+ Parameters:
    - category_id: String
+ Returns:
    {
        entries: [
            {
                post_id: String,
                category_id: String,
                view_scope: Array,
                interact_scope: Array,
                modify_scope: Array,
                title: String,
                tags: Array,
                meta: {
                    create_user: String,
                    create_time: Date,
                    modify_user: String,
                    modify_time: Date,
                },
                stat: {
                    read_count: Integer,
                    like_count: Integer,
                    comment_count: Integer,
                    last_interact_time: Date,
                },
                allow_comment: Boolean
            }
        ]
    }

+ Errors:
+

# Example

curl localhost:8083/api -H "Authorization: Bearer 9cIlpMWE19DjUlhW7HK7xvzJ8F9dipQDQvJkWjfQfZ1Q1WC5K1A5LIVIRFZCdliC" | jq .

+ Create space:
curl localhost:8083/api/space/create-space -H "Authorization: Bearer rojqxUUXvuxXghpZXl2Xon6yDWILReRxLTt6iduOHL9hdstFL7w0bo8PNdEwhnOM" -d "name=Dong Hanh JSC&short_name=donghanhjsc&uname=donghanhjsc&phone=1234567&address=Ha Noi" | jq .

+ List spaces
curl localhost:8083/api/space/list-spaces -H "Authorization: Bearer 0IpdPZ8dWhHblv59on8XcjQLoLs6uI0DO9CsiyG42wdQHUfmmbumsTqveVxzvVMS" | jq .

+ List users of space
curl localhost:8083/api/space/list-users?space_id=586bc9119a29be1a9d9c6122 -H "Authorization: Bearer vp8J3Qo0UA9yOsotRhLmoVPh09aERtc3DMQJUA3JCXJxfEmcZ6R5ipaFlCS80dDA" | jq .

+ Get space info
curl localhost:8083/api/space/space-info -H "Authorization: Bearer mKDlOHu4L7ieLinOYIuEt7l4PTEfZqR2sxouiwS2gC228APtd4IXY5TIHBsRpuAk" | jq .

+ Get user profie
curl localhost:8083/api/space/user-profile -H "Authorization: Bearer 21U1ij9lltaGXJJYXKmkUeclwEo7I4Nk5jSa5Gzniy6K1jO8WvP68GFWm5LYwrlL" | jq .

+ Get list groups
curl localhost:8080/api/space/group/list -H "Authorization: Bearer FR5txyChUk5Bg72GElZhjRL9dB3KEhEcWrlrYSRJpFZ2zpvTdy6I1AfgDvkqSncS" | jq .

+ Get list groups
curl localhost:8080/api/space/group/add -H "Authorization: Bearer FR5txyChUk5Bg72GElZhjRL9dB3KEhEcWrlrYSRJpFZ2zpvTdy6I1AfgDvkqSncS" -d "name=Group 1&description=Group 1" | jq .


21U1ij9lltaGXJJYXKmkUeclwEo7I4Nk5jSa5Gzniy6K1jO8WvP68GFWm5LYwrlL
