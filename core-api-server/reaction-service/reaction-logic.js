'use strict';

// const asyn = require("async");
const data = require('./reaction-data');
// const cmUtils = require('../utils/common');
// const logger = require('../../logger');

function getStream(streamId, context, done) {
    data.findStreamByID(streamId, done);
}

function addStream(stream, context, done) {
    data.insertStream(stream, done);
}

function removeStream(streamId, context, done) {
    data.deleteStream(streamId, done);
}

// reaction {stream_id, action, data}
function react(reaction, context, done) {
    const reactData = {
        issue_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        issue_time: new Date(),
        data: reaction.data
    };

    return data.insertReaction(reaction.stream_id, reaction.action, reactData, function(err){
        if (err) return done(err);
        done(null, {
            reacted: {
                stream_id: reaction.stream_id,
                action: reaction.action,
                data: reactData
            }
        });
    });
}

// reaction {stream_id, action}
function undoReact(reaction, context, done) {
    return data.deleteReaction(reaction.stream_id, reaction.action, context.user_id, function(err){
        if (err) return done(err);
        done(null, {
            undoreacted: {
                stream_id: reaction.stream_id,
                action: reaction.action,
                user_id: context.user_id
            }
        });
    });
}

function getUsersToNofity(ref, context, done) {
    data.findOne({
        ref_type: ref.ref_type,
        ref_id: ref.ref_id,
    }, function(err, r) {
        if (err) return done(err);
        if (!r) return done(null, []);

        const liked = getUserIds(r.reaction.liked);
        const followed = getUserIds(r.reaction.followed);
        const unfollowed = getUserIds(r.reaction.unfollowed);
        const bookmarked = getUserIds(r.reaction.bookmarked);

        const userIds = liked.concat(followed).concat(bookmarked).filter((item) => unfollowed.indexOf(item) === -1);
        done(null, userIds);
    });
}

function getUserIds(arr) {
    return arr.map((item) => item.issue_user.user_id);
}

module.exports = {
    addStream,
    removeStream,
    getStream,
    react,
    undoReact,
    getUsersToNofity
};
