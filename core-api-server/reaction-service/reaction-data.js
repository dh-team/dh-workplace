'use strict';

const ReactionStream = require('../mongodb/models/reaction-stream');
const cmUtils = require('../utils/common');

function findOne(cond, done) {
    ReactionStream.findOne(cond, function(err, post) {
        if (err) return done(err);
        return done(null, post);
    });
}

function findStreamByID(id, done) {
    return findOne({
        _id: id
    }, done);
}

function insertStream(stream, done) {
    var newStream = new ReactionStream();
    cmUtils.copy(stream, newStream);
    newStream.save(function(err) {
        if (err) return done(err);
        return done(null, newStream);
    });
}

function deleteStream(streamId, done) {
    ReactionStream.findOneAndRemove({
        _id: streamId
    }, function(err) {
        if (err) return done(err);
        return done(null);
    });
}

/**
 * Add new comment
 * @param {string} streamId 
 * @param {string} action 'liked', 'followed', 'unfollowed', 'rated', 'bookmarked'  
 * @param {object} react {issue_user, issue_time, data}
 * @param {function} done 
 */
function insertReaction(streamId, action, react, done) {
    ReactionStream.update({
        _id: streamId,
        ["reaction." + action + " .issue_user.user_id"]: react.issue_user.user_id
    }, {
        "$set": {
            // ["reaction." + action + ".$.issue_user.user_id"]: react.issue_user.user_id,
            // ["reaction." + action + ".$.issue_user.uname"]: react.issue_user.uname,
            // ["reaction." + action + ".$.issue_time"]: react.issue_time,
            // ["reaction." + action + ".$.data"]: react.data,
            ["reaction." + action + ".$"]: react,
        }
    }, function(err, r) {
        if (err) return done(err);
        // r: { "nMatched" : 0, "nUpserted" : 0, "nModified" : 0 }
        if (r.nModified > 0) return done();

        ReactionStream.update({
            _id: streamId,
        }, {
            "$push": {
                ["reaction." + action]: react
            }
        }, function(err, r) {
            if (err) return done(err);
            return done();
        });
    });
}

function deleteReaction(streamId, action, userId, done) {
    ReactionStream.update({
        _id: streamId,
    }, {
        "$pull": {
            ["reaction." + action]: {
                "issue_user.user_id": userId
            }
        }
    }, done);
}

module.exports = {
    findOne,
    findStreamByID,
    insertStream,
    deleteStream,
    insertReaction,
    deleteReaction
};
