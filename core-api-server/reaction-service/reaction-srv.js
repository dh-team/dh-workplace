const spaceAuth = require('../resource-auth');
const httpUtils = require('../utils/http');
const logger = require('../logger');
const business = require('./reaction-logic');

function getReactionStream(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['stream_id']);

    business.getReactStream(params.stream_id, context, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function react(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['stream_id', 'action', 'data']);
    logger.debug('add reaction, params: ' + JSON.stringify(params));

    business.react(params, context, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function undoReact(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['stream_id', 'action']);
    logger.debug('remove reaction, params: ' + JSON.stringify(params));

    business.undoReact(params, context, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function registerHandler(app) {
    app.post('/api/space/reaction-streams/:stream_id', [spaceAuth.spaceAuthSuccess, getReactionStream]);
    app.post('/api/space/reaction-streams/:stream_id/react', [spaceAuth.spaceAuthSuccess, react]);
    app.post('/api/space/reaction-streams/:stream_id/undo-react', [spaceAuth.spaceAuthSuccess, undoReact]);
}

module.exports = {
    registerHandler: registerHandler
};
