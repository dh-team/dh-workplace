'use strict';

const asyn = require("async");
const data = require("./notification-data");
const Membership = require('../mongodb/space-mbs');
const Groups = require('../mongodb/groups');

/**
 * Push notification to users
 * @param {object} notification Notification content {space, type, ref_data}
 * @param {object} target Target users or groups {scope, users}, scope = ["g:all", "g:GROUP_ID", "u:owner", "u:USER_ID"], users = [USER_ID] 
 * @param {object} context Request context {space_id, user_id, uname, roles, groups} 
 * @param {function} done 
 */
function pushNotification(notification, target, context, done) {
    if (!target.scope) return notifyToUsers(notification, target.users, done);
    getUsersInScopes(notification.space, target.scope, function (err, users) {
        if (err) return done(err);
        notifyToUsers(notification, users.concat(target.users || []), done);
    });
}

function notifyToUsers(notification, users, done) {
    notification.notified_users = [];
    // make unique
    users = users.filter(function (item, index, self) {
        return index == self.indexOf(item);
    });
    users.map(function (userID) {
        if (userID && userID != notification.ref_data.issue_user.user_id) {
            notification.notified_users.push({
                user_id: userID,
                is_read: false,
                is_active: true
            });
        }
    });

    if (notification.notified_users.length > 0) return data.insert(notification, done);
    // no notification inserted
    return process.nextTick(function () {
        done();
    });
}

function getUsersInScopes(spaceID, scopes, done) {
    console.log("notification scope: " + JSON.stringify(scopes));
    asyn.concat(scopes, function (scope, cb) {
        getUsersInScope(spaceID, scope, cb);
    }, function (err, users) {
        if (err) return done(err);
        console.log("notification users: " + JSON.stringify(users));
        return done(null, users);
    });
}

function getUsersInScope(spaceID, scope, done) {
    if (!scope || scope === "u:owner") {
        return process.nextTick(function () {
            done(null, []);
        });
    }

    if (scope.startsWith("u:")) {
        return process.nextTick(function () {
            done(null, [scope.substr(2)]);
        });
    }

    if (scope === "g:all") {
        return getUsersInSpace(spaceID, done);
    }

    if (scope.startsWith("g:")) {
        return getUsersInGroup(spaceID, scope.substr(2), done);
    }

    return process.nextTick(function () {
        done(null, []);
    });
}

function getUsersInSpace(spaceID, done) {
    Membership.filter({
        space_id: spaceID
    }, function (err, mbs) {
        if (err) return done(err);
        var users = mbs.map(function (item) {
            return item.user_id;
        });
        done(null, users);
    });
}

function getUsersInGroup(spaceID, groupID, done) {
    // get all descendant groups
    Groups.listDescendantGroups(groupID, function (err, groups) {
        if (err) return done(err);

        if (!groups) groups = [];
        groups.push(groupID);

        Membership.filter({
            space_id: spaceID,
            groups: {
                "$in": groups
            }
        }, function (err, mbs) {
            if (err) return done(err);
            var users = mbs.map(function (item) {
                return item.user_id;
            });
            done(null, users);
        });
    });
}

/**
 * Get list notifications for user
 * @param {string} cursor Filter notification bellow this id 
 * @param {number} limit Max number of notification
 * @param {object} context Authentication info
 * @param {function} done 
 */
function getNotificationsForUser(cursor, limit, context, done) {
    if('number' !== typeof(limit)) limit = parseInt(limit);
    if (limit > 10) limit = 10;
    const cond = {
        space: context.space_id,
        "notified_users.user_id": context.user_id
    };
    const sort = {
        "_id": -1
    };
    return data.find(cond, sort, cursor, limit, function (err, r) {
        if (err) return done(err);
        const result = r.map((item) => {
            const notifiedUser = item.notified_users.find((item1) => item1.user_id === context.user_id);
            item.user_id = notifiedUser.user_id;
            item.is_read = notifiedUser.is_read;
            item.is_active = notifiedUser.is_active;

            delete item.notified_users;
            return item;
        });

        done(null, result);
    });
};

function countUnreadNotifications(cursor, context, done) {
    data.countUnread(context.space_id, context.user_id, cursor, done);
}

function markNotificationIsRead(noId, context, done) {
    data.markIsRead(noId, context.user_id, done);
}

function removeUserNotification(noId, context, done) {
    data.removeUser(noId, context.user_id, done);
}

module.exports = {
    pushNotification,
    getNotificationsForUser,
    countUnreadNotifications,
    markNotificationIsRead,
    removeUserNotification
};