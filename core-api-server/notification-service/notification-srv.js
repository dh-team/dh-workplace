const spaceAuth = require('../resource-auth');
const httpUtils = require("../utils/http");
const logger = require("../logger");
const notificationLogic = require("./notification-logic");

function getNotifications(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const param = httpUtils.extractObject(req, ["cursor", "limit"]);

    notificationLogic.getNotificationsForUser(param.cursor, param.limit, auth, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}


function countUnread(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const cursor = httpUtils.extractParam(req, "cursor");

    notificationLogic.countUnreadNotifications(cursor, auth, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function markIsRead(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const id = httpUtils.extractParam(req, "id");
    logger.debug("mark notification read", {
        user_id: auth.user_id,
        notification_id: id
    });

    notificationLogic.markNotificationIsRead(id, auth, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function registerHandler(app) {
    app.get("/api/space/notifications", [spaceAuth.spaceAuthSuccess, getNotifications]);
    app.get("/api/space/notifications/count-unread", [spaceAuth.spaceAuthSuccess, countUnread]);    
    app.post("/api/space/notifications/mark-read", [spaceAuth.spaceAuthSuccess, markIsRead]);
}

module.exports = {
    registerHandler
}
