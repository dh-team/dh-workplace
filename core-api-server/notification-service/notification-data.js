'use strict'

const Notification = require('../mongodb/models/notification');
const cmUtils = require('../utils/common');

function find(cond, sort, cursor, limit, done) {
    if (limit <= 0) limit = 0;
    if (cursor) {
        cond["_id"] = {
            $lt: cursor
        };
    }

    let query = Notification.find(cond);
    if (sort) query = query.sort(sort);
    if (limit) query = query.limit(limit);

    return query.exec(function (err, r) {
        if (err) return done(err);
        done(null, r.map((item) => item.toObject()));
    });
}

function countUnread(spaceId, userId, cursor, done) {
    var cond = {
        "space": spaceId,
        "notified_users": {
            $elemMatch: {
                "user_id": userId,
                "is_read": false
            }
        }
    };

    if (cursor) {
        cond["_id"] = {
            $lt: cursor
        };
    }
    return Notification.count(cond, done);
}

function insert(notification, done) {
    var newNotification = new Notification();
    cmUtils.copy(notification, newNotification);
    newNotification.save(function (err) {
        if (err) return done(err);
        return done(null, newNotification);
    });
}

function markIsRead(noId, userId, done) {
    Notification.update({
        "_id": noId,
        "notified_users.user_id": userId
    }, {
            $set: {
                "notified_users.$.is_read": true
            }
        }, {
            multi: true
        }, done);
}

function removeUser(noId, userId, done) {
    Notification.update({
        _id: noId
    }, {
            "$pull": {
                "notified_users.user_id": userId
            }
        }, {
            multi: true
        }, done);
}

function remove(noId, done) {
    Notification.findOneAndRemove({
        _id: noId
    }, function (err, deleted) {
        if (err) return done(err);
        return done(null, deleted);
    });
}

module.exports = {
    insert,
    remove,
    removeUser,
    markIsRead,
    countUnread,
    find
};
