var assert = require("assert");
var mongoose = require('mongoose');
var db = require("../mongodb");
var Test = require("./test-model.js");

db.init(function(err) {
    assert.equal(err, null);
    testInsert();
    testInsert();
    testInsert();
});


function testFindSpaceByUser() {
    db.spaces.findByUserID("586b74f620cb7d1407cfd9d0", function(err, r) {
        assert.equal(err, null);
        console.log(JSON.stringify(r));
    });
}


function testInsert() {
    var t = new Test();
    t.space_id = "586b74f620cb7d1407cfd9d0";
    t.save(function(err) {
        assert.equal(err, null);
        console.log(JSON.stringify(t));
    });
}
