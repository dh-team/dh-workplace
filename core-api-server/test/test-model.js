'use strict';

var mongoose = require('mongoose');

var Test = new mongoose.Schema({
    _id: {
        type: String,
        required: true,
        default: function() {
            return mongoose.Types.ObjectId().toString();
        }
    },
    space_id: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    }
});

module.exports = mongoose.model('tests', Test);
