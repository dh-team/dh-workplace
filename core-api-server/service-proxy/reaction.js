// proxy for internal services interact with the reaction service

'use strict';
const reactionLogic = require('../reaction-service/reaction-logic');

/**
 * Add new comment stream 
 * @param {object} stream New comment stream {id, space, ref_type, ref_id}
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function addReactionStream(stream, context, done) {
    reactionLogic.addStream(stream, context, done);
}

/**
 * Delete comment stream 
 * @param {String} streamId Id of comment stream will be deleted
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function removeReactionStream(streamId, context, done) {
    reactionLogic.removeStream(streamId, context, done);
}

/**
 * Push notification to users
 * @param {object} ref Object reference {ref_type, ref_id}
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function getUsersToNofity(ref, context, done) {
    reactionLogic.getUsersToNofity(ref, context, done);
}

module.exports = {
    getUsersToNofity,
    addReactionStream,
    removeReactionStream
};
