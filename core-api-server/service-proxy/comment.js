// proxy for internal services interact with the comment service

'use strict';
const commentLogic = require('../comment-service/comment-logic');

/**
 * Add new comment stream 
 * @param {object} stream New comment stream {id, space, ref_type, ref_id}
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function addCommentStream(stream, context, done) {
    commentLogic.addStream(stream, context, done);
}

/**
 * Delete comment stream 
 * @param {String} streamId Id of comment stream will be deleted
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function removeCommentStream(streamId, context, done) {
    commentLogic.removeStream(streamId, context, done);
}


/**
 * Update reference object of the stream
 * @param {String} streamId Id of comment stream
 * @param {object} obj New reference object value
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function updateStream(stream, context, done) {
    commentLogic.updateStream(stream, context, done);
}


module.exports = {
    addCommentStream,
    removeCommentStream,
    updateStream
};
