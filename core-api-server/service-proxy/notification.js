// proxy for internal services interact with the notification service

'use strict';
const notificationLogic = require('../notification-service/notification-logic');

/**
 * Push notification to users
 * @param {object} notification Notification content {space, type, ref_data}
 * @param {object} target Target users or groups {scope, users}, scope = ["g:all", "g:GROUP_ID", "u:owner", "u:USER_ID"], users = [USER_ID] 
 * @param {object} context Request context {space_id, user_id, uname, roles, groups} 
 * @param {function} done 
 */
function pushNotification(notification, target, context, done) {
    notificationLogic.pushNotification(notification, target, context, done);
}

module.exports = {
    pushNotification
};

