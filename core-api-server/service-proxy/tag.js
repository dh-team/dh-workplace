// proxy for internal services interact with the tag service

'use strict';
const tagLogic = require('../tag-service/tag-logic');

/**
 * Upsert tags
 * @param {[String]} tags List of tags to insert (if not exist)
 * @param {String} subject subject to filter 
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function updateTags(tags, subject, context, done) {
    tagLogic.updateTags(tags, subject, context, done);
}

module.exports = {
    updateTags
};

