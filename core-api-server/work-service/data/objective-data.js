/**
 * module provides CRUD functions of objectives data
 */
'use strict';

var asyn = require("async");
var utils = require('../../utils/common');

var Objective = require('../../mongodb/models/app-work/objective');

function findOne(cond, done) {
    Objective.findOne(cond, function (err, objective) {
        if (err) return done(err);
        return done(null, objective);
    });
};

/**
 * find objective by id
 * @param {string} id
 * @param {function} done 
 */
function findById(id, done) {
    return findOne({
        _id: id
    }, done);
};

/**
 * find by condition, sorting, and paging
 * @param {object} param {cond, sorting, paging}
 * @param {function} done 
 */
function find(param, done) {
    var query = Objective.find(param.cond);
    if (param.sorting) {
        query = query.sort(param.sorting);
    }
    if (param.paging) {
        if (param.paging.skip) {
            query = query.skip(param.paging.skip);
        }
        if (param.paging.limit) {
            query = query.limit(param.paging.limit);
        }
    }
    return query.exec(done);
}
/**
 * Insert new objective
 * @param {object} objective 
 * @param {fuction} done 
 */
function insert(objective, done) {
    var newObjective = new Objective();
    utils.copy(objective, newObjective);
    newObjective.save(function (err) {
        if (err) return done(err);
        done(null, newObjective);
    });
};

/**
 * Update objective
 * @param {object} objective 
 * @param {function} done 
 */
function update(objective, done) {
    Objective.findOne({
        _id: objective.id
    }, function (err, r) {
        if (err || !r) return done(err, r);
        utils.update(objective, r);
        r.save(function (err) {
            if (err) return done(err);
            return done(null, r);
        });
    });
};

/**
 * Remove objective
 * @param {string} id 
 * @param {function} done 
 */
function remove(id, done) {
    Objective.findOneAndRemove({
        _id: id
    }, function (err, deleted) {
        if (err) return done(err);
        return done(null, deleted);
    });
}

module.exports = {
    findOne: findOne,
    findById: findById,
    find: find,
    insert: insert,
    update: update,
    remove: remove
}
