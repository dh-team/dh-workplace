/**
 * module provides CRUD functions of works data
 */
'use strict';

var asyn = require("async");
var mongoose = require('mongoose');

var utils = require('../../utils/common');
var Work = require('../../mongodb/models/app-work/work');

function findOne(cond, done) {
    Work.findOne(cond, function (err, work) {
        if (err) return done(err);
        return done(null, work);
    });
};

/**
 * find work by id
 * @param {string} id
 * @param {function} done 
 */
function findById(id, done) {
    return findOne({
        _id: id
    }, function (err, r) {
        if (err || !r) return done(err, r);
        done(null, r.toObject());
    });
};

/**
 * find by condition, sorting, and paging
 * @param {object} param {cond, sorting, paging}
 * @param {function} done 
 */
function find(param, done) {
    var query = Work.find(param.cond);
    if (param.sorting) {
        query = query.sort(param.sorting);
    }
    if (param.paging) {
        if (param.paging.skip) {
            query = query.skip(param.paging.skip);
        }
        if (param.paging.limit) {
            query = query.limit(param.paging.limit);
        }
    }
    return query.exec(done);
}
/**
 * Insert new work
 * @param {object} work 
 * @param {fuction} done 
 */
function insert(work, done) {
    var newWork = new Work();
    utils.copy(work, newWork);
    newWork.save(function (err) {
        if (err) return done(err);
        done(null, newWork);
    });
};

/**
 * Update work
 * @param {object} work 
 * @param {function} done 
 */
function update(work, done) {
    console.log(JSON.stringify(work));
    Work.findOne({
        _id: work.id
    }, function (err, r) {
        if (err || !r) return done(err, r);
        utils.update(work, r);
        r.save(function (err) {
            if (err) return done(err);
            return done(null, r);
        });
    });
};

/**
 * Remove work
 * @param {string} id 
 * @param {function} done 
 */
function remove(id, done) {
    Work.findOneAndRemove({
        _id: id
    }, function (err, deleted) {
        if (err) return done(err);
        return done(null, deleted);
    });
}

/**
 * Insert new task
 * @param {object} work 
 * @param {fuction} done 
 */
function insertTask(workId, task, done) {
    task._id = mongoose.Types.ObjectId().toString();
    Work.update({
        "_id": workId
    }, {
            "$push": {
                "doing.tasks": task
            }
        }, function (err, r) {
            if (err) return done(err);
            return done(null, task);
        });
}

function updateTask(workId, task, done) {
    var update = {};
    if (task.note) update["doing.tasks.$.note"] = task.note;
    if (task.status) update["doing.tasks.$.status"] = task.status;
    if (task.start_time) update["doing.tasks.$.start_time"] = task.start_time;
    if (task.stop_time) update["doing.tasks.$.stop_time"] = task.stop_time;

    Work.findOneAndUpdate({
        "_id": workId,
        "doing.tasks._id": task.id,
    }, {
            "$set": update
        }, {
            "new": true
        }, function (err, r) {
            if (err) return done(err);
            if (!r || !r.doing || !r.doing.tasks) return done();
            return done(null, r.doing.tasks.find((item) => item._id === task.id));
        });
}

function deleteTask(workId, taskId, done) {
    Work.update({
        "_id": workId
    }, {
            "$pull": {
                "doing.tasks": {
                    _id: taskId
                }
            }
        }, done);
}

function updateFields(cond, fields, done) {
    Work.update(cond, {
        "$set": fields
    }, done);
}

// function updateDoingStatus(workId, progress, status, done) {
//     updateFields({
//         _id: workId
//     }, {
//         "doing.progress": progress,
//         "doing.status": status
//     }, done);
// }

function calcDoing(workId, done) {
    Work.aggregate([{
        $match: {
            parent: workId
        }
    }, {
        $group: {
            _id: "$parent",
            avg_progress: {
                $avg: "$doing.progress"
            },
            total_spent_time: {
                $sum: "$doing.spent_time"
            },
            all_status: {
                $addToSet: "$doing.status"
            }
        }
    }], function (err, r) {
        if (err || !r || r.length === 0) return done(err, null);
        return done(null, r[0]);
    });
}

module.exports = {
    findOne: findOne,
    findById: findById,
    find: find,
    insert: insert,
    update: update,
    remove: remove,
    insertTask: insertTask,
    updateTask: updateTask,
    deleteTask: deleteTask,
    updateFields: updateFields,
    calcDoing: calcDoing,
};
