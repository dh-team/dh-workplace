const objectiveService = require('./objective-srv');
const workService = require('./work-srv');

function registerHandler(app) {
    app.get("/api/space/objectives", objectiveService.listObjectives);
    app.post("/api/space/objectives", objectiveService.addObjective);
    app.put("/api/space/objectives/:id", objectiveService.editObjective);
    app.delete("/api/space/objectives/:id", objectiveService.deleteObjective);

    app.get("/api/space/works", workService.listWorks);
    app.post("/api/space/works", workService.addWork);
    app.get("/api/space/works/:id", workService.getWork);
    app.put("/api/space/works/:id", workService.editWork);
    app.delete("/api/space/works/:id", workService.deleteWork);
    app.post("/api/space/works/:work_id/tasks", workService.addWorkTask);
    app.put("/api/space/works/:work_id/tasks/:task_id", workService.editWorkTask);
    app.delete("/api/space/works/:work_id/tasks/:task_id", workService.deleteWorkTask);
}

module.exports = {
    registerHandler: registerHandler
};
