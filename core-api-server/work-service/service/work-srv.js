const auth = require('../../resource-auth');
const httpUtils = require('../../utils/http');
const logger = require('../../logger');

const workLogic = require('../logic/work-logic');
const workTaskLogic = require('../logic/work-task-logic');

function listWorks(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['descendant_id', 'parent_id', 'source', 'status', 'performer', 'monitor', 'start_date', 'due_date', 'name', 'hierarchy', 'followed']);
    logger.debug('find works, params: ' + JSON.stringify(params));

    workLogic.findWorks(params, context, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function getWork(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const objId = httpUtils.extractParam(req, 'id');
    workLogic.getWork(objId, context, function(err, r) {
        httpUtils.response(res, err, r);
    });
}


function addWork(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['name', 'detail', 'parent', 'objective', 'source', 'knowledge_tags', 'category_tags', 'priority', 'resource', 'planning']);

    if (params.planning && params.planning.start_date && typeof(params.planning.start_date) === 'string') {
        params.planning.start_date = new Date(params.planning.start_date);
    }
    if (params.planning && params.planning.due_date && typeof(params.planning.due_date) === 'string') {
        params.planning.due_date = new Date(params.planning.due_date);
    }
    logger.debug('add work, params: ' + JSON.stringify(params));

    workLogic.addWork(params, context, function(err, inserted) {
        httpUtils.response(res, err, inserted);
    });
}

function editWork(req, res) {
    const updateArea = httpUtils.extractParam(req, 'update_area');
    if (updateArea === 'content') {
        return editWorkContent(req, res);
    }

    if (updateArea === 'planning') {
        return editWorkPlanning(req, res);
    }

    if (updateArea === 'resource') {
        return editWorkResource(req, res);
    }

    if (updateArea === 'status') {
        return editWorkStatus(req, res);
    }

    if (updateArea === 'attachments') {
        return editWorkAttachments(req, res);
    }

    if (updateArea === 'progress') {
        return editWorkProgress(req, res);
    }

    if (updateArea === 'result') {
        return editWorkResult(req, res);
    }

    return httpUtils.responseInvalid(res, 'params_invalid');
}

function editWorkContent(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'name', 'detail', 'objective', 'knowledge_tags', 'category_tags', 'priority_label', 'doing']);
    if (params.doing) params.doing = {
        display_type: params.doing.display_type
    };
    logger.debug('edit work content, params: ' + JSON.stringify(params));

    workLogic.editWorkContent(params, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkPlanning(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'planning']);
    if (params.planning && params.planning.start_date && typeof(params.planning.start_date) === 'string') {
        params.planning.start_date = new Date(params.planning.start_date);
    }
    if (params.planning && params.planning.due_date && typeof(params.planning.due_date) === 'string') {
        params.planning.due_date = new Date(params.planning.due_date);
    }
    logger.debug('edit work planning, params: ' + JSON.stringify(params));

    workLogic.editWorkPlanning(params, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkResource(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'resource']);

    logger.debug('edit work resource, params: ' + JSON.stringify(params));

    workLogic.editWorkResource(params, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkStatus(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'status']);

    logger.debug('edit work status, params: ' + JSON.stringify(params));

    workLogic.editWorkStatus(params.id, params.status, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkAttachments(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'attachments']);

    logger.debug('edit work attachments, params: ' + JSON.stringify(params));

    workLogic.editWorkAttachments(params, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkProgress(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'progress']);

    logger.debug('edit work progress, params: ' + JSON.stringify(params));

    workLogic.editWorkProgress(params.id, params.progress, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function editWorkResult(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'result']);

    logger.debug('edit work result, params: ' + JSON.stringify(params));

    workLogic.editWorkResult(params, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function deleteWork(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const objId = httpUtils.extractParam(req, 'id');
    logger.debug('delete work, id: ' + objId);

    workLogic.deleteWork(objId, context, function(err, deleted) {
        httpUtils.response(res, err, deleted);
    });
}

function addWorkTask(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['work_id', 'note', 'status']);
    logger.debug('add task, params: ' + JSON.stringify(params));

    workTaskLogic.addWorkTask(params.work_id, params, context, function(err, inserted) {
        httpUtils.response(res, err, inserted);
    });
}

function editWorkTask(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['work_id', 'task_id', 'note', 'status', 'display_order', 'spent_time']);
    logger.debug('edit task, params: ' + JSON.stringify(params));
    if (params.display_order) {
        return workTaskLogic.changeTaskOrder(params.work_id, params.task_id, params.display_order, context, function(err, edited) {
            httpUtils.response(res, err, edited);
        });
    }
    workTaskLogic.editWorkTask(params.work_id, {
        display_order: params.task_id,
        note: params.note,
        status: params.status,
        spent_time: params.spent_time
    }, context, function(err, edited) {
        httpUtils.response(res, err, edited);
    });
}

function deleteWorkTask(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['work_id', 'task_id']);
    logger.debug('delete work task, params: ' + JSON.stringify(params));

    workTaskLogic.deleteWorkTask(params.work_id, params.task_id, context, function(err, r) {
        httpUtils.response(res, err, r);
    });
}

module.exports = {
    listWorks: [auth.spaceAuthSuccess, listWorks],
    getWork: [auth.spaceAuthSuccess, getWork],
    addWork: [auth.spaceAuthSuccess, addWork],
    editWork: [auth.spaceAuthSuccess, editWork],
    deleteWork: [auth.spaceAuthSuccess, deleteWork],
    addWorkTask: [auth.spaceAuthSuccess, addWorkTask],
    editWorkTask: [auth.spaceAuthSuccess, editWorkTask],
    deleteWorkTask: [auth.spaceAuthSuccess, deleteWorkTask]
};
