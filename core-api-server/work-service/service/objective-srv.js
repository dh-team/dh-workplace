const auth = require('../../resource-auth');
const httpUtils = require('../../utils/http');
const logger = require('../../logger');

const objectiveLogic = require('../logic/objective-logic');

function listObjectives(req, res) {
    const context = httpUtils.extractAuthInfo(req);

    objectiveLogic.listObjectives(null, context, function (err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function addObjective(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['content', 'detail', 'owner', 'time', 'key_results']);

    if (params.time && params.time.from && typeof (params.time.from) === 'string') {
        params.time.from = new Date(params.time.from);
    }
    if (params.time && params.time.to && typeof (params.time.to) === 'string') {
        params.time.to = new Date(params.time.to);
    }
    logger.debug('add objective, params: ' + JSON.stringify(params));

    objectiveLogic.addObjective(params, context, function (err, inserted) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(inserted);
    });
}

function editObjective(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ['id', 'content', 'detail', 'owner', 'time', 'key_results']);

    if (params.time && params.time.from && typeof (params.time.from) === 'string') {
        params.time.from = new Date(params.time.from);
    }
    if (params.time && params.time.to && typeof (params.time.to) === 'string') {
        params.time.to = new Date(params.time.to);
    }
    logger.debug('edit objective, params: ' + JSON.stringify(params));

    objectiveLogic.editObjective(params, context, function (err, edited) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(edited);
    });
}

function deleteObjective(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const objId = httpUtils.extractParam(req, 'id');
    logger.debug('delete objective, id: ' + objId);

    objectiveLogic.deleteObjective(objId, context, function (err, deleted) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(deleted);
    });
}


module.exports = {
    listObjectives: [auth.spaceAuthSuccess, listObjectives],
    addObjective: [auth.spaceAuthSuccess, addObjective],
    editObjective: [auth.spaceAuthSuccess, editObjective],
    deleteObjective: [auth.spaceAuthSuccess, deleteObjective]
};
