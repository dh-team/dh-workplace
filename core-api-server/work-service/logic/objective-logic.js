const data = require("../data");
const authUtils = require("../../utils/authorize");

/**
 * List objectives
 * @param {object} filter To filter {owner} 
 * @param {object} context 
 * @param {function} done 
 */
function listObjectives(filter, context, done) {
    var param = {
        cond: {
            "space": context.space_id
        },
        sorting: {
            "time.to": -1,
            // "display_order": -1
        }
    };

    if (filter && filter.owner) {
        param.cond.owner = filter.owner;
    }

    data.objectives.find(param, function (err, list) {
        if (err) return done(err);
        // view scope
        var myScope = authUtils.buildUserScope(context.user_id);
        list = list.filter(function (item) {
            if (item.owner === "g:all" || item.owner === myScope) return true;
            if (item.owner && item.owner.startsWith("g:")) {
                var groupId = item.owner.substr(2);
                return (context.groups && context.groups.indexOf(groupId) > -1);
            }
            return false;
        });
        return done(null, list);
    });
}

/**
 * Add new objective
 * @param {object} objective 
 * @param {object} context Authentication info {space_id, user_id, uname} 
 * @param {function} done 
 */
function addObjective(objective, context, done) {
    var err = validate(objective, context);
    if (err) return done(err);

    // fill info
    objective.space = context.space_id;
    objective.meta = {
        create_user: {
            user_id: context.user_id,
            uname: context.uname
        }
    };

    // save to database    
    data.objectives.insert(objective, done);
}

function editObjective(objective, context, done) {
    var err = validate(objective, context);
    if (err) return done(err);

    // fill info    
    objective.meta = {
        modify_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        modify_time: new Date()
    };

    // save to database
    data.objectives.update(objective, done);
}


function deleteObjective(objectiveId, context, done) {
    data.objectives.findById(objectiveId, function (err, objective) {
        if (err) return done(err);
        // check permission
        var pErr = authUtils.checkInScope(objective.owner, context);
        if (pErr) return done(pErr);
        // delete in database
        data.objectives.remove(objectiveId, done);
    });
}

function validate(objective, context) {
    // check permission
    if (!objective.owner || objective.owner === "u:own") {
        objective.owner = authUtils.buildUserScope(context.user_id);
    } else {
        var pErr = authUtils.checkInScope(objective.owner, context);
        console.log("permission", objective.owner, context);
        if (pErr) return pErr;
    }

    switch (objective.time.range) {
        case "annual": {
            objective.time.quarter = 4;
            objective.time.to = new Date(objective.time.year, 11, 31);
            break;
        }
        case "quarterly": {
            objective.time.to = new Date(objective.time.year, objective.time.quarter * 3, 0);
            break;
        }
        default: {
            objective.time.year = objective.time.to.getFullYear();
            objective.time.quarter = Math.floor(objective.time.to.getMonth() / 3) + 1;
        }
    }
}


module.exports = {
    listObjectives,
    addObjective,
    editObjective,
    deleteObjective
};
