'use strict';
const logger = require('../../logger');
const notifyService = require('../../service-proxy/notification');

function defaultCallback(err) {
    if (err) return logger.warn("notification for work error", err);
}

function notifyPerformer(work, context, done) {
    const no = {
        space: work.space,
        type: "work_assign_performer",
        ref_data: getNotifyRefData(work)
    };
    const target = {
        users: [work.resource.performer]
    };
    notifyService.pushNotification(no, target, context, done || defaultCallback);
}

function notifyMonitor(work, context, done) {
    const no = {
        space: work.space,
        type: "work_assign_monitor",
        ref_data: getNotifyRefData(work)
    };
    const target = {
        users: [work.resource.monitor]
    };
    notifyService.pushNotification(no, target, context, done || defaultCallback);
}

function notifyStatus(work, oldStatus, context, done) {
    const no = {
        space: work.space,
        type: "work_change_status",
        ref_data: getNotifyRefData(work)
    };
    no.ref_data.work.new_status = work.doing.status;
    no.ref_data.work.old_status = oldStatus;
    const target = {
        users: [work.resource.monitor, work.resource.performer].concat(work.resource.followers || [])
    };
    notifyService.pushNotification(no, target, context, done || defaultCallback);
}

function getNotifyRefData(work) {
    return {
        preview_content: work.name,
        issue_user: work.meta.modify_user,
        issue_time: work.meta.modify_time,
        work: {
            id: work._id,
            source: work.source,
            resource: work.resource,
            planning: work.planning
        }
    };
}

module.exports = {
    notifyPerformer,
    notifyMonitor,
    notifyStatus
};