'use strict';

const asyn = require('async');
const data = require('../data');
const cm = require('./work-cm-logic');
const cmUtils = require('../../utils/common');
const constant = require('../../utils/constant');
const workNotify = require('./work-notify');
const commentService = require('../../service-proxy/comment');
const reactionService = require('../../service-proxy/reaction');
const tagService = require('../../service-proxy/tag');
const dbUtils = require('../../mongodb/utils');
const logger = require('../../logger');

/**
 * List works
 * @param {object} filter {'parent_id', 'source', 'status', 'performer', 'monitor', 'start_date', 'due_date', 'name', 'exclude_childs', 'only_leaf', 'followed'} 
 * @param {object} context 
 * @param {function} done 
 */
function findWorks(filter, context, done) {
    let param = {
        cond: {
            'space': context.space_id,
        },
        sorting: {
            'meta.create_time': 1
        }
    };

    if (filter.parent_id) param.cond.parent = filter.parent_id;
    if (filter.descendant_id) param.cond.childs = filter.descendant_id;

    if (filter.source) param.cond['source.type'] = filter.source;
    if (filter.status) param.cond['doing.status'] = filter.status;
    if (filter.status === 'unfinished') {
        param.cond['doing.status'] = {
            $nin: ['closed', 'completed']
        };
    }
    if (filter.performer) {
        let userId = filter.performer;
        if (userId === 'me') userId = context.user_id;
        param.cond['resource.performer'] = userId;
    }

    if (filter.monitor) {
        let userId = filter.monitor;
        if (userId === 'me') userId = context.user_id;
        param.cond['resource.monitor'] = userId;
        param.cond['resource.performer'] = {
            $ne: userId
        };
    }

    if (filter.followed) {
        let userId = context.user_id;
        param.cond['resource.followers'] = userId;
        // param.cond['resource.performer'] = {
        //     $ne: userId
        // };
    }

    if (filter.start_date) param.cond['planning.start_date'] = {
        $gte: new Date(filter.start_date)
    };

    if (filter.due_date) param.cond['planning.due_date'] = {
        $lte: new Date(filter.due_date)
    };

    if (filter.hierarchy === 'top') {
        param.cond.parent = null;
    }

    if (filter.hierarchy === 'leaf') {
        param.cond.child_number = 0;
    }

    data.works.find(param, done);
}

/**
 * Add new work
 * @param {object} work 
 * @param {object} context Authentication info {space_id, user_id, uname} 
 * @param {function} done 
 */
function addWork(work, context, done) {
    let err = cm.validate(work, context);
    if (err) return done(err);

    // fill parent
    if (!work.parent) {
        return doAddWork(work, context, done);
    }

    // fill all parents
    data.works.findById(work.parent, function(err, parentWork) {
        if (err) return done(err);
        if (!parentWork) {
            work.parent = null;
            return doAddWork(work, context, done);
        }

        // fill inherited fields
        if (!parentWork.all_parents) {
            work.all_parents = [work.parent];
        }
        else {
            work.all_parents = [];
            parentWork.all_parents.map(function(item) {
                work.all_parents.push(item);
            });
            work.all_parents.push(work.parent);
        }
        
        // set source
        work.source = parentWork.source;

        return doAddWork(work, context, function(err, inserted) {
            if (err) return done(err);
            // update childs of parent work
            let childs = Array.from(parentWork.childs);
            childs.push(inserted._id);
            data.works.update({
                id: parentWork._id,
                childs: childs,
                child_number: childs.length,
            }, function(err) {
                if (err) return done(err);

                // update parents doing status                
                updateDoingForParents(work.all_parents, function(err) {
                    if (err) return done(err);
                    done(null, inserted);
                });
            });
        });
    });
}

function doAddWork(work, context, done) {
    // fill info
    work.has_breakdown = false;
    work.space = context.space_id;
    work.meta = {
        create_user: {
            user_id: context.user_id,
            uname: context.uname
        }
    };
    
    // set default monitor is creator
    work.resource = Object.assign({}, {
        monitor: context.user_id
    }, work.resource);
    
    // set default source is personal
    if (!work.source) {
        work.source = {
            type: 'personal'
        };
    }

    work.doing = Object.assign({}, cm.constant.DOING);

    if (work.source.type === 'personal') {
        work.resource = {
            performer: context.user_id,
            monitor: context.user_id
        };
        work.doing.status = 'assigned_accept';
    }
    
    // generate comment stream id
    work.comment_stream = dbUtils.newId();
    // generate reaction stream id
    work.reaction_stream = dbUtils.newId();

    // save to database    
    data.works.insert(work, function(err, inserted) {
        if (err) return done(err);
        done(null, inserted);

        // insert new comment stream
        commentService.addCommentStream({
            _id: inserted.comment_stream,
            space: context.space_id,
            owner: context.user_id,
            ref_type: 'work',
            ref_id: inserted._id,
            ref_obj: {
                source: inserted.source,
                resource: inserted.resource
            }
        }, context, function(error) {
            if (error) logger.error('insert comment stream for work fail', error);
        });

        // insert new reaction stream
        reactionService.addReactionStream({
            _id: inserted.reaction_stream,
            space: context.space_id,
            owner: context.user_id,
            ref_type: 'work',
            ref_id: inserted._id,
        }, context, function(error) {
            if (error) logger.error('insert reaction stream for work fail', error);
        });

        // update tags
        updateTags(inserted.category_tags, inserted.knowledge_tags, context);
    });
}

function editWorkContent(work, context, done) {
    // TODO: validate data
    // remove null props
    cmUtils.removeEmptyProps(work);

    // fill info    
    cm.logEditingInfo(work, context);

    data.works.findById(work.id, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow creator, monitor and performer edit content
        let pErr = cm.authorize(foundWork, context);
        if (pErr) return done(pErr);

        // save to database
        data.works.update(work, done);

        // update tags
        updateTags(work.category_tags, work.knowledge_tags, context);
    });
}

function updateTags(categoryTags, knowledgeTags, context) {
    // update tags
    if (categoryTags && categoryTags.length > 0) {
        tagService.updateTags(categoryTags, cm.constant.WORK_CATEGORY_TAG_SUBJ, context, function(err) {
            if (err) logger.error('update category tags for work fail', err);
            else logger.debug('update category tags for work ok');
        });
    }

    if (knowledgeTags && knowledgeTags.length > 0) {
        tagService.updateTags(knowledgeTags, cm.constant.WORK_KNOWLEDGE_TAG_SUBJ, context, function(err) {
            if (err) logger.error('update knowledge tags for work fail', err);
            else logger.debug('update knowledge tags for work ok');
        });
    }
}

function editWorkPlanning(work, context, done) {
    // TODO: validate data
    // remove null props
    cmUtils.removeEmptyProps(work);

    // fill info    
    cm.logEditingInfo(work, context);

    data.works.findById(work.id, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow creator, monitor and performer edit content
        let pErr = cm.authorize(foundWork, context);
        if (pErr) return done(pErr);

        // save to database
        data.works.update(work, done);
    });
}

function editWorkResource(work, context, done) {
    // TODO: validate data
    // remove null props
    cmUtils.removeEmptyProps(work);

    // fill info    
    cm.logEditingInfo(work, context);

    data.works.findById(work.id, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow only creator to assign resource
        let pErr = cm.authorize(foundWork, context);
        if (pErr) return done(pErr);

        const currentStatus = (foundWork.doing && foundWork.doing.status) || 'created';
        // check final status
        if (currentStatus === 'completed' || currentStatus === 'closed') {
            return done('final_status');
        }

        const oldPerformer = foundWork.resource && foundWork.resource.performer;
        const newPerformer = work.resource && work.resource.performer;

        if (newPerformer && newPerformer !== oldPerformer) {
            // changing performer
            work.doing = {
                status: newPerformer === context.user_id ? 'assigned_accept' : 'assigned'
            };
        }

        // save to database
        data.works.update(work, function(err, updatedWork) {
            if (err) return done(err);
            done(null, updatedWork);

            // notify to performer
            if (newPerformer && newPerformer !== oldPerformer) workNotify.notifyPerformer(updatedWork, context);

            // notify to monitor
            const oldMonitor = foundWork.resource && foundWork.resource.monitor;
            const newMonitor = work.resource && work.resource.monitor;
            if (newMonitor && newMonitor !== oldMonitor) workNotify.notifyMonitor(updatedWork, context);

            // update reference object of comment stream
            if (updatedWork.comment_stream) {
                commentService.updateStream({
                    id: updatedWork.comment_stream,
                    ref_obj: {
                        resource: cmUtils.toPlainObj(updatedWork.resource)
                    }
                }, context, function(err) {
                    if (err) return logger.warn("update reference work object for comment stream error", err);
                });
            }
        });
    });
}

function editWorkStatus(workId, status, context, done) {
    // TODO: validate data
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow creator assign resource
        const performer = foundWork.resource && foundWork.resource.performer;
        const monitor = foundWork.resource && foundWork.resource.monitor;
        const oldStatus = foundWork.doing && foundWork.doing.status;
        console.log('edit status', oldStatus, monitor, context.user_id);

        if ((oldStatus === 'done' || oldStatus === 'completed') && context.user_id !== monitor) return done(constant.err.permission_deny);
        if (oldStatus !== 'done' && oldStatus !== 'completed' && context.user_id !== performer) return done(constant.err.permission_deny);
        if (oldStatus === 'completed' && status !== 'done') return done(constant.err.params_invalid);
        updateDoing(foundWork, null, status, context, done);
    });
}

function editWorkAttachments(work, context, done) {
    // TODO: validate data
    // remove null props
    cmUtils.removeEmptyProps(work);

    // fill info    
    cm.logEditingInfo(work, context);

    data.works.findById(work.id, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow creator assign resource
        let pErr = cm.authorize(foundWork, context, ["creator", "monitor"]);
        if (pErr) return done(pErr);

        // save to database
        data.works.update(work, done);
    });
}

function editWorkProgress(workId, progress, context, done) {
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);

        // check permission
        const performer = foundWork.resource && foundWork.resource.performer;
        if (context.user_id !== performer) return done(constant.err.permission_deny);

        // allow update progress when status is 'doing' or 'done'
        const currentStatus = foundWork.doing && foundWork.doing.status;
        if (currentStatus !== 'doing' && currentStatus !== 'done') return done('work_not_editable_status');

        updateDoing(foundWork, progress, null, context, done);
    });
}

function editWorkResult(work, context, done) {
    // validate data
    if(!work || !work.id || !work.result) return done(constant.err.params_invalid);

    // remove null props
    cmUtils.removeEmptyProps(work);
    cmUtils.removeEmptyProps(work.result);

    // fill info    
    cm.logEditingInfo(work, context);

    data.works.findById(work.id, function(err, foundWork) {
        if (err) return done(err);

        // check permission, allow performer update result of work
        let pErr = cm.authorize(foundWork, context, ["performer"]);
        if (pErr) return done(pErr);

        work.result = Object.assign({}, foundWork.result, work.result);

        // save to database
        data.works.update(work, done);
    });
}

function deleteWork(workId, context, done) {
    data.works.findById(workId, function(err, work) {
        if (err) return done(err);
        if (!work) return done(constant.err.not_exist);
        // check permission
        let pErr = cm.authorize(work, context, ["creator", "performer"]);
        if (pErr) return done(pErr);

        // delete in database
        data.works.remove(workId, function(err, deleted) {
            if (err) return done(err);
            if (!deleted || !deleted.parent) return done(null, deleted);

            // update childs of parent work
            data.works.findById(deleted.parent, function(err, parentWork) {
                if (err || !parentWork) return done(err, deleted);

                let childs = parentWork.childs.filter(function(item) {
                    return item !== deleted._id;
                });
                data.works.update({
                    id: parentWork._id,
                    childs: childs,
                    child_number: childs.length,
                }, function(err) {
                    if (err) return done(err);

                    // update parents doing status                
                    updateDoingForParents(work.all_parents, function(err) {
                        if (err) return done(err);
                        done(null, deleted);
                    });
                });
            });
        });
    });
}

function getWork(workId, context, done) {
    data.works.findById(workId, function(err, work) {
        if (err || !work) return done(err, work);
        asyn.parallel([function(cb) {
                // get parents
                if (!work.all_parents || work.all_parents.length === 0) return cb();
                data.works.find({
                    cond: {
                        _id: {
                            $in: work.all_parents
                        }
                    }
                }, function(err, r) {
                    if (err) return cb(err);
                    work.all_parent_works = r;
                    cb();
                });
            },
            function(cb) {
                // get childs            
                data.works.find({
                    cond: {
                        parent: work._id
                    }
                }, function(err, r) {
                    if (err) return cb(err);
                    work.child_works = r;
                    cb();
                });

            }
        ], function(err) {
            return done(null, work);
        });
    });
}

function updateDoing(work, progress, status, context, done) {
    if (!progress && !status) return done(null, work);
    const oldStatus = work.doing && work.doing.status;

    if (oldStatus !== "assigned" && work.child_number > 0) return done('work_has_child'); // do not update status & progress of parent work manually


    if (progress) {
        work.doing.progress = progress;
        if (status) work.doing.status = status;
        if (progress == 100 && work.doing.status === 'doing') {
            work.doing.status = 'done';
        }
        else if (progress < 100 && work.doing.status === 'done') {
            work.doing.status = 'doing';
        }
    }
    else if (status) {
        work.doing.status = status;
        if (progress) work.doing.progress = progress;
        if (work.doing.status === 'done') work.doing.progress = 100;
    }

    const newStatus = work.doing && work.doing.status;

    // logging editing user
    cm.logEditingInfo(work, context);

    // update work doing
    data.works.updateFields({
        _id: work._id
    }, {
        'doing.status': work.doing.status,
        'doing.progress': work.doing.progress,
        'meta.modify_user': work.meta.modify_user,
        'meta.modify_date': work.meta.modify_date,
    }, function(err) {
        if (err) return done(err);
        // notify work status
        if (newStatus && newStatus !== oldStatus) workNotify.notifyStatus(work, oldStatus, context);

        if (!work.all_parents || work.all_parents.length === 0) return done(null, work);
        updateDoingForParents(work.all_parents, function(err) {
            if (err) return done(err);
            done(null, work);
        });
    });

}

function updateDoingForParents(parentIds, done) {
    const parents = Array.from(parentIds);
    parents.reverse();
    asyn.eachSeries(parents, updateDoingForParent, done);
}

function updateDoingForParent(workId, done) {
    // console.log('updateDoingByChilds', workId);

    data.works.calcDoing(workId, function(err, r) {
        if (err || !r) return done(err);
        console.log('calcDoing', r);

        const childStatus = r.all_status;
        let update = {
            'doing.progress': r.avg_progress,
            'doing.spent_time': r.total_spent_time
        };

        let avgScore = 0;
        if (childStatus.length > 0) {
            const totalScore = r.all_status.reduce(function(sum, currentValue) {
                return sum + cm.getStatusScore(currentValue);
            }, 0);
            avgScore = totalScore / childStatus.length;
        }

        let status = 'assigned_accept';
        if (avgScore <= 1) status = 'assigned_accept';
        else if (avgScore < 3) status = 'doing';
        else if (avgScore < 3.1) status = 'done';
        else status = 'completed';

        update['doing.status'] = status;
        console.log('update parent status: ', childStatus, avgScore, status);

        data.works.updateFields({
            _id: workId
        }, update, done);
    });
}

module.exports = {
    findWorks,
    addWork,
    editWorkContent,
    editWorkPlanning,
    editWorkResource,
    editWorkStatus,
    editWorkAttachments,
    editWorkProgress,
    editWorkResult,
    deleteWork,
    getWork,
};
