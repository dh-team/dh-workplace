'use strict';
const constant = require('../../utils/constant');

function getStatusScore(status) {
    switch (status) {
        case 'created':
        case 'assigned':
        case 'assigned_accept':
        case 'assigned_reject':
        case 'todo':
            return 1;

        case 'doing':
        case 'redo':
            return 2;

        case 'done':
            return 3;

        case 'completed':
        case 'closed':
            return 3.1;
    }
}

function validate(work, context) {
    // TODO
}

function authorize(work, context, allow) {
    // check permission, allow creator, monitor and performer edit content
    let creator = work.meta && work.meta.create_user && work.meta.create_user.user_id;
    let performer = work.resource && work.resource.performer;
    let monitor = work.resource && work.resource.monitor;

    // console.log('authorize', allow, context.user_id, creator, performer, monitor);

    if ((!allow || allow.indexOf("performer") !== -1) && context.user_id === performer) return null;
    if ((!allow || allow.indexOf("monitor") !== -1) && context.user_id === monitor) return null;
    if ((!allow || allow.indexOf("creator") !== -1) && context.user_id === creator) return null;

    return constant.err.permission_deny;
}

function logEditingInfo(work, context) {
    // fill info    
    work.meta = Object.assign({}, work.meta, {
        modify_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        modify_time: new Date()
    });
}


module.exports = {
    constant: {
        DOING: {
            status: 'created',
            tasks: [],
            spent_time: 0, // in hours
            progress: 0, // percentage
            display_type: 'historical'
        },
        TASK: {
            display_order: 1,
            note: null,
            status: 'created',
            start_time: null,
            stop_time: null,
            spent_time: 0
        },
        WORK_CATEGORY_TAG_SUBJ: 'work_category',
        WORK_KNOWLEDGE_TAG_SUBJ: 'work_knowledge'
    },
    getStatusScore,
    validate,
    authorize,
    logEditingInfo,
};
