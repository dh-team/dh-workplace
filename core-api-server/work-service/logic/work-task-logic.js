'use strict';

const cm = require('./work-cm-logic');
const constant = require('../../utils/constant');
const data = require('../data');
const cmUtils = require('../../utils/common');

function addWorkTask(workId, task, context, done) {
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);
        if (!foundWork) return done(constant.err.not_exist);

        // check permission, allow only performer add tas
        let pErr = cm.authorize(foundWork, context, ["performer"]);
        if (pErr) return done(pErr);

        // update doing task
        let doing = Object.assign({}, cm.constant.DOING, foundWork.doing);
        let newTask = Object.assign({}, cm.constant.TASK, {
            note: task.note,
            display_order: doing.tasks.length + 1
        });
        if (task.status === 'running') {
            newTask.status = 'running';
            newTask.start_time = new Date();
        }
        doing.tasks.push(newTask);

        updateTasks(doing);

        // save to database
        data.works.update({
            id: workId,
            doing: doing
        }, done);
    });
}

function editWorkTask(workId, task, context, done) {
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);
        if (!foundWork) return done(constant.err.not_exist);

        // check permission, allow only performer add tas
        let pErr = cm.authorize(foundWork, context, ["performer"]);
        if (pErr) return done(pErr);

        cmUtils.removeEmptyProps(task);

        // update doing task
        let doing = Object.assign({}, cm.constant.DOING, foundWork.doing);
        doing.tasks = doing.tasks.map(function(item) {
            if (item.display_order != task.display_order) return item;
            let updated = Object.assign({}, item, task);
            if (task.status === 'running') {
                updated.start_time = new Date();
            }
            else if (task.status === 'stopped' || task.status === 'completed') {
                updated.stop_time = new Date();
                if (item.status === 'running' && updated.start_time) { // old status is 'running'
                    let spentTime = Math.round((updated.stop_time - updated.start_time) / 1000);
                    updated.spent_time = (updated.spent_time || 0) + (spentTime > 0 ? spentTime : 0);
                }
            }
            return updated;
        });
        updateTasks(doing);

        // save to database
        data.works.update({
            id: workId,
            doing: doing
        }, done);
    });
}

function changeTaskOrder(workId, from, to, context, done) {
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);
        if (!foundWork) return done(constant.err.not_exist);

        // check permission, allow only performer add tas
        let pErr = cm.authorize(foundWork, context, ["performer"]);
        if (pErr) return done(pErr);


        // update doing task
        let doing = Object.assign({}, cm.constant.DOING, foundWork.doing);
        doing.tasks = cmUtils.changeItemOrder(doing.tasks, from - 1, to - 1);
        // console.log('change order', JSON.stringify(doing.tasks));
        updateTasks(doing);

        // save to database
        data.works.update({
            id: workId,
            doing: doing
        }, done);
    });
}

function deleteWorkTask(workId, taskId, context, done) {
    data.works.findById(workId, function(err, foundWork) {
        if (err) return done(err);
        if (!foundWork) return done(constant.err.not_exist);

        // check permission, allow only performer add tas
        let pErr = cm.authorize(foundWork, context, ["performer"]);
        if (pErr) return done(pErr);


        // update doing task
        let doing = Object.assign({}, cm.constant.DOING, foundWork.doing);
        doing.tasks = doing.tasks.filter(function(item) {
            return item.display_order != taskId;
        });
        updateTasks(doing);

        // save to database
        data.works.update({
            id: workId,
            doing: doing
        }, done);
    });
}

function updateTasks(doing) {
    doing.spent_time = doing.tasks.reduce(function(acc, curr) {
        return acc + curr.spent_time || 0;
    }, 0);

    doing.tasks.map(function(item, index) {
        item.display_order = index + 1;
    });

    if (doing.display_type === 'checklist') {
        let completed = doing.tasks.reduce(function(acc, curr) {
            return acc + (curr.status === 'completed' ? 1 : 0);
        }, 0);
        doing.progress = Math.round(completed * 100 / doing.tasks.length);
        if (doing.status === 'doing' && doing.progress == 100) {
            doing.status = 'done';
        }
        else if (doing.status === 'done' && doing.progress != 100) {
            doing.status = 'doing';
        }
    }
}

module.exports = {
    addWorkTask,
    editWorkTask,
    changeTaskOrder,
    deleteWorkTask
};
