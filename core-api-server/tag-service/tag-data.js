'use strict';

const Tag = require('../mongodb/models/tag');

function filter(cond, sort, done) {
    if ("function" === typeof sort) {
        done = sort;
        Tag.find(cond, done);
    }
    else {
        Tag.find(cond).sort(sort).exec(done);
    }
}

function insertIfNotExist(tag, done) {
    Tag.update(tag, tag, {
        upsert: true
    }, done);
}

module.exports = {
    filter,
    insertIfNotExist
};
