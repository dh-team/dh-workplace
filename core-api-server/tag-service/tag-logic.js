'use strict';

const asyn = require('async');
const data = require('./tag-data');

/**
 * Find tags by subject
 * @param {string} subject Subject to filter 
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function listTagsBySubject(subject, context, done) {
    return data.filter({
        space: context.space_id,
        subject: subject
    }, {
        keyword: 1
    }, done);
}

/**
 * Upsert tags
 * @param {[string]} tags List of tags to insert (if not exist)
 * @param {string} subject subject to filter 
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function updateTags(tags, subject, context, done) {
    if (!tags || tags.length === 0) return done(null);
    asyn.each(tags, function(tag, cb) {
        data.insertIfNotExist({
            space: context.space_id,
            subject: subject,
            keyword: tag.trim()            
        }, cb);
    }, done);
}

module.exports = {
    listTagsBySubject,
    updateTags
};
