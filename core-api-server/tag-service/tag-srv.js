const auth = require('../resource-auth');
const httpUtils = require("../utils/http");
const tagLogic = require("./tag-logic");

function listTags(req, res) {
    var context = httpUtils.extractAuthInfo(req);
    var subject = httpUtils.extractParam(req, "subject");
    tagLogic.listTagsBySubject(subject, context, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function registerHandler(app) {
    app.get("/api/space/tags", [auth.spaceAuthSuccess, listTags]);
}

module.exports = {
    registerHandler: registerHandler
};
