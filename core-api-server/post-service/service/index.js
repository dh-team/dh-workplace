const categoryService = require('./category-srv');
const postService = require('./post-srv');

function registerHandler(app) {

    // post categories
    app.post("/api/space/post/category/list", categoryService.getCategories);
    app.get("/api/space/post/category/list", categoryService.getCategories);

    app.post("/api/space/post/category/edit", categoryService.editCategory);
    app.get("/api/space/post/category/edit", categoryService.editCategory);

    app.post("/api/space/post/category/add", categoryService.addCategory);
    app.get("/api/space/post/category/add", categoryService.addCategory);

    app.post("/api/space/post/category/update-parent", categoryService.updateCategoryParent);
    app.get("/api/space/post/category/update-parent", categoryService.updateCategoryParent);

    app.post("/api/space/post/category/delete", categoryService.deleteCategory);
    app.get("/api/space/post/category/delete", categoryService.deleteCategory);

    app.post("/api/space/post/category/info", categoryService.getCategory);
    app.get("/api/space/post/category/info",  categoryService.getCategory);

    // posts
    app.post("/api/space/post/list", postService.getPosts);
    app.get("/api/space/post/list", postService.getPosts);

    app.post("/api/space/post/edit", postService.editPost);
    app.get("/api/space/post/edit", postService.editPost);

    app.post("/api/space/post/add", postService.addPost);
    app.get("/api/space/post/add", postService.addPost);

    app.post("/api/space/post/delete", postService.deletePost);
    app.get("/api/space/post/delete", postService.deletePost);

    app.post("/api/space/post/get", postService.getPost);
    app.get("/api/space/post/get", postService.getPost);
}

module.exports = {
    registerHandler: registerHandler
};
