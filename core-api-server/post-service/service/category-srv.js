const spaceAuth = require('../../resource-auth');
const categoryLogic = require('../logic/category-logic');
const httpUtils = require("../../utils/http");
const logger = require("../../logger");

function getCategories(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    categoryLogic.getCategories(context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function addCategory(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const cat = httpUtils.extractObject(req, ["name", "description", "parent"]);
    logger.debug("add cat, params: " + JSON.stringify(cat));

    categoryLogic.addCategory(cat, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function editCategory(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const category = httpUtils.extractObject(req, ["id", "name", "description"]);
    logger.debug("edit category, params: " + JSON.stringify(category));

    categoryLogic.editCategory(category, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function updateCategoryParent(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const param = httpUtils.extractObject(req, ["id", "parent_id"]);
    logger.debug("update category parent, params: " + JSON.stringify(param));

    if (!param.id || !param.parent_id) {
        return httpUtils.responseInvalid(res, "param_invalid");
    }

    const updateCategory = {
        id: param.id,
        parent: param.parent_id
    }

    categoryLogic.editCategory(updateCategory, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function deleteCategory(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const categoryId = httpUtils.extractParam(req, "id");
    logger.debug("delete category, id: " + categoryId);

    categoryLogic.removeCategory(categoryId, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function getCategory(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const catId = httpUtils.extractParam(req, "category_id");

    categoryLogic.getCategoryById(catId, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

module.exports = {
    getCategories: [spaceAuth.spaceAuthSuccess, getCategories],
    getCategory: [spaceAuth.spaceAuthSuccess, getCategory],
    addCategory: [spaceAuth.spaceAuthSuccess, spaceAuth.adminAuthorized, addCategory],
    editCategory: [spaceAuth.spaceAuthSuccess, spaceAuth.adminAuthorized, editCategory],
    updateCategoryParent: [spaceAuth.spaceAuthSuccess, spaceAuth.adminAuthorized, updateCategoryParent],
    deleteCategory: [spaceAuth.spaceAuthSuccess, spaceAuth.adminAuthorized, deleteCategory]
}
