'use strict';
const spaceAuth = require('../../resource-auth');
const postLogic = require('../logic/post-logic');
const httpUtils = require("../../utils/http");
const stringUtils = require("../../utils/string");
const logger = require("../../logger");

function getPosts(req, res) {
    const type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        getLivePosts(req, res);
    }
    else {
        getArchivePosts(req, res);
    }
}

function getLivePosts(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const param = httpUtils.extractObject(req, ["group_id", "cursor", "limit"]);

    postLogic.getLivePosts(param.group_id, param.cursor, param.limit, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function getArchivePosts(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const catId = httpUtils.extractParam(req, "category_id");

    postLogic.getArchivePosts(catId, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function addPost(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    let post;
    const type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        post = httpUtils.extractObject(req, ["type", "content", "group_id", "attachments"]);
    }
    else {
        post = httpUtils.extractObject(req, ["type", "title", "content", "tags", "category_id"]);
    }
    logger.debug("add post, params: " + JSON.stringify(post));

    postLogic.addPost(post, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function editPost(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    let post;
    const type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        post = httpUtils.extractObject(req, ["id", "content"]);
    }
    else {
        post = httpUtils.extractObject(req, ["id", "title", "content", "tags", "category_id"]);
    }
    logger.debug("edit post: " + JSON.stringify(post));

    postLogic.editPost(post, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function deletePost(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const id = httpUtils.extractParam(req, "id");
    logger.debug("delete post, id: " + id);

    postLogic.removePost(id, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

function getPost(req, res) {
    const context = httpUtils.extractAuthInfo(req);
    const id = httpUtils.extractParam(req, "post_id");

    postLogic.getPostById(id, context, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

module.exports = {
    getPosts: [spaceAuth.spaceAuthSuccess, getPosts],
    getPost: [spaceAuth.spaceAuthSuccess, getPost],
    addPost: [spaceAuth.spaceAuthSuccess, addPost],
    editPost: [spaceAuth.spaceAuthSuccess, editPost],
    deletePost: [spaceAuth.spaceAuthSuccess, deletePost]
}
