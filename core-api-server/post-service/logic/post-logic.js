'use strict';

// const asyn = require('async');
const lo = require('lodash');

const data = require('../data');
const constant = require('../../utils/constant');

const commentService = require('../../service-proxy/comment');
const reactionService = require('../../service-proxy/reaction');
const tagService = require('../../service-proxy/tag');
const notifyService = require('../../service-proxy/notification');

const stringUtils = require('../../utils/string');
const authUtils = require('../../utils/authorize');
const dbUtils = require('../../mongodb/utils');
const logger = require('../../logger');

const POST_TAG_SUBJ = 'post';

function getPostById(id, context, done) {
    return data.posts.findById(id, function (err, r) {
        if (err) return done(err);
        if (!r) return done(constant.err.not_exist);

        // authorize
        if (r.space !== context.space_id) return done(constant.err.permission_deny);

        return done(null, r);
    });
}

function getArchivePosts(catId, context, done) {
    data.posts.find({
        type: 'archive',
        space: context.space_id,
        categories: catId
    }, {
            'meta.create_time': -1
        }, function (err, r) {
            if (err) return done(err);
            var posts = r.map(function (item) {
                item = item.toObject();
                item.content = stringUtils.plainPreviewOfHtml(item.content);
                return item;
            });
            return done(null, posts);
        });
}

function getLivePosts(group, cursor, limit, context, done) {
    if (!group) {
        group = ['all'].concat(context.groups);
    }

    data.posts.findByGroup(context.space_id, group, cursor, limit, function (err, posts) {
        if (err) return done(err);
        return done(null, posts);
    });
}

function addPost(post, context, done) {
    // TODO: validate
    // TODO: authorize

    post.space = context.space_id;
    normalizePostParams(post);
    post.meta = {
        create_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        create_time: new Date()
    };

    // generate comment stream id
    post.comment_stream = dbUtils.newId();
    // generate reaction stream id
    post.reaction_stream = dbUtils.newId();

    data.posts.insertPost(post, function (err, inserted) {
        if (err) return done(err);
        done(null, inserted);

        // insert new comment stream
        addCommentStream(inserted, context);

        // insert new reaction stream
        addReactionStream(inserted, context);

        // update tags
        updateTags(inserted, context);

        // archive post: increase post_count of categories
        if (inserted.categories && inserted.categories.length > 0) {
            data.categories.updateCategoriesStat(inserted.categories, 1, function (error) {
                if (error) logger.error("update post categories stat fail", error);
            });
        }

        // notify new post
        notifyNewPost(inserted, context);
    });
}

function editPost(post, context, done) {
    // TODO: validate
    // TODO: authorize

    normalizePostParams(post);
    post.meta = {
        modify_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        modify_time: new Date()
    };

    data.posts.findById(post.id, function (err, foundPost) {
        if (err) return done(err);
        if (!foundPost) return done(constant.err.not_exist);

        // authorize
        const pErr = authUtils.authorizeAction(foundPost, constant.act.modify, context);
        if (pErr) return done(pErr);

        data.posts.updatePost(post, function (err, updated) {
            if (err) return done(err);
            done(null, updated);

            // update tags
            if (!lo.isEqual(foundPost.tags, updated.tags)) {
                updateTags(updated, context);
            }

            // notify edit post
            notifyEditPost(updated, context);

            // update categories stat
            if (!lo.isEqual(foundPost.categories, updated.categories)) {
                data.categories.updateCategoriesStat(foundPost.categories, -1, function (error) {
                    if (error) logger.error("update post categories stat fail", error);
                });

                data.categories.updateCategoriesStat(updated.categories, 1, function (error) {
                    if (error) logger.error("update post categories stat fail", error);
                });
            }
        });
    });
}

function removePost(postId, context, done) {
    data.posts.findById(postId, function (err, foundPost) {
        if (err) return done(err);
        if (!foundPost) return done(constant.err.not_exist);

        // authorize
        const pErr = authUtils.authorizeAction(foundPost, constant.act.manage, context);
        if (pErr) return done(pErr);

        data.posts.deletePost(postId, function (err, deleted) {
            if (err) return done(err);
            done(null, foundPost);

            // update categories stat
            if (foundPost.categories && foundPost.categories.length > 0) {
                data.categories.updateCategoriesStat(foundPost.categories, -1, function (error) {
                    if (error) logger.error("update post categories stat fail", error);
                });
            }
        });
    });
}

function normalizePostParams(post) {
    // normarlize scope
    if (post.group_id) {
        var scope = ['g:' + post.group_id];
        post.scope = {
            view_scope: scope,
            interact_scope: scope,
            modify_scope: ['u:owner'],
            manage_scope: ['u:owner'],
            notify_scope: scope
        };
    }

    // normarlize category
    if (post.category_id) {
        post.categories = [post.category_id];
        delete post.category_id;
    }

    // normarlize tags
    if ('string' === typeof (post.tags)) {
        post.tags = post.tags.split(',');
    }
    if (post.tags && post.tags.length > 0) {
        post.tags = post.tags.map((tag) => tag.trim());
    }

    // attachments
    if (post.attachments && 'object' !== typeof (post.attachments)) {
        try {
            post.attachments = JSON.parse(post.attachments);
        }
        catch (e) {
            delete post.attachments;
        }
    }
}

function addCommentStream(post, context) {
    commentService.addCommentStream({
        _id: post.comment_stream,
        space: post.space,
        owner: context.user_id,
        ref_type: "post",
        ref_id: post._id,
        ref_obj: {
            type: post.type,
            create_user: context.user_id
        }
    }, context, function (error) {
        if (error) logger.error("insert comment stream for post fail", error);
    });
}

function addReactionStream(post, context) {
    reactionService.addReactionStream({
        _id: post.reaction_stream,
        space: post.space,
        owner: context.user_id,
        ref_type: "post",
        ref_id: post._id,
    }, context, function (error) {
        if (error) logger.error("insert reaction stream for post fail", error);
    });
}

function updateTags(post, context) {
    tagService.updateTags(post.tags, POST_TAG_SUBJ, context, function (error) {
        if (error) logger.error("update category tags for post fail", error);
    });
}

function notifyNewPost(post, context) {
    var no = {
        space: post.space,
        type: 'post_new',
        ref_data: {
            post: {
                id: post._id,
                type: post.type,
                categories: post.categories,
                view_scope: post.scope.view_scope
            },
            preview_content: post.title,
            issue_user: post.meta.create_user,
            issue_time: post.meta.create_time,
        }
    };

    if (!no.ref_data.preview_content && post.type === "live") {
        no.ref_data.preview_content = stringUtils.inlinePreviewOfPlainText(post.content);
    }

    const target = {
        scope: post.scope.notify_scope
    };

    notifyService.pushNotification(no, target, context, function (error) {
        if (error) logger.error("notify new post fail", error);
    });
}

function notifyEditPost(post, context) {
    var no = {
        space: post.space,
        type: 'post_modify',
        ref_data: {
            post: {
                id: post._id,
                type: post.type,
                categories: post.categories,
                view_scope: post.scope.view_scope
            },
            preview_content: post.title,
            issue_user: post.meta.modify_user,
            issue_time: post.meta.modify_time
        }
    };

    if (!no.ref_data.preview_content && post.type === "live") {
        no.ref_data.preview_content = stringUtils.inlinePreviewOfPlainText(post.content);
    }

    const target = {
        scope: post.scope.notify_scope
    };

    notifyService.pushNotification(no, target, context, function (error) {
        if (error) logger.error("notify edit post fail", error);
    });
}

module.exports = {
    getPostById,
    getArchivePosts,
    getLivePosts,
    addPost,
    editPost,
    removePost
};
