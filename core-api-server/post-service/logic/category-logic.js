'use strict';

const asyn = require("async");
const data = require('../data');
const constant = require('../../utils/constant');
const notifyService = require('../../service-proxy/notification');

function getCategoryById(id, context, done) {
    return data.findById(id, function (err, r) {
        if (err) return done(err);
        if (!r) return done(constant.err.not_exist);

        // authorize
        if (r.space !== context.space_id) return done(constant.err.permission_deny);

        return done(null, r);
    });
};

function getCategories(context, done) {
    return data.categories.find({ space: context.space_id }, { name: 1 }, function (err, cats) {
        if (err) return done(res, err);
        if (!cats || cats.length === 0) return done(null, cats);
        // calc statistic
        const stats = data.categories.calcStat(cats);
        cats = cats.map(function (cat) {
            cat = cat.toObject();
            const stat = stats.find((item1) => item1.id === cat._id);
            if (stat) {
                if (!cat.stat) cat.stat = {};
                cat.stat.child_count = stat.child_count;
                cat.stat.post_count = stat.post_count;
                cat.stat.total_child_count = stat.total_child_count;
                cat.stat.total_post_count = stat.total_post_count;
            }
            return cat;
        });
        done(null, cats);
    });
};

function addCategory(cat, context, done) {
    // TODO: validate
    // TODO: authorize

    cat.space = context.space_id;
    cat.meta = {
        create_user: {
            user_id: context.user_id,
            uname: context.uname
        }
    }

    data.categories.insertCategory(cat, function (err, inserted) {
        if (err) return done(err);
        done(null, inserted);

        // notify
        notifyNewPostCategory(inserted, context);
    });
};

function editCategory(cat, context, done) {
    // TODO: validate
    // TODO: authorize

    cat.meta = {
        modify_user: {
            user_id: context.user_id,
            uname: context.uname
        },
        modify_time: new Date()
    }

    data.categories.updateCategory(cat, function (err, updated) {
        if (err) return done(err);
        done(null, updated);

        // notify
        notifyModifyPostCategory(updated, context);
    });
};

function removeCategory(catId, context, done) {
    // TODO: validate
    // TODO: authorize

    data.categories.deleteCategory(catId, done);
}

// insert notification when new post category inserted
function notifyNewPostCategory(cat, context) {
    var no = {
        space: cat.space,
        type: 'post_cat_new',
        ref_data: {
            category: {
                id: cat._id,
                parent: cat.parent
            },
            issue_user: cat.meta.create_user,
            issue_time: cat.meta.create_time,
            preview_content: cat.name
        }
    }

    const target = {
        scope: cat.scope.notify_scope
    };

    notifyService.pushNotification(no, target, context, function (error) {
        if (error) logger.error("notify new post category fail", error);
    });
}

function notifyModifyPostCategory(cat, context) {
    var no = {
        space: cat.space,
        type: 'post_cat_modify',
        ref_data: {
            category: {
                id: cat._id,
                parent: cat.parent
            },
            issue_user: cat.meta.modify_user,
            issue_time: cat.meta.modify_time,
            preview_content: cat.name
        }
    }

    const target = {
        scope: cat.scope.notify_scope
    };

    notifyService.pushNotification(no, target, context, function (error) {
        if (error) logger.error("notify edit post category fail", error);
    });
}

module.exports = {
    getCategoryById,
    getCategories,
    addCategory,
    editCategory,
    removeCategory
}
