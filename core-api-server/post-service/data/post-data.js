'use strict';

const Post = require('../../mongodb/models/post');
const utils = require('../../utils/common');

function findOne(cond, done) {
    Post.findOne(cond, function(err, post) {
        if (err) return done(err);
        return done(null, post);
    });
}

function findById(id, done) {
    return findOne({
        _id: id
    }, done);
}

function find(cond, sort, done) {
    if ("function" === typeof sort) {
        done = sort;
        Post.find(cond, done);
    }
    else {
        Post.find(cond).sort(sort).exec(done);
    }
}


function findByGroup(spaceId, groupId, cursor, limit, done) {
    if (!limit || limit > 10 || limit < 1) limit = 10;
    var cond = {
        "type": "live",
        "space": spaceId
    };
    if (groupId instanceof Array) {
        var groups = groupId.map(function(item) {
            return ("g:" + item);
        });
        cond["scope.view_scope"] = {
            "$in": groups
        };
    }
    else {
        cond["scope.view_scope"] = "g:" + groupId;
    }

    if (cursor) {
        cond["_id"] = {
            $lt: cursor
        };
    }

    Post.aggregate([{
        $match: cond
    }, {
        $sort: {
            "_id": -1
        }
    }, {
        $limit: limit
    }, {
        $lookup: {
            from: "comment_streams",
            localField: "comment_stream",
            foreignField: "_id",
            as: "comment_stream_docs"
        }
    },{
        $lookup: {
            from: "reaction_streams",
            localField: "reaction_stream",
            foreignField: "_id",
            as: "reaction_stream_docs"
        }
    }, {
        $project: {
            _id: 1,
            title: 1,
            content: 1,
            tags: 1,
            stat: 1,
            allow_comment: 1,
            scope: 1,
            meta: 1,
            reactions: 1,
            comment_stream: 1,
            reaction_stream: 1,
            attachments: 1,
            "comment_stream_docs.stat": 1,
            reaction_stream_docs: 1
        }
    }], function(err, r) {
        if (err) return done(err);

        return done(null, r.map(function(item) {
            if (item.comment_stream_docs.length > 0) {
                item.comment_data = item.comment_stream_docs[0];
            }
            delete item.comment_stream_docs;
            
            if (item.reaction_stream_docs.length > 0) {
                item.reaction_data = item.reaction_stream_docs[0];
            }
            delete item.reaction_stream_docs;
            
            return item;
        }));
    });
}


function insertPost(post, done) {
    var newPost = new Post();
    utils.copy(post, newPost);
    newPost.save(function(err) {
        if (err) return done(err);
        return done(err, newPost);
    });
}

function updatePost(post, done) {
    Post.findOne({
        _id: post.id
    }, function(err, r) {
        if (err || !r) return done(err, r);
        utils.update(post, r);
        r.save(function(err) {
            if (err) return done(err);
            return done(null, r);
        });
    });
}

function deletePost(id, done) {
    Post.findOneAndRemove({
        _id: id
    }, function(err, deleted) {
        if (err) return done(err, deleted);
        return done(null, deleted);
    });
}

module.exports = {
    findOne,
    findById,
    find,
    findByGroup,
    insertPost,
    updatePost,
    deletePost
};
