'use strict';

const asyn = require('async');
const Category = require('../../mongodb/models/post-category');
const cmUtils = require('../../utils/common');

function findOne(cond, done) {
    Category.findOne(cond, function(err, cat) {
        if (err) return done(err);
        return done(null, cat);
    });
}

function findByID(id, done) {
    return findOne({
        _id: id
    }, done);
}

function find(cond, sort, done) {
    if ('function' === typeof sort) {
        done = sort;
        Category.find(cond, done);
    }
    else {
        Category.find(cond).sort(sort).exec(done);
    }
}

function insertCategory(cat, done) {
    var newCategory = new Category();
    cmUtils.copy(cat, newCategory);
    newCategory.save(function(err) {
        if (err) return done(err);
        buildTree(newCategory, function(err) {
            if (err) return done(err);
            return done(err, newCategory);
        });
    });
}

function updateCategory(cat, done) {
    Category.findOne({
        _id: cat.id
    }, function(err, thisCat) {
        if (err || !thisCat) return done(err, thisCat);
        cmUtils.update(cat, thisCat);
        var rebuild = thisCat.isModified('parent');
        thisCat.save(function(err) {
            if (err) return done(err);
            if (!rebuild) return done(null, thisCat);

            // rebuild tree
            console.log('change parent => rebuild tree');
            buildTreeForSpace(thisCat.space, function(err) {
                if (err) return done(err);
                return done(null, thisCat);
            });
        });
    });
}

function deleteCategory(id, done) {
    Category.findOneAndRemove({
        _id: id
    }, function(err, deleted) {
        if (err || !deleted) return done(err, deleted);
        moveChildsUp(deleted, function(err) {
            if (err) return done(err);
            buildTreeForSpace(deleted.space, function(err) {
                if (err) return done(err);
                return done(null, deleted);
            });
        });
    });
}

function moveChildsUp(deleted, done) {
    // move childs to parent of this
    Category.update({
        'parent': deleted._id
    }, {
        $set: {
            'parent': deleted.parent
        }
    }, {
        multi: true
    }, done);
}

function buildTreeForSpace(spaceID, done) {
    Category.update({
        'parent': null
    }, {
        $set: {
            'all_parents': []
        }
    }, {
        multi: true
    }, function(err, r) {
        if (err) return done(err);
        find({
            space: spaceID,
            parent: null
        }, function(err, cats) {
            if (err) return done(err);
            asyn.each(cats, buildTreeR, done);
        });
    });
}

function buildTree(cat, done) {
    getParents(cat, function(err, parents) {
        if (err) return done(err);
        cat.all_parents = parents;
        cat.save(function(err) {
            if (err) return done(err);
            buildTreeR(cat, function(err) {
                if (err) return done(err);
                done(null, cat);
            });
        });
    });
}

function buildTreeR(cat, done) {
    console.log('buildTreeR: ' + cat._id + ', parents: ' + JSON.stringify(cat.all_parents));
    var parents = cat.all_parents;
    parents.unshift(cat._id);
    var cond = {
        'parent': cat._id
    };
    // update childs
    Category.update(cond, {
        $set: {
            'all_parents': parents
        }
    }, {
        multi: true
    }, function(err, r) {
        if (err) return done(err);
        if (r.nMatched === 0) return done(null); // no childs
        find(cond, function(err, childs) {
            if (err) return done(err);
            if (!childs || childs.length === 0) return done(null);
            asyn.each(childs, buildTreeR, function(err) {
                if (err) return done(err);
                done(null);
            });
        });
    });
}

function getParents(cat, done) {
    getParentsR([], cat, done);
}

function getParentsR(parents, cat, done) {
    if (!cat.parent) {
        return done(null, parents);
    }
    findOne({
        _id: cat.parent
    }, function(err, r) {
        if (err || !r) return done(err, parents);
        parents.push(r._id);
        getParentsR(parents, r, done);
    });
}

function getStat(cat, done) {
    find({
        all_parents: cat._id
    }, function(err, childs) {
        if (err) return done(err);
        var stat = calcStatOfCat(cat, childs);
        done(null, stat);
    });
}

function findInMap(id, map) {
    var item = map[id];
    if (!item) {
        item = {
            id: id,
            child_count: 0,
            post_count: 0,
            total_child_count: 0,
            total_post_count: 0,
            childs: []
        };
        map[id] = item;
    }
    return item;
}

function addToMap(cat, map) {
    var obj = findInMap(cat._id, map);
    if (cat.stat) {
        obj.post_count = cat.stat.post_count;
    }
    return obj;
}

function calcStatOfCat(cat, childs) {
    var map = {};
    var stat = addToMap(cat, map);
    childs.map(function(item) {
        var childStat = addToMap(item, map);
        if (item.parent) {
            findInMap(item.parent, map).childs.push(childStat);
        }
    });

    calcStatOfCatR(stat);
    return stat;
}

function calcStatOfCatR(cat) {
    cat.total_post_count = cat.post_count;
    cat.total_child_count = cat.child_count = cat.childs.length;
    if (cat.child_count === 0) return;
    cat.childs.map(function(child) {
        calcStatOfCatR(child);
        cat.total_child_count += child.total_child_count;
        cat.total_post_count += child.total_post_count;
    });
}

function calcStat(cats) {
    var map = {};
    cats.map(function(item) {
        var catStat = addToMap(item, map);
        if (item.parent) {
            findInMap(item.parent, map).childs.push(catStat);
        }
    });

    Object.keys(map).map(function(field) {
        var cat = map[field];
        if (!cat.parent) {
            calcStatOfCatR(cat);
        }
    });

    return Object.keys(map).map(function(field) {
        return map[field];
    });
}

function updateCategoriesStat(catIDs, updateNum, done) {
    Category.update({
        '_id': {
            $in: catIDs
        }
    }, {
        $inc: {
            'stat.post_count': updateNum
        }
    }, {
        multi: true
    }, done);
}

function updateCategoryStat(catID, updateNum, done) {
    Category.update({
        '_id': catID
    }, {
        $inc: {
            'stat.post_count': updateNum
        }
    }, {
        multi: true
    }, done);
}

module.exports = {
    findOne,
    findByID,
    find,
    getParents,
    buildTree,
    buildTreeForSpace,
    insertCategory,
    updateCategory,
    deleteCategory,
    getStat,
    updateCategoriesStat,
    updateCategoryStat,
    calcStat
};
