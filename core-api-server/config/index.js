/*jslint node: true */
'use strict';
module.exports = require("../conf.json");
//
// The configuration options of the server
//

/**
 * Session configuration
 *
 * type - The type of session to use.  MemoryStore for "in-memory",
 * or MongoStore for the mongo database store
 * maxAge - The maximum age in milliseconds of the session.  Use null for
 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
 * for a year.
 * secret - The session secret that you should change to what you want
 * dbName - The database name if you're using Mongo
 */
module.exports.session = {
    type: "MemoryStore",
    maxAge: 3600000 * 24 * 7 * 52,
    //TODO You need to change this secret to something that you choose for your secret
    secret: "PxGZT2bJpGyrFddDphNW",
    dbName: "core-api-server-session"
};


/**
 * type - Webserver type, "http" or "https" 
 * port - Listening port 
 */
// module.exports.server = {
//     type: 'http',
//     port: 8083,

// }

// module.exports.authServer = {
//     tokenInfoUri: module.exports.authServer.url + "/as/oauth/tokeninfo",
// }

module.exports.db = {
    type: "mongodb",
    dbName: "main-db"
};
