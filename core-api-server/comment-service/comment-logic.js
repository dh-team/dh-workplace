'use strict';

// const asyn = require("async");
const data = require('./comment-data');
const notifyService = require('../service-proxy/notification');
const reactionService = require('../service-proxy/reaction');
const dbUtils = require('../mongodb/utils');
const logger = require('../logger');


/**
 * Add new comment
 * @param {object} newComment New comment to insert {stream_id, content, reply_to}
 * @param {object} context Request context {space_id, user_id, uname, roles, groups}
 * @param {function} done 
 */
function addComment(newComment, context, done) {
    const comment = {
        _id: dbUtils.newId(),
        content: newComment.content,
        reply_to: newComment.reply_to,
        meta: {
            create_user: {
                user_id: context.user_id,
                uname: context.uname
            },
            create_time: new Date()
        },
        reaction_stream: dbUtils.newId()
    };

    data.findStreamByID(newComment.stream_id, function(err, cs) {
        if (err) return done(err);
        if (!cs) return done("comment_stream_not_exist");

        // update stat
        const stat = {
            comment_count: (cs.comments && (cs.comments.length + 1)) || 1,
            last_comment_time: comment.meta.create_time,
            last_comment_user: comment.meta.create_user
        };

        data.insertComment({
            id: cs._id,
            stat: stat
        }, comment, function(err) {
            if (err) return done(err);
            done(null, comment);
            
            // insert reaction stream
            addReactionStream(cs, comment, context);
            
            // notify to other users
            notifyNewComment(cs, comment, context);
        });
    });
}       

function addReactionStream(stream, comment, context) {
    reactionService.addReactionStream({
        _id: comment.reaction_stream,
        space: stream.space,
        owner: comment.meta.create_user.user_id,
        ref_type: "comment",
        ref_id: comment._id,
    }, context, function (error) {
        if (error) logger.error("insert reaction stream for comment fail", error);
    });
}

function notifyNewComment(stream, comment, context) {
    var no = {
        space: stream.space,
        type: 'comment_new',
        ref_data: {
            comment: {
                comment_id: comment._id,
                stream_id: stream._id,
                ref_type: stream.ref_type,
                ref_id: stream.ref_id,
                ref_obj: stream.ref_obj
            },
            issue_user: comment.meta.create_user,
            issue_time: comment.meta.create_time
        }
    };

    // notify to all user who commented on this stream
    const commentedUsers = stream.comments.map((item) => {
        return item.meta && item.meta.create_user && item.meta.create_user.user_id;
    });

    const target = {
        users: commentedUsers.concat(stream)
    };
    
    // notify owner comment stream
    if(stream.owner) target.users.push(stream.owner);

    // notify to all user who reacted
    reactionService.getUsersToNofity({
        ref_type: stream.ref_type,
        ref_id: stream.ref_id
    }, context, function(_err, r) {
        if (r) target.users = target.users.concat(r);
        notifyService.pushNotification(no, target, context, function(err) {
            if (err) return logger.warn("notification for comment error", err);
        });
    });
}

function removeComment(streamId, commentId, context, done) {
    data.findStreamByID(streamId, function(err, cs) {
        if (err) return done(err);
        if (!cs) return done("comment_stream_not_exist");
        if (!cs.comments) return done();

        const comments = cs.comments.filter((item) => item._id !== commentId);

        // update stat
        let stat = null;
        if (comments.length > 0) {
            var last = comments[comments.length - 1];
            stat = {
                comment_count: comments.length,
                last_comment_time: last.meta.create_time,
                last_comment_user: last.meta.create_user
            };
        }
        else {
            stat = {
                comment_count: 0,
                last_comment_time: null,
                last_comment_user: null
            };
        }
        // remove and update stat
        data.deleteComment({
            id: streamId,
            stat: stat
        }, commentId, function(err) {
            if (err) return done(err);
            return done(null, {
                deleted_comment: commentId
            });
        });
    });
}

function getStream(streamId, context, done) {
    data.findStreamDetail(streamId, done);
}

function addStream(stream, context, done) {
    data.insertStream(stream, done);
}

function removeStream(streamId, context, done) {
    data.deleteStream(streamId, done);
}

function updateStream(stream, context, done){
    data.updateStream(stream, done);
}

module.exports = {
    addComment,
    removeComment,
    addStream,
    removeStream,
    getStream,
    updateStream
};
