'use strict';

const CommentStream = require('../mongodb/models/comment');
const ReactionStream = require('../mongodb/models/reaction-stream');
const cmUtils = require('../utils/common');

function findOne(cond, done) {
    CommentStream.findOne(cond, done);
}

function findStreamDetail(id, done) {
    return findOne({
        _id: id
    }, function(err, cmStream) {
        if (err || !cmStream) return done(err, cmStream);
        cmStream = cmStream.toObject();
        if (!cmStream.comments || cmStream.comments.length === 0) {
            return done(null, cmStream);
        }

        const ids = cmStream.comments.map((item) => item.reaction_stream);
        console.log("comment reaction ids", ids);

        ReactionStream.find({
            _id: {
                $in: ids
            }
        }, function(err, reStreams) {
            console.log("comment reactions", reStreams);
            if (err) return done(err);
            if (!reStreams || reStreams.length === 0) return done(null, cmStream);

            cmStream.comments.map((item) => {
                item.reaction_data = reStreams.find((item1) => item.reaction_stream && item1._id === item.reaction_stream);
            });
            console.log("comment full", cmStream);
            done(null, cmStream);
        });
    });
}

function findStreamByID(id, done) {
    return findOne({
        _id: id
    }, done);
}

function insertStream(stream, done) {
    var newStream = new CommentStream();
    cmUtils.copy(stream, newStream);
    newStream.save(function(err) {
        if (err) return done(err);
        return done(null, newStream);
    });
}

function deleteStream(streamId, done) {
    CommentStream.findOneAndRemove({
        _id: streamId
    }, function(err) {
        if (err) return done(err);
        return done(null);
    });
}

// insert comment to list comments in comment stream, also update 'stat' of comment stream
function insertComment(stream, newComment, done) {
    CommentStream.update({
        "_id": stream.id,
    }, {
        "$push": {
            "comments": newComment
        },
        "$set": {
            "stat.comment_count": stream.stat.comment_count,
            "stat.last_comment_time": stream.stat.last_comment_time,
            "stat.last_comment_user": stream.stat.last_comment_user,
        }
    }, done);
}

function updateComment(streamId, comment, done) {
    CommentStream.update({
        _id: streamId,
        "comments._id": comment.id,
    }, {
        "$set": {
            "comments.$.content": comment.content
        }
    }, done);
}

function deleteComment(stream, commentId, done) {
    CommentStream.update({
        "_id": stream.id,
    }, {
        "$pull": {
            comments: {
                _id: commentId
            }
        },
        "$set": {
            "stat.comment_count": stream.stat.comment_count,
            "stat.last_comment_time": stream.stat.last_comment_time,
            "stat.last_comment_user": stream.stat.last_comment_user,
        }
    }, done);
}

function insertCommentReaction(streamId, commentId, reaction, done) {
    CommentStream.update({
        _id: streamId,
        "comments._id": commentId,
        "comments.reactions.issue_user.user_id": reaction.issue_user.user_id
    }, {
        "$set": {
            "comments.reactions.$$.issue_user.user_id": reaction.issue_user.user_id,
            "comments.reactions.$$.issue_user.uname": reaction.issue_user.uname,
            "comments.reactions.$$.issue_time": reaction.issue_time,
            "comments.reactions.$$.reaction": reaction.reaction,
        }
    }, function(err, r) {
        if (err) return done(err);
        // r: { "nMatched" : 0, "nUpserted" : 0, "nModified" : 0 }
        if (r.nModified > 0) return done();

        CommentStream.update({
            _id: streamId,
            "comments._id": commentId,

        }, {
            "$push": {
                "comments.$.reactions": reaction
            }
        }, function(err, r) {
            if (err) return done(err);
            return done();
        });
    });
}

function deleteCommentReaction(streamId, commentId, userId, done) {
    CommentStream.update({
        _id: streamId,
        "comments._id": commentId,
    }, {
        "$pull": {
            "comments.$.reactions": {
                "issue_user.user_id": userId
            }
        }
    }, done);
}

function updateStream(stream, done) {
    const update = cmUtils.objectToMap(stream);
    delete update.id;
    
    CommentStream.update({
        _id: stream.id
    }, {
        "$set": update
    }, done);
}

module.exports = {
    findOne,
    findStreamByID,
    findStreamDetail,
    insertStream,
    updateStream,
    deleteStream,
    insertComment,
    updateComment,
    deleteComment,
    insertCommentReaction,
    deleteCommentReaction
};
