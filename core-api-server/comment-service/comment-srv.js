const spaceAuth = require('../resource-auth');
const httpUtils = require("../utils/http");
const logger = require("../logger");
const logic = require("./comment-logic");

function getCommentStream(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ["stream_id"]);

    logic.getStream(params.stream_id, auth, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function addComment(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ["stream_id", "content", "reply_to"]);
    logger.debug("add comment, params: " + JSON.stringify(params));

    logic.addComment(params, auth, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function editComment(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ["stream_id", "comment_id", "content"]);
    logger.debug("edit comment, params: " + JSON.stringify(params));

    logic.editComment(params, auth, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function deleteComment(req, res) {
    const auth = httpUtils.extractAuthInfo(req);
    const params = httpUtils.extractObject(req, ["stream_id", "comment_id"]);
    logger.debug("delete comment, params: " + JSON.stringify(params));

    logic.removeComment(params.stream_id, params.comment_id, auth, function(err, r) {
        return httpUtils.response(res, err, r);
    });
}

function registerHandler(app) {
    app.get("/api/space/comment-streams/:stream_id", [spaceAuth.spaceAuthSuccess, getCommentStream]);
    app.post("/api/space/comment-streams/:stream_id/comments", [spaceAuth.spaceAuthSuccess, addComment]);
    app.put("/api/space/comment-streams/:stream_id/comments/:comment_id", [spaceAuth.spaceAuthSuccess, editComment]);
    app.delete("/api/space/comment-streams/:stream_id/comments/:comment_id", [spaceAuth.spaceAuthSuccess, deleteComment]);
}

module.exports = {
    registerHandler: registerHandler
};
