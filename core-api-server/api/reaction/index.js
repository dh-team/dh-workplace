var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function react(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var reference = httpUtils.extractObject(req, ["ref_type", "ref_id"]);
    var react = {
        issue_user: {
            user_id: auth.user_id,
            uname: auth.uname
        },
        issue_time: new Date(),
        reaction: "like"
    };
    console.log("react, reference: " + JSON.stringify(reference) + ", data: " + JSON.stringify(react));

    db.reactions.addReaction(reference, react, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json({
            reference: reference,
            reaction: react
        });
    });
}


function undoReact(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var reference = httpUtils.extractObject(req, ["ref_type", "ref_id"]);
    console.log("undoReact, reference: " + JSON.stringify(reference) + ", user: " + auth.user_id);

    db.reactions.removeReaction(reference, auth.user_id, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json({
            reference: reference,
            undo_react_user: auth.user_id
        });
    });
}

module.exports = {
    react: [auth.spaceAuthSuccess, react],
    undoReact: [auth.spaceAuthSuccess, undoReact]
}
