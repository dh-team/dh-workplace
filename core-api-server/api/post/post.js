var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var stringUtils = require("../../utils/string");
var logger = require("../../logger");

function listPosts(req, res) {
    var type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        listLivePosts(req, res);
    }
    else {
        listArchivePosts(req, res);
    }
}

function listLivePosts(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var param = httpUtils.extractObject(req, ["group_id", "cursor", "limit"]);
    if (!param.group_id) {
        param.group_id = ["all"].concat(auth.groups);
    }
    // console.log("list live post: " + JSON.stringify(param.group_id));

    db.posts.listByGroup(auth.space_id, param.group_id, param.cursor, param.limit, function(err, posts) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(posts);
    });
}

function listArchivePosts(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var catID = httpUtils.extractParam(req, "category_id");

    db.posts.listByCat(auth.space_id, catID, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        var posts = r.map(function(item) {
            item = item.toObject();
            item.content = stringUtils.plainPreviewOfHtml(item.content);
            return item;
        });
        return res.json(posts);
    });
}

function addPost(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var post;
    var type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        post = httpUtils.extractObject(req, ["type", "content", "group_id", "attachments"]);
    }
    else {
        post = httpUtils.extractObject(req, ["type", "title", "content", "tags", "category_id"]);
    }
    post.space = auth.space_id;
    normalizePostParams(post);
    post.meta = {
        create_user: {
            user_id: auth.user_id,
            uname: auth.uname
        }
    }
    logger.debug("add post, params: " + JSON.stringify(post));

    db.posts.insert(post, function(err, inserted) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(inserted);
    });
}

function editPost(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var post;
    var type = httpUtils.extractParam(req, "type");
    if (type === "live") {
        post = httpUtils.extractObject(req, ["id", "content"]);
    }
    else {
        post = httpUtils.extractObject(req, ["id", "title", "content", "tags", "category_id"]);
    }
    normalizePostParams(post);
    post.meta = {
        modify_user: {
            user_id: auth.user_id,
            uname: auth.uname
        },
        modify_time: new Date()
    };
    
    logger.debug("edit post: " + JSON.stringify(post));

    db.posts.update(post, function(err, updated) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(updated);
    });
}

function normalizePostParams(post) {
    if (post.group_id) {
        var scope = ["g:" + post.group_id];
        post.scope = {
            view_scope: scope,
            interact_scope: scope,
            modify_scope: ["u:owner"],
            manage_scope: ["u:owner"],
            notify_scope: scope
        }
    }

    if (post.category_id) {
        post.categories = [post.category_id];
        delete post.category_id;
    }

    if ("string" === typeof post.tags) {
        post.tags = post.tags.split(",");
    }
    if (post.tags && post.tags.length > 0) {
        post.tags = post.tags.map(function(tag) {
            return tag.trim();
        });
    }

    // attachments
    if (post.attachments && typeof(post.attachments) !== "object") {
        try {
            post.attachments = JSON.parse(post.attachments);
        }
        catch (e) {
            delete post.attachments;
        }
    }
}

function deletePost(req, res) {
    // var auth = httpUtils.extractAuthInfo(req);
    var id = httpUtils.extractParam(req, "id");
    logger.debug("delete post, id: " + id);

    db.posts.remove(id, function(err, deleted) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!deleted) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(deleted);
    });
}

function getPost(req, res) {
    // var auth = httpUtils.extractAuthInfo(req);
    var id = httpUtils.extractParam(req, "post_id");

    db.posts.findByID(id, function(err, cat) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!cat) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(cat);
    });
}

module.exports = {
    listPosts: [auth.spaceAuthSuccess, listPosts],
    getPost: [auth.spaceAuthSuccess, getPost],
    addPost: [auth.spaceAuthSuccess, addPost],
    editPost: [auth.spaceAuthSuccess, editPost],
    deletePost: [auth.spaceAuthSuccess, deletePost]
}
