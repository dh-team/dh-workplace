var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function listCategories(req, res) {
    var spaceID = req.authInfo.token.using_space.space_id;
    db.postCats.list(spaceID, function(err, cats) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!cats || cats.length === 0) return res.json(cats);
        // calc statistic
        var stats = db.postCats.calcStat(cats);
        cats = cats.map(function(cat) {
            cat = cat.toObject();
            var stat = stats.find(function(item1) {
                return item1.id === cat._id;
            });
            if (stat) {
                if (!cat.stat) cat.stat = {};
                cat.stat.child_count = stat.child_count;
                cat.stat.post_count = stat.post_count;
                cat.stat.total_child_count = stat.total_child_count;
                cat.stat.total_post_count = stat.total_post_count;
            }
            return cat;
        });
        return res.json(cats);
    });
}

function addCategory(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var cat = httpUtils.extractObject(req, ["name", "description", "parent"]);
    cat.space = auth.space_id;
    cat.meta = {
        create_user: {
            user_id: auth.user_id,
            uname: auth.uname
        }
    }
    logger.debug("add cat, params: " + JSON.stringify(cat));

    db.postCats.insert(cat, function(err, inserted) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(inserted);
    });
}

function editCategory(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var category = httpUtils.extractObject(req, ["id", "name", "description"]);
    category.meta = {
        modify_user: {
            user_id: auth.user_id,
            uname: auth.uname
        },
        modify_time: new Date()
    }
    logger.debug("edit category, params: " + JSON.stringify(category));

    db.postCats.update(category, function(err, updated) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(updated);
    });
}

function updateCategoryParent(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var param = httpUtils.extractObject(req, ["id", "parent_id"]);
    logger.debug("update category parent, params: " + JSON.stringify(param));
    if (!param.id || !param.parent_id) {
        return httpUtils.responseInvalid(res, "param_invalid");
    }

    var updateCategory = {
        id: param.id,
        parent: param.parent_id,
        meta: {
            modify_user: {
                user_id: auth.user_id,
                uname: auth.uname
            },
            modify_time: new Date()
        }
    }

    db.postCats.update(updateCategory, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function deleteCategory(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var categoryID = httpUtils.extractParam(req, "id");
    logger.debug("delete category, id: " + categoryID);

    db.postCats.remove(categoryID, function(err, deleted) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!deleted) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(deleted);
    });
}

function getCategoryInfo(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var catID = httpUtils.extractParam(req, "category_id");

    db.postCats.findByID(catID, function(err, cat) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!cat) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(cat);

        // db.postCats.getStat(cat, function(err, stat) {
        //     if (err) return httpUtils.responseInvalid(res, err);
        //     cat.stat.child_count = stat.child_count;
        //     cat.stat.total_child_count = stat.total_child_count;
        //     cat.stat.total_post_count = stat.total_post_count;
        //     return res.json(cat);
        // });
    });
}

module.exports = {
    listCategories: [auth.spaceAuthSuccess, listCategories],
    getCategoryInfo: [auth.spaceAuthSuccess, getCategoryInfo],
    addCategory: [auth.spaceAuthSuccess, auth.adminAuthorized, addCategory],
    editCategory: [auth.spaceAuthSuccess, auth.adminAuthorized, editCategory],
    updateCategoryParent: [auth.spaceAuthSuccess, auth.adminAuthorized, updateCategoryParent],
    deleteCategory: [auth.spaceAuthSuccess, auth.adminAuthorized, deleteCategory]
}
