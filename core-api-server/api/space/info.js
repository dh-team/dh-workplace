var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function getSpaceInfo(req, res) {
    var spaceID = req.authInfo.token.using_space.space_id;
    db.spaces.findByID(spaceID, function(err, space) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!space) return httpUtils.responseInvalid(res, "space_not_exist");
        return res.json(space);
    });
}

function getUserProfile(req, res) {
    //TODO: get user profile in database
    var userProfile = req.user;
    userProfile.uname = req.authInfo.token.using_space.uname;
    userProfile.roles = req.authInfo.token.using_space.roles;
    userProfile.positions = req.authInfo.token.using_space.positions;
    userProfile.groups = req.authInfo.token.using_space.groups;

    return res.json(userProfile);
}



module.exports = {
    getSpaceInfo: [auth.spaceAuthSuccess, getSpaceInfo],
    getUserProfile: [auth.spaceAuthSuccess, getUserProfile]
}
