module.exports = Object.assign({},
    require("./info"),
    require("./manage"),
    require("./space-user")
);
