var config = require('./../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function listSpaceUsers(req, res) {
    // var user = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;

    db.spaceMbs.getUsersInSpace(spaceID, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        res.json(r);
    });
}

function addSpaceUser(req, res) {
    // var curUser = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;
    var data = httpUtils.extractObject(req, ["uname", "user"]);
    logger.debug("add user to space, params: " + JSON.stringify(data));

    // check user exist
    db.users.findByUsername(data.user, function(err, user) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!user) return httpUtils.responseInvalid(res, "user_not_exist");

        // check membership not exist
        db.spaceMbs.findByUserID(spaceID, user.id, function(err, mbs) {
            if (err) return httpUtils.responseInvalid(res, err);
            if (mbs) return httpUtils.responseInvalid(res, "user_already_added");

            // check uname not exist in space
            db.spaces.findUname(spaceID, data.uname, function(err, r) {
                if (err) return httpUtils.responseInvalid(res, err);
                if (r) return httpUtils.responseInvalid(res, "uname_exist");

                // add membership
                var newMbs = {
                    user_id: user.id,
                    space_id: spaceID,
                    uname: data.uname,
                    groups: [],
                    roles: [],
                    // is_default: false,
                    is_active: false
                };

                db.spaceMbs.insert(newMbs, function(err, insertedMbs) {
                    if (err) return httpUtils.responseInvalid(res, err);
                    var result = Object.assign({}, newMbs, {
                        user: user
                    });
                    return res.json(result);
                });
            });
        });
    });
}

function deleteSpaceUser(req, res) {
    // var user = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;
    var userID = httpUtils.extractParam(req, "user_id");
    logger.debug("delete user from space, params: " + userID);

    db.spaceMbs.findByUserID(spaceID, userID, function(err, mbs) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!mbs) return httpUtils.responseInvalid(res, "user_not_exist");
        if (mbs.roles && mbs.roles.indexOf("administrator") > -1) {
            return httpUtils.responseInvalid(res, "denied_delete_admin");
        }
        db.spaceMbs.delete(spaceID, userID, function(err, r) {
            if (err) return httpUtils.responseInvalid(res, err);
            return res.json(mbs);
        });
    });
}


function joinSpace(req, res) {
    var user = req.user;
    var spaceID = httpUtils.extractParam(req, "space_id");
    // validate
    db.spaceMbs.activeMembership(spaceID, user.user_id, function(err, mbs) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!mbs) return httpUtils.responseInvalid(res, "space_not_exist");
        res.json(mbs);
    });
}

function unjoinSpace(req, res) {
    var user = req.user;
    var spaceID = httpUtils.extractParam(req, "space_id");
    // validate
    db.spaceMbs.delete(spaceID, user.user_id, function(err, mbs) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!mbs) return httpUtils.responseInvalid(res, "space_not_exist");
        res.json(mbs);
    });
}

module.exports = {
    listSpaceUsers: [auth.spaceAuthSuccess, listSpaceUsers],
    addSpaceUser: [auth.spaceAuthSuccess, auth.adminAuthorized, addSpaceUser],
    deleteSpaceUser: [auth.spaceAuthSuccess, auth.adminAuthorized, deleteSpaceUser],
    joinSpace: [auth.authSuccess, joinSpace],
    unjoinSpace: [auth.authSuccess, unjoinSpace]
}
