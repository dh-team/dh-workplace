'use strict';

var config = require('../../config');
var db = require('../../' + config.db.type);
var auth = require('../../resource-auth');
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function createSpace(req, res) {
    var user = req.user;
    // var token = req.authInfo.token;
    var space = httpUtils.extractObject(req, ["name", "short_name", "uname", "address", "phone", "size", "type"]);
    var username = httpUtils.extractParam(req, "username");
    logger.debug("create space, params: " + JSON.stringify(space) + ", username: " + username);

    // validate
    db.spaces.findByUname(space.uname, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (r) return httpUtils.responseInvalid(res, "space_url_exist");

        // insert new space
        space.is_verified = true;
        space.is_active = true;
        space.meta = {
            create_user: user.user_id,
            create_time: new Date()
        }
        db.spaces.insert(space, function(err, newSpace) {
            if (err) return httpUtils.responseInvalid(res, err);
            // insert space membership, creator is administrator
            db.spaceMbs.insert({
                uname: username,
                user_id: user.user_id,
                space_id: newSpace.id,
                // username: null,
                groups: [],
                positions: [],
                roles: ["administrator"],
                // is_default: false,
                // is_active: true
            }, function(err) {
                if (err) return httpUtils.responseInvalid(res, err);
                return res.json(newSpace);
            });
        });
    });
}

function listSpaces(req, res) {
    var user = req.user;
    // validate
    db.spaceMbs.getSpacesOfUser(user.user_id, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        res.json(r);
    });
}

function updateSpaceInfo(req, res) {
    var user = req.user;
    // var token = req.authInfo.token;
    var space = httpUtils.extractObject(req, ["name", "short_name", "slogan", "address", "phone"]);
    space.id = req.authInfo.token.using_space.space_id;
    logger.debug("update space, params: " + JSON.stringify(space));

    db.spaces.update(space, function(err, updated) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!updated) return httpUtils.responseInvalid(res, "space_not_exist");
        return res.json(updated);
    });
}

module.exports = {
    createSpace: [auth.authSuccess, createSpace],
    listSpaces: [auth.authSuccess, listSpaces],
    updateSpaceInfo: [auth.spaceAuthSuccess, updateSpaceInfo]
}
