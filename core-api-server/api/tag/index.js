var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");

function listTags(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var subject = httpUtils.extractParam(req, "subject");
    db.tags.listBySubject(auth.space_id, subject, function (err, r) {
        return httpUtils.response(res, err, r);
    });
}

module.exports = {
    listTags: [auth.spaceAuthSuccess, listTags]
}
