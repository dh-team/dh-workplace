var htmlToText = require('html-to-text');
var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function getCommentStream(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var ref = httpUtils.extractObject(req, ["stream_id", "ref_type", "ref_id"]);

    db.comments.findStreamByID(ref.stream_id, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function addComment(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var param = httpUtils.extractObject(req, ["stream_id", "ref_type", "ref_id", "content", "reply_to"]);
    logger.debug("add comment, params: " + JSON.stringify(param));

    var cmt = {
        content: param.content,
        reply_to: param.reply_to,
        meta: {
            create_user: {
                user_id: auth.user_id,
                uname: auth.uname
            },
            create_time: new Date()
        }
    };

    db.comments.insertComment(param.stream_id, cmt, function(err, inserted) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(inserted);
    });
}

// function editComment(req, res) {
//     var auth = httpUtils.extractAuthInfo(req);
//     var cmt = httpUtils.extractObject(req, ["id", "content"]);
//     cmt.meta = {
//         modify_time: new Date()
//     }
//     logger.debug("edit comment: " + JSON.stringify(cmt));

//     db.comments.update(cmt, function(err, updated) {
//         if (err) return httpUtils.responseInvalid(res, err);
//         return res.json(updated);
//     });
// }

function deleteComment(req, res) {
    // var auth = httpUtils.extractAuthInfo(req);
    var param = httpUtils.extractObject(req, ["stream_id", "comment_id"]);
    logger.debug("delete comment, param: " + JSON.stringify(param));

    db.comments.removeComment(param.stream_id, param.comment_id, function(err, deleted) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!deleted) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(deleted);
    });
}

module.exports = {
    getCommentStream: [auth.spaceAuthSuccess, getCommentStream],
    addComment: [auth.spaceAuthSuccess, addComment],
    // editComment: [auth.spaceAuthSuccess, editComment],
    deleteComment: [auth.spaceAuthSuccess, deleteComment]
}
