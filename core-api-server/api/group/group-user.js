var config = require('./../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function listGroupUsers(req, res) {
    // var user = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;
    var groupID = httpUtils.extractParam(req, "group_id");
    db.spaceMbs.getUsersInGroup(spaceID, groupID, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        res.json(r);
    });
}

function addGroupUser(req, res) {
    // var curUser = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;
    var data = httpUtils.extractObject(req, ["uname", "group_id"]);
    logger.debug("add user to group, params: " + JSON.stringify(data));

    // check membership not exist
    db.spaceMbs.findByUname(spaceID, data.uname, function(err, mbs) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!mbs) return httpUtils.responseInvalid(res, "user_not_exist");

        // check uname not exist in space
        db.spaceMbs.addUserToGroup(spaceID, mbs.user_id, data.group_id, function(err, r) {
            if (err) return httpUtils.responseInvalid(res, err);
            return res.json(r);
        });
    });
}

function deleteGroupUser(req, res) {
    // var user = req.user;
    var spaceID = req.authInfo.token.using_space.space_id;
    var data = httpUtils.extractObject(req, ["user_id", "group_id"]);
    logger.debug("delete user in group, params: " + JSON.stringify(data));

    db.spaceMbs.findByUserID(spaceID, data.user_id, function(err, mbs) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!mbs) return httpUtils.responseInvalid(res, "user_not_exist");

        db.spaceMbs.deleteUserInGroup(spaceID, data.user_id, data.group_id, function(err, r) {
            if (err) return httpUtils.responseInvalid(res, err);
            return res.json(r);
        });
    });
}

module.exports = {
    listGroupUsers: [auth.spaceAuthSuccess, listGroupUsers],
    addGroupUser: [auth.spaceAuthSuccess, auth.adminAuthorized, addGroupUser],
    deleteGroupUser: [auth.spaceAuthSuccess, auth.adminAuthorized, deleteGroupUser]
}
