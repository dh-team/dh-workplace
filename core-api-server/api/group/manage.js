var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function listGroups(req, res) {
    var spaceID = req.authInfo.token.using_space.space_id;
    db.groups.list(spaceID, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function addGroup(req, res) {
    var user = req.user;
    var token = req.authInfo.token;
    var group = httpUtils.extractObject(req, ["uname", "name", "description", "org_info"]);
    group.space_id = token.using_space.space_id;
    group.type = "org";
    group.meta = {
        create_user: user.user_id
    }
    logger.debug("add group, params: " + JSON.stringify(group));

    // check group uname unique
    db.spaces.findUname(group.space_id, group.uname, function(err, r, type) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (r) return httpUtils.responseInvalid(res, "uname_exist");

        db.groups.insert(group, function(err, newGroup) {
            if (err) return httpUtils.responseInvalid(res, err);
            return res.json(newGroup);
        });
    });
}

function editGroup(req, res) {
    var user = req.user;
    var token = req.authInfo.token;
    var group = httpUtils.extractObject(req, ["id", "name", "description", "org_info"]);
    group.meta = {
        modify_user: user.user_id,
        modify_time: new Date()
    }
    logger.debug("edit group, params: " + JSON.stringify(group));

    db.groups.update(group, function(err, updated) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(updated);
    });
}

function updateGroupParent(req, res) {
    var user = req.user;
    var token = req.authInfo.token;
    var param = httpUtils.extractObject(req, ["id", "parent_id"]);
    logger.debug("update group parent, params: " + JSON.stringify(param) + ", user: " + JSON.stringify(user));

    var updateGroup = {
        id: param.id,
        org_info: {
            parent_id: param.parent_id
        },
        meta: {
            modify_user: user.user_id,
            modify_time: new Date()
        }
    }

    db.groups.update(updateGroup, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function deleteGroup(req, res) {
    var user = req.user;
    var token = req.authInfo.token;
    var groupID = httpUtils.extractParam(req, "id");
    logger.debug("delete group, id: " + groupID);

    db.groups.delete(groupID, function(err, deleted) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!deleted) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(deleted);
    });
}

function listGroupsOfUser(req, res) {
    var userID = req.user.user_id;
    var spaceID = req.authInfo.token.using_space.space_id;

    db.groups.listGroupsOfUser(spaceID, userID, function(err, groups) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!groups) return res.json([]);
        groups = groups.map(function(item) {
            if (item.type === "org") {
                item.sort_key = "org";
                if (!item.org_info || !item.org_info.all_parents || item.org_info.all_parents.length === 0) {
                    item.sort_key += "0";
                }
                else {
                    item.sort_key += item.org_info.all_parents.join("");
                }
            }
            else {
                item.sort_key = "user";
            }
            item.sort_key += item.name;
            return item;
        }).sort(function(item1, item2) {
            return item1.sort_key.localeCompare(item2.sort_key);
        })
        return res.json(groups);
    });
}

function getGroup(req, res) {
    var user = req.user;
    var token = req.authInfo.token;
    var groupID = httpUtils.extractParam(req, "group_id");

    db.groups.findByID(groupID, function(err, group) {
        if (err) return httpUtils.responseInvalid(res, err);
        if (!group) return httpUtils.responseInvalid(res, "not_exist");
        return res.json(group);
    });
}

module.exports = {
    listGroups: [auth.spaceAuthSuccess, listGroups],
    getGroup: [auth.spaceAuthSuccess, getGroup],
    addGroup: [auth.spaceAuthSuccess, auth.adminAuthorized, addGroup],
    editGroup: [auth.spaceAuthSuccess, auth.adminAuthorized, editGroup],
    deleteGroup: [auth.spaceAuthSuccess, auth.adminAuthorized, deleteGroup],
    updateGroupParent: [auth.spaceAuthSuccess, auth.adminAuthorized, updateGroupParent],
    listGroupsOfUser: [auth.spaceAuthSuccess, listGroupsOfUser]
}
