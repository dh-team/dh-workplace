var config = require('../../config');
var auth = require('../../resource-auth');
var db = require('../../' + config.db.type);
var httpUtils = require("../../utils/http");
var logger = require("../../logger");

function listNotifications(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var param = httpUtils.extractObject(req, ["cursor", "limit"]);
    // console.log("query notification: " + JSON.stringify(param));
    db.notifications.listByUser(auth.space_id, auth.user_id, param.cursor, param.limit, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}


function countUnread(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var cursor = httpUtils.extractParam(req, "cursor");
    // console.log("count notification: " + JSON.stringify(cursor));
    db.notifications.countUnread(auth.space_id, auth.user_id, cursor, function(err, r) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(r);
    });
}

function markIsRead(req, res) {
    var auth = httpUtils.extractAuthInfo(req);
    var id = httpUtils.extractParam(req, "id");
    logger.debug("mark notification read: " + JSON.stringify({
        user_id: auth.user_id,
        notification_id: id
    }));

    db.notifications.markIsRead(id, auth.user_id, function(err, updated) {
        if (err) return httpUtils.responseInvalid(res, err);
        return res.json(updated);
    });
}

module.exports = {
    listNotifications: [auth.spaceAuthSuccess, listNotifications],
    countUnread: [auth.spaceAuthSuccess, countUnread],
    markIsRead: [auth.spaceAuthSuccess, markIsRead]
}
