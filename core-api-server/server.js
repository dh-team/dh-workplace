'use strict';

const https = require('https');
const http = require('http');
const path = require('path');
const fs = require('fs');
const cors = require('cors')

const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const passport = require('passport');

const config = require('./config');
const auth = require('./resource-auth');
auth.configure({
    authServerUrl: config.authServer.url
});

const api = require('./api');

const postService = require('./post-service/service');
const workService = require('./work-service/service');
const commentService = require('./comment-service/comment-srv');
const reactionService = require('./reaction-service/reaction-srv');
const tagService = require('./tag-service/tag-srv');
const notificationService = require('./notification-service/notification-srv');

// const spaceApi = require('./api/space/space-info');
// const errorHandler = require("./error");

// init database
const db = require('./' + config.db.type);
db.init(config.db.dbName);
// Express configuration
const app = express();
app.use(cors());
app.use(cookieParser());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

// Routing
app.get('/api', passport.authenticate('bearer', {
        session: false
    }),
    function(req, res) {
        res.json({
            text: 'Welcome to Upspace Core API Server',
            user: req.user || null,
            authInfo: req.authInfo || null
        });
    });

app.post("/api/space/create-space", api.space.createSpace);
app.get("/api/space/list-spaces", api.space.listSpaces);
app.get("/api/space/list-users", api.space.listSpaceUsers);
app.post("/api/space/add-user", api.space.addSpaceUser);
app.post("/api/space/join", api.space.joinSpace);
app.post("/api/space/unjoin", api.space.unjoinSpace);

app.get("/api/space/space-info", api.space.getSpaceInfo);
app.get("/api/space/user-profile", api.space.getUserProfile);
app.post("/api/space/update-space-info", api.space.updateSpaceInfo);

app.get("/api/space/group/info", api.group.getGroup);
app.post("/api/space/group/info", api.group.getGroup);
app.get("/api/space/group/list", api.group.listGroups);
app.post("/api/space/group/add", api.group.addGroup);
app.post("/api/space/group/edit", api.group.editGroup);
app.post("/api/space/group/update-parent", api.group.updateGroupParent);
app.post("/api/space/group/delete", api.group.deleteGroup);
app.get("/api/space/group/delete", api.group.deleteGroup);
app.post("/api/space/group/user-groups", api.group.listGroupsOfUser);
app.get("/api/space/group/user-groups", api.group.listGroupsOfUser);

app.get("/api/space/user/list", api.space.listSpaceUsers);
app.post("/api/space/user/add", api.space.addSpaceUser);
app.post("/api/space/user/delete", api.space.deleteSpaceUser);
app.get("/api/space/user/delete", api.space.deleteSpaceUser);

app.post("/api/space/group/user/list", api.group.listGroupUsers);
app.get("/api/space/group/user/list", api.group.listGroupUsers);

app.post("/api/space/group/user/add", api.group.addGroupUser);
app.post("/api/space/group/user/delete", api.group.deleteGroupUser);

// notification
notificationService.registerHandler(app);

// post
postService.registerHandler(app);

// tag
tagService.registerHandler(app);

// objectives && work
workService.registerHandler(app);

// reaction
reactionService.registerHandler(app);

// comment
commentService.registerHandler(app);

// Create HTTP | HTTPS server
if (config.coreApiServer.type === 'http') {
    http.createServer(app).listen(config.coreApiServer.port, "0.0.0.0");
}
else {
    const options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.coreApiServer.port, "0.0.0.0");
}
console.log("Api Server started on port " + config.coreApiServer.port);
