cd ..
dir=$(pwd)

cd $dir/auth-server
npm install

cd $dir/core-api-server
npm install

cd $dir/file-server
npm install

cd $dir/web-app
npm install

cd $dir/space-app
npm install

cd $dir/proxy-server
npm install

cd $dir/common
npm install

sudo npm install -g bower
cd $dir/proxy-server/static
bower install

