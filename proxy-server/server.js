'use strict';

var path = require("path");
var https = require('https');
var http = require('http');
var fs = require('fs');
var Express = require("express");
var cookieParser = require('cookie-parser');
var httpProxy = require("http-proxy");
var config = require("./config");

const app = new Express();
app.use(cookieParser());

// config proxy routing
const proxy = httpProxy.createProxyServer();
app.use('/as', (req, res) => {
    proxy.web(req, res, {
        target: config.authServer.url + "/as",
    });
});
app.use('/fs', (req, res) => {
    if(!req.headers["Authorization"] && req.cookies.space_token){
        req.headers["Authorization"] = "Bearer " + req.cookies.space_token;
    }
    proxy.web(req, res, {
        target: config.fileServer.url + "/fs",
    });
});
app.use('/api', (req, res) => {
    proxy.web(req, res, {
        target: config.coreApiServer.url + "/api",
    });
});
app.use('/ws', (req, res) => {
    
    proxy.web(req, res, {
        target: config.webServer.url + "/ws",
    });
});

app.use('/space', (req, res) => {
    proxy.web(req, res, {
        target: config.spaceWebServer.url + "/space",
    });
});

app.use('/static/ws/built', (req, res) => {
    proxy.web(req, res, {
        target: config.webServer.url + "/static/ws/built",
    });
});

app.use('/static/space/built', (req, res) => {
    proxy.web(req, res, {
        target: config.spaceWebServer.url + "/static/space/built",
    });
});

// added the error handling to avoid https://github.com/nodejitsu/node-http-proxy/issues/527
proxy.on('error', (error, req, res) => {
    let json;
    if (error.code !== 'ECONNRESET') {
        console.error('proxy error', error);
    }
    if (!res.headersSent) {
        res.writeHead(500, {
            'content-type': 'application/json'
        });
    }

    json = {
        error: 'proxy_error',
        reason: error.message
    };
    res.end(JSON.stringify(json));
});

// define the folder that will be used for static assets
app.use("/static", Express.static(path.join(__dirname, 'static')));

app.get('/', function(req, res) {
    res.redirect('/space/');
});

// Create HTTP | HTTPS server
if (config.proxyServer.type === 'http') {
    http.createServer(app).listen(config.proxyServer.port, "0.0.0.0");
}
else {
    var options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.proxyServer.port, "0.0.0.0");
}
console.log("Proxy server started on port " + config.proxyServer.port);
