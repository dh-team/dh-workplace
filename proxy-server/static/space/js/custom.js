(function($) {

    $(function() {
        // toastr
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "50",
            "hideDuration": "500",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
        // $('.checkbox').iCheck({
        //   checkboxClass: 'icheckbox_flat-blue',
        //   radioClass: 'iradio_flat-blue'
        // });

        $('li.dropdown-mega a').on('click', function(event) {
            $(this).parent().toggleClass("open");
        });

        $('body').on('click', function(e) {
            if (!$('li.dropdown-mega').is(e.target) && $('li.dropdown-mega').has(e.target).length === 0 && $('.open').has(e.target).length === 0) {
                $('li.dropdown-mega').removeClass('open');
            }
        });
    });




})(jQuery);
