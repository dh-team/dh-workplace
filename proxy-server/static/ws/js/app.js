// Some general UI pack related JS
// Extend JS String with repeat method
String.prototype.repeat = function(num) {
    return new Array(Math.round(num) + 1).join(this);
};

(function($) {

    $(function() {
        // Checkboxes and Radio buttons
        $('[data-toggle="checkbox"]').radiocheck();
        $('[data-toggle="radio"]').radiocheck();

        // Tooltips
        $('[data-toggle=tooltip]').tooltip('show');

        // Dropdown
        // $('[data-toggle=dropdown]').dropdown();

        // Focus state for append/prepend inputs
        $('.input-group').on('focus', '.form-control', function() {
            $(this).closest('.input-group, .form-group').addClass('focus');
        }).on('blur', '.form-control', function() {
            $(this).closest('.input-group, .form-group').removeClass('focus');
        });

        // Switches
        if ($('[data-toggle="switch"]').length) {
            $('[data-toggle="switch"]').bootstrapSwitch();
        }

        // toastr
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "50",
            "hideDuration": "500",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    });

})(jQuery);
