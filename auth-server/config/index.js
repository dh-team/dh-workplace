/*jslint node: true */
'use strict';

module.exports = require("../conf.json");

//
// The configuration options of the server
//

/**
 * Configuration of access tokens.
 *
 * expiresIn - The time in seconds before the access token expires
 * calculateExpirationDate - A simple function to calculate the absolute
 * time that th token is going to expire in.
 * authorizationCodeLength - The length of the authorization code
 * accessTokenLength - The length of the access token
 * refreshTokenLength - The length of the refresh token
 */
module.exports.token = {
    expiresIn: 3600 * 24 * 7, // 1 week
    calculateExpirationDate: function() {
        return new Date(new Date().getTime() + (this.expiresIn * 1000));
    },
    authorizationCodeLength: 16,
    accessTokenLength: 64,
    refreshTokenLength: 64
};

/**
 * Database configuration for access and refresh tokens.
 *
 * timeToCheckExpiredTokens - The time in seconds to check the database
 * for expired access tokens.  For example, if it's set to 3600, then that's
 * one hour to check for expired access tokens.
 * type - The type of database to use.  "db" for "in-memory", or
 * "mongodb" for the mongo database store.
 * dbName - The database name to use.
 */
module.exports.db = {
    timeToCheckExpiredTokens: 3600,
    type: "mongodb",
    dbName: "main-db"
};

/**
 * Session configuration
 *
 * type - The type of session to use.  MemoryStore for "in-memory",
 * or MongoStore for the mongo database store
 * maxAge - The maximum age in milliseconds of the session.  Use null for
 * web browser session only.  Use something else large like 3600000 * 24 * 7 * 52
 * for a year.
 * secret - The session secret that you should change to what you want
 * dbName - The database name if you're using Mongo
 */
module.exports.session = {
    type: "MemoryStore",
    maxAge: 3600000 * 24 * 7 * 52,
    //TODO You need to change this secret to something that you choose for your secret
    secret: "PxGZT2bJpGyrFddDphNW",
    dbName: "auth-session-db"
};

/**
 * type - Webserver type, "http" or "https"
 * port - Listening port
 */
// module.exports.server = {
//     type: 'http',
//     port: 8081
// }


module.exports.facebookAuth = {
    'clientID': '1793734537547625', // your App ID
    'clientSecret': '28409eececed0bdec87526414d66d392', // your App Secret
    'callbackURL': module.exports.publishUrl + '/as/auth/facebook/callback',
    'profileURL': 'https://graph.facebook.com/v2.5/me?fields=first_name,last_name,email'
};

module.exports.googleAuth = {
    'clientID': '406859144316-reclc7iv0quh2gj57i5nin0mloh0gq2d.apps.googleusercontent.com',
    'clientSecret': '8znjY7YO857Dr9BOMqu-y2c9',
    'callbackURL': module.exports.publishUrl + '/as/auth/google/callback'
}

module.exports.urlPrefix = "/as";
