"use strict";

var passport = require("passport");
var login = require("connect-ensure-login");
var reg = require("./membership/register.js");
var logger = require('./logger');

function renderWithLayout(req, res, view, data) {
    if (!data.user) {
        data.user = req.user;
    }
    res.render(view, data);
}

exports.index = function(req, res) {
    renderWithLayout(req, res, "index", {
        user: req.user
    });
};

exports.loginForm = function(req, res) {
    var errs = req.flash("login_error");

    renderWithLayout(req, res, "login", {
        error: errs && errs.length > 0 ? errs : null
    });
};

exports.login = function(req, res, next) {
    req.checkBody({
        "username": {
            notEmpty: true,
            isLength: {
                options: [{
                    max: 250
                }]
            },
            errorMessage: "Email/Số điện thoại đăng nhập không hợp lệ"
        },
        "password": {
            notEmpty: true,
            isLength: {
                options: [{
                    max: 250
                }]
            },
            errorMessage: "Mật khẩu không hợp lệ" // Error message for the parameter
        },
    });

    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            req.flash("login_error", result.array());
            return res.redirect("/as/login");
        }
        logger.debug("request login, returnTo: " + req.session.returnTo);
        passport.authenticate("local", {
            successRedirect: req.query.redirect || req.session.returnTo || "/as/",
            failureRedirect: "/as/login",
            failureFlash: true // allow flash messages
        })(req, res, next);
    });
};

exports.logout = function(req, res) {
    req.logout();
    res.redirect(req.query.redirect || "/as/");
};

exports.signup = function(req, res) {
    req.checkBody({
        "display_name": { //
            notEmpty: true,
            isLength: {
                options: [{
                    min: 5,
                    max: 100
                }],
                errorMessage: "Họ tên phải từ 5 đến 100 ký tự" // Error message for the validator, takes precedent over parameter message
            },
            errorMessage: "Họ tên không hợp lệ"
        },
        "email": {
            isEmail: {
                errorMessage: "Email không hợp lệ"
            }
        },
        "password": {
            notEmpty: true,
            isLength: {
                options: [{
                    min: 5,
                    max: 20
                }],
                errorMessage: "Mật khẩu phải từ 5 đến 20 ký tự" // Error message for the validator, takes precedent over parameter message
            },
            errorMessage: "Mật khẩu không hợp lệ" // Error message for the parameter
        },
    });
    req.checkBody("re_password", "Mật khẩu nhập lại không đúng").equals(req.body.password);

    var signupUrl = "/as/signup?email=" + req.body.email + "&code=" + req.body.code;
    req.getValidationResult().then(function(result) {
        if (!result.isEmpty()) {
            req.flash("error", result.array());
            return res.redirect(signupUrl);
        }

        reg.registerUser({
            display_name: req.body.display_name,
            email: req.body.email,
            password: req.body.password
        }, req.body.code, function(err) {
            if (err) {
                req.flash("error", [err]);
                return res.redirect(signupUrl);
            }
            return renderWithLayout(req, res, "signup-ok", {
                display_name: req.body.display_name,
                email: req.body.email,
            });
        });
    });
};

exports.signupForm = function(req, res) {
    var email = req.query.email;
    var code = req.query.code;

    if (!email || !code) {
        return renderWithLayout(req, res, "error", {
            error: {
                content: "Thông tin xác thực không hợp lệ."
            }
        });
    }

    var errs = req.flash("error");
    return renderWithLayout(req, res, "signup", {
        error: errs && errs.length > 0 ? errs : null,
        email: email,
        code: code
    });
}

exports.registerForm = function(req, res) {
    var errs = req.flash("error");
    
    renderWithLayout(req, res, "register-email", {
        error: errs && errs.length > 0 ? errs : null
    });
}

exports.register = function(req, res) {
    req.checkBody({
        "email": {
            isEmail: {
                errorMessage: "Email không hợp lệ"
            }
        },
    });

    req.getValidationResult().then(function(result) {

        if (!result.isEmpty()) {
            req.flash("error", result.array());
            return res.redirect("/as/register");
        }

        reg.registerEmail(req.body.email, function(err) {
            if (err) {
                req.flash("error", err);
                return res.redirect("/as/register");
            }
            return renderWithLayout(req, res, "register-email-ok", {
                email: req.body.email
            });
        });
    });
};
