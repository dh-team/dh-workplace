'use strict';

var passport = require('passport');

var config = require('../config');
var db = require('../' + config.db.type);
var utils = require("../utils/common");
var logger = require('../logger');

/**
 * This endpoint is for verifying a token.  This has the same signature to
 * Google's token verification system from:
 * https://developers.google.com/accounts/docs/OAuth2UserAgent#validatetoken
 *
 * You call it like so
 * https://localhost:3000/oauth/tokeninfo?token=someToken
 *
 * If the token is valid you get returned
 * {
 *   "audience":"someClientId",
 *   "user_id":"someUserId",
 *   "scope":"profile email",
 *   "expires_in":436 // in seconds
 *   "org":{
 *          org_id,
 *          username,
 *          positions,
 *          groups,
 *          roles
 *      }
 * }
 *
 * If the token is not valid you get a 400 Status and this returned
 * {
 *   "error": "invalid_token"
 * }
 */
exports.info = [
    function(req, res) {
        if (!req.query.token) {
            return responseInvalid(res);
        }

        db.accessTokens.find(req.query.token, function(err, token) {
            if (err || !token) {
                logger.debug("verify token: token " + token + " not found");
                return responseInvalid(res);
            }

            var expirationLeft = Math.floor((token.expiration_date.getTime() - new Date().getTime()) / 1000);
            if (expirationLeft <= 0) {
                logger.debug("verify token: token " + token + " expired");
                return responseInvalid(res);
            }

            // get client info
            db.clients.find(token.client_id, function(err, client) {
                if (err) return responseInvalid(res);
                // get user info
                db.users.findByID(token.user_id, function(err, user) {
                    if (err) return responseInvalid(res);

                    return res.json({
                        client: {
                            client_id: client.id,
                            name: client.name
                        },
                        user: {
                            user_id: user.id,
                            name: user.display_name
                        },
                        using_space: token.using_space,
                        scope: token.scope ? token.scope : "all",
                        expires_in: expirationLeft
                    });
                });
            });

        });
    }
];

exports.getSpaceToken = [
    passport.authenticate('bearer', {
        session: false
    }),
    function(req, res) {
        var user = req.user;
        var token = req.authInfo.token;
        var spaceId = req.query.space_id;

        db.users.findSpaceMembership(user.id, spaceId, function(err, mbs) {
            if (err) return responseInvalid(res, err);
            if (!mbs) return responseInvalid(res, "permission_denied");
            db.groups.listParentGroups(mbs.groups, function(err, parentGroups) {
                if (err) return responseInvalid(res, err);
                // insert access token for space
                db.accessTokens.save({
                    value: utils.uid(config.token.accessTokenLength),
                    user_id: user.id,
                    client_id: token.client_id,
                    scope: token.scope,
                    using_space: {
                        space_id: spaceId,
                        uname: mbs.uname,
                        groups: mbs.groups.concat(parentGroups), // ['group_id', ...]
                        // positions: mbs.positions, // ['position_id', ...]
                        roles: mbs.roles, // ['role1', ...]
                    },
                    expiration_date: config.token.calculateExpirationDate()
                }, function(err, newToken) {
                    if (err) return responseInvalid(res, err);
                    return res.json(newToken);
                });
            });
        });
    }
];

function responseInvalid(res, err) {
    res.status(400);
    return res.json({
        error: err || "invalid_token"
    });
}
