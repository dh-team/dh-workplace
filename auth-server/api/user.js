'use strict';

var passport = require('passport');

exports.profile = [
    passport.authenticate('bearer', {
        session: false
    }),
    function(req, res) {
        res.json({
            user: {
                user_id: req.user.id,
                display_name: req.user.display_name,
                emails: req.user.emails.map(function(item) {
                    return item.email
                }),
                phones: req.user.phones.map(function(item) {
                    return item.phone
                })
            },
            // authInfo: req.authInfo
        });
    }
];
