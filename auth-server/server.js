'use strict';

var https = require('https');
var http = require('http');
var path = require('path');
var fs = require('fs');

var express = require('express');
var expressSession = require('express-session');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var flash = require('connect-flash');
var expressValidator = require('express-validator');
var expressLayouts = require('express-ejs-layouts');

var config = require('./config');
var site = require('./site');
var auth = require('./auth/auth.js');
var oauth2 = require('./auth/oauth2.js');
var tokenApi = require('./api/token.js');
var userApi = require('./api/user.js');
var logger = require('./logger');

//Pull in the mongo store if we're configured to use it
//else pull in MemoryStore for the session configuration
var sessionStorage;
if (config.session.type === 'MongoStore') {
    var MongoStore = require('connect-mongo')({
        session: expressSession
    });
    logger.info('Using MongoDB for the Session');
    sessionStorage = new MongoStore({
        db: config.session.dbName
    });
}
else if (config.session.type === 'MemoryStore') {
    var MemoryStore = expressSession.MemoryStore;
    logger.info('Using MemoryStore for the Session');
    sessionStorage = new MemoryStore();
}
else {
    //We have no idea here
    throw new Error("Within config/index.js the session.type is unknown: " + config.session.type);
}

//Pull in the mongo store if we're configured to use it
//else pull in MemoryStore for the database configuration
var db = require('./' + config.db.type);
db.init(config.db.dbName);
if (config.db.type === 'mongodb') {
    logger.info('Using MongoDB for the data store');
}
else if (config.db.type === 'db') {
    logger.info('Using MemoryStore for the data store');
}
else {
    //We have no idea here
    throw new Error("Within config/index.js the db.type is unknown: " + config.db.type);
}


// Express configuration
var app = express();
app.set('view engine', 'ejs');
app.use(expressLayouts);
app.set('layout', 'layout.ejs');

app.use(cookieParser());
//Session Configuration
app.use(expressSession({
    saveUninitialized: true,
    resave: true,
    secret: config.session.secret,
    store: sessionStorage,
    key: "authorization.sid",
    cookie: {
        maxAge: config.session.maxAge
    }
}));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(expressValidator());
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

app.get(config.urlPrefix + '/', site.index);
app.get(config.urlPrefix + '/login', site.loginForm);
app.post(config.urlPrefix + '/login', site.login);
app.get(config.urlPrefix + '/logout', site.logout);

app.get(config.urlPrefix + '/register', site.registerForm);
app.post(config.urlPrefix + '/register', site.register);

app.get(config.urlPrefix + '/signup', site.signupForm);
app.post(config.urlPrefix + '/signup', site.signup);

app.post(config.urlPrefix + '/oauth/token', oauth2.token);
app.get(config.urlPrefix + '/oauth/tokeninfo', tokenApi.info);
app.get(config.urlPrefix + '/oauth/space-token', tokenApi.getSpaceToken);
app.get(config.urlPrefix + '/user/profile', userApi.profile);

app.get(config.urlPrefix + '/dialog/oauth', oauth2.authorization);
app.post(config.urlPrefix + '/dialog/oauth-decision', oauth2.decision);

// facebook -------------------------------

// send to facebook to do the authentication
app.get(config.urlPrefix + '/auth/facebook', passport.authenticate('facebook', {
    scope: 'email'
}));

// handle the callback after facebook has authenticated the user
app.get(config.urlPrefix + '/auth/facebook/callback', function(req, res, next) {
    passport.authenticate('facebook', {
        successRedirect: req.session.returnTo || '/',
        failureRedirect: config.urlPrefix + '/login'
    })(req, res, next);
});

// send to google to do the authentication
app.get(config.urlPrefix + '/auth/google', passport.authenticate('google', {
    scope: ['profile', 'email']
}));

// the callback after google has authenticated the user
app.get(config.urlPrefix + '/auth/google/callback', function(req, res, next) {
    passport.authenticate('google', {
        successRedirect: req.session.returnTo || '/',
        failureRedirect: config.urlPrefix + '/login'
    })(req, res, next);
});

//static resources for stylesheets, images, javascript files
// app.use(express.static(path.join(__dirname, 'public')));

// Create HTTP | HTTPS server
if (config.authServer.type === 'http') {
    http.createServer(app).listen(config.authServer.port, "0.0.0.0");
}
else {
    var options = {
        key: fs.readFileSync('certs/privatekey.pem'),
        cert: fs.readFileSync('certs/certificate.pem')
    };
    https.createServer(options, app).listen(config.authServer.port, "0.0.0.0");
}
logger.info("OAuth 2.0 Authorization Server started on port " + config.authServer.port);
