"use strict";

var config = require('../config');
var db = require('../' + config.db.type);
var mail = require("../utils/mail");
var logger = require('../logger');

exports.registerEmail = function(email, callback) {
    // check email used
    db.users.findByUsername(email, function(err, r) {
        if (err) return callback(err);
        if (r) return callback("email_used");

        // generate verify code
        db.verifyCodes.insertCode({
            to_verify: email,
            type: "email"
        }, function(err, r) {
            if (err) return callback(err);
            // send email
            sendVerifyMail(email, r.code, function(err, r) {
                if(err){
                    logger.debug("send mail error: " + JSON.stringify(err));
                }
                callback(null);
            });
        });
    });
}

exports.registerUser = function(user, verifyCode, callback) {
    db.users.findByUsername(user.email, function(err, r) {
        if (err) return callback(err);
        if (r) return callback("email_used");

        // verify code
        db.verifyCodes.verifyCode(user.email, verifyCode, function(err, r) {
            if (err) return callback(err);
            if (!r) return callback("verification_invalid");

            // insert new user
            var newUser = {
                display_name: user.display_name,
                password: user.password,
                emails: [{
                    email: user.email,
                    is_main: true
                }],
                phones: [],
                is_active: true
            };
            db.users.insert(newUser, function(err, insertedUser) {
                if (err) return callback(err);
                callback(null, insertedUser);
            });
        });
    });
}

function sendVerifyMail(email, code, callback) {
    var link = config.publishUrl + "/as/signup?email=" + email + "&code=" + code;
    mail.sendEmail({
        to: email,
        subject: "Xác nhận email đăng ký tài khoản Upspace",
        content: "Bấm vào <a href='" + link + "'>đây</a> để xác nhận email đăng ký tài khoản Upspace."
    }, function() {
        callback(null);
    });
}

exports.verifyEmail = function(email, code, callback) {
    // verify code
    db.verifyCodes.verifyCode(email, code, function(err, r) {
        if (err) return callback(err);
        if (!r) return callback("verification_invalid");
    });
};
