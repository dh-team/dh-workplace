        Authentication Server
*************************************
1. Database Design: Mongodb

# "users"
{
    id: {
        type: String,
        unique: true,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    emails: {
        type: Array, // [ {email, is_verified, is_main}, ... ]
        required: false
    },
    phones: {
        type: Array, // [ {phone, is_verified, is_main}, ... ]
        required: false
    },
    is_active: {
        type: Boolean,
        required: true
    }
}

# "oauth_memberships"
{
    user_id: {
        type: String,
        required: true
    },
    provider: {
        type: String,
        required: true
    },
    provider_user_id: {
        type: String,
        required: true
    }
}

# "space_memberships"
{
    user_id: {
        type: String,
        required: true
    },
    space_id: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    groups: {
        type: Array, // ['group_id', ...]
        required: true
    },
    positions: {
        type: Array, // ['position_id', ...]
        required: true
    },
    roles: {    
        type: Array, // ['role1', ...]
        required: true
    },
    is_default: {
        type: Boolean,
        required: true
    },
    is_active: {
        type: Boolean,
        required: true
    }
}

# "user_profiles"
{
    user_id: {
        type: String,
        unique: true,
        required: true
    },
    full_name: {
        type: String,
        required: false
    },
    gender: {
        type: String,
        required: false
    },
    birthday: {
        type: Date,
        required: false
    },
    locale: {
        type: String,
        required: false
    },
    language: {
        type: String,
        required: false
    }
}

2. Login process:
- 3 ways to login:
    1) Use email/phone and password
    2) Use oauth account (Google, Facebook)
    3) Use spaceanization account - username@space_id and password
- Login success: redirect to user's main space
- 1 user can join multiple spaceanizations

3. Run:
- Run mongodb:  mongod --smallfiles
- Run server:   node server.js

4. API Design

# Get token using authentication
+ URL: /oauth/token
+ Method: POST
+ Header:
    - Authorization: Basic base64(CLIENT_ID:CLIENT_SECRET)
+ Parameters:
    - grant_type: 'password'
    - username: EMAIL_OR_PHONE
    - password: PASSWORD
+ Returns:
    {
        access_token: 'rE7oKCDoZHx2wESVwDbn14JAUuCKE0TisvVNyTszmAHLGpaHWMf4awj246qQ1D8P',
        refresh_token: 'rE7oKCDoZHx2wESVwDbn14JAUuCKE0TisvVNyTszmAHLGpaHWMf4awj246qQ1D8P',
        expires_in: 3600,
        token_type: "bearer"
    }

+ Errors:
    {
        error: 'credential_invalid',
        error_detail: 'User or password is invalid'
    }

    {
        error: 'account_disabled',
        error_detail: 'Account is locked or disabled'
    }
+ Example:
curl localhost:8081/oauth/token -H "Authorization: Basic ZGhfd29ya3BsYWNlX3dlYmFwcDphYmNAMTIz" -d "grant_type=password&username=EMAIL_OR_PHONE&password=PASSWORD"

# Get access_token for using a space:
+ URL: /oauth/space-token
+ Method: POST | GET
+ Header:
    - Authorization: Bearer ACCESS_TOKEN
+ Parameters:
    - space_id: SPACE_ID
+ Returns:
    {
        space_access_token: SPACE_ACCESS_TOKEN         
    }
+ Errors:
    {
        error: 'permission_denied',
        error_detail: 'Current user does not have permission to access this space'
    }
+ Example: curl localhost:8081/oauth/space-token -H "Authorization: Bearer rE7oKCDoZHx2wESVwDbn14JAUuCKE0TisvVNyTszmAHLGpaHWMf4awj246qQ1D8P" -d "space_id=SPACE_ID"

# Get new access_token using refresh_token:
+ URL: /oauth/refresh-token
+ Header:
+ Method: GET
+ Parameters:
    - refresh_token: REFRESH_TOKEN
    - space_id: SPACE_ID // optional
+ Returns:
    {
        access_token: ACCESS_TOKEN
    }
    or
    {
        space_access_token: SPACE_ACCESS_TOKEN
    }
+ Errors:
    {
        error: 'token_invalid',
        error_detail: 'Current refresh token is invalid'
    }

    {
        error: 'token_expired',
        error_detail: 'Current refresh token is expired'
    }
+ Example: curl localhost:8081/oauth/refresh-token?refresh_token=rE7oKCDoZHx2wESVwDbn14JAUuCKE0TisvVNyTszmAHLGpaHWMf4awj246qQ1D8P&space_id=donghanhjsc

# Get token info
+ URL: /oauth/tokeninfo
+ Header:
+ Method: GET
+ Parameters:
    - token: ACCESS_TOKEN | SPACE_ACCESS_TOKEN
+ Returns:
    {
        audience : "someClientId",
        user_id": "someUserId",
        scope": "profile email",
        expires_in: 436 // in seconds
        space: {
            space_id: 'someOrgID',
            username: 'someUsername',
            positions: ['position_id',...],
            groups: ['group_id',...],
            roles: ['role_id',...]
        }  // optional
    }
+ Errors:
    {
        error: 'token_invalid',
        error_detail: 'Current refresh token is invalid'
    }

    {
        error: 'token_expired',
        error_detail: 'Current refresh token is expired'
    }
+ Example: curl localhost:8080/as/oauth/tokeninfo?token=9cIlpMWE19DjUlhW7HK7xvzJ8F9dipQDQvJkWjfQfZ1Q1WC5K1A5LIVIRFZCdliC | jq .

# Get user's profile
curl localhost:8081/as/user/profile -H "Authorization: Bearer OxJpg6T33OBG3wEP4qmRLC9ovhiv3uUFC6IjDXs6KBgrPNnOzczZ0d2kRy6K1d5e"  | jq .

5. Site URL

# Login:
https://dh-workplace-duybinh.c9users.io/as/login

# Logout:
https://dh-workplace-duybinh.c9users.io/as/logout

# OAuth2 login dialog:
- Get authorization code:
+ Trusted client
https://dh-workplace-duybinh.c9users.io/as/dialog/oauth?client_id=upspace-web-app&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fauth%2Fhandle

+ Non trusted client
https://dh-workplace-duybinh.c9users.io/as/dialog/oauth?client_id=585371df7d0c9018be284745&response_type=code&redirect_uri=http%3A%2F%2Flocalhost%2Fauth%2Fhandle

# Examples
- Get access token
curl localhost:8080/as/oauth/token -H "Authorization: Basic NTg1MzcxZGY3ZDBjOTAxOGJlMjg0NzQ1OnFYOEpjSXRKdFpERWMwb3o=" -d "grant_type=authorization_code&code=t4oSDqJCxn7dbsK6&client_id=585371df7d0c9018be284745&redirect_uri=http://localhost/auth/handle"  | jq .

- Get Space access token
curl localhost:8080/as/oauth/space-token?space_id=5875ae0f029c4a249de8f390 -H "Authorization: Bearer g87JSneS4y7FCG4VpRaGYaws7D8kr0hH2UrfyfCBgQZgFfygYW9m1llZYUCw6glF"

- Get token info
curl localhost:8080/as/oauth/tokeninfo?token=95WSlg2UEIm5z1o5cqSGoT4TxSa0JwWsGwH2Ph4xoBgpGQj69Od0fPGOSqjrl6rO | jq .


NODE_ENV=production node_modules/.bin/webpack -p

node_modules/.bin/http-server public
