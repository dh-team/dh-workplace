/*jslint node: true */
'use strict';

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var BasicStrategy = require('passport-http').BasicStrategy;
var ClientPasswordStrategy = require('passport-oauth2-client-password').Strategy;
var BearerStrategy = require('passport-http-bearer').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var config = require('../config');
var db = require('../' + config.db.type);
var logger = require('../logger');

/**
 * LocalStrategy
 *
 * This strategy is used to authenticate users based on a username and password.
 * Anytime a request is made to authorize an application, we must ensure that
 * a user is logged in before asking them to approve the request.
 */
passport.use(new LocalStrategy({
        passReqToCallback: true // allows us to pass back the entire request to the callback
    },
    function(req, username, password, done) {
        logger.debug('auth with local strategy, username: ' + username);
        db.users.findByUsername(username, function(err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                logger.debug("user not found");
                return done(null, false, req.flash('login_error', 'User is not exist'));
            }
            
            db.users.verifyPassword(user.password, password, function(err, isMatch) {
                if (err) {
                    return done(err);
                }
                if (isMatch) {
                    return done(null, user);
                }
                logger.debug("password not match");
                return done(null, false, req.flash('login_error', 'Password fail'));
            });
        });
    }
));

/**
 * BasicStrategy & ClientPasswordStrategy
 *
 * These strategies are used to authenticate registered OAuth clients.  They are
 * employed to protect the `token` endpoint, which consumers use to obtain
 * access tokens.  The OAuth 2.0 specification suggests that clients use the
 * HTTP Basic scheme to authenticate.  Use of the client password strategy
 * allows clients to send the same credentials in the request body (as opposed
 * to the `Authorization` header).  While this approach is not recommended by
 * the specification, in practice it is quite common.
 */
passport.use(new BasicStrategy(
    function(clientID, password, done) {
        logger.debug('auth with basic strategy, client : ' + clientID);
        db.clients.find(clientID, function(err, client) {
            if (err) {
                return done(err);
            }
            if (!client) {
                logger.debug('client not found');
                return done(null, false);
            }
            if (client.secret != password) {
                logger.debug('client secret not match');
                return done(null, false);
            }

            return done(null, client);
        });
    }
));

/**
 * Client Password strategy
 *
 * The OAuth 2.0 client password authentication strategy authenticates clients
 * using a client ID and client secret. The strategy requires a verify callback,
 * which accepts those credentials and calls done providing a client.
 */
passport.use(new ClientPasswordStrategy(
    function(clientId, clientSecret, done) {
        logger.debug("auth with client password strategy: " + clientId);
        db.clients.find(clientId, function(err, client) {
            if (err) {
                return done(err);
            }
            if (!client) {
                logger.debug("client not found");
                return done(null, false);
            }
            if (client.secret != clientSecret) {
                logger.debug("client secret not match");
                return done(null, false);
            }
            return done(null, client);
        });
    }
));

/**
 * BearerStrategy
 *
 * This strategy is used to authenticate either users or clients based on an access token
 * (aka a bearer token).  If a user, they must have previously authorized a client
 * application, which is issued an access token to make requests on behalf of
 * the authorizing user.
 */
passport.use(new BearerStrategy(
    function(accessToken, done) {
        logger.debug("auth with bearer strategy, access token: " +  accessToken);
        db.accessTokens.find(accessToken, function(err, token) {
            if (err) {
                return done(err);
            }
            if (!token) {
                logger.debug("token not found");
                return done(null, false);
            }
            if (new Date() > token.expiration_date) {
                logger.debug("token expired");
                db.accessTokens.delete(accessToken, function(err) {
                    return done(err || "token_expired");
                });
            }
            else {
                if (token.user_id !== null) {
                    db.users.findByID(token.user_id, function(err, user) {
                        if (err) {
                            return done(err);
                        }
                        if (!user) {
                            return done(null, false);
                        }
                        // to keep this example simple, restricted scopes are not implemented,
                        // and this is just for illustrative purposes
                        var info = {
                            token: token
                        };
                        return done(null, user, info); // info will be present in req.authInfo
                    });
                }
                else {
                    //The request came from a client only since userId is null
                    //therefore the client is passed back instead of a user
                    db.clients.find(token.client_id, function(err, client) {
                        if (err) {
                            return done(err);
                        }
                        if (!client) {
                            return done(null, false);
                        }
                        // to keep this example simple, restricted scopes are not implemented,
                        // and this is just for illustrative purposes
                        var info = {
                            scope: '*'
                        };
                        return done(null, client, info);
                    });
                }
            }
        });
    }
));

// =========================================================================
// FACEBOOK ================================================================
// =========================================================================
var fbStrategy = config.facebookAuth;
fbStrategy.passReqToCallback = true; // allows us to pass in the req from our route (lets us check if a user is logged in or not)
passport.use(new FacebookStrategy(fbStrategy, function(req, token, refreshToken, profile, done) {
    logger.debug("facebook user: " + JSON.stringify(profile));

    var info = {
        user_id: profile.id,
        email: profile.emails[0].value,
        name: profile.name.givenName + " " + profile.name.familyName,
    };

    handleOAuth(req, "facebook", info, done);
}));


// =========================================================================
// GOOGLE ==================================================================
// =========================================================================
var gStrategy = config.googleAuth;
gStrategy.passReqToCallback = true; // allows us to pass in the req from our route (lets us check if a user is logged in or not)
passport.use(new GoogleStrategy(gStrategy, function(req, token, refreshToken, profile, done) {
    logger.debug("google user: " + JSON.stringify(profile));

    var info = {
        user_id: profile.id,
        email: profile.emails[0].value,
        name: profile.displayName,
    };

    process.nextTick(function() {
        handleOAuth(req, "google", info, done);
    });
}));

function handleOAuth(req, provider, info, done) {
    if (req.user) {
        return done(null, req.user);
    }

    // user not logged in
    db.users.findOAuthMembership(provider, info, function(err, linkedUser) {
        if (err) return done(err);

        if (linkedUser) {
            logger.debug("found linked user to " + provider + " oauth: " + linkedUser);
            return done(null, linkedUser);
        }

        // create new user
        db.users.insert({
            display_name: info.name,
            emails: [{
                email: info.email,
                is_main: true
            }],
            is_active: true
        }, function(err, insertedUser) {
            if (err) return done(err);
            logger.debug("created user link to " + provider + " oauth: " + insertedUser);
            db.users.insertOAuthMembership({
                user_id: insertedUser.id,
                provider: provider,
                provider_user_id: info.user_id
            }, function(err) {
                if (err) return done(err);
                return done(null, insertedUser);
            });
        });
    });
}

// Register serialialization and deserialization functions.
//
// When a client redirects a user to user authorization endpoint, an
// authorization transaction is initiated.  To complete the transaction, the
// user must authenticate and approve the authorization request.  Because this
// may involve multiple HTTPS request/response exchanges, the transaction is
// stored in the session.
//
// An application must supply serialization functions, which determine how the
// client object is serialized into the session.  Typically this will be a
// simple matter of serializing the client's ID, and deserializing by finding
// the client by ID from the database.

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    db.users.findByID(id, function(err, user) {
        done(err, user);
    });
});
