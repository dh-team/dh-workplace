sudo service mongod start
sudo /etc/init.d/rethinkdb start

killall screen
sudo killall nodejs

dir=$(pwd)
cd $dir/auth-server
screen -dmS auth sh -c 'nodejs server.js; exec bash'

cd $dir/core-api-server
screen -dmS core-api sh -c 'nodejs server.js; exec bash'

cd $dir/file-server
screen -dmS file sh -c 'nodejs server.js; exec bash'

cd $dir/web-app
screen -dmS web sh -c './run.sh now; exec bash'

cd $dir/space-app
screen -dmS space sh -c './run.sh now; exec bash'

cd $dir/proxy-server
screen -dmS proxy sh -c 'sudo nodejs server.js; exec bash'

sleep 2
screen -ls

